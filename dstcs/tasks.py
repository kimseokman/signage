import os
from celery import Celery
from PIL import Image

app = Celery('tasks', broker='redis://localhost')

THUMBNAIL_WIDTH_DEFAULT = 128
THUMBNAIL_HEIGHT_DEFAULT = 128

@app.task
def resize(full_path, width=None, height=None):
    thumbnail_width = width
    if thumbnail_width is None:
        thumbnail_width = THUMBNAIL_WIDTH_DEFAULT

    thumbnail_height = height
    if thumbnail_height is None:
        thumbnail_height = THUMBNAIL_HEIGHT_DEFAULT

    filename = os.path.basename(full_path)
    elements = filename.split(".")
    new_image_path = os.path.dirname(full_path) + '/' + elements[0] + "_thumbnail.jpg"

    try:
        im = Image.open(full_path)
        im.thumbnail((thumbnail_width, thumbnail_height))

        im.save(new_image_path, "JPEG")
        print("Image resize completed: %s -> %s" % (full_path, new_image_path))

        return True

    except IOError, e:
        print("File creation failed: %s" % str(e))

        return False