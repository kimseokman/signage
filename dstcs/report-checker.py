__author__ = 'pjh@koino.net'
# -*- coding: utf-8 -*-

import os
import sys
import time
import signal
import platform
import logging
from logging import handlers
import MySQLdb
from MySQLdb import Error
from datetime import datetime
import re

# Multilanguages
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

current_dir = os.path.dirname(os.path.realpath(__file__))

g_platform = platform.system()

if g_platform == "Linux":
    LOG_DEFAULT_DIR = '/var/log/dstcs/'
elif g_platform == "Windows":
    LOG_DEFAULT_DIR = '.'
elif g_platform == "Darwin":
    LOG_DEFAULT_DIR = '.'


class ReportChecker(object):
    def __init__(self, logger, db_info, intervals):
        # Properties for Daemon
        self.stdin_path = '/dev/null'
        self.stdout_path = current_dir + '/report-checker.out'
        self.stderr_path = current_dir + '/report-checker.err'
        self.pidfile_path = current_dir + '/report-checker.pid'
        self.pidfile_timeout = 5

        self.db_info = db_info
        self.db = None

        self.logger = logger
        self.is_running = True
#        self.intervals = intervals
        self.intervals = 60

        self.history_delete_period = 10
        self.history_delete_counter = 0

    def connect_to_db(self, db_info=None):
        if db_info is None:
            info = self.db_info
        else:
            info = db_info

        try:
            self.db = MySQLdb.connect(
                info['host'],
                info['username'],
                info['password'],
                info['database'])

            self.logger.info('Database connected.')
            self.db.autocommit(True)

            return True

        except Error, e:
            self.logger.error('Database connection failed: %s' % str(e))

            return False

    def disconnect(self):
        if self.db:
            self.db.close()

    def cursor(self):
        if self.db is None:
            return None

        return self.db.cursor(MySQLdb.cursors.DictCursor)

    def check(self, nTime):
        pMap = dict()
        sqls = []
        insert_sqls = []
        update_sqls = []
        delete_sqls = []
        cursor = None

        utc_datetime_date = datetime.utcnow()
        utc_datetime_date.strftime("%Y-%m-%d %H:%M:%S")
        utc_datetime = str(utc_datetime_date)

        # 'R01' and 'R02' daily (UTC time)
        sqls.append("SELECT serial_number, COUNT(*), SUM(TIMESTAMPDIFF(MINUTE, created, solved_time)), type FROM incidents" \
                    " WHERE (created BETWEEN DATE_FORMAT('"+ utc_datetime +"','%Y-%m-%d 00:00:00') and '"+utc_datetime+"') and (error_code='R01' or error_code='R02') and solved_time is not null GROUP BY serial_number")

        sqls.append("SELECT serial_number, COUNT(*), SUM(TIMESTAMPDIFF(MINUTE, created, '"+utc_datetime+"')), type FROM incidents" \
                    " WHERE (created BETWEEN DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and '"+utc_datetime+"') and (error_code='R01' or error_code='R02') and solved_time is null GROUP BY serial_number")

        sqls.append("SELECT serial_number, 0, SUM(TIMESTAMPDIFF(MINUTE, DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), '"+utc_datetime+"')), type FROM incidents" \
                    " WHERE created < DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and (error_code='R01' or error_code='R02') and solved_time is null GROUP BY serial_number")

        sqls.append("SELECT serial_number, 0, SUM(TIMESTAMPDIFF(MINUTE, DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), solved_time)), type FROM incidents" \
                    " WHERE created < DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and (error_code='R01' or error_code='R02') and solved_time is not null and (solved_time BETWEEN DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and '"+utc_datetime+"') GROUP BY serial_number")

        sqls.append("SELECT serial_number, 0 FROM mediaplayers")
        sqls.append("SELECT serial_number, 1 FROM displays")

        try:
            cursor = self.cursor()

            for sql in sqls:
                #self.logger.info(sql)
                cursor.execute(sql)
                recv_data = cursor.fetchall()

                for recv in recv_data:
                    pDic = dict()
                    pDic['serial_number'] = recv['serial_number']
                    pDic['default'] = 0
                    pDic['type'] = recv.get('type')
                    if pDic['type'] is None:
                        pDic['default'] = 1
                        pDic['type'] = recv.get('0')
                        if pDic['type'] is None:
                            pDic['type'] = recv.get('1')
                    pDic['count'] = recv.get('COUNT(*)')
                    if pDic['count'] is None:
                        pDic['count'] = 0
                    pDic['down_time'] = recv.get('SUM(TIMESTAMPDIFF(MINUTE, created, solved_time))')
                    if pDic['down_time'] is None:
                        pDic['down_time'] = recv.get("SUM(TIMESTAMPDIFF(MINUTE, created, '"+utc_datetime+"'))")
                        if pDic['down_time'] is None:
                            pDic['down_time'] = recv.get("SUM(TIMESTAMPDIFF(MINUTE, DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), '"+utc_datetime+"'))")
                            if pDic['down_time'] is None:
                                pDic['down_time'] = recv.get("SUM(TIMESTAMPDIFF(MINUTE, DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), solved_time))")
                                if pDic['down_time'] is None:
                                    pDic['down_time'] = 0

                    if pMap.get(recv['serial_number']) is None:
                        pMap[recv['serial_number']] = pDic
                    else:
                        pMap[recv['serial_number']]['count'] += pDic['count']
                        pMap[recv['serial_number']]['down_time'] += pDic['down_time']

            for pItem in pMap.keys():
                items = pMap.get(pItem)

                if items['type'] == 0:

                    check_mp_sqls = "SELECT serial_number FROM daily_mp_report" \
                                    " WHERE (date BETWEEN DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and '"+utc_datetime+"') and serial_number = '" + items['serial_number'] + "'"

                    cursor.execute(check_mp_sqls)
                    rm_data = cursor.fetchone()

                    if rm_data is None:
                        insert_sqls.append("INSERT INTO daily_mp_report(date, serial_number, incident_cnt, down_time)" \
                                        " VALUES('%s', '%s', %d, %d)" % (utc_datetime, items['serial_number'], items['count'], items['down_time']) )
                    else:
                        if items['default'] == 0:
                            update_sqls.append("UPDATE daily_mp_report SET date = '"+utc_datetime+"', incident_cnt = '"+ str(items['count']) + "', down_time = '" + str(items['down_time']) + "'" \
                                            " WHERE (date BETWEEN DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and '"+utc_datetime+"') and serial_number = '" + items['serial_number'] + "'" )
                else:

                    check_dp_sqls = "SELECT serial_number FROM daily_dp_report" \
                                    " WHERE (date BETWEEN DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and '"+utc_datetime+"') and serial_number = '" + items['serial_number'] + "'"

                    cursor.execute(check_dp_sqls)
                    rd_data = cursor.fetchone()

                    if rd_data is None:
                        insert_sqls.append("INSERT INTO daily_dp_report(date, serial_number, incident_cnt, down_time)" \
                                        " VALUES('%s', '%s', %d, %d)" % (utc_datetime, items['serial_number'], items['count'], items['down_time']) )
                    else:
                        if items['default'] == 0:
                            update_sqls.append("UPDATE daily_dp_report SET date = '"+utc_datetime+"', incident_cnt = '"+ str(items['count']) + "', down_time = '" + str(items['down_time']) + "'" \
                                            " WHERE (date BETWEEN DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00') and '"+utc_datetime+"') and serial_number = '" + items['serial_number'] + "'" )


            for insert_sql in insert_sqls:
                #print insert_sql
                cursor.execute(insert_sql)

            for update_sql in update_sqls:
                #print update_sql
                cursor.execute(update_sql)

            # if nTime == '0002':
            #     # history delete logic
            #     self.logger.info("Delete history.")
            #     delete_sqls.append("DELETE FROM mediaplayers_history" \
            #                     " WHERE last_updated_date < DATE_ADD('"+utc_datetime+"', INTERVAL -7 DAY)")
            #
            #     delete_sqls.append("DELETE FROM displays_history" \
            #                     " WHERE last_updated_date < DATE_ADD('"+utc_datetime+"', INTERVAL -7 DAY)")
            #
            #     for delete_sql in delete_sqls:
            #         cursor.execute(delete_sql)
            #         #print delete_sql

            return []

        except Error, e:
            self.logger.error("Check sql statement failed: %s" % str(e))
            if not self.connect_to_db():
                self.logger.error('Database initialize failed.')
            return []

        finally:
            if not cursor:
                cursor.close()


    def run(self):
        self.init_signal_handler()

        if not self.connect_to_db():
            self.logger.error('Database initialize failed. Exit.')
            return False

        while self.is_running:

            now = datetime.now()
            nTime = now.strftime('%H%M')

            #print nTime

#            if nHour == '0002':
            checked_users = self.check(nTime)

            for _ in range(self.intervals):
                if self.is_running:
                    time.sleep(1)

        self.disconnect()

    def init_signal_handler(self):
        def signal_term_handler(signum, frame):
            self.logger.info("Got TERM signal. Stopping application.")
            self.is_running = False

        signal.signal(signal.SIGTERM, signal_term_handler)

if __name__ == '__main__':
    file_logger = logging.getLogger("ReportChecker")
    file_logger.setLevel(logging.INFO)

    file_handler = handlers.RotatingFileHandler(
        "report-checker.log",
        maxBytes=(1024 * 1024 * 1),
        backupCount=3
    )

    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
    file_handler.setFormatter(formatter)
    file_logger.addHandler(file_handler)

    app = ReportChecker(file_logger, {
        'host': '127.0.0.1',
        'username': 'root',
        'password': '1234',
        'database': 'dstcs'
    }, 10)

    if g_platform == "Linux":
    #if False:
        from daemon import runner
        try:
            daemon_runner = runner.DaemonRunner(app)
            daemon_runner.daemon_context.files_preserve = [file_handler.stream]
            daemon_runner.do_action()

        except runner.DaemonRunnerStopFailureError:
            print("Daemon already stopped.")

        except runner.DaemonRunnerStartFailureError:
            print("Daemon start failed.")
    else:
        app.run()


