__author__ = 'rainmaker'
# -*- coding: utf-8 -*-

import time
import os
import sys
import platform
import logging
from logging import handlers
import threading

current_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(current_dir + '/lib')

from kazoo.client import KazooClient
from kazoo.protocol.states import KazooState
from kazoo.exceptions import NoNodeError, NodeExistsError, KazooException

from flask import Flask, request
from flask.ext.restful import reqparse, Resource, Api
import requests

# Multilanguages
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

ZK_PATH = {
    'membership_dir': '/signage/membership',
    'membership': '/signage/membership/%s',
    'command_dir': '/signage/command/%s',
    'display_command': '/signage/command/%s/DISPLAY',
    #------------------------------------------------------------------------------------------------------------------
    # WEB - LiveStatus/MP/RunningStatus
    'application_command': '/signage/command/%s/APPLICATION',
    'agent_command': '/signage/command/%s/AGENT',
    'mediaplayer_command': '/signage/command/%s/MEDIAPLAYER',
    #------------------------------------------------------------------------------------------------------------------
    'setting_command': '/signage/command/%s/SETTING',
    'remotecontrol_command': '/signage/command/%s/REMOTECONTROL',
    'check_command': '/signage/command/%s/CHECK'
}
ZK_USERNAME = ''
ZK_PASSWORD = ''
ZK_SERVERS = 'dev.adamssuite.com:80'

API_ADDRESS = "http://127.0.0.1:8088"
API_HEADERS = {'Content-type': 'application/json'}

g_platform = platform.system()

if g_platform == "Linux":
    LOG_DEFAULT_DIR = '/var/log/dstcs/'
elif g_platform == "Windows":
    LOG_DEFAULT_DIR = '.'
elif g_platform == "Darwin":
    LOG_DEFAULT_DIR = '.'


web_app = Flask(__name__)


# Singleton Source from http://stackoverflow.com/questions/42558/python-and-the-singleton-pattern
class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Other than that, there are
    no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    Limitations: The decorated class cannot be inherited from.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


class Helper(object):
    @staticmethod
    def get_file_logger(app_name, filename):
        log_dir_path = LOG_DEFAULT_DIR
        try:
            if not os.path.exists(log_dir_path):
                os.mkdir(log_dir_path)

            full_path = '%s/%s' % (log_dir_path, filename)
            file_logger = logging.getLogger(app_name)
            file_logger.setLevel(logging.INFO)

            file_handler = handlers.RotatingFileHandler(
                full_path,
                maxBytes=(1024 * 1024 * 10),
                backupCount=5
            )
            formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')

            file_handler.setFormatter(formatter)
            file_logger.addHandler(file_handler)

            return file_logger

        except IOError, e:
            print("Can't create log directory for '%s': \n  %s" % (filename, str(e)))
            return logging.getLogger(app_name)

        except OSError, e:
            print("Can't create log directory for '%s': \n  %s" % (filename, str(e)))
            return logging.getLogger(app_name)


@Singleton
class DaemonWorker(object):
    def __init__(self):
        self.zk = None
        self.zk_enabled = False
        self.current_agents = set()

        # Headers for requests.
        self.api_headers = {'Content-type': 'application/json'}

        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger('SignageDaemon')

    def set_logger(self, logger):
        self.logger = logger

    def connect_to_zk(self, servers=None):
        if self.zk_enabled is True:
            return

        if servers is None:
            self.servers = ZK_SERVERS
        else:
            self.servers = servers

        if isinstance(self.servers, list):
            self.server_hosts = ",".join(self.servers)
        else:
            self.server_hosts = self.servers

        self.zk = KazooClient(hosts=self.server_hosts, logger=self.logger, timeout=15)
        self.event = threading.Event()

        @self.zk.add_listener
        def connection_handler(state):
            if state == KazooState.CONNECTED:
                self.logger.info('ZK connection established.')
                self.zk_enabled = True

                self.event.set()

            if state == KazooState.SUSPENDED:
                self.logger.info('ZK connection suspended.')
                self.zk_enabled = False

                self.connect_to_zk(self.server_hosts)

        try:
            self.zk.start()

        except KazooException:
            self.logger.error('ZK connection failed.')
            return

        self.event.wait()

    def get_new_agent(self, agents_set):
        if len(self.current_agents) == 0 and len(agents_set) == 0:
            return set()

        return agents_set - self.current_agents

    def get_deleted_agent(self, agents_set):
        if len(self.current_agents) == 0 and len(agents_set) == 0:
            return set()

        return self.current_agents - agents_set

    def notify_new_agents(self, new_agents_set):
        print("New agents: %s" % str(new_agents_set))


        for sn in new_agents_set:
            req_url = API_ADDRESS + '/api/v1/media-players/alive/%s' % str(sn)
            r = requests.put(req_url, verify=False, data='{"alive":"1"}', headers=API_HEADERS)


    def notify_deleted_agents(self, deleted_agents_set):
        print("Deleted agents: %s" % str(deleted_agents_set))

        for sn in deleted_agents_set:
            req_url = API_ADDRESS + '/api/v1/media-players/alive/%s' % str(sn)
            r = requests.put(req_url, verify=False, data='{"alive":"0"}', headers=API_HEADERS)

    def setup_watch_to_membership(self):
        if self.zk_enabled is False:
            return

        self.zk.ensure_path(ZK_PATH['membership_dir'])

        @self.zk.ChildrenWatch(ZK_PATH['membership_dir'])
        def watch_membership(children):
            print('Membership changed.')

            children_set = set(children)

            new_agents = self.get_new_agent(children_set)
            self.notify_new_agents(new_agents)

            deleted_agents = self.get_deleted_agent(children_set)
            self.notify_deleted_agents(deleted_agents)

            self.current_agents = self.current_agents.union(new_agents)
            self.current_agents -= deleted_agents

    def write_remotecontrol_command(self, unique_name, value):
        print("Write remotecontrol_command to %s [%s]" % (unique_name, value))

        remotecontrol_command_node = ZK_PATH['remotecontrol_command'] % unique_name
        self.zk.set(remotecontrol_command_node, value)

    def write_application_command(self, unique_name, value):
        print("Write application_command to %s [%s]" % (unique_name, value))

        application_command_node = ZK_PATH['application_command'] % unique_name
        self.zk.set(application_command_node, value)

    def write_display_command(self, unique_name, value):
        print("Write display_command to %s [%s]" % (unique_name, value))

        display_command_node = ZK_PATH['display_command'] % unique_name
        self.zk.set(display_command_node, value)

    def write_check_command(self, unique_name, value):
        print("Write check_command to %s [%s]" % (unique_name, value))

        check_command_node = ZK_PATH['check_command'] % unique_name
        self.zk.set(check_command_node, value)

    def write_agent_command(self, unique_name, value):
        print("Write agent_command to %s [%s]" % (unique_name, value))

        agent_command_node = ZK_PATH['agent_command'] % unique_name
        self.zk.set(agent_command_node, value)

    def write_mediaplayer_command(self, unique_name, value):
        print("Write mediaplayer_command to %s [%s]" % (unique_name, value))

        mediaplayer_command_node = ZK_PATH['mediaplayer_command'] % unique_name
        self.zk.set(mediaplayer_command_node, value)

    def init_all(self):
        self.connect_to_zk()
        self.setup_watch_to_membership()


class WebAPI(Resource):
    api_global_logger = Helper.get_file_logger("WebAPIServer", "web_api.log")

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('type', type=str, location='json')
        self.parser.add_argument('command', type=str, location='json')
        self.parser.add_argument('data', type=str, location='json')

        self.logger = WebAPI.api_global_logger

        super(WebAPI, self).__init__()

    def put(self, unique_name):
        input = self.parser.parse_args()
        worker = DaemonWorker.instance()

        try:
            self.logger.info('POST %s' % str(input))

            if 'type' not in input or 'command' not in input or 'data' not in input:
                return {
                    'success': 'NO',
                    'reason': 'Missing required property'
                }

            # [ Combine ] command + display_index + data
            # - by KSM
            if input['data'] != 'EMPTY':
                combine_command = input['command'] + " " + input['data']
            else:
                combine_command = input['command']

            if input["type"] == "remote_control":
                worker.write_remotecontrol_command(unique_name, combine_command)
            elif input["type"] == "application_control":
                worker.write_application_command(unique_name, combine_command)
            elif input["type"] == "display_control":
                worker.write_display_command(unique_name, combine_command)
            elif input["type"] == "check_control":
                worker.write_check_command(unique_name, combine_command)
            elif input["type"] == "agent_control":
                worker.write_agent_command(unique_name, combine_command)
            elif input["type"] == "mediaplayer_control":
                worker.write_mediaplayer_command(unique_name, combine_command)

            return {
                'success': 'YES',
                'reason': ''
            }

        except Exception, e:
            self.logger.error("POST failed: %s" % str(e))
            return {
                'success': 'NO',
                'reason': str(e)
            }, 404

    def get(self, unique_name):
        worker = DaemonWorker.instance()

        try:
            return {
                'success': 'YES',
                'reason': '',
            }

        except Exception, e:
            self.logger.error("GET failed: %s" % str(e))
            return {
                'success': 'NO',
                'reason': str(e)
            }, 404


class SignageDaemon(object):
    def __init__(self, logger):
        # Properties for Daemon
        self.stdin_path = '/dev/null'
        self.stdout_path = current_dir + '/signage-daemon.out'
        self.stderr_path = current_dir + '/signage-daemon.err'
        self.pidfile_path = current_dir + '/signage-daemon.pid'
        self.pidfile_timeout = 5

        self.logger = logger

        self.worker = DaemonWorker.instance()
        self.is_running = True

        self.web_api = Api(web_app)
        self.web_api.add_resource(
            WebAPI,
            '/controller/<string:unique_name>')

    def run(self):
        def start_web_api(args):
            web_app.run(debug=False, port=8002, host='0.0.0.0')
            #web_app.run(debug=False, port=8003, host='0.0.0.0')

        def start_daemon(args):
            self.worker.set_logger(self.logger)
            self.worker.init_all()

            while self.is_running:
                try:
                    for wait_time in range(10):
                        if not self.is_running:
                            break

                        time.sleep(1)

                except KeyboardInterrupt:
                    print('Exiting.')

        # web_thread = threading.Thread(target=start_web_api, args=(None, ))
        # web_thread.start()

        daemon_thread = threading.Thread(target=start_daemon, args=(None, ))
        daemon_thread.start()

        web_app.run(debug=False, port=8002, host='0.0.0.0')
        #web_app.run(debug=False, port=8003, host='0.0.0.0')
        self.is_running = False

if __name__ == '__main__':
    file_logger = logging.getLogger("SignageDaemon")
    file_logger.setLevel(logging.INFO)

    file_handler = handlers.RotatingFileHandler(
        "signage-daemon.log",
        maxBytes=(1024 * 1024 * 5),
        backupCount=10
    )

    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
    file_handler.setFormatter(formatter)
    file_logger.addHandler(file_handler)

    app = SignageDaemon(logger=file_logger)

    if g_platform == "Linux":
    #if False:
        from daemon import runner

        try:
            daemon_runner = runner.DaemonRunner(app)
            daemon_runner.daemon_context.files_preserve = [file_handler.stream]
            daemon_runner.do_action()

        except runner.DaemonRunnerStopFailureError:
            print("Daemon already stopped.")

        except runner.DaemonRunnerStartFailureError:
            print("Daemon start failed.")
    else:
        app.run()

