#!/bin/sh

echo "Kill uwsgi pid : "$(cat ./uwsgi.pid)

kill -9 $(cat ./uwsgi.pid)

echo "Start uwsgi command : sudo -u www-data uwsgi ./uwsgi.ini"

sudo -u www-data uwsgi ./uwsgi.ini