__author__ = 'pwb@koino.net'

import requests
import smtplib
import re
from clickatell.api import Clickatell
from clickatell import constants as cc
import platform
import logging
from logging import handlers
import os
import time
from ConfigParser import ConfigParser
import datetime

# Multilanguages
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

current_dir = os.path.dirname(os.path.realpath(__file__))

g_platform = platform.system()

if g_platform == "Linux":
    LOG_DEFAULT_DIR = '/var/log/dstcs/'
elif g_platform == "Windows":
    LOG_DEFAULT_DIR = '.'
elif g_platform == "Darwin":
    LOG_DEFAULT_DIR = '.'


class server(object):
    def __init__(self, logger=None):
        self.logger = logger
        self.delay = 60
        self.config = None
        self.recipient_sms = None
        self.send_time = None
        self.server_url = None
        self.email_time = 0
        self.sms_time = 0

    def check_server(self):
        self.config = self.read_from_file(current_dir + '/alarm-recipients.ini', 'recipient')
        self.recipient_sms = self.read_from_file(current_dir + '/alarm-recipients.ini', 'sms')
        self.send_time = self.read_from_file(current_dir + '/alarm-recipients.ini', 'send')
        self.server_url = self.read_from_file(current_dir + '/alarm-recipients.ini', 'server_url')

        recipients = []
        sms_recipients = []
        server_url = []
        for i in self.config:
            recipients.append(self.config[i])
        for i in self.recipient_sms:
            sms_recipients.append(self.recipient_sms[i])
        for i in self.server_url:
            print i
            server_url.append((self.server_url[i]))

        while True:
            if self.email_time == int(self.send_time["email_time"]):
                self.email_time = 0
            if self.sms_time == int(self.send_time["sms_time"]):
                self.sms_time = 0

            for num in range(len(server_url)):
                try:
                    kkey = requests.get(server_url[num], params=None, verify=False)
                    kkey.json()
                    print server_url[num], kkey.json()["status"]

                    if kkey.json()["status"] != 200:
                        self.logger.info("Alarm Send E-mail or SMS")
                        self.logger.info(server_url[num])

                        if self.email_time == 0:
                            print 'send E-Mail'
                            from_email = "adamssuite@gmail.com"
                            to_email = recipients
                            headers = [
                                "From: " + from_email,
                                "Subject: " + "AdamsSuite Server Error",
                                "To: " + ", ".join(recipients),
                                "MIME-Version: 1.0",
                                "Content-Type: text/html"]

                            headers = "\r\n".join(headers)
                            #print headers
                            message = headers + '\r\n\r\n\r\n' + server_url[num] + "Error"

                            # Send mail
                            server = smtplib.SMTP('smtp.mandrillapp.com', 587)
                            server.login('hchkim@unet.kr', 'T238oVg4BMzaqAbSEOsQaQ')
                            server.sendmail(from_email, to_email, message)
                            server.quit()

                        if self.sms_time == 0:
                            print 'send SMS'
                            #phone = re.sub('[=.^#/!?%@:&$*}()+-]', '', '821012341234')

                            sendsms = Clickatell('adamssuite', 'PadFaSCSLOIeAZ', '3550442', sendmsg_defaults={'callback': cc.YES,'msg_type': cc.SMS_DEFAULT,
                                                                                             'deliv_ack': cc.YES,
                                                                                             'req_feat': cc.FEAT_ALPHA + cc.FEAT_NUMER + cc.FEAT_DELIVACK})

                            resp = sendsms.sendmsg(recipients=sms_recipients, text="AdamsSuite Server Error")

                except KeyboardInterrupt:
                    break
                except (requests.exceptions.RequestException, ValueError) as e:
                    self.logger.info("Exception Alarm Send E-mail or SMS")
                    self.logger.info(server_url[num])

                    if self.email_time == 0:
                        print 'Exception send E-Mail'
                        from_email = "adamssuite@gmail.com"
                        to_email = recipients
                        headers = [
                            "From: " + from_email,
                            "Subject: " + "AdamsSuite Server Error",
                            "To: " + ", ".join(recipients),
                            "MIME-Version: 1.0",
                            "Content-Type: text/html"]

                        headers = "\r\n".join(headers)
                        #print headers
                        message = headers + '\r\n\r\n\r\n' + server_url[num] + "Error"

                        # Send mail
                        server = smtplib.SMTP('smtp.mandrillapp.com', 587)
                        server.login('hchkim@unet.kr', 'T238oVg4BMzaqAbSEOsQaQ')
                        server.sendmail(from_email, to_email, message)
                        server.quit()

                    if self.sms_time == 0:
                        print 'Exception send SMS'
                        #phone = re.sub('[=.^#/!?%@:&$*}()+-]', '', '821083396980')

                        sendsms = Clickatell('adamssuite', 'PadFaSCSLOIeAZ', '3550442', sendmsg_defaults={'callback': cc.YES,'msg_type': cc.SMS_DEFAULT,
                                                                                         'deliv_ack': cc.YES,
                                                                                         'req_feat': cc.FEAT_ALPHA + cc.FEAT_NUMER + cc.FEAT_DELIVACK})

                        resp = sendsms.sendmsg(recipients=sms_recipients, text="AdamsSuite Server Error")

            self.email_time += 1
            self.sms_time += 1
            time.sleep(self.delay)

    def read_from_file(self, filename, section, required_props=None):
        config_parser = ConfigParser()
        data = dict()

        try:
            data_set = config_parser.read(filename)
            if len(data_set) == 0:
                self.logger.error("Open '%s' file failed." % (filename))
                return None

            if section not in config_parser.sections():
                return dict()

            for k, v in config_parser.items(section):
                data[k] = v

            if required_props is not None:
                for prop in required_props:
                    if prop not in data:
                        self.logger.error("%s must have %s property." % (filename, prop))
                        return None

            return data

        except IOError, e:
            self.logger.error("Open '%s' file failed: %s" % (filename, str(e)))
            return None

    def run(self):
        self.check_server()

if __name__ == '__main__':
    file_logger = logging.getLogger("server")
    file_logger.setLevel(logging.INFO)

    date = datetime.date.today()
    year = str(date.year)
    month = "%02d" % date.month
    day = "%02d" % date.day

    file_handler = handlers.RotatingFileHandler(
        year + month + day + ".log",
        maxBytes=(1024 * 1024 * 1),
        backupCount=3
    )

    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
    file_handler.setFormatter(formatter)
    file_logger.addHandler(file_handler)

    app = server(file_logger)

    if g_platform == "Linux":
        from daemon import runner
        try:
            daemon_runner = runner.DaemonRunner(app)
            daemon_runner.daemon_context.files_preserve = [file_handler.stream]
            daemon_runner.do_action()

        except runner.DaemonRunnerStopFailureError:
            print("Daemon already stopped.")

        except runner.DaemonRunnerStartFailureError:
            print("Daemon start failed.")
    else:
        app.logger.info('request-checker initialize.')
        app.run()


