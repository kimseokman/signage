__author__ = 'rainmaker'
# -*- coding: utf-8 -*-

from flask import Flask, request
from flask import Response
from flask.ext import excel
from flask.ext.cors import CORS
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.restful import reqparse, Resource, Api
from werkzeug import secure_filename
import time
import hashlib
import base64
import requests
import json
import os
import logging
import random
import bcrypt
import qrcode
from otpauth import OtpAuth
import StringIO
from logging import handlers
from sqlalchemy import or_, and_

from datetime import datetime, timedelta
from pytz import timezone
from pytz import all_timezones
import platform
import traceback
import xlsxwriter

import smtplib
from clickatell.api import Clickatell
from clickatell import constants as cc
import re

app = Flask(__name__)
var_cros_v1 = {'Content-Type', 'token', 'If-Modified-Since', 'Cache-Control', 'Pragma'}
#var_cros_v2 = {'Content-Type', 'token'}
CORS(app, resources=r'/api/*', headers=var_cros_v1)

from models import *

db.init_app(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:1234@127.0.0.1/dstcs'
#app.config['SQLALCHEMY_BINDS'] = {
#    'mssql': 'mssql+pymssql://mps:unet@2008@61.43.248.35:1433/mps'
#}

current_dir = os.path.dirname(os.path.realpath(__file__))

UPLOAD_FOLDER = '../uploads'
ALLOWED_EXTENSIONS = set(['epk'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Multilanguages
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

#----------------------------------------------------------------------------------------------------------------------
#                                            Static Area
#----------------------------------------------------------------------------------------------------------------------
# [ OPT ]
OTP_ISSUER = ""

# [ Device ] default value
DEFAULT_DP_MODEL = "UNKNOWN"
DEFAULT_MP_MODEL = "UNKNOWN"
DEFAULT_GROUP = "DEFAULT"
DEFAULT_STATUS = 0
DEFAULT_CNT = 0
DEFAULT_POLLING_TIME = 30
TIMER_BEGIN = "09:00"
TIMER_END = "23:00"

# [ User ] rating
USER_TYPE_SUPERADMIN = "SUPERADMIN"
USER_TYPE_ADMIN = "ADMIN"
USER_TYPE_MANAGER = "MANAGER"
USER_TYPE_MEMBER = "MEMBER"
USER_TYPE_GUEST = "GUEST"

# [ Key ] type
WHAT_KEY_ALIAS = "ALIAS"
WHAT_KEY_IP = "IP"
WHAT_KEY_MAC = "MAC"

QR_CODE_ROOT = "../qr_code/"
LIVE_VIEW_ROOT = "../live_view/"
DAEMON_ADDRESS = "http://127.0.0.1:8002"
#DAEMON_ADDRESS = "http://lgesvr1.adamssuite.com:8002"
DAEMON_HEADERS = {'Content-type': 'application/json'}

g_platform = platform.system()

if g_platform == "Linux":
    LOG_DEFAULT_DIR = '/var/log/dstcs/'
elif g_platform == "Windows":
    LOG_DEFAULT_DIR = '.'
elif g_platform == "Darwin":
    LOG_DEFAULT_DIR = '.'

#----------------------------------------------------------------------------------------------------------------------
#                                            Function Area
#-----------------------------------------------------------------------------------------------------------------------
def bool2int(v):
    if v is True:
        return 1
    else:
        return 0

def str2bool(v):
  return v.lower() in ("yes", "true", "1")

def totimestamp(dt, epoch=datetime(1970,1,1)):
    td = dt - epoch
    # return td.total_seconds()
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 1e6

def generateOTP(userID, userPW, otp_key, server_key, server_label):
    auth = OtpAuth(server_key + userID + userPW + otp_key)
    qrcode_uri = auth.to_uri('totp', userID + server_label, OTP_ISSUER)

    img = qrcode.make(qrcode_uri)

    # img size limit 300
    if img.size[0] < 300:
        base_width = img.size[0]
    else:
        base_width = 300
    wpercent = (base_width/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((base_width, hsize))

    stream = StringIO.StringIO()
    capture_path = QR_CODE_ROOT + userID + ".png"
    img.save(capture_path)
    image = stream.getvalue()
    stream.close()

    return Response(image, mimetype='image/png')

def json_encoder(thing):
    list_date = str(thing).split(":")

    if hasattr(thing, 'isoformat'):
        if len(list_date[0]) == 1:
            return "0" + thing.isoformat()
        return thing.isoformat()
    else:
        if len(list_date[0]) == 1:
            return "0" + str(thing)
        return str(thing)

def make_dateTime(str_date):
    temp = str_date.split(" ")
    str_datetime = temp[0]+"T"+temp[1]+"Z"

    return datetime.strptime(str_datetime, '%Y-%m-%dT%H:%M:%SZ')

def time_to(local_time, to_zone):
    fmt = "%Y-%m-%d %H:%M:%S %Z%z"
    local_obj = datetime.strptime(local_time, "%Y-%m-%d %H:%M:%S")

    if to_zone == 'UTC':
        datetime_obj = local_obj.replace(tzinfo=timezone('UTC'))
    else:
        datetime_obj = timezone(to_zone).localize(local_obj)
    return datetime_obj.strftime(fmt)

def time_to_from(local_time, to_zone, from_zone):
    fmt = "%Y-%m-%d %H:%M:%S %Z%z"
    time_obj = datetime.strptime(local_time, "%Y-%m-%d %H:%M:%S")

    if from_zone == 'UTC':
        from_obj = time_obj.replace(tzinfo=timezone('UTC'))
    else:
        from_obj = timezone(from_zone).localize(time_obj)

    to_obj = from_obj.astimezone(timezone(to_zone))

    return to_obj.strftime(fmt)

def timer_parser(list):
    list_obj = {
        'f1': 'NG',
        'f2': 'NG',
        'f3': 'NG',
        'f4': 'NG',
        'f5': 'NG',
        'f6': 'NG',
        'f7': 'NG'
    }

    if list is not None:
        node_list = list.split('_')

        for node in node_list:
            if node[0:2] != 'NG':
                list_obj[node[0:2]] = str(node[2:])

    return list_obj

def tile_parser(value):
    if value is None:
        return 'NG'
    else:
        on_off_flag = value[0:2]
        tile_mode_check_value = value[2:]
        # [ Case by ] ON
        if on_off_flag == '00':
            return '00'
        # [ Case by ] OFF
        elif on_off_flag == '01':
            return tile_mode_check_value

def password_encoder(password):
    pass1 = hashlib.sha1(password).digest()
    pass2 = hashlib.sha1(pass1).hexdigest()
    hashed_pw = "*" + pass2.upper()
    return hashed_pw

def check_member_auth(auth):
    auth_obj = []
    # [ Check ] Auth
    if auth is 'ADMIN':
        auth_obj.append(1)
        auth_obj.append(2)
        auth_obj.append(4)
        auth_obj.append(8)
    elif auth is not None:
        if auth & 1:
            # auth: Device Information
            auth_obj.append(1)
        if auth & 2:
            # auth: Media Player
            auth_obj.append(2)
        if auth & 4:
            # auth: Display
            auth_obj.append(4)
        if auth & 8:
            # auth: Report
            auth_obj.append(8)

    return auth_obj

def result(code, notice, objects, meta, author):
    """
    html status code def
    [ 200 ] - OK
    [ 400 ] - Bad Request
    [ 401 ] - Unauthorized
    [ 404 ] - Not Found
    [ 500 ] - Internal Server Error
    [ 503 ] - Service Unavailable
    - by KSM
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    if author is None:
        author = "by rainmaker"

    result = {
        "status": code,
        "notice": notice,
        "author": author
    }

    log_byKSM = ''

    # [ Check ] Objects
    if objects != None:
        result["objects"] = objects

    # [ Check ] Meta
    if meta != None:
        result["meta"] = meta

    if code == 200:
        result["message"] = "OK"
        log_byKSM = OKBLUE
    elif code == 400:
        result["message"] = "Bad Request"
        log_byKSM = FAIL
    elif code == 401:
        result["message"] = "Unauthorized"
        log_byKSM = WARNING
    elif code == 404:
        result["message"] = "Not Found"
        log_byKSM = FAIL
    elif code == 500:
        result["message"] = "Internal Server Error"
        log_byKSM = FAIL
    elif code == 503:
        result["message"] = "Service Unavailable"
        log_byKSM = WARNING

    log_byKSM = log_byKSM + 'RES : [' + str(code) + '] ' + str(notice) + ENDC
    print log_byKSM
    return result

#----------------------------------------------------------------------------------------------------------------------
#                                            Class Area
#----------------------------------------------------------------------------------------------------------------------
class Helper(object):
    @staticmethod
    def get_file_logger(app_name, filename):
        log_dir_path = LOG_DEFAULT_DIR
        try:
            if not os.path.exists(log_dir_path):
                os.mkdir(log_dir_path)

            full_path = '%s/%s' % (log_dir_path, filename)
            file_logger = logging.getLogger(app_name)
            file_logger.setLevel(logging.INFO)

            file_handler = handlers.RotatingFileHandler(
                full_path,
                maxBytes=(1024 * 1024 * 10),
                backupCount=5
            )
            formatter = logging.Formatter('%(asctime)s %(message)s')

            file_handler.setFormatter(formatter)
            file_logger.addHandler(file_handler)

            return file_logger

        except IOError, e:
            print("Can't create log directory for '%s': \n  %s" % (filename, str(e)))
            return logging.getLogger(app_name)

        except OSError, e:
            print("Can't create log directory for '%s': \n  %s" % (filename, str(e)))
            return logging.getLogger(app_name)

exception_logger = Helper.get_file_logger("exception", "exception.log")

@app.errorhandler(500)
def internal_error(exception):
    exception_logger.info(traceback.format_exc())
    return 500

@app.errorhandler(404)
def internal_error(exception):
    exception_logger.info(traceback.format_exc())
    return 404

# Singleton Source from http://stackoverflow.com/questions/42558/python-and-the-singleton-pattern
class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Other than that, there are
    no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    Limitations: The decorated class cannot be inherited from.
    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)

@Singleton
class TokenManager(object):
    def generate_token(self, userID):
        m = hashlib.sha1()

        m.update(str(userID))
        m.update(datetime.now().isoformat())

        return m.hexdigest()

    def validate_token(self, token):
        token_result = Tokens.query.filter_by(token=token).first()

        if token_result is None:
            return ""
        return token_result.userID

    def get_user_type(self, userID):
        manager = Managers.query.filter_by(userID=userID).first()

        if manager is None:
            user = Users.query.filter_by(userID=userID).first()
            if user is None:
                return USER_TYPE_GUEST, user.id, user.FK_managers_id
            else:
                return USER_TYPE_MEMBER, user.id, user.FK_managers_id
        else:
            if manager.isSuper:
                return USER_TYPE_SUPERADMIN, manager.id, -1
            elif manager.isAdmin:
                return USER_TYPE_ADMIN, manager.id, -1
            else:
                return USER_TYPE_MANAGER, manager.id, -1

    def get_admin_type(self, userID):
        manager = Managers.query.filter_by(userID=userID).first()
        if manager is None:
            return False
        else:
            if manager.isSuper:
                return True
            else:
                return False

#----------------------------------------------------------------------------------------------------------------------
#                                            Basic
#----------------------------------------------------------------------------------------------------------------------
class BasicAPI(Resource):
    """
    [ User Information ]
    @ GET : Server Alive Check.
    - by KSM
    """
    def __init__(self):
        super(BasicAPI, self).__init__()

    def get(self):
        return result(200, "Welcome to dstcs API Server", None, None, None)

class AccessAPI(Resource):
    """
    [ Access History]
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("type", type=bool, location="json")
        self.parser.add_argument("os_ver", type=str, location="json")
        self.parser.add_argument("browser_ver", type=str, location="json")

        super(AccessAPI, self).__init__()

    def get(self):
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))

        # [ Make ] ORDER BY
        order_by = request.args.get('order_by')
        order_by_str = ''
        if order_by is not None:
            order_by_str = 'access_history.' + str(order_by)

        order = request.args.get('order')
        order_str = ''
        if order == '-':
            order_str = 'DESC'
        else:
            order_str = 'ASC'

        result_order_by = order_by_str + ' ' + order_str

        objects = []
        meta = {}


        if self.userType == USER_TYPE_ADMIN:
            self.filter_group = []
            if self.userType is USER_TYPE_ADMIN:
                manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                if len(manager_ids) is 0:
                    self.filter_group.append(Managers.id == -1)
                else:
                    for mid in manager_ids:
                        self.filter_group.append(Managers.id == mid.FK_manager_id)

            total_count = Access_History.query.join(Managers, Access_History.FK_managers_id == Managers.id).filter(Managers.isAdmin == 0).filter(or_(*self.filter_group)).order_by(result_order_by).count()
            result_acc_history = Access_History.query.join(Managers, Access_History.FK_managers_id == Managers.id).filter(Managers.isAdmin == 0).filter(or_(*self.filter_group)).order_by(result_order_by).limit(limit).offset(offset)
        elif self.userType == USER_TYPE_MANAGER:
            total_count = Access_History.query.join(Users, Access_History.FK_users_id == Users.id).filter(Users.FK_managers_id == self.user_manager_id).order_by(result_order_by).count()
            result_acc_history = Access_History.query.join(Users, Access_History.FK_users_id == Users.id).filter(Users.FK_managers_id == self.user_manager_id).order_by(result_order_by).limit(limit).offset(offset)

        current_count = result_acc_history.count()

        next = ''
        previous = ''

        previous_offset = offset-limit
        next_offset = offset+limit
        # Creating a Pagenation
        if offset == 0:
            previous = None
            next = None
        elif offset != 0:
            previous = "/api/v1/access_history?limit="+str(limit)+"&offset="+str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next = "/api/v1/access_history?limit="+str(limit)+"&offset="+str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        if current_count == 0:
            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({

            })
            return result(404, "No Incidents Info", None, meta, "by KSM")
        else:
            for access in result_acc_history:
                objects.append({
                    'id': access.id,
                    'type': access.type,
                    'update_time': json_encoder(access.update_time),
                    'userID': access.userID,
                    'ip_addr': access.ip_addr,
                    'os_ver': access.os_ver,
                    'browser_ver': access.browser_ver
                })
            return result(200, "Get the Access History", objects, meta, "by rainmaker")

    def post(self):
        input = self.parser.parse_args()

        new_access_history = Access_History()

        new_access_history.type = input["type"]
        new_access_history.os_ver = input["os_ver"]
        new_access_history.browser_ver = input["browser_ver"]

        new_access_history.ip_addr = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)

        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
            user = Managers.query.filter_by(userID=self.userID).first()
            new_access_history.FK_managers_id = user.id
        elif self.userType == USER_TYPE_MEMBER:
            user = Users.query.filter_by(userID=self.userID).first()
            new_access_history.FK_users_id = user.id

        new_access_history.userID = self.userID

        # [ TIME ] UTC
        now = time.gmtime()
        current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
        new_access_history.update_time = current_time

        db.session.add(new_access_history)
        db.session.commit()

        return result(200, "Access History Logging successful", None, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Auth Area
#----------------------------------------------------------------------------------------------------------------------
class OtpAPI(Resource):
    """
    [ Otp ]
    @ GET : otp_code (digit code 6)
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("createOTP", type=bool, location="json")
        self.parser.add_argument("key", type=str, location="json")
        self.parser.add_argument("put_type", type=str, location="json")

        super(OtpAPI, self).__init__()

    def get(self):
        otp_code = request.args.get('otp_code')

        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
            user = Managers.query.filter_by(userID=self.userID).first()
        elif self.userType == USER_TYPE_MEMBER:
            user = Users.query.filter_by(userID=self.userID).first()

        passOTP = False
        objects = []

        sv_info = Servers.query.first()

        if user is not None:
            auth = OtpAuth(sv_info.server_key + user.userID + user.userPW + user.otp_key)
            if otp_code and auth.valid_totp(otp_code):
                passOTP = True
            else:
                passOTP = False

            objects.append({
                'pass_otp': passOTP
            })

        return result(200, "OTP authentication success", objects, None, "by PWB")

    def put(self):
        input = self.parser.parse_args()
        del input['token']
        put_user_type = input['put_type']
        del input['put_type']
        put_user_key = input['key']
        del input['key']

        # [ Case by ] OTP Reset : Super -> Admin -> Mananger -> User
        if put_user_type == 'ADMIN' or put_user_type == 'MANAGER':
            input['otp_key'] = random.random()
            db.session.query(Managers).filter_by(id=put_user_key).update(input)
        elif put_user_type == 'MEMBER':
            input['otp_key'] = random.random()
            db.session.query(Users).filter_by(id=put_user_key).update(input)
        # [ Case by ] OTP Reset : Self
        elif put_user_type == 'SELF':
            input['otp_key'] = random.random()
            # [ Check ] Update
            if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
                db.session.query(Managers).filter_by(userID=self.userID).update(input)
            elif self.userType == USER_TYPE_MEMBER:
                db.session.query(Users).filter_by(userID=self.userID).update(input)
        # [ Case by ] Create OTP Update
        else:
            # [ Check ] Update
            if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
                db.session.query(Managers).filter_by(userID=self.userID).update(input)
            elif self.userType == USER_TYPE_MEMBER:
                db.session.query(Users).filter_by(userID=self.userID).update(input)
        db.session.commit()

        return result(200, "Create OTP update successful", None, None, "by KSM")

class AuthAPI(Resource):
    """
    [ Auth ]
    @ POST : Generate OR Update Token
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("userID", type=str, location="json")
        self.parser.add_argument("userPW", type=str, location="json")

        self.token_manager = TokenManager.instance()

        super(AuthAPI, self).__init__()

    def post(self):
        input = self.parser.parse_args()

        # [ Check ] property
        check = ["userID", "userPW"]
        for i in range(len(check)):
            if not str(check[i]) in input:
                return result(400, "Missing required properties", None, None, "by KSM")

        manager_query = "SELECT id, userID FROM %(table_name)s WHERE userID='%(ID)s' AND userPW=password('%(PW)s')" % {
            "table_name": Managers.__tablename__,
            "ID": input["userID"],
            "PW": input["userPW"]
        }
        # [ Check ] Manager
        manager = Managers.query.from_statement(manager_query).first()

        new_token = Tokens()
        objects = []

        sv_info = Servers.query.first()

        server_key = sv_info.server_key
        server_label = sv_info.server_label

        # [ Case by ] is Manager
        if manager is not None:
            # [ Case by ] generate Token
            new_token.userID = manager.userID
            new_token.token = self.token_manager.generate_token(manager.userID)
            new_token.FK_managers_id = manager.id

            db.session.add(new_token)
            db.session.commit()

            # [ Check ] OTP
            if manager.userOTP == 1 and manager.createOTP == 0:
                generateOTP(manager.userID, manager.userPW, manager.otp_key, server_key, server_label)

            objects.append({
                'login': True,
                'userOTP': manager.userOTP,
                'token': new_token.token
            })

            return result(200, "Admin - New token is generated.", objects, None, "by KSM")
        # [ Case by ] is not Manager
        else:
            query = "SELECT id, userID FROM %(table_name)s WHERE userID='%(ID)s' AND userPW=password('%(PW)s')" % {
                "table_name": Users.__tablename__,
                "ID": input["userID"],
                "PW": input["userPW"]
            }
            # [ Check ] User
            user = Users.query.from_statement(query).first()

            # [ Case by ] is User
            if user is not None:
                # [ Case by ] generate Token
                new_token.userID = user.userID
                new_token.token = self.token_manager.generate_token(user.userID)
                new_token.FK_users_id = user.id

                db.session.add(new_token)
                db.session.commit()

                if user.userOTP == 1 and user.createOTP == 0:
                    generateOTP(user.userID, user.userPW, user.otp_key, server_key, server_label)

                objects.append({
                    'login': True,
                    'userOTP': user.userOTP,
                    'token': new_token.token
                })

                return result(200, "User - New token is generated.", objects, None, "by KSM")
            # [ Case by ] Account does not exist
            else:
                objects.append({
                    "login": False
                })
                return result(400, "Because the account does not exist, you can not create a token.", objects, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            User Area
#----------------------------------------------------------------------------------------------------------------------

class UserAPI(Resource):
    """
    [ User Information ]
    @ GET : Returns the user information with the same token value.
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(UserAPI, self).__init__()

    def get(self):
        objects = []

        # [ Case by ] is Manager
        if self.userType == USER_TYPE_MANAGER or self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_SUPERADMIN:
            # [ Check ] Manager
            manager = Managers.query.filter_by(userID=self.userID).first()
            objects.append({
                'userID': manager.userID,
                'userName': manager.userName,
                'phone': manager.phone,
                'email': manager.email,
                'auth': check_member_auth('ADMIN'),
                'createOTP': manager.createOTP,
                'comp_key': manager.comp_key,
                'what_key': manager.what_key,
                'time_zone': manager.time_zone,
                'date_format': manager.date_format,
                'refresh_time': manager.refresh_time,
                'isAdmin': manager.isAdmin,
                'isSuper': manager.isSuper,
                'userType': self.userType
            })
            return result(200, "GET the User Type : Manager", objects, None, "by KSM")
        # [ Case by ] is not Manager
        else:
            # [ Case by ] is User
            if self.userType == USER_TYPE_MEMBER:
                user = Users.query.filter_by(userID=self.userID).first()

                member_auth = 0
                # [ case by ] No user group
                if user.FK_user_groups_id == -1:
                    member_auth = check_member_auth(user.auth)
                # [ case by ] user group
                else:
                    user_group = User_Groups.query.filter_by(id=user.FK_user_groups_id).first()
                    member_auth = check_member_auth(user_group.auth)

                objects.append({
                    'userID': user.userID,
                    'userName': user.userName,
                    'phone': user.phone,
                    'email': user.email,
                    'auth': member_auth,
                    'createOTP': user.createOTP,
                    'time_zone': user.time_zone,
                    'date_format': user.date_format,
                    'refresh_time': user.refresh_time,
                    'FK_managers_id': user.FK_managers_id,
                    'isSuper': False,
                    'userType': self.userType
                })
                return result(200, "GET the User Type : Member", objects, None, "by KSM")
            # [ Case by ] No Auth
            else:
                return result(401, "You do not have permission", objects, None, "by KSM")


class CheckUserAPI(Resource):
    """
    [ User Information ]
    @ GET : Returns the result of checking the user information.
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(CheckUserAPI, self).__init__()

    def get(self, user_id):
        # [ Check ] Manager

        manager = Managers.query.filter_by(userID=user_id).first()
        user = Users.query.filter_by(userID=user_id).first()

        if manager is None:
            if user is None:
                return result(200, "User ID is available.", None, None, "by KSM")
            else:
                return result(400, "The duplicate ID.", None, None, "by KSM")
        else:
            return result(400, "The duplicate ID.", None, None, "by KSM")


class CheckGroupAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        super(CheckGroupAPI, self).__init__()

    def get(self, user_id):
        # [ Check ] Group
        search_type = request.args.get('type')

        if search_type == 'Device_Group':
            device_group = Device_Groups.query.filter_by(name=user_id, FK_managers_id=self.user_manager_id).first()

            if device_group is None:
                return result(200, "User ID is available.", None, None, "by PWB")
            else:
                return result(400, "The duplicate ID.", None, None, "by PWB")

        elif search_type == 'User_Group':
            user_group = User_Groups.query.filter_by(name=user_id, FK_managers_id=self.user_manager_id).first()

            if user_group is None:
                return result(200, "User ID is available.", None, None, "by PWB")
            else:
                return result(400, "The duplicate ID.", None, None, "by PWB")


class UserGroupListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("auth", type=str, location="json")

        super(UserGroupListAPI, self).__init__()

    def get(self):
        if self.isValidToken == False or self.userType == USER_TYPE_GUEST:
            return result(401, "You do not have permission.", None, None, "by KSM")
        else:
            ug_list = User_Groups.query.join(Managers, self.userID == Managers.userID).filter(User_Groups.FK_managers_id == Managers.id).all()

            objects = []

            if len(ug_list) == 0:
                return result(404, "No data", None, None, "by KSM")
            else:
                for ug in ug_list:
                    objects.append({
                        'key': ug.id,
                        'name': ug.name
                    })
                return result(200, "GET the User Group data", objects, None, "by KSM")

    def post(self):
        input = self.parser.parse_args()

        if self.userType == USER_TYPE_GUEST:
            return result(401, "You do not have permission.", None, None, "by KSM")
        else:
            manager = Managers.query.filter_by(userID=self.userID).first()
            ug = User_Groups()

            ug.name = input['name']
            ug.auth = input['auth']

            ug.FK_managers_id = manager.id

            db.session.add(ug)
            db.session.commit()

            return result(200, "Generate User Group", None, None, "by KSM")

class UserGroupAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("auth", type=str, location="json")

        super(UserGroupAPI, self).__init__()

    def get(self, key):
        if self.userType is not USER_TYPE_MANAGER:
            return result(400, 'You do not have permission', None, None, "by KSM")
        if key is None:
            return result(400, 'There is no essential parameters.', None, None, "by KSM")
        else:
            objects = []
            # [ Check ] Device Group
            ug = User_Groups.query.filter_by(id=key).first()
            if ug is None:
                return result(404, 'Can not find the User group.', None, None, "by KSM")
            else:
                objects.append({
                    "key": ug.id,
                    "name": ug.name,
                    "auth": check_member_auth(ug.auth)
                })

                return result(200, "GET the User Group Info", objects, None, "by KSM")

    def put(self, key):
        if self.userType is not USER_TYPE_MANAGER:
            return result(400, 'You do not have permission', None, None, "by KSM")

        input = self.parser.parse_args()

        if key is None:
            return result(400, 'There is no essential parameters.', None, None, "by KSM")
        else:
            # [ Check ] Device Group
            ug = User_Groups.query.filter_by(id=key).first()

            del input['token']
            for k, v in input.items():
                if v is None:
                    del input[k]

            if ug is None:
                return result(404, 'Can not find the User group.', None, None, "by KSM")
            else:
                db.session.query(User_Groups).filter_by(id=key).update(input)
                db.session.commit()

                return result(200, "Touch the User Group Info", None, None, "by KSM")

    def delete(self, key):
        if key is None:
            return result(400, 'There is no essential parameters.', None, None, "by KSM")
        else:
            # [ Check ] Device Group
            ug = User_Groups.query.filter_by(id=key).first()

            if ug is not None:
                db.session.delete(ug)
                db.session.commit()
                return result(200, "Delete the User Group Info", None, None, "by KSM")
        return result(200, "Delete the User Group Info", None, None, "by KSM")

class CONN_UserGroupAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("group_key", type=str, location="json")
        self.parser.add_argument("user_key", type=str, location="json")

        super(CONN_UserGroupAPI, self).__init__()

    def put(self):
        input = self.parser.parse_args()

        # [ check ] User Groups
        ug = User_Groups.query.filter_by(id=input['group_key']).first()

        if ug is None:
            if input['group_key'] == '-1':
                update_list = {}

                update_list['FK_user_groups_id'] = input['group_key']
                update_list['user_group_name'] = 'empty'
                db.session.query(Users).filter_by(id=input['user_key']).update(update_list)
                db.session.commit()
                return result(200, "Reset User Group", None, None, "by KSM")
            else:
                return result(404, "No Data", None, None, "by KSM")
        else:
            update_list = {}

            update_list['FK_user_groups_id'] = ug.id
            update_list['user_group_name'] = ug.name

            db.session.query(Users).filter_by(id=input['user_key']).update(update_list)
            db.session.commit()
            return result(200, "Change User Group", None, None, "by KSM")
#----------------------------------------------------------------------------------------------------------------------
#                                MediaPlayers Controller(ZK), MediaPlayers Revise(DB) Area
#----------------------------------------------------------------------------------------------------------------------
class MediaPlayersControllerAPI(Resource):
    """
    [ Media Players Controller ]
    - Used to give a URI to the daemon. To zookeeper
    by rainmaker
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("type", type=str, location="json")
        self.parser.add_argument("command", type=str, location="json")
        self.parser.add_argument("data", type=str, location="json")
        self.parser.add_argument("log", type=str, location="json")

        super(MediaPlayersControllerAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()
        log_text = input['log']
        del input['log']
        # [ Check ] serial_number
        mp = MediaPlayers.query.filter_by(serial_number=serial_number).first()

        if mp is not None:
            req_url = DAEMON_ADDRESS + '/controller/%s' % serial_number
            data = json.dumps(input)
            r = requests.put(req_url, data=data, headers=DAEMON_HEADERS)
            sh = Support_History()

            # [ TIME ] UTC
            now = time.gmtime()
            current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
            sh.action_date = current_time

            sh.userID = self.userID
            # media_player
            sh.device = "0"
            if input["type"] == "display_control":
                sh.action_type = "Display Control"
                sh.action_command = log_text
            elif input["type"] == "remote_control":
                sh.action_type = "Remote Control"
                sh.action_command = log_text
            elif input["type"] == "application_control":
                sh.action_type = "Application Control"
                sh.action_command = input["command"] + " " + input["data"]
            elif input["type"] == "check_control":
                sh.action_type = "Check Control"
                sh.action_command = log_text
            elif input["type"] == "agent_control":
                sh.action_type = "Agent Control"
                sh.action_command = log_text
            elif input["type"] == "mediaplayer_control":
                sh.action_type = "OS Restart Control"
                sh.action_command = log_text
            else:
                sh.action_type = "Unknown"

            sh.mp_serial_number = serial_number

            db.session.add(sh)
            db.session.commit()

            return result(200, "Request to Daemon", None, None, "by rainmaker")
        else:
            return result(400, "No valid serial number", None, None, "by rainmaker")

class MediaPlayersReviseAPI(Resource):
    """
    [ Media Players Revise ]
    - Mediaplayer column is modified. To DB
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("activation_date", type=str, location="json")
        self.parser.add_argument("expiration_date", type=str, location="json")
        self.parser.add_argument("warrent_start_date", type=str, location="json")
        self.parser.add_argument("warrent_end_date", type=str, location="json")
        self.parser.add_argument("service_offer", type=str, location="json")
        self.parser.add_argument("status", type=str, location="json")
        self.parser.add_argument("model", type=str, location="json")
        self.parser.add_argument("manage_serial_number", type=str, location="json")
        self.parser.add_argument("group_status", type=str, location="json")
        self.parser.add_argument('polling_time', type=str, location="json")
        self.parser.add_argument('app_path1', type=str, location="json")
        self.parser.add_argument('app_path2', type=str, location="json")
        self.parser.add_argument('app_path3', type=str, location="json")
        self.parser.add_argument('app_path4', type=str, location="json")

        super(MediaPlayersReviseAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()

        # [ Check ] polling time
        if input['polling_time'] is not None:
            if int(input['polling_time']) > 0 and int(input['polling_time']) < 30:
                return result(400, 'The polling time value is not between 1 and 29.', None, None, "by KSM")

        # [ Create ] check
        check = []
        for key in input:
            check.append(key)

        # [ Check ] property Null remove
        for i in range(len(check)):
            if input[check[i]] is None:
                del input[check[i]]

        # [ Check ] serial number
        mp = MediaPlayers.query.filter_by(serial_number=serial_number).first()

        # [ Case by ] MediaPlayer update
        if mp is not None:
            db.session.query(MediaPlayers).filter_by(serial_number=serial_number).update(input)
            db.session.commit()
            return result(200, "MediaPlayer update successful", None, None, "by KSM")

        # [ Case by ] MediaPlayer does not exist
        else:
            return result(400, "MediaPlayer does not exist", None, None, "by KSM")

class MediaPlayersAliveAPI(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("alive", type=str, location="json")

        super(MediaPlayersAliveAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()

        if input["alive"] == "1":
            db.session.query(MediaPlayers).filter_by(serial_number=serial_number).update(dict(alive=1, status=0, group_status=0))
            db.session.query(Displays).filter_by(mp_serial_number=serial_number).update(dict(alive=1))
            db.session.commit()
        elif input["alive"] == "0":
            db.session.query(MediaPlayers).filter_by(serial_number=serial_number).update(dict(alive=0, disconnected_date=datetime.now(), status=1, group_status=1))
            db.session.query(Displays).filter_by(mp_serial_number=serial_number).update(dict(alive=0))
            db.session.commit()

        return result(200, "Alive Update successful", None, None, None)

class MediaPlayersCountAPI(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()

        super(MediaPlayersCountAPI, self).__init__()

    def get(self):
        site = request.args.get('site')
        location = request.args.get('location')
        shop = request.args.get('shop')

        dict_count = dict()
        dict_count["normal_count"] = 0
        dict_count["check_required_count"] = 0
        dict_count["unresolved_count"] = 0
        dict_count["disconnected_count"] = 0
        dict_count["total_count"] = 0

        filter_and_group = []
        if site is not None:
            filter_and_group.append(MediaPlayers.site == site)
        if location is not None:
            filter_and_group.append(MediaPlayers.location == location)
        if shop is not None:
            filter_and_group.append(MediaPlayers.shop == shop)

        mp_list = MediaPlayers.query.add_columns(MediaPlayers.group_status, db.func.count(MediaPlayers.group_status).label("count")).\
            filter(and_(*filter_and_group)).group_by(MediaPlayers.group_status).all()

        if mp_list is None:
            return result(404, "Not Found Media Player Count", dict_count, None, "by KSM")

        for mp in mp_list:
            if mp.group_status is 0:
                dict_count["normal_count"] = mp.count
            elif mp.group_status is 1:
                dict_count["check_required_count"] = mp.count
            elif mp.group_status is 2:
                dict_count["unresolved_count"] = mp.count
            elif mp.group_status is -1:
                dict_count["disconnected_count"] = mp.count

        dict_count["total_count"] = dict_count["normal_count"] + dict_count["check_required_count"] + dict_count["unresolved_count"] + dict_count["disconnected_count"]

        return result(200, "GET Count Info", dict_count, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Device List Area
#----------------------------------------------------------------------------------------------------------------------
class IncidentListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------

        super(IncidentListAPI, self).__init__()

    def get(self):
        mp_serial_number = request.args.get('mp_serial_number')

        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))

        # [ Make ] ORDER BY
        order_by = request.args.get('order_by')
        order_by_str = ''
        if order_by is not None:
            order_by_str = 'incidents.' + str(order_by)

        order = request.args.get('order')
        order_str = ''
        if order == '-':
            order_str = 'DESC'
        else:
            order_str = 'ASC'

        result_order_by = order_by_str + ' ' + order_str

        objects = []
        meta = {}

        total_count = Incidents.query.filter_by(mp_serial_number=mp_serial_number).count()
        result_incidents = Incidents.query.filter_by(mp_serial_number=mp_serial_number).order_by(result_order_by).limit(limit).offset(offset)

        current_count = result_incidents.count()

        next = ''
        previous = ''

        previous_offset = offset-limit
        next_offset = offset+limit
        # Creating a Pagenation
        if offset == 0:
            previous = None
            next = None
        elif offset != 0:
            previous = "/api/v1/incident-list?limit="+str(limit)+"&offset="+str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next = "/api/v1/incident-list?limit="+str(limit)+"&offset="+str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        if current_count == 0:
            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({

            })
            return result(404, "No Incidents Info", None, meta, "by KSM")
        else:
            for incident in result_incidents:
                objects.append({
                    'id': incident.id,
                    'dp_serial_number': incident.dp_serial_number,
                    'mp_serial_number': incident.mp_serial_number,
                    'type': incident.type,
                    'alias': incident.alias,
                    'created': json_encoder(incident.created),
                    'error_code': incident.error_code,
                    'level': incident.level
                })
            return result(200, "Get the Incident list", objects, meta, "by rainmaker")

class DisplayListByMediaPlayerAPI(Resource):
    """
    [ Display List By MediaPlayer ]
    @ GET : Returns Display List Info By MediaPlayer
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------

        super(DisplayListByMediaPlayerAPI, self).__init__()

    def get(self):
        # For Tree Click Event Type Check In Web
        VeiwTypeInWeb = ""

        mp_serial_number = request.args.get('mp_serial_number')
        if mp_serial_number is None:
            return result(400, "Missing required properties", None, None, "by KSM")
        else:
            VeiwTypeInWeb = "MP"

        dp_group_name = request.args.get('dp_group_name')

        # [ Check ] parents MediaPlayer
        mp = MediaPlayers.query.filter_by(serial_number=mp_serial_number).first()
        if mp is None:
            return result(500, "Can not find the parents MediaPlayer", None, None, "by KSM")

        if dp_group_name is None:
            filter_or_group = list()
            filter_or_group.append(Displays.dp_group==None)
            filter_or_group.append(Displays.dp_group=='')
            dp_list = Displays.query.filter_by(FK_mp_id=mp.id).filter(or_(*filter_or_group)).order_by("number").all()
        else:
            VeiwTypeInWeb = "DG"
            if dp_group_name == '_ALL_DATA_':
                dp_list = Displays.query.filter_by(FK_mp_id=mp.id).order_by("number").all()
            else:
                dp_list = Displays.query.filter_by(FK_mp_id=mp.id, dp_group=dp_group_name).order_by("number").all()

        objects = []

        # [ Case by ] Get List Info by MP
        if mp_serial_number is not None:
            # [ Check ] MP
            mp = MediaPlayers.query.filter_by(serial_number=mp_serial_number).first()

            objects.append({
                "name": "MediaPlayer",
                "type": VeiwTypeInWeb,
                "serial_number": mp.serial_number,
            })

            if len(dp_list) != 0:
                VeiwTypeInWeb = "DP"
                for dp in dp_list:
                    objects.append({
                        "name": "Display",
                        "type": VeiwTypeInWeb,
                        "serial_number": dp.serial_number,
                        "number": dp.number,
                        "mp_serial_number": mp.serial_number
                    })

            return result(200, "GET the Device List", objects, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            MediaPlayer Area
#----------------------------------------------------------------------------------------------------------------------
class MediaPlayerListAPI(Resource):
    """
    [ Media Players List ]
    @ GET : Returns the full Media Player.
        [ Case by ] Meta Only
            - Used for pagenation.
            - Used for the total count
        [ Case by ] Meta & Objects
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("shop", type=str, location="json")
        self.parser.add_argument("activation_date", type=str, location="json")
        self.parser.add_argument("location", type=str, location="json")
        self.parser.add_argument("expire_date", type=str, location="json")
        self.parser.add_argument("serial_number", type=str, location="json")
        self.parser.add_argument("manage_serial_number", type=str, location="json")
        self.parser.add_argument("model", type=str, location="json")
        self.parser.add_argument("manufact", type=str, location="json")
        self.parser.add_argument("version", type=str, location="json")
        self.parser.add_argument("uuid", type=str, location="json")
        self.parser.add_argument("wakeup", type=str, location="json")
        self.parser.add_argument("sku", type=str, location="json")
        self.parser.add_argument("family", type=str, location="json")
        self.parser.add_argument("processor", type=str, location="json")
        self.parser.add_argument("bios", type=str, location="json")
        self.parser.add_argument("os", type=str, location="json")
        self.parser.add_argument("physical_memory", type=float, location="json")
        self.parser.add_argument("physical_hdd", type=float, location="json")
        self.parser.add_argument("media_player_alias", type=str, location="json")
        self.parser.add_argument("firmware_version", type=str, location="json")
        self.parser.add_argument("site", type=str, location="json")
        self.parser.add_argument("comp_key", type=str, location="json")

        super(MediaPlayerListAPI, self).__init__()

    def get(self):
        if self.isValidToken==False or self.userType==USER_TYPE_GUEST:

            return result(401, "You do not have permission.", None, None, "by KSM")

        #---------------------------------------------------------------------------------------------------------------
        # meta info
        #---------------------------------------------------------------------------------------------------------------
        meta_only = request.args.get('meta_only')
        order_by = request.args.get('item')
        order_flag = request.args.get('order')
        order = 'ASC'
        if order_flag == '-':
            order = 'DESC'
        #---------------------------------------------------------------------------------------------------------------
        # object info
        #---------------------------------------------------------------------------------------------------------------
        enabled = request.args.get('enabled')
        parm_alive = request.args.get('alive')
        parm_status = request.args.get('status')
        parm_group_status = request.args.get('group_status')
        parm_si_company = request.args.get('si_company')
        parm_organization = request.args.get('organization')
        parm_site = request.args.get('site')
        parm_location = request.args.get('location')
        parm_shop = request.args.get('shop')
        parm_media_player_serial_number = request.args.get('mp_serial_number')
        parm_media_player_alias = request.args.get('mp_alias')
        parm_display_alias = request.args.get('display_alias')
        parm_created_date = request.args.get('created_date')
        parm_resolved_date = request.args.get('resolved_date')
        parm_activation_date = request.args.get('activation_date')
        limit = request.args.get('limit') and int(request.args.get('limit')) or 20
        next = None
        offset = request.args.get('offset') and int(request.args.get('offset')) or 0
        previous = None
        total_count = 0

        # [ Case by ] Meta Only
        if meta_only != None:
            mp_list = MediaPlayers.query.add_columns(MediaPlayers.group_status, db.func.count(MediaPlayers.group_status).label("count")).\
                filter(or_(*self.filter_group)).group_by(MediaPlayers.group_status).all()

            meta = dict()
            meta["normal_count"] = 0
            meta["check_required_count"] = 0
            meta["unresolved_count"] = 0
            meta["disconnected_count"] = 0
            meta["total_count"] = 0
            meta["total_display_count"] = 0

            for mp in mp_list:
                if mp.group_status is 0:
                    meta["normal_count"] = mp.count
                elif mp.group_status is 1:
                    meta["check_required_count"] = mp.count
                elif mp.group_status is 2:
                    meta["unresolved_count"] = mp.count
                elif mp.group_status is -1:
                    meta["disconnected_count"] = mp.count

            meta["total_count"] = meta["normal_count"] + meta["check_required_count"] + meta["unresolved_count"] + meta["disconnected_count"]
            yesterday = datetime.now() - timedelta(days=1)
            meta["today_activation_count"] = MediaPlayers.query.filter(MediaPlayers.created_date>=yesterday.strftime("%Y-%m-%d")).filter(or_(*self.filter_group)).count()

            three_days_ago = datetime.now() - timedelta(days=3)
            meta["recent_resolved_count"] = MediaPlayers.query.filter(MediaPlayers.resolved_date>=three_days_ago.strftime("%Y-%m-%d")).filter(or_(*self.filter_group)).count()

            meta["total_display_count"] = MediaPlayers.query.join(Displays, MediaPlayers.id == Displays.FK_mp_id).filter(or_(*self.filter_group)).count()
            return result(200, "GET the Media Player Meta only", None, meta, "by KSM")

        # [ Case by ] Meta & Objects
        else:
            filter_and_group = []
            if enabled != None:
                filter_and_group.append(MediaPlayers.enabled == enabled)
            else:
                filter_and_group.append(MediaPlayers.enabled == 1)

            parm_count = 0
            if parm_status != None:
                filter_and_group.append(MediaPlayers.status == str(parm_status))
                parm_count = parm_count + 1

            if parm_alive != None:
                filter_and_group.append(MediaPlayers.alive == str(parm_alive))
                parm_count = parm_count + 1

            if parm_site != None:
                filter_and_group.append(MediaPlayers.site == str(parm_site))
                parm_count = parm_count + 1

            if parm_location != None:
                filter_and_group.append(MediaPlayers.location == str(parm_location))
                parm_count = parm_count + 1

            if parm_shop != None:
                filter_and_group.append(MediaPlayers.shop == str(parm_shop))
                parm_count = parm_count + 1

            if parm_media_player_alias != None:
                filter_and_group.append(MediaPlayers.alias == str(parm_media_player_alias))
                parm_count = parm_count + 1

            if parm_media_player_serial_number != None:
                filter_and_group.append(MediaPlayers.serial_number == str(parm_media_player_serial_number))
                parm_count = parm_count + 1

            # why?
            #if parm_media_player_alias != None and parm_media_player_alias != '':
            #    filter_and_group.append(MediaPlayers.alias >= parm_media_player_alias)
            #    parm_count = parm_count + 1

            if parm_created_date != None and parm_created_date != '':
                filter_and_group.append(MediaPlayers.created_date >= str(parm_created_date))
                parm_count = parm_count + 1
            if parm_resolved_date != None and parm_resolved_date != '':
                three_days_ago = datetime.now() - timedelta(days=3)
                filter_and_group.append(MediaPlayers.resolved_date>=three_days_ago.strftime("%Y-%m-%d"))
                parm_count = parm_count + 1
            if parm_activation_date != None and parm_activation_date != '':
                yesterday = datetime.now() - timedelta(days=1)
                filter_and_group.append(MediaPlayers.created_date >= yesterday.strftime("%Y-%m-%d"))
                parm_count = parm_count + 1

            list_dp_alias = []
            if parm_display_alias != None and parm_display_alias != '':
                search_dp_result = Displays.query.filter_by(alias=parm_display_alias).all()
                for search_dp in search_dp_result:
                    list_dp_alias.append(search_dp.FK_mp_id)

                filter_and_group.append(MediaPlayers.id.in_(list_dp_alias))
                parm_count = parm_count + 1

            list_org = []
            if parm_organization != None and parm_organization != '':
                search_dp_result = Displays.query.filter_by(organization=parm_organization).all()
                for search_dp in search_dp_result:
                    list_org.append(search_dp.FK_mp_id)

                filter_and_group.append(MediaPlayers.id.in_(list_org))
                parm_count = parm_count + 1


            list_si_company = []
            if parm_si_company != None and parm_si_company != '':
                search_dp_result = Displays.query.filter_by(si_company=parm_si_company).all()
                for search_dp in search_dp_result:
                    list_si_company.append(search_dp.FK_mp_id)

                filter_and_group.append(MediaPlayers.id.in_(list_si_company))
                parm_count = parm_count + 1

        # TODO : have to be modified
        filter_or_group = list()

        if parm_group_status != None:
            if str(parm_group_status) != "all":
                filter_and_group.append(MediaPlayers.group_status == str(parm_group_status))
        elif parm_count is 0:
            filter_or_group.append(MediaPlayers.group_status == 0)
            filter_or_group.append(MediaPlayers.group_status == 1)
            filter_or_group.append(MediaPlayers.group_status == 2)


        mp_results = MediaPlayers.query.filter(and_(*filter_and_group)).filter(or_(*self.filter_group)).filter(or_(*filter_or_group))
        total_count = mp_results.count()

        mp_results = MediaPlayers.query.filter(and_(*filter_and_group)).filter(or_(*self.filter_group)).filter(or_(*filter_or_group)).order_by(order_by + ' ' + order).limit(limit).offset(offset)
        current_count = mp_results.count()

        # Creating a Pagenation
        if offset == 0:
            previous = None
            if current_count < total_count:
                next_offset = offset+limit
                next = "/api/v1/media-players/?limit="+str(limit)+"&offset="+str(next_offset)
            elif current_count == total_count:
                next = None
            else:
                return result(500, "Media Player Count Error", None, None, "by KSM")
        elif offset != 0:
            previous_offset = offset-limit
            previous = "/api/v1/media-players/?limit="+str(limit)+"&offset="+str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next_offset = offset+limit
                next = "/api/v1/media-players/?limit="+str(limit)+"&offset="+str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        objects = []

        for search_mp in mp_results:
            dp_results = Displays.query.filter_by(FK_mp_id=search_mp.id).all()
            dp_alias_array = []
            for dp in dp_results:
                dp_alias_array.append(
                    str(dp.alias)
                )

            objects.append({
                "id": search_mp.id,
                "alive": search_mp.alive,
                "activation_date": json_encoder(search_mp.activation_date),
                "app_cpu1": search_mp.app_cpu1,
                "app_cpu2": search_mp.app_cpu2,
                "app_cpu3": search_mp.app_cpu3,
                "app_cpu4": search_mp.app_cpu4,
                "app_memory1": search_mp.app_memory1,
                "app_memory2": search_mp.app_memory2,
                "app_memory3": search_mp.app_memory3,
                "app_memory4": search_mp.app_memory4,
                "app_path1": search_mp.app_path1,
                "app_path2": search_mp.app_path2,
                "app_path3": search_mp.app_path3,
                "app_path4": search_mp.app_path4,
                "app_status1": search_mp.app_status1,
                "app_status2": search_mp.app_status2,
                "app_status3": search_mp.app_status3,
                "app_status4": search_mp.app_status4,
                "cpu_temp": search_mp.cpu_temp,
                "cpu_usage": search_mp.cpu_usage,
                "created": json_encoder(search_mp.created_date),
                "description": search_mp.description,
                "dns": search_mp.dns,
                "enabled": search_mp.enabled,
                "expiration_date": json_encoder(search_mp.expiration_date),
                "fan_speed": search_mp.fan_speed,
                "firmware_version": search_mp.firmware_version,
                "s_bytes": str(search_mp.s_bytes),
                "r_bytes": str(search_mp.r_bytes),
                "gateway": search_mp.gateway,
                "hdd_temp": search_mp.hdd_temp,
                "hdd_usage": search_mp.hdd_usage,
                "ip": search_mp.ip,
                "mac": search_mp.mac,
                "media_player_alias": search_mp.alias,
                "mem_usage": search_mp.mem_usage,
                "model": search_mp.model,
                "netmask": search_mp.netmask,
                "os": search_mp.os,
                "resolved_date": json_encoder(search_mp.resolved_date),
                "running_time": search_mp.running_time,
                "serial_number": search_mp.serial_number,
                "manage_serial_number": search_mp.manage_serial_number,
                "service_offer": search_mp.service_offer,
                "status": search_mp.status,
                "group_status": search_mp.group_status,
                "status_cpu_usage": search_mp.status_cpu_usage,
                "status_mem_usage": search_mp.status_mem_usage,
                "status_hdd_usage": search_mp.status_hdd_usage,
                "status_cpu_temp": search_mp.status_cpu_temp,
                "status_hdd_temp": search_mp.status_hdd_temp,
                "status_fan_speed": search_mp.status_fan_speed,
                "status_res_time": search_mp.status_res_time,
                "status_app1": search_mp.status_app1,
                "status_app2": search_mp.status_app2,
                "status_app3": search_mp.status_app3,
                "status_app4": search_mp.status_app4,
                "system_time": json_encoder(search_mp.system_time),
                "system_time_zone": search_mp.system_time_zone,
                "polling_time": search_mp.polling_time,
                "response_time": search_mp.response_time,
                "warrent_end_date": json_encoder(search_mp.warrent_end_date),
                "warrent_start_date": json_encoder(search_mp.warrent_start_date),
                "site": search_mp.site,
                "location": search_mp.location,
                "shop": search_mp.shop,
                "display_aliases": dp_alias_array
            })

        if meta_only == '1':
            return result(200, "GET the Media Player Meta only", None, meta, "by KSM")
        else:
            return result(200, "GET the Media Player List", objects, meta, "by KSM")

class MediaPlayerAPI(Resource):
    """
    [ Media Player ]
    @ GET : Returns one of the MediaPlayer information. - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------

        super(MediaPlayerAPI, self).__init__()

    def get(self, serial_number):
        # [ Check ] serial_number
        mp = MediaPlayers.query.filter_by(serial_number=serial_number).first()

        # [ Case by ] Media Player GET
        if mp is not None:
            objects = []

            dp_results = Displays.query.filter_by(FK_mp_id=mp.id).group_by(Displays.dp_group).all()
            dp_alias_objs = []
            if dp_results is not None:
                for search_dp in dp_results:
                    dp_alias_objs.append({
                        "id": search_dp.id,
                        "serial_number": search_dp.serial_number,
                        "alias": search_dp.alias
                    })

            dp_results = Displays.query.add_columns(Displays.dp_group, Displays.serial_number, db.func.count(Displays.dp_group).label("count")).\
                            filter(Displays.FK_mp_id == mp.id).filter(db.func.length(Displays.dp_group) > 0).group_by(Displays.dp_group).all()

            dp_group_objs = []
            if dp_results is not None:
                for search_dp in dp_results:
                    if search_dp.count > 0:
                        search_dps_by_dpg_list = Displays.query.filter_by(dp_group=search_dp.dp_group).all()
                        search_dps_by_dpg_obj = []
                        if len(search_dps_by_dpg_list) != 0:
                            for search_dps in search_dps_by_dpg_list:
                                search_dps_by_dpg_obj.append({
                                    "id": search_dps.id,
                                    "serial_number": search_dps.serial_number
                                })

                        dp_group_objs.append({
                            "name": search_dp.dp_group,
                            "count": search_dp.count,
                            "dp_serial_number": search_dp.serial_number,
                            "search_dps_by_dpg_obj": search_dps_by_dpg_obj
                        })

            objects.append({
                "id": mp.id,
                "alive": mp.alive,
                "svr_conn": mp.svr_conn,
                "model": mp.model,
                "alias": mp.alias,
                "serial_number": mp.serial_number,
                "manage_serial_number": mp.manage_serial_number,
                "manufact": mp.manufact,
                "version": mp.version,
                "uuid": mp.uuid,
                "wakeup": mp.wakeup,
                "sku": mp.sku,
                "family": mp.family,
                "dp_aliases": dp_alias_objs,
                "enabled": mp.enabled,
                "service_offer": mp.service_offer,
                "warrent_start_date": json_encoder(mp.warrent_start_date),
                "warrent_end_date": json_encoder(mp.warrent_end_date),
                "created_date": json_encoder(mp.created_date),
                "resolved_date": json_encoder(mp.resolved_date),
                "activation_date": json_encoder(mp.activation_date),
                "expiration_date": json_encoder(mp.expiration_date),
                "cpu_temp": str(mp.cpu_temp),
                "cpu_usage": str(mp.cpu_usage),
                "hdd_temp": str(mp.hdd_temp),
                "hdd_usage": str(mp.hdd_usage),
                "mem_usage": str(mp.mem_usage),
                "dns": mp.dns,
                "fan_speed": str(mp.fan_speed),
                "firmware_version": mp.firmware_version,
                "agent_version": mp.agent_version,
                "r_bytes": str(mp.r_bytes),
                "s_bytes": str(mp.s_bytes),
                "gateway": mp.gateway,
                "ip": mp.ip,
                "mac": mp.mac,
                "netmask": mp.netmask,
                "os": mp.os,
                "processor": mp.processor,
                "bios": mp.bios,
                "physical_memory": mp.physical_memory,
                "physical_hdd": mp.physical_hdd,
                "system_time": json_encoder(mp.system_time),
                "system_time_zone": mp.system_time_zone,
                "running_time": json_encoder(mp.running_time),
                "polling_time": mp.polling_time,
                "response_time": mp.response_time,
                "app_path1": mp.app_path1,
                "app_path2": mp.app_path2,
                "app_path3": mp.app_path3,
                "app_path4": mp.app_path4,
                "app_status1": mp.app_status1,
                "app_status2": mp.app_status2,
                "app_status3": mp.app_status3,
                "app_status4": mp.app_status4,
                "app_cpu1": str(mp.app_cpu1),
                "app_cpu2": str(mp.app_cpu2),
                "app_cpu3": str(mp.app_cpu3),
                "app_cpu4": str(mp.app_cpu4),
                "app_memory1": str(mp.app_memory1),
                "app_memory2": str(mp.app_memory2),
                "app_memory3": str(mp.app_memory3),
                "app_memory4": str(mp.app_memory4),
                "description": mp.description,
                "status": mp.status,
                "group_status": mp.group_status,
                "status_cpu_usage": mp.status_cpu_usage,
                "status_mem_usage": mp.status_mem_usage,
                "status_hdd_usage": mp.status_hdd_usage,
                "status_cpu_temp": mp.status_cpu_temp,
                "status_hdd_temp": mp.status_hdd_temp,
                "status_fan_speed": mp.status_fan_speed,
                "status_res_time": mp.status_res_time,
                "status_app1": mp.status_app1,
                "status_app2": mp.status_app2,
                "status_app3": mp.status_app3,
                "status_app4": mp.status_app4,
                "site": mp.site,
                "location": mp.location,
                "shop": mp.shop,
                "dp_group_objs": dp_group_objs,
                "FK_managers_id": mp.FK_managers_id,
                "disconnected_date": json_encoder(mp.disconnected_date),
                "FK_device_groups_id": mp.FK_device_groups_id
            })

            return result(200, "GET the MediaPlayer", objects, None, "by KSM")
        # [ Case by ] MediaPlayer does not exist
        else:
            return result(400, "MediaPlayer does not exist", None, None, "by KSM")

class MediaPlayersTop10API(Resource):
    """
    [ Media Players Top 10 ]
    @ GET : Returns the top 10 information sorted by the item.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(MediaPlayersTop10API, self).__init__()

    def get(self):
        if self.isValidToken == False or self.userType == USER_TYPE_GUEST:
            return result(401, "You do not have permission.", None, None, "by KSM")
        item = request.args.get('item')
        order_by_flag = request.args.get('order')
        order_by = 'ASC'
        if order_by_flag == '-':
            order_by = 'DESC'

        mp_results = MediaPlayers.query.filter(MediaPlayers.alive==1).filter(or_(*self.filter_group)).order_by(item + " " + order_by).limit(10)

        objects = []
        for mp in mp_results:
            objects.append({
                "key": mp.id,
                "mp_alias": mp.alias,
                "cpu_usage": mp.cpu_usage,
                "mem_usage": mp.mem_usage,
                "cpu_temp": mp.cpu_temp,
                "serial_number": mp.serial_number,
                "manage_serial_number": mp.manage_serial_number,
                "status_cpu_usage": mp.status_cpu_usage,
                "status_mem_usage": mp.status_mem_usage,
                "status_cpu_temp": mp.status_cpu_temp
            })

        # [ Case by ] length != 10
        if len(objects) != 10:
            limit_range = 10 - len(objects)
            for i in range(0, limit_range):
                objects.append({
                    "key": 'vertual_key' + str(i),
                    "mp_alias": '',
                    "cpu_temp": -1,
                    "cpu_usage": -1,
                    "mem_usage": -1
                })

        return result(200, "GET the Top 1api/v1/media-players 0 MediaPlayer information", objects, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                               Displays Controller(ZK), Displays Revise(DB) Area
#----------------------------------------------------------------------------------------------------------------------
class DisplaysControllerAPI(Resource):
    """
    [ Display Controller ]
    - Used to give a URI to the daemon. To zookeeper
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("type", type=str, location="json")
        self.parser.add_argument("command", type=str, location="json")
        self.parser.add_argument("data", type=str, location="json")
        self.parser.add_argument("log", type=str, location="json")

        super(DisplaysControllerAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()
        log_text = input['log']
        del input['log']

        # [ Check ] serial_number
        dp = Displays.query.filter_by(serial_number=serial_number).first()

        if dp is not None:
            # [ Parse ] command
            set_ID = str(dp.number).zfill(2)
            temp = input['command'].split('_')
            if temp[0] == 'tag':
                column_name = input['command']
                query = "SELECT id, %(column)s FROM %(table_name)s WHERE id='%(id)s'" % {
                    "table_name": Display_Models.__tablename__,
                    "id": dp.FK_models_id,
                    "column": column_name
                }
                parse_command = Display_Models.query.from_statement(query).first()
                # [ Combine ] command + display_index
                # - by KSM
                input['command'] = str(parse_command.__dict__[column_name]) + " " + set_ID
                if parse_command.__dict__[column_name] == '' or parse_command.__dict__[column_name] == None:
                    return result(500, "The command does not exist", None, None, "by KSM")

                # [ Check ] Second Command
                if input['data'].find('.') > -1:
                    split_line = input['data'].split('.')
                    command_data = split_line[0]
                    SEC_line = split_line[1]
                    temp = SEC_line.split('_')
                    if temp[0] == 'tag':
                        SEC = SEC_line.split('|')
                        SEC_command = SEC[0]
                        SEC_data = SEC[1]
                        query = "SELECT id, %(column)s FROM %(table_name)s WHERE id='%(id)s'" % {
                            "table_name": Display_Models.__tablename__,
                            "id": dp.FK_models_id,
                            "column": SEC_command
                        }
                        parse_command = Display_Models.query.from_statement(query).first()
                        parse_SEC_command = str(parse_command.__dict__[SEC_command]) + " " + set_ID
                        input['data'] = command_data + '.' + parse_SEC_command + ' ' + SEC_data

            req_url = DAEMON_ADDRESS + '/controller/%s' % dp.mp_serial_number
            data = json.dumps(input)
            r = requests.put(req_url, data=data, headers=DAEMON_HEADERS)

            sh = Support_History()

            # [ TIME ] UTC
            now = time.gmtime()
            current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
            sh.action_date = current_time

            sh.userID = self.userID
            sh.device = dp.number

            if input["type"] == "display_control":
                sh.action_type = "Display Control"
                sh.action_command = log_text
            elif input["type"] == "remote_control":
                sh.action_type = "Remote Control"
                sh.action_command = log_text
            elif input["type"] == "application_control":
                sh.action_type = "Application Control"
                sh.action_command = input["command"] + " " + input["data"]
            elif input["type"] == "check_control":
                sh.action_type = "Check Control"
                sh.action_command = log_text
            elif input["type"] == "agent_control":
                sh.action_type = "Agent Control"
                sh.action_command = log_text
            elif input["type"] == "mediaplayer_control":
                sh.action_type = "OS Restart Control"
                sh.action_command = log_text
            else:
                sh.action_type = "Unknown"

            sh.mp_serial_number = dp.mp_serial_number

            db.session.add(sh)
            db.session.commit()

            return result(200, "Request to Daemon", None, None, "by rainmaker")
        else:
            return result(400, "No valid serial number", None, None, "by rainmaker")

class DisplaysReviseAPI(Resource):
    """
    [ Display Revise ]
    - Display column is modified. To DB
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("model", type=str, location="json")
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("activation_date", type=str, location="json")
        self.parser.add_argument("expiration_date", type=str, location="json")
        self.parser.add_argument("warrent_start_date", type=str, location="json")
        self.parser.add_argument("warrent_end_date", type=str, location="json")
        self.parser.add_argument("service_offer", type=str, location="json")

        self.parser.add_argument("si_company", type=str, location="json")
        self.parser.add_argument("se_name", type=str, location="json")
        self.parser.add_argument("se_email", type=str, location="json")
        self.parser.add_argument("se_mobile", type=str, location="json")

        self.parser.add_argument("display_timer_flag", type=bool, location="json")
        self.parser.add_argument("timer_onoff_sun", type=bool, location="json")
        self.parser.add_argument("timer_begin_sun", type=str, location="json")
        self.parser.add_argument("timer_end_sun", type=str, location="json")
        self.parser.add_argument("timer_onoff_mon", type=bool, location="json")
        self.parser.add_argument("timer_begin_mon", type=str, location="json")
        self.parser.add_argument("timer_end_mon", type=str, location="json")
        self.parser.add_argument("timer_onoff_tue", type=bool, location="json")
        self.parser.add_argument("timer_begin_tue", type=str, location="json")
        self.parser.add_argument("timer_end_tue", type=str, location="json")
        self.parser.add_argument("timer_onoff_wed", type=bool, location="json")
        self.parser.add_argument("timer_begin_wed", type=str, location="json")
        self.parser.add_argument("timer_end_wed", type=str, location="json")
        self.parser.add_argument("timer_onoff_thu", type=bool, location="json")
        self.parser.add_argument("timer_begin_thu", type=str, location="json")
        self.parser.add_argument("timer_end_thu", type=str, location="json")
        self.parser.add_argument("timer_onoff_fri", type=bool, location="json")
        self.parser.add_argument("timer_begin_fri", type=str, location="json")
        self.parser.add_argument("timer_end_fri", type=str, location="json")
        self.parser.add_argument("timer_onoff_sat", type=bool, location="json")
        self.parser.add_argument("timer_begin_sat", type=str, location="json")
        self.parser.add_argument("timer_end_sat", type=str, location="json")

        super(DisplaysReviseAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()

        # [ Create ] check
        check = []
        for key in input:
            check.append(key)

        # [ Check ] property Null remove
        for i in range(len(check)):
            if input[check[i]] is None:
                del input[check[i]]

        # [ Check ] serial number
        dp = Displays.query.filter_by(serial_number=serial_number).first()

        # [ Case by ] Display update
        if dp is not None:
            db.session.query(Displays).filter_by(serial_number=serial_number).update(input)
            db.session.commit()
            return result(200, "Display update successful", None, None, "by KSM")

        # [ Case by ] Display does not exist
        else:
            return result(400, "Display does not exist", None, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Display Area
#----------------------------------------------------------------------------------------------------------------------
class DisplayListAPI(Resource):
    """
    [ Displays List]
    @ GET : Returns the full Display.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("se_email", type=str, location="json")
        self.parser.add_argument("memo", type=str, location="json")
        self.parser.add_argument("activation_date", type=str, location="json")
        self.parser.add_argument("serial_number", type=str, location="json")
        self.parser.add_argument("se_name", type=str, location="json")
        self.parser.add_argument("firmware_version", type=str, location="json")
        self.parser.add_argument("port_number", type=str, location="json")
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("si_company", type=str, location="json")
        self.parser.add_argument("se_mobile", type=str, location="json")
        self.parser.add_argument("display_index", type=str, location="json")
        self.parser.add_argument("media_player", type=str, location="json")
        self.parser.add_argument("expire_date", type=str, location="json")
        self.parser.add_argument("organization", type=str, location="json")
        self.parser.add_argument("model", type=str, location="json")

        super(DisplayListAPI, self).__init__()

    def get(self):
        mp_serial_number = request.args.get('mp_serial_number')
        dp_group_name = request.args.get('dp_group_name')

        # [ Check ] parents MediaPlayer
        mp = MediaPlayers.query.filter_by(serial_number=mp_serial_number).first()
        if mp is None:
            return result(500, "Can not find the parents MediaPlayer", None, None, "by KSM")

        if dp_group_name is None:
            filter_or_group = list()
            filter_or_group.append(Displays.dp_group==None)
            filter_or_group.append(Displays.dp_group=='')
            dp_results = Displays.query.filter_by(FK_mp_id=mp.id).filter(or_(*filter_or_group)).order_by("number").all()
        else:
            if dp_group_name == '_ALL_DATA_':
                dp_results = Displays.query.filter_by(FK_mp_id=mp.id).order_by("number").all()
            else:
                dp_results = Displays.query.filter_by(FK_mp_id=mp.id, dp_group=dp_group_name).order_by("number").all()

        objects = []
        for dp in dp_results:
            val_parseOnTimer = timer_parser(dp.val_OnTimer)
            val_parseOffTimer = timer_parser(dp.val_OffTimer)
            val_parseOnTimerInput = timer_parser(dp.val_OnTimerInput)
            val_parseTileModeCheck = tile_parser(dp.val_TileModeCheck)

            objects.append({
                "id": dp.id,
                "activation_date": json_encoder(dp.activation_date),
                "created_date": json_encoder(dp.created_date),
                "alias": str(dp.alias),
                "display_number": dp.number,
                "set_id": dp.set_id,
                "dp_group": dp.dp_group,
                "port_number": dp.port_number,
                "expiration_date": json_encoder(dp.expiration_date),
                "firmware_version": str(dp.firmware_version),
                "memo": str(dp.memo),
                "alive": dp.alive,
                "model": str(dp.model),
                "mp_serial_number": dp.mp_serial_number,
                "organization": dp.organization,
                "se_email": dp.se_email,
                "se_mobile": dp.se_mobile,
                "se_name": dp.se_name,
                "serial_number": dp.serial_number,
                "service_offer": dp.service_offer,
                "si_company": dp.si_company,
                "status": dp.status,
                "status_temp": dp.status_temp,
                "val_AspectRatio": dp.val_AspectRatio,
                "val_AutoOff": dp.val_AutoOff,
                "val_AutoStandby": dp.val_AutoStandby,
                "val_AutoVolume": dp.val_AutoVolume,
                "val_Backlight": dp.val_Backlight,
                "val_Balance": dp.val_Balance,
                "val_Bass": dp.val_Bass,
                "val_BlueGain": dp.val_BlueGain,
                "val_BlueOffset": dp.val_BlueOffset,
                "val_Brightness": dp.val_Brightness,
                "val_Color": dp.val_Color,
                "val_ColorTemp": dp.val_ColorTemp,
                "val_Constrast": dp.val_Constrast,
                "val_Date": dp.val_Date,
                "val_EnergySaving": dp.val_EnergySaving,
                "val_Fan": dp.val_Fan,
                "val_FirmwareVer": dp.val_FirmwareVer,
                "val_GreenGain": dp.val_GreenGain,
                "val_GreenOffset": dp.val_GreenOffset,
                "val_InputSelect": dp.val_InputSelect,
                "val_Lamp": dp.val_Lamp,
                "val_Lang": dp.val_Lang,
                "val_Remote": dp.val_Remote,
                "val_PowerOnDelay": dp.val_PowerOnDelay,
                "val_TileModeCheck": dp.val_TileModeCheck,
                "val_TileID": dp.val_TileID,
                "val_TileNaturalMode": dp.val_TileNaturalMode,
                "val_SizeH": dp.val_SizeH,
                "val_SizeV": dp.val_SizeV,
                "val_MonitorStatus": dp.val_MonitorStatus,
                "val_OffTimer": val_parseOffTimer,
                "val_OnTimer": val_parseOnTimer,
                "val_OnTimerInput": val_parseOnTimerInput,
                "val_PictureMode": dp.val_PictureMode,
                "val_PositionH": dp.val_PositionH,
                "val_PositionV": dp.val_PositionV,
                "val_PowerOnDelay": dp.val_PowerOnDelay,
                "val_PowerStatus": dp.val_PowerStatus,
                "val_RedGain": dp.val_RedGain,
                "val_RedOffset": dp.val_RedOffset,
                "val_Remote": dp.val_Remote,
                "val_SerialNo": dp.val_SerialNo,
                "val_Sharpness": dp.val_Sharpness,
                "val_Signal": dp.val_Signal,
                "val_SizeH": dp.val_SizeH,
                "val_SizeV": dp.val_SizeV,
                "val_SleepTime": dp.val_SleepTime,
                "val_SoundMode": dp.val_SoundMode,
                "val_Speaker": dp.val_Speaker,
                "val_Temp": dp.val_Temp,
                "val_TileID": dp.val_Temp,
                "val_TileModeCheck": val_parseTileModeCheck,
                "val_TileNaturalMode": dp.val_TileNaturalMode,
                "val_Time": dp.val_Time,
                "val_Tint": dp.val_Tint,
                "val_Treble": dp.val_Treble,
                "val_VolumControl": dp.val_VolumControl,
                "val_VolumMute": dp.val_VolumMute,
                "val_dpmSelect": dp.val_dpmSelect,
                "val_ismMode": dp.val_ismMode,
                "val_osdSelect": dp.val_osdSelect,
                "warrent_end_date": json_encoder(dp.warrent_end_date),
                "warrent_start_date": json_encoder(dp.warrent_start_date),
                "dp_timer_flag": dp.display_timer_flag,
                "timer_onoff_sun": dp.timer_onoff_sun,
                "timer_begin_sun": json_encoder(dp.timer_begin_sun),
                "timer_end_sun": json_encoder(dp.timer_end_sun),
                "timer_onoff_mon": dp.timer_onoff_mon,
                "timer_begin_mon": json_encoder(dp.timer_begin_mon),
                "timer_end_mon": json_encoder(dp.timer_end_mon),
                "timer_onoff_tue": dp.timer_onoff_tue,
                "timer_begin_tue": json_encoder(dp.timer_begin_tue),
                "timer_end_tue": json_encoder(dp.timer_end_tue),
                "timer_onoff_wed": dp.timer_onoff_wed,
                "timer_begin_wed": json_encoder(dp.timer_begin_wed),
                "timer_end_wed": json_encoder(dp.timer_end_wed),
                "timer_onoff_thu": dp.timer_onoff_thu,
                "timer_begin_thu": json_encoder(dp.timer_begin_thu),
                "timer_end_thu": json_encoder(dp.timer_end_thu),
                "timer_onoff_fri": dp.timer_onoff_fri,
                "timer_begin_fri": json_encoder(dp.timer_begin_fri),
                "timer_end_fri": json_encoder(dp.timer_end_fri),
                "timer_onoff_sat": dp.timer_onoff_sat,
                "timer_begin_sat": json_encoder(dp.timer_begin_sat),
                "timer_end_sat": json_encoder(dp.timer_end_sat)
            })
        return result(200, "Display List GET", objects, None, "by KSM")

class DisplayAPI(Resource):
    """
    [ Display ]
    @ GET : Returns one of the Display information. - by KSM
    """

    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------

        super(DisplayAPI, self).__init__()

    def get(self, serial_number):
        # [ Check ] serial_number
        dp = Displays.query.filter_by(serial_number=serial_number).first()

        # [ Case by ] Display Get
        if dp is not None:
            objects = []

            val_parseOnTimer = timer_parser(dp.val_OnTimer)
            val_parseOffTimer = timer_parser(dp.val_OffTimer)
            val_parseOnTimerInput = timer_parser(dp.val_OnTimerInput)
            val_parseTileModeCheck = tile_parser(dp.val_TileModeCheck)

            objects.append({
                "id": dp.id,
                "activation_date": json_encoder(dp.activation_date),
                "created_date": json_encoder(dp.created_date),
                "alias": str(dp.alias),
                "display_number": dp.number,
                "port_number": dp.port_number,
                "expiration_date": json_encoder(dp.expiration_date),
                "firmware_version": str(dp.firmware_version),
                "memo": str(dp.memo),
                "alive": dp.alive,
                "model": str(dp.model),
                "mp_serial_number": dp.mp_serial_number,
                "organization": dp.organization,
                "se_email": dp.se_email,
                "se_mobile": dp.se_mobile,
                "se_name": dp.se_name,
                "serial_number": dp.serial_number,
                "service_offer": dp.service_offer,
                "si_company": dp.si_company,
                "status": dp.status,
                "status_temp": dp.status_temp,
                "val_AspectRatio": dp.val_AspectRatio,
                "val_AutoOff": dp.val_AutoOff,
                "val_AutoStandby": dp.val_AutoStandby,
                "val_AutoVolume": dp.val_AutoVolume,
                "val_Backlight": dp.val_Backlight,
                "val_Balance": dp.val_Balance,
                "val_Bass": dp.val_Bass,
                "val_BlueGain": dp.val_BlueGain,
                "val_BlueOffset": dp.val_BlueOffset,
                "val_Brightness": dp.val_Brightness,
                "val_Color": dp.val_Color,
                "val_ColorTemp": dp.val_ColorTemp,
                "val_Constrast": dp.val_Constrast,
                "val_Date": dp.val_Date,
                "val_EnergySaving": dp.val_EnergySaving,
                "val_Fan": dp.val_Fan,
                "val_FirmwareVer": dp.val_FirmwareVer,
                "val_GreenGain": dp.val_GreenGain,
                "val_GreenOffset": dp.val_GreenOffset,
                "val_InputSelect": dp.val_InputSelect,
                "val_Lamp": dp.val_Lamp,
                "val_Lang": dp.val_Lang,
                "val_MonitorStatus": dp.val_MonitorStatus,
                "val_OffTimer": val_parseOffTimer,
                "val_OnTimer": val_parseOnTimer,
                "val_OnTimerInput": val_parseOnTimerInput,
                "val_PictureMode": dp.val_PictureMode,
                "val_PositionH": dp.val_PositionH,
                "val_PositionV": dp.val_PositionV,
                "val_PowerOnDelay": dp.val_PowerOnDelay,
                "val_PowerStatus": dp.val_PowerStatus,
                "val_RedGain": dp.val_RedGain,
                "val_RedOffset": dp.val_RedOffset,
                "val_Remote": dp.val_Remote,
                "val_SerialNo": dp.val_SerialNo,
                "val_Sharpness": dp.val_Sharpness,
                "val_Signal": dp.val_Signal,
                "val_SizeH": dp.val_SizeH,
                "val_SizeV": dp.val_SizeV,
                "val_SleepTime": dp.val_SleepTime,
                "val_SoundMode": dp.val_SoundMode,
                "val_Speaker": dp.val_Speaker,
                "val_Temp": dp.val_Temp,
                "val_TileID": dp.val_Temp,
                "val_TileModeCheck": dp.val_TileModeCheck,
                "val_TileNaturalMode": dp.val_TileNaturalMode,
                "val_Time": dp.val_Time,
                "val_Tint": dp.val_Tint,
                "val_Treble": dp.val_Treble,
                "val_VolumControl": dp.val_VolumControl,
                "val_VolumMute": dp.val_VolumMute,
                "val_dpmSelect": dp.val_dpmSelect,
                "val_ismMode": dp.val_ismMode,
                "val_osdSelect": dp.val_osdSelect,
                "warrent_end_date": json_encoder(dp.warrent_end_date),
                "warrent_start_date": json_encoder(dp.warrent_start_date),
                "dp_timer_flag": dp.display_timer_flag,
                "timer_onoff_sun": dp.timer_onoff_sun,
                "timer_begin_sun": json_encoder(dp.timer_begin_sun),
                "timer_end_sun": json_encoder(dp.timer_end_sun),
                "timer_onoff_mon": dp.timer_onoff_mon,
                "timer_begin_mon": json_encoder(dp.timer_begin_mon),
                "timer_end_mon": json_encoder(dp.timer_end_mon),
                "timer_onoff_tue": dp.timer_onoff_tue,
                "timer_begin_tue": json_encoder(dp.timer_begin_tue),
                "timer_end_tue": json_encoder(dp.timer_end_tue),
                "timer_onoff_wed": dp.timer_onoff_wed,
                "timer_begin_wed": json_encoder(dp.timer_begin_wed),
                "timer_end_wed": json_encoder(dp.timer_end_wed),
                "timer_onoff_thu": dp.timer_onoff_thu,
                "timer_begin_thu": json_encoder(dp.timer_begin_thu),
                "timer_end_thu": json_encoder(dp.timer_end_thu),
                "timer_onoff_fri": dp.timer_onoff_fri,
                "timer_begin_fri": json_encoder(dp.timer_begin_fri),
                "timer_end_fri": json_encoder(dp.timer_end_fri),
                "timer_onoff_sat": dp.timer_onoff_sat,
                "timer_begin_sat": json_encoder(dp.timer_begin_sat),
                "timer_end_sat": json_encoder(dp.timer_end_sat)
            })

            return result(200, "GET the Display", objects, None, "by KSM")
        # [ Case by ] Display does not exist
        else:
            return result(400, "Display does not exist", None, None, "by KSM")

class DisplaysTop10API(Resource):
    """
    [ Displays Top 10 ]
    @ GET : Returns the top 10 information sorted by the item.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(DisplaysTop10API, self).__init__()

    def get(self):
        if self.isValidToken == False or self.userType == USER_TYPE_GUEST:
            return result(401, "You do not have permission.", None, None, "by KSM")
        item = request.args.get('item')
        order_by_flag = request.args.get('order')
        order_by = 'DESC'

        if order_by_flag != '-':
            order_by = 'ASC'

        dp_results = Displays.query.join(MediaPlayers, Displays.mp_serial_number==MediaPlayers.serial_number).filter(MediaPlayers.alive==1).filter(or_(*self.filter_group)).order_by(item + " " + order_by).limit(10)

        objects = []
        for dp in dp_results:
            objects.append({
                "key": dp.id,
                "dp_alias": dp.mediaplayer.alias,
                "dp_temp": dp.val_Temp,
                "serial_number": dp.serial_number,
                "status_temp": dp.status_temp,
                "mp_serial_number": dp.mp_serial_number
            })

        # [ Case by ] length != 10
        if len(objects) != 10:
            limit_range = 10 - len(objects)
            for i in range(0, limit_range):
                objects.append({
                    "key": 'vertual_key' + str(i),
                    "dp_alias": '',
                    "dp_temp": -1
                })

        return result(200, "GET the Top 10 Display information", objects, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Tree Area
#----------------------------------------------------------------------------------------------------------------------
class TreeListAPI(Resource):
    """
    [ Tree List ]
    @ GET : Return the whole tree information.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(TreeListAPI, self).__init__()

    def get(self):
        tree_type = request.args.get('type')

        manager_results = MediaPlayers.query.add_columns(Managers.id, Managers.userID).join(Managers, MediaPlayers.FK_managers_id == Managers.id).filter(or_(*self.filter_group)).group_by(Managers.userID).all()
        objects = []

        if len(manager_results) == 0:
            return result(200, "There are no managers.", None, None, "by KSM")

        dCountDic = dict()

        mp_results = MediaPlayers.query.order_by(MediaPlayers.site, MediaPlayers.location, MediaPlayers.shop, MediaPlayers.alias).all()
        dp_results = Displays.query.filter(Displays.FK_mp_id == MediaPlayers.id).all()

        for dpr in dp_results:
            if dpr.dp_group is not None and dpr.dp_group != "":
                dCountDic[dpr.id] = dpr.dp_group

        if tree_type == "by_site":
            # [ Start ] Manager
            for tree_manager in manager_results:

                current_site = ""
                current_location = ""
                current_shop = ""

                mp_dict = dict()
                for mp in mp_results:
                    if mp.FK_managers_id == tree_manager.id:
                        current_site = mp.site
                        current_location = mp.location
                        current_shop = mp.shop
                        current_alias = mp.alias
                        mp_info = dict()
                        dpg_check = dict()

                        # dpg_results = Displays.query.add_columns(Displays.dp_group, Displays.serial_number, db.func.count(Displays.dp_group).label("count"), Displays.FK_mp_id).\
                        #          filter(Displays.FK_mp_id == mp.id).filter(db.func.length(Displays.dp_group) > 0).group_by(Displays.dp_group).all()

                        if mp_dict.has_key(current_site) is False:
                            mp_dict[current_site] = dict()

                        if mp_dict[current_site].has_key(current_location) is False:
                            mp_dict[current_site][current_location] = dict()

                        if mp_dict[current_site][current_location].has_key(current_shop) is False:
                            mp_dict[current_site][current_location][current_shop] = dict()

                        if mp_dict[current_site][current_location][current_shop].has_key(current_alias) is False:
                            mp_dict[current_site][current_location][current_shop][current_alias] = dict()

                        mp_info['id'] = mp.id
                        mp_info['alias'] = mp.alias
                        mp_info['group_status'] = mp.group_status
                        mp_info['serial_number'] = mp.serial_number
                        mp_info['site'] = mp.site
                        mp_info['location'] = mp.location
                        mp_info['shop'] = mp.shop

                        dp_group_objs = []

                        for dpg in dp_results:
                            dg_count = 0
                            if dpg_check.has_key(dpg.dp_group) is False:
                                if dpg.FK_mp_id == mp.id and dpg.dp_group is not None and dpg.dp_group != "":
                                    search_dps_by_dpg_obj = []
                                    for dpgl in dp_results:
                                        if dpgl.dp_group == dpg.dp_group and dpgl.FK_mp_id == mp.id and dpgl.dp_group is not None and dpgl.dp_group != "":
                                            search_dps_by_dpg_obj.append({
                                                "id": dpgl.id,
                                                "serial_number": dpgl.serial_number
                                            })



                                    dp_group_objs.append({
                                        "id": mp.id,
                                        "serial_number": mp.serial_number,
                                        "manage_serial_number": mp.manage_serial_number,
                                        "alias": mp.alias,
                                        "status": mp.status,
                                        "group_status": mp.group_status,
                                        "name": dpg.dp_group,
                                        "count": dg_count,
                                        "dp_serial_number": dpg.serial_number,
                                        "search_dps_by_dpg_obj": search_dps_by_dpg_obj
                                    })

                                    dpg_check[dpg.dp_group] = dict()

                        mp_info['dp_group_objs'] = dp_group_objs

                        mp_dict[current_site][current_location][current_shop][current_alias] = mp_info

                shop_text = ""
                location_text = ""
                site_text = ""
                site_objs = []
                manager_count = 0
                manager_status = 0
                manager_dp_count = 0
                for site in sorted(mp_dict):
                    location_objs = []
                    site_count = 0
                    site_status = 0
                    site_dp_count = 0

                    for location in sorted(mp_dict[site]):
                        shop_objs = []
                        location_count = 0
                        location_status = 0
                        location_dp_count = 0

                        for shop in sorted(mp_dict[site][location]):
                            media_objs = []
                            shop_count = 0
                            shop_status = 0
                            shop_dp_count = 0

                            for alias in mp_dict[site][location][shop]:
                                mp = dict()
                                mp = mp_dict[site][location][shop][alias]

                                search_dps_by_dpg_none_obj = []
                                for dpgl_none in dp_results:
                                    if dpgl_none.FK_mp_id == mp['id'] and (dpgl_none.dp_group == "" or dpgl_none.dp_group is None):
                                        search_dps_by_dpg_none_obj.append({
                                            "id": dpgl_none.id,
                                            "serial_number": dpgl_none.serial_number
                                        })

                                media_objs.append({
                                    "alias": mp['alias'],
                                    "serial_number": mp['serial_number'],
                                    "group_status": mp['group_status'],
                                    "dp_group_objs": mp['dp_group_objs'],
                                    "search_dps_by_dpg_none_obj": search_dps_by_dpg_none_obj
                                })
                                shop_count = shop_count + 1

                                if mp['group_status'] == -1 and shop_status == 0:
                                    shop_status = -1
                                elif mp['group_status']== 0 and shop_status == -1:
                                    shop_status = -1
                                else:
                                    shop_status = max(shop_status, mp['group_status'])
                                shop_text = shop

                                for dpc in dp_results:
                                    if dpc.FK_mp_id == mp['id']:
                                        shop_dp_count = shop_dp_count + 1

                            shop_objs.append({
                                "text": shop_text,
                                "sub_count": shop_count,
                                "dp_count": shop_dp_count,
                                "status": shop_status,
                                "mediaplayer": media_objs
                            })
                            location_count = location_count + shop_count
                            location_dp_count = location_dp_count + shop_dp_count

                            if shop_status == -1 and location_status == 0:
                                location_status = -1
                            elif shop_status == 0 and location_status == -1:
                                location_status = -1
                            else:
                                location_status = max(location_status, shop_status)
                            location_text = location
                        location_objs.append({
                            "text": location_text,
                            "sub_count": location_count,
                            "dp_count": location_dp_count,
                            "shop": shop_objs,
                            "status": location_status
                        })
                        site_count = site_count + location_count
                        site_dp_count = site_dp_count + location_dp_count

                        if location_status == -1 and site_status == 0:
                            site_status = -1
                        elif location_status == 0 and site_status == -1:
                            site_status = -1
                        else:
                            site_status = max(site_status, location_status)
                        site_text = site

                    site_objs.append({
                        "text": site_text,
                        "sub_count": site_count,
                        "dp_count": site_dp_count,
                        "location": location_objs,
                        "status": site_status
                    })
                    manager_count = manager_count + site_count
                    manager_dp_count = manager_dp_count + site_dp_count

                    if site_status == -1 and manager_status == 0:
                        manager_status = -1
                    elif site_status == 0 and manager_status == -1:
                        manager_status = -1
                    else:
                        manager_status = max(manager_status, site_status)

                objects.append({
                    "id": tree_manager.id,
                    "text": tree_manager.userID,
                    "sub_count": manager_count,
                    "dp_count": manager_dp_count,
                    "site": site_objs,
                    "status": manager_status
                })
            # [ End ] Manager
            return result(200, "GET the TreeList", objects, None, "by KSM")
        elif tree_type == "by_dg":
            # Start Manager
            for manager in manager_results:
                dg_list = Device_Groups.query.add_columns(Device_Groups.id, Device_Groups.name, \
                        db.func.count(MediaPlayers.id).label("count"), db.func.max(MediaPlayers.group_status).label("status")).join(MediaPlayers, Device_Groups.id == MediaPlayers.FK_device_groups_id).filter(Device_Groups.FK_managers_id==manager.id).filter(or_(*self.filter_group)) \
                       .group_by(Device_Groups.name).all()

                dg_objs = []
                manager_count = 0
                manager_status = 0
                manager_dp_count = 0
                # Start DG
                for dg in dg_list:
                    dg_dp_count = Displays.query.join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id)\
                                        .filter(MediaPlayers.FK_device_groups_id == dg.id).count()
                    dg_objs.append({
                        "id": dg.id,
                        "text": dg.name,
                        "sub_count": dg.count,
                        "dp_count": dg_dp_count,
                        "status": dg.status
                    })
                    manager_count = manager_count + dg.count
                    manager_dp_count = manager_dp_count + dg_dp_count

                    if dg.status == -1 and manager_status == 0:
                        manager_status = -1
                    elif dg.status == 0 and manager_status == -1:
                        manager_status = -1
                    else:
                        manager_status = max(manager_status, dg.status)
                # End DG
                objects.append({
                    "id": manager.id,
                    "text": manager.userID,
                    "sub_count": manager_count,
                    "dp_count": manager_dp_count,
                    "dg": dg_objs,
                    "status": manager_status
                })
            # [ End ] Manager
            return result(200, "GET the TreeList", objects, None, "by KSM")


class TreeAPI(Resource):
    """
    [ Tree node ]
    @ GET : Returns a list of the last child node of the tree.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(TreeAPI, self).__init__()

    def get(self, shop):
        site = request.args.get('site')
        location = request.args.get('location')
        device_group_id = request.args.get('device_group_id')

        if device_group_id:
            mp_results = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == device_group_id).filter(or_(*self.filter_group)).all()
            mp_results = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == device_group_id).all()
            objects = []
            for tree_mp in mp_results:
                dp_results = Displays.query.add_columns(Displays.dp_group, Displays.serial_number, db.func.count(Displays.dp_group).label("count")).\
                            filter(Displays.FK_mp_id == tree_mp.id).filter(db.func.length(Displays.dp_group) > 0).group_by(Displays.dp_group).all()

                dp_group_objs = []
                if dp_results is not None:
                    for search_dp in dp_results:
                        if search_dp.count > 0:
                            dp_group_objs.append({
                                "name": search_dp.dp_group,
                                "count": search_dp.count,
                                "dp_serial_number": search_dp.serial_number
                            })

                objects.append({
                    "id": tree_mp.id,
                    "serial_number": tree_mp.serial_number,
                    "manage_serial_number": tree_mp.manage_serial_number,
                    "alias": tree_mp.alias,
                    "status": tree_mp.status,
                    "group_status": tree_mp.group_status,
                    "dp_group_objs": dp_group_objs
                })
            return result(200, "GET the Media Player List from the Tree", objects, None, "by KSM")
        else:
            mp_results = MediaPlayers.query.filter(MediaPlayers.site == site, MediaPlayers.location == location, MediaPlayers.shop == shop).filter(or_(*self.filter_group)).all()

            objects = []
            for tree_mp in mp_results:
                dp_results = Displays.query.add_columns(Displays.dp_group, Displays.serial_number, db.func.count(Displays.dp_group).label("count")).\
                            filter(Displays.FK_mp_id == tree_mp.id).filter(db.func.length(Displays.dp_group) > 0).group_by(Displays.dp_group).all()

                dp_group_objs = []
                if dp_results is not None:
                    for search_dp in dp_results:
                        if search_dp.count > 0:
                            dp_group_objs.append({
                                "name": search_dp.dp_group,
                                "count": search_dp.count,
                                "dp_serial_number": search_dp.serial_number
                            })

                objects.append({
                    "id": tree_mp.id,
                    "serial_number": tree_mp.serial_number,
                    "manage_serial_number": tree_mp.manage_serial_number,
                    "alias": tree_mp.alias,
                    "status": tree_mp.status,
                    "group_status": tree_mp.group_status,
                    "dp_group_objs": dp_group_objs
                })
            return result(200, "GET the Media Player List from the Tree", objects, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            History Area
#----------------------------------------------------------------------------------------------------------------------
class HistoryListAPI(Resource):
    """
    [ History ]
    @ GET : Returns a list of the history.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("memo_id", type=str, location="json")
        self.parser.add_argument("memo", type=str, location="json")

        super(HistoryListAPI, self).__init__()

    def get(self):
        mp_serial_number = request.args.get('mp_serial_number')
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))

        total_history = Support_History.query.filter_by(mp_serial_number=mp_serial_number).all()
        total_count = len(total_history)

        result_history = Support_History.query.order_by(Support_History.action_date.desc()).filter_by(mp_serial_number=mp_serial_number).limit(limit).offset(offset)
        current_count = result_history.count()

        next = ''
        previous = ''
        # Creating a Pagenation
        if offset == 0:
            previous = None
            if current_count < total_count:
                next_offset = offset+limit
                next = "/api/v1/history/?limit="+str(limit)+"&mp_serial_number="+mp_serial_number+"&offset="+str(next_offset)
            elif current_count == total_count:
                next = None
            else:
                return result(500, "Media Player Count Error", None, None, "by KSM")
        elif offset != 0:
            previous_offset = offset-limit
            previous = "/api/v1/history/?limit="+str(limit)+"&mp_serial_number="+mp_serial_number+"&offset="+str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next_offset = offset+limit
                next = "/api/v1/history/?limit="+str(limit)+"&mp_serial_number="+mp_serial_number+"&offset="+str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        objects = []

        if current_count == 0:
            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({
                    "key": '',
                    "userID": '',
                    "action_date": '',
                    "mp_serial_number": '',
                    "device": '',
                    "action_type": '',
                    "action_command": 'Empty',
                    "memo": '',
                    "editable": False
                })
            return result(404, "No History Log", None, meta, "by KSM")
        else:
            for history in result_history:
                if self.userID == history.userID:
                    editable_flag = True
                else:
                    editable_flag = False
                objects.append({
                    "key": history.id,
                    "userID": history.userID,
                    "action_date": json_encoder(history.action_date),
                    "mp_serial_number": history.mp_serial_number,
                    "device": history.device,
                    "action_type": history.action_type,
                    "action_command": history.action_command,
                    "memo": history.memo,
                    "editable": editable_flag
                })

            return result(200, "GET the Support Histroy", objects, meta, "by KSM")

    def put(self):
        input = self.parser.parse_args()

        memo_id = int(input['memo_id'])
        del input['memo_id']

        if memo_id is None:
            return result(404, "No histroy information.", None, None, "by KSM")
        else:
            del input['token']
            db.session.query(Support_History).filter_by(id=memo_id).update(input)
            db.session.commit()
            return result(200, "change histroy information", None, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Autocomplete Area
#----------------------------------------------------------------------------------------------------------------------
class AUTO_MediaPlayerAliasesAPI(Resource):
    """
    [ media player alias ]
    - Used for auto-completion.
    @ GET : Returns only Objects.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_MediaPlayerAliasesAPI, self).__init__()

    def get(self):
        alias_text = request.args.get('text')
        like_filter = '%' + alias_text + '%'

        mp_results = db.session.query(MediaPlayers.alias.distinct().label("alias")).filter(or_(*self.filter_group)).filter(MediaPlayers.alias.like(like_filter)).all()

        objects = []
        for search_mp in mp_results:
            objects.append({
                "id": search_mp.alias,
                "text": search_mp.alias
            })
        return objects

class AUTO_MediaPlayerSerialNumberAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_MediaPlayerSerialNumberAPI, self).__init__()

    def get(self):
        serial_number_text = request.args.get('text')
        like_filter = '%' + serial_number_text + '%'

        mp_results = db.session.query(MediaPlayers.manage_serial_number.distinct().label("serial_number")).filter(or_(*self.filter_group)).filter(MediaPlayers.manage_serial_number.like(like_filter)).all()

        objects = []
        for search_mp in mp_results:
            objects.append({
                "id": search_mp.serial_number,
                "text": search_mp.serial_number
            })
        return objects

class AUTO_DisplayAliasesAPI(Resource):
    """
    [ display alias ] - since 14.12.16 NOT USE by KSM
    - Used for auto-completion.
    @ GET : Returns only Objects.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_DisplayAliasesAPI, self).__init__()

    def get(self):
        alias_text = request.args.get('text')
        like_filter = '%' + alias_text + '%'

        dp_results = db.session.query(Displays.alias.distinct().label("alias")).filter(or_(*self.filter_group)).filter(Displays.alias.like(like_filter)).all()

        objects = []
        for search_dp in dp_results:
            objects.append({
                "id": search_dp.alias,
                "text": search_dp.alias
            })
        return objects

class AUTO_SitesAPI(Resource):
    """
    [ site ]
    - Used for auto-completion.
    @ GET : Returns only Objects.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_SitesAPI, self).__init__()

    def get(self):
        site_text = request.args.get('text')
        like_filter = '%' + site_text + '%'

        mp_results = db.session.query(MediaPlayers.site.distinct().label("site")).filter(or_(*self.filter_group)).filter(MediaPlayers.site.like(like_filter)).all()

        objects = []
        for mp in mp_results:
            objects.append({
                "id": mp.site,
                "text": mp.site
            })
        return objects

class AUTO_LocationsAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_LocationsAPI, self).__init__()

    def get(self):
        location_text = request.args.get('text')
        like_filter = '%' + location_text + '%'

        mp_results = db.session.query(MediaPlayers.location.distinct().label("location")).filter(or_(*self.filter_group)).filter(MediaPlayers.location.like(like_filter)).all()

        objects = []
        for mp in mp_results:
            objects.append({
                "id": mp.location,
                "text": mp.location
            })
        return objects

class AUTO_ShopsAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_ShopsAPI, self).__init__()

    def get(self):
        shop_text = request.args.get('text')
        like_filter = '%' + shop_text + '%'

        mp_results = db.session.query(MediaPlayers.shop.distinct().label("shop")).filter(or_(*self.filter_group)).filter(MediaPlayers.shop.like(like_filter)).all()

        objects = []
        for mp in mp_results:
            objects.append({
                "id": mp.shop,
                "text": mp.shop
            })
        return objects

class AUTO_SiCompaniesAPI(Resource):
    """
    [ si company ]
    - Used for auto-completion.
    @ GET : Returns only Objects.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_SiCompaniesAPI, self).__init__()

    def get(self):
        si_com_text = request.args.get('text')
        dp = Displays()

        dp_query = "SELECT * FROM %(table_name)s WHERE id IN (SELECT id FROM (SELECT id FROM %(table_name)s GROUP BY si_company) as b WHERE si_company LIKE '%(si_com_text)s')" % {
                    "table_name": dp.__tablename__,
                    "si_com_text": '%'+si_com_text+'%'
        }
        dp_results = Displays.query.from_statement(dp_query).all()

        objects = []
        for dp in dp_results:
            objects.append({
                "id": dp.si_company,
                "text": dp.si_company
            })
        return objects

class AUTO_OrganizationsAPI(Resource):
    """
    [ oranization ]
    - Used for auto-completion.
    @ GET : Returns only Objects.
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        super(AUTO_OrganizationsAPI, self).__init__()

    def get(self):
        org_text = request.args.get('text')
        dp = Displays()

        dp_query = "SELECT * FROM %(table_name)s WHERE id IN (SELECT id FROM (SELECT id FROM %(table_name)s GROUP BY organization) as b WHERE organization LIKE '%(org_text)s')" % {
                    "table_name": dp.__tablename__,
                    "org_text": '%'+org_text+'%'
        }
        dp_results = Displays.query.from_statement(dp_query).all()

        objects = []
        for dp in dp_results:
            objects.append({
                "id": dp.organization,
                "text": dp.organization
            })
        return objects

#----------------------------------------------------------------------------------------------------------------------
#                                            Report Area
#----------------------------------------------------------------------------------------------------------------------
class ReportIncidentListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("serial_number", type=str, location="json")
        self.parser.add_argument("type", type=str, location="json")
        self.parser.add_argument("subtype", type=str, location="json")
        self.parser.add_argument("status", type=str, location="json")

        super(ReportIncidentListAPI, self).__init__()


    def get(self):
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))

        filter_and_group = []
        filter_or_group = []

        param = {}
        param["base"] = str(request.args.get('base'))
        if request.args.get('manager_id'):
            param["manager_id"] = str(request.args.get('manager_id')) if str(request.args.get('manager_id')) == "All" else int(request.args.get('manager_id'))

            if param["manager_id"] != "All":
                filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
            else:
                if self.userType is USER_TYPE_ADMIN:
                    manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                    if len(manager_ids) is 0:
                        filter_and_group.append(MediaPlayers.id == -1)
                    else:
                        for mid in manager_ids:
                            filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                elif self.userType is USER_TYPE_MANAGER:
                    filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                elif self.userType is USER_TYPE_MEMBER:
                    group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

                    # User doesn't have any device group
                    if len(group_ids) is 0:
                        filter_and_group.append(MediaPlayers.id == -1)
                    else:
                        for id in group_ids:
                            mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                            if mp_ids is None:
                                filter_and_group.append(MediaPlayers.id == -1)
                            else:
                                for mp_id in mp_ids:
                                    filter_or_group.append(MediaPlayers.id == mp_id.id)

        if request.args.get('account'):
            param["account"] = str(request.args.get('account'))
            if param["account"] != "All":
                filter_and_group.append(MediaPlayers.site==param["account"])
        if request.args.get('region'):
            param["region"] = str(request.args.get('region'))
            if param["region"] != "All":
                filter_and_group.append(MediaPlayers.location==param["region"])
        if request.args.get('shop'):
            param["shop"] = str(request.args.get('shop'))
            if param["shop"] != "All":
                filter_and_group.append(MediaPlayers.shop==param["shop"])
        if request.args.get('dg_id'):
            param["dg_id"] = str(request.args.get('dg_id')) if str(request.args.get('dg_id')) == "All" else int(request.args.get('dg_id'))
            if param["dg_id"] != "All":
                filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])
        if request.args.get('mp_sn'):
            param["mp_sn"] = str(request.args.get('mp_sn'))
            if param["mp_sn"] != "All":
                filter_and_group.append(MediaPlayers.serial_number==param["mp_sn"])
        if request.args.get('dg_mp_sn'):
            param["dg_mp_sn"] = str(request.args.get('dg_mp_sn'))
            if param["dg_mp_sn"] != "All":
                filter_and_group.append(MediaPlayers.serial_number==param["dg_mp_sn"])

        if request.args.get('mp_serial_number'):
            param["mp_serial_number"] = request.args.get('mp_serial_number')
            filter_and_group.append(Incidents.type==0)
            filter_and_group.append(Incidents.serial_number==param["mp_serial_number"])
        if request.args.get('dp_serial_number'):
            param["dp_serial_number"] = request.args.get('dp_serial_number')
            filter_and_group.append(Incidents.type==1)
            filter_and_group.append(Incidents.serial_number==param["dp_serial_number"])

        # [ Make ] ORDER BY
        order_by = request.args.get('order_by')
        order_by_str = ''
        if order_by is not None:
            order_by_str = 'incidents.' + str(order_by)

        order = request.args.get('order')
        order_str = ''
        if order == '-':
            order_str = 'DESC'
        else:
            order_str = 'ASC'

        result_order_by = order_by_str + ' ' + order_str

        objects = []
        meta = {}

        total_count = Incidents.query.join(MediaPlayers, Incidents.mp_serial_number == MediaPlayers.serial_number).filter(or_(*self.filter_group)).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).count()
        result_incidents = Incidents.query.join(MediaPlayers, Incidents.mp_serial_number == MediaPlayers.serial_number).filter(or_(*self.filter_group)).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(result_order_by).limit(limit).offset(offset)

        current_count = result_incidents.count()

        next = ''
        previous = ''

        previous_offset = offset-limit
        next_offset = offset+limit
        # Creating a Pagenation
        if offset == 0:
            previous = None
            next = None
        elif offset != 0:
            previous = "/api/v1/report/incident-list?limit="+str(limit)+"&offset="+str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next = "/api/v1/report/incident-list?limit="+str(limit)+"&offset="+str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        if current_count == 0:
            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({

            })
            return result(404, "No Incidents Info", None, meta, "by KSM")
        else:
            for incident in result_incidents:
                objects.append({
                    'id': incident.id,
                    'mp_serial_number': incident.mp_serial_number,
                    'dp_serial_number': incident.dp_serial_number,
                    'type': incident.type,
                    'alias': incident.alias,
                    'created': json_encoder(incident.created),
                    'error_code': incident.error_code,
                    'level': incident.level
                })
            return result(200, "Get the Incident list", objects, meta, "by rainmaker")

    def post(self):
        input = self.parser.parse_args()

        return result(200, "POST insident-list successful", None, None, None)

class ReportTop10API(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        type = request.args.get('type')

        # ---------------------------------------------------------------------------------------------------------------
        # Filtering
        # ---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                if type == "mp_cpu_usage":
                    self.filter_group.append(Cumulative_Top10_Cpu_Usage.id == -1)
                elif type == "mp_mem_usage":
                    self.filter_group.append(Cumulative_Top10_Mem_Usage.id == -1)
                elif type == "mp_hdd_usage":
                    self.filter_group.append(Cumulative_Top10_Hdd_Usage.id == -1)
                elif type == "mp_cpu_temp":
                    self.filter_group.append(Cumulative_Top10_Cpu_Temp.id == -1)
                else:
                    self.filter_group.append(Cumulative_Top10_Val_Temp.id == -1)
            else:
                for mid in manager_ids:
                    if type == "mp_cpu_usage":
                        self.filter_group.append(Cumulative_Top10_Cpu_Usage.FK_managers_id == mid.FK_manager_id)
                    elif type == "mp_mem_usage":
                        self.filter_group.append(Cumulative_Top10_Mem_Usage.FK_managers_id == mid.FK_manager_id)
                    elif type == "mp_hdd_usage":
                        self.filter_group.append(Cumulative_Top10_Hdd_Usage.FK_managers_id == mid.FK_manager_id)
                    elif type == "mp_cpu_temp":
                        self.filter_group.append(Cumulative_Top10_Cpu_Temp.FK_managers_id == mid.FK_manager_id)
                    else:
                        self.filter_group.append(Cumulative_Top10_Val_Temp.FK_managers_id == mid.FK_manager_id)

        elif self.userType is USER_TYPE_MANAGER:
            if type == "mp_cpu_usage":
                self.filter_group.append(Cumulative_Top10_Cpu_Usage.FK_managers_id == self.user_manager_id)
            elif type == "mp_mem_usage":
                self.filter_group.append(Cumulative_Top10_Mem_Usage.FK_managers_id == self.user_manager_id)
            elif type == "mp_hdd_usage":
                self.filter_group.append(Cumulative_Top10_Hdd_Usage.FK_managers_id == self.user_manager_id)
            elif type == "mp_cpu_temp":
                self.filter_group.append(Cumulative_Top10_Cpu_Temp.FK_managers_id == self.user_manager_id)
            else:
                self.filter_group.append(Cumulative_Top10_Val_Temp.FK_managers_id == self.user_manager_id)

        super(ReportTop10API, self).__init__()

    def get(self):
        type = request.args.get('type')
        time_range = request.args.get('time_range') or 3
        result_limit = 10
        objects = []

        interval_time = datetime.now() - timedelta(hours=int(time_range))

        if type == "mp_hdd_usage":
            if self.userType is USER_TYPE_ADMIN:
                i = 0
                value_list = {}
                real_list = {}
                items = db.session.query(Cumulative_Top10_Hdd_Usage).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Hdd_Usage.time_range == time_range).all()

                if items is not None:
                    for item in items:
                        if item.avg_hdd_usage1 is not None:
                            value_list[i] = item.avg_hdd_usage1
                            real_list[i] = [item.avg_hdd_usage1_alias, item.avg_hdd_usage1_sn]
                            i = i + 1
                        if item.avg_hdd_usage2 is not None:
                            value_list[i] = item.avg_hdd_usage2
                            real_list[i] = [item.avg_hdd_usage2_alias, item.avg_hdd_usage2_sn]
                            i = i + 1
                        if item.avg_hdd_usage3 is not None:
                            value_list[i] = item.avg_hdd_usage3
                            real_list[i] = [item.avg_hdd_usage3_alias, item.avg_hdd_usage3_sn]
                            i = i + 1
                        if item.avg_hdd_usage4 is not None:
                            value_list[i] = item.avg_hdd_usage4
                            real_list[i] = [item.avg_hdd_usage4_alias, item.avg_hdd_usage4_sn]
                            i = i + 1
                        if item.avg_hdd_usage5 is not None:
                            value_list[i] = item.avg_hdd_usage5
                            real_list[i] = [item.avg_hdd_usage5_alias, item.avg_hdd_usage5_sn]
                            i = i + 1
                        if item.avg_hdd_usage6 is not None:
                            value_list[i] = item.avg_hdd_usage6
                            real_list[i] = [item.avg_hdd_usage6_alias, item.avg_hdd_usage6_sn]
                            i = i + 1
                        if item.avg_hdd_usage7 is not None:
                            value_list[i] = item.avg_hdd_usage7
                            real_list[i] = [item.avg_hdd_usage7_alias, item.avg_hdd_usage7_sn]
                            i = i + 1
                        if item.avg_hdd_usage8 is not None:
                            value_list[i] = item.avg_hdd_usage8
                            real_list[i] = [item.avg_hdd_usage8_alias, item.avg_hdd_usage8_sn]
                            i = i + 1
                        if item.avg_hdd_usage9 is not None:
                            value_list[i] = item.avg_hdd_usage9
                            real_list[i] = [item.avg_hdd_usage9_alias, item.avg_hdd_usage9_sn]
                            i = i + 1
                        if item.avg_hdd_usage10 is not None:
                            value_list[i] = item.avg_hdd_usage10
                            real_list[i] = [item.avg_hdd_usage10_alias, item.avg_hdd_usage10_sn]
                            i = i + 1

                    sorted_index = sorted(value_list, key=value_list.get, reverse=True)

                    i = 0
                    for sorted_item in sorted_index:
                        objects.append({
                            'serial_number': real_list[sorted_item][1],
                            'alias': real_list[sorted_item][0],
                            'value': value_list[sorted_item]
                        })

                        i = i + 1
                        if i == 10:
                            break
            elif self.userType is USER_TYPE_MANAGER:
                items = db.session.query(Cumulative_Top10_Hdd_Usage).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Hdd_Usage.time_range == time_range).first()

                if items is not None:
                    if items.avg_hdd_usage1_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage1_sn,
                            'alias': items.avg_hdd_usage1_alias,
                            'value': items.avg_hdd_usage1
                        })
                    if items.avg_hdd_usage2_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage2_sn,
                            'alias': items.avg_hdd_usage2_alias,
                            'value': items.avg_hdd_usage2
                        })
                    if items.avg_hdd_usage3_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage3_sn,
                            'alias': items.avg_hdd_usage3_alias,
                            'value': items.avg_hdd_usage3
                        })
                    if items.avg_hdd_usage4_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage4_sn,
                            'alias': items.avg_hdd_usage4_alias,
                            'value': items.avg_hdd_usage4
                        })
                    if items.avg_hdd_usage5_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage5_sn,
                            'alias': items.avg_hdd_usage5_alias,
                            'value': items.avg_hdd_usage5
                        })
                    if items.avg_hdd_usage6_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage6_sn,
                            'alias': items.avg_hdd_usage6_alias,
                            'value': items.avg_hdd_usage6
                        })
                    if items.avg_hdd_usage7_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage7_sn,
                            'alias': items.avg_hdd_usage7_alias,
                            'value': items.avg_hdd_usage7
                        })
                    if items.avg_hdd_usage8_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage8_sn,
                            'alias': items.avg_hdd_usage8_alias,
                            'value': items.avg_hdd_usage8
                        })
                    if items.avg_hdd_usage9_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage9_sn,
                            'alias': items.avg_hdd_usage9_alias,
                            'value': items.avg_hdd_usage9
                        })
                    if items.avg_hdd_usage10_sn is not None:
                        objects.append({
                            'serial_number': items.avg_hdd_usage10_sn,
                            'alias': items.avg_hdd_usage10_alias,
                            'value': items.avg_hdd_usage10
                        })
        elif type == "mp_cpu_temp":
            if self.userType is USER_TYPE_ADMIN:
                i = 0
                value_list = {}
                real_list = {}
                items = db.session.query(Cumulative_Top10_Cpu_Temp).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Cpu_Temp.time_range == time_range).all()

                if items is not None:
                    for item in items:
                        if item.avg_cpu_temp1 is not None:
                            value_list[i] = item.avg_cpu_temp1
                            real_list[i] = [item.avg_cpu_temp1_alias, item.avg_cpu_temp1_sn]
                            i = i + 1
                        if item.avg_cpu_temp2 is not None:
                            value_list[i] = item.avg_cpu_temp2
                            real_list[i] = [item.avg_cpu_temp2_alias, item.avg_cpu_temp2_sn]
                            i = i + 1
                        if item.avg_cpu_temp3 is not None:
                            value_list[i] = item.avg_cpu_temp3
                            real_list[i] = [item.avg_cpu_temp3_alias, item.avg_cpu_temp3_sn]
                            i = i + 1
                        if item.avg_cpu_temp4 is not None:
                            value_list[i] = item.avg_cpu_temp4
                            real_list[i] = [item.avg_cpu_temp4_alias, item.avg_cpu_temp4_sn]
                            i = i + 1
                        if item.avg_cpu_temp5 is not None:
                            value_list[i] = item.avg_cpu_temp5
                            real_list[i] = [item.avg_cpu_temp5_alias, item.avg_cpu_temp5_sn]
                            i = i + 1
                        if item.avg_cpu_temp6 is not None:
                            value_list[i] = item.avg_cpu_temp6
                            real_list[i] = [item.avg_cpu_temp6_alias, item.avg_cpu_temp6_sn]
                            i = i + 1
                        if item.avg_cpu_temp7 is not None:
                            value_list[i] = item.avg_cpu_temp7
                            real_list[i] = [item.avg_cpu_temp7_alias, item.avg_cpu_temp7_sn]
                            i = i + 1
                        if item.avg_cpu_temp8 is not None:
                            value_list[i] = item.avg_cpu_temp8
                            real_list[i] = [item.avg_cpu_temp8_alias, item.avg_cpu_temp8_sn]
                            i = i + 1
                        if item.avg_cpu_temp9 is not None:
                            value_list[i] = item.avg_cpu_temp9
                            real_list[i] = [item.avg_cpu_temp9_alias, item.avg_cpu_temp9_sn]
                            i = i + 1
                        if item.avg_cpu_temp10 is not None:
                            value_list[i] = item.avg_cpu_temp10
                            real_list[i] = [item.avg_cpu_temp10_alias, item.avg_cpu_temp10_sn]
                            i = i + 1

                    sorted_index = sorted(value_list, key=value_list.get, reverse=True)

                    i = 0
                    for sorted_item in sorted_index:
                        objects.append({
                            'serial_number': real_list[sorted_item][1],
                            'alias': real_list[sorted_item][0],
                            'value': value_list[sorted_item]
                        })

                        i = i + 1
                        if i == 10:
                            break
            elif self.userType is USER_TYPE_MANAGER:
                items = db.session.query(Cumulative_Top10_Cpu_Temp).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Cpu_Temp.time_range == time_range).first()

                if items is not None:
                    if items.avg_cpu_temp1_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp1_sn,
                            'alias': items.avg_cpu_temp1_alias,
                            'value': items.avg_cpu_temp1
                        })
                    if items.avg_cpu_temp2_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp2_sn,
                            'alias': items.avg_cpu_temp2_alias,
                            'value': items.avg_cpu_temp2
                        })
                    if items.avg_cpu_temp3_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp3_sn,
                            'alias': items.avg_cpu_temp3_alias,
                            'value': items.avg_cpu_temp3
                        })
                    if items.avg_cpu_temp4_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp4_sn,
                            'alias': items.avg_cpu_temp4_alias,
                            'value': items.avg_cpu_temp4
                        })
                    if items.avg_cpu_temp5_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp5_sn,
                            'alias': items.avg_cpu_temp5_alias,
                            'value': items.avg_cpu_temp5
                        })
                    if items.avg_cpu_temp6_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp6_sn,
                            'alias': items.avg_cpu_temp6_alias,
                            'value': items.avg_cpu_temp6
                        })
                    if items.avg_cpu_temp7_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp7_sn,
                            'alias': items.avg_cpu_temp7_alias,
                            'value': items.avg_cpu_temp7
                        })
                    if items.avg_cpu_temp8_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp8_sn,
                            'alias': items.avg_cpu_temp8_alias,
                            'value': items.avg_cpu_temp8
                        })
                    if items.avg_cpu_temp9_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp9_sn,
                            'alias': items.avg_cpu_temp9_alias,
                            'value': items.avg_cpu_temp9
                        })
                    if items.avg_cpu_temp10_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_temp10_sn,
                            'alias': items.avg_cpu_temp10_alias,
                            'value': items.avg_cpu_temp10
                        })
        elif type == "mp_mem_usage":
            if self.userType is USER_TYPE_ADMIN:
                i = 0
                value_list = {}
                real_list = {}
                items = db.session.query(Cumulative_Top10_Mem_Usage).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Mem_Usage.time_range == time_range).all()

                if items is not None:
                    for item in items:
                        if item.avg_mem_usage1 is not None:
                            value_list[i] = item.avg_mem_usage1
                            real_list[i] = [item.avg_mem_usage1_alias, item.avg_mem_usage1_sn]
                            i = i + 1
                        if item.avg_mem_usage2 is not None:
                            value_list[i] = item.avg_mem_usage2
                            real_list[i] = [item.avg_mem_usage2_alias, item.avg_mem_usage2_sn]
                            i = i + 1
                        if item.avg_mem_usage3 is not None:
                            value_list[i] = item.avg_mem_usage3
                            real_list[i] = [item.avg_mem_usage3_alias, item.avg_mem_usage3_sn]
                            i = i + 1
                        if item.avg_mem_usage4 is not None:
                            value_list[i] = item.avg_mem_usage4
                            real_list[i] = [item.avg_mem_usage4_alias, item.avg_mem_usage4_sn]
                            i = i + 1
                        if item.avg_mem_usage5 is not None:
                            value_list[i] = item.avg_mem_usage5
                            real_list[i] = [item.avg_mem_usage5_alias, item.avg_mem_usage5_sn]
                            i = i + 1
                        if item.avg_mem_usage6 is not None:
                            value_list[i] = item.avg_mem_usage6
                            real_list[i] = [item.avg_mem_usage6_alias, item.avg_mem_usage6_sn]
                            i = i + 1
                        if item.avg_mem_usage7 is not None:
                            value_list[i] = item.avg_mem_usage7
                            real_list[i] = [item.avg_mem_usage7_alias, item.avg_mem_usage7_sn]
                            i = i + 1
                        if item.avg_mem_usage8 is not None:
                            value_list[i] = item.avg_mem_usage8
                            real_list[i] = [item.avg_mem_usage8_alias, item.avg_mem_usage8_sn]
                            i = i + 1
                        if item.avg_mem_usage9 is not None:
                            value_list[i] = item.avg_mem_usage9
                            real_list[i] = [item.avg_mem_usage9_alias, item.avg_mem_usage9_sn]
                            i = i + 1
                        if item.avg_mem_usage10 is not None:
                            value_list[i] = item.avg_mem_usage10
                            real_list[i] = [item.avg_mem_usage10_alias, item.avg_mem_usage10_sn]
                            i = i + 1

                    sorted_index = sorted(value_list, key=value_list.get, reverse=True)

                    i = 0
                    for sorted_item in sorted_index:
                        objects.append({
                            'serial_number': real_list[sorted_item][1],
                            'alias': real_list[sorted_item][0],
                            'value': value_list[sorted_item]
                        })

                        i = i + 1
                        if i == 10:
                            break
            elif self.userType is USER_TYPE_MANAGER:
                items = db.session.query(Cumulative_Top10_Mem_Usage).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Mem_Usage.time_range == time_range).first()

                if items is not None:
                    if items.avg_mem_usage1_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage1_sn,
                            'alias': items.avg_mem_usage1_alias,
                            'value': items.avg_mem_usage1
                        })
                    if items.avg_mem_usage2_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage2_sn,
                            'alias': items.avg_mem_usage2_alias,
                            'value': items.avg_mem_usage2
                        })
                    if items.avg_mem_usage3_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage3_sn,
                            'alias': items.avg_mem_usage3_alias,
                            'value': items.avg_mem_usage3
                        })
                    if items.avg_mem_usage4_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage4_sn,
                            'alias': items.avg_mem_usage4_alias,
                            'value': items.avg_mem_usage4
                        })
                    if items.avg_mem_usage5_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage5_sn,
                            'alias': items.avg_mem_usage5_alias,
                            'value': items.avg_mem_usage5
                        })
                    if items.avg_mem_usage6_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage6_sn,
                            'alias': items.avg_mem_usage6_alias,
                            'value': items.avg_mem_usage6
                        })
                    if items.avg_mem_usage7_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage7_sn,
                            'alias': items.avg_mem_usage7_alias,
                            'value': items.avg_mem_usage7
                        })
                    if items.avg_mem_usage8_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage8_sn,
                            'alias': items.avg_mem_usage8_alias,
                            'value': items.avg_mem_usage8
                        })
                    if items.avg_mem_usage9_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage9_sn,
                            'alias': items.avg_mem_usage9_alias,
                            'value': items.avg_mem_usage9
                        })
                    if items.avg_mem_usage10_sn is not None:
                        objects.append({
                            'serial_number': items.avg_mem_usage10_sn,
                            'alias': items.avg_mem_usage10_alias,
                            'value': items.avg_mem_usage10
                        })
        elif type == "mp_cpu_usage":
            if self.userType is USER_TYPE_ADMIN:
                i = 0
                value_list = {}
                real_list = {}
                items = db.session.query(Cumulative_Top10_Cpu_Usage).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Cpu_Usage.time_range == time_range).all()

                if items is not None:
                    for item in items:
                        if item.avg_cpu_usage1 is not None:
                            value_list[i] = item.avg_cpu_usage1
                            real_list[i] = [item.avg_cpu_usage1_alias, item.avg_cpu_usage1_sn]
                            i = i + 1
                        if item.avg_cpu_usage2 is not None:
                            value_list[i] = item.avg_cpu_usage2
                            real_list[i] = [item.avg_cpu_usage2_alias, item.avg_cpu_usage2_sn]
                            i = i + 1
                        if item.avg_cpu_usage3 is not None:
                            value_list[i] = item.avg_cpu_usage3
                            real_list[i] = [item.avg_cpu_usage3_alias, item.avg_cpu_usage3_sn]
                            i = i + 1
                        if item.avg_cpu_usage4 is not None:
                            value_list[i] = item.avg_cpu_usage4
                            real_list[i] = [item.avg_cpu_usage4_alias, item.avg_cpu_usage4_sn]
                            i = i + 1
                        if item.avg_cpu_usage5 is not None:
                            value_list[i] = item.avg_cpu_usage5
                            real_list[i] = [item.avg_cpu_usage5_alias, item.avg_cpu_usage5_sn]
                            i = i + 1
                        if item.avg_cpu_usage6 is not None:
                            value_list[i] = item.avg_cpu_usage6
                            real_list[i] = [item.avg_cpu_usage6_alias, item.avg_cpu_usage6_sn]
                            i = i + 1
                        if item.avg_cpu_usage7 is not None:
                            value_list[i] = item.avg_cpu_usage7
                            real_list[i] = [item.avg_cpu_usage7_alias, item.avg_cpu_usage7_sn]
                            i = i + 1
                        if item.avg_cpu_usage8 is not None:
                            value_list[i] = item.avg_cpu_usage8
                            real_list[i] = [item.avg_cpu_usage8_alias, item.avg_cpu_usage8_sn]
                            i = i + 1
                        if item.avg_cpu_usage9 is not None:
                            value_list[i] = item.avg_cpu_usage9
                            real_list[i] = [item.avg_cpu_usage9_alias, item.avg_cpu_usage9_sn]
                            i = i + 1
                        if item.avg_cpu_usage10 is not None:
                            value_list[i] = item.avg_cpu_usage10
                            real_list[i] = [item.avg_cpu_usage10_alias, item.avg_cpu_usage10_sn]
                            i = i + 1

                    sorted_index = sorted(value_list, key=value_list.get, reverse=True)

                    i = 0
                    for sorted_item in sorted_index:
                        objects.append({
                            'serial_number': real_list[sorted_item][1],
                            'alias': real_list[sorted_item][0],
                            'value': value_list[sorted_item]
                        })

                        i = i + 1
                        if i == 10:
                            break


            elif self.userType is USER_TYPE_MANAGER:
                items = db.session.query(Cumulative_Top10_Cpu_Usage).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Cpu_Usage.time_range == time_range).first()

                if items is not None:
                    if items.avg_cpu_usage1_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage1_sn,
                            'alias': items.avg_cpu_usage1_alias,
                            'value': items.avg_cpu_usage1
                        })
                    if items.avg_cpu_usage2_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage2_sn,
                            'alias': items.avg_cpu_usage2_alias,
                            'value': items.avg_cpu_usage2
                        })
                    if items.avg_cpu_usage3_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage3_sn,
                            'alias': items.avg_cpu_usage3_alias,
                            'value': items.avg_cpu_usage3
                        })
                    if items.avg_cpu_usage4_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage4_sn,
                            'alias': items.avg_cpu_usage4_alias,
                            'value': items.avg_cpu_usage4
                        })
                    if items.avg_cpu_usage5_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage5_sn,
                            'alias': items.avg_cpu_usage5_alias,
                            'value': items.avg_cpu_usage5
                        })
                    if items.avg_cpu_usage6_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage6_sn,
                            'alias': items.avg_cpu_usage6_alias,
                            'value': items.avg_cpu_usage6
                        })
                    if items.avg_cpu_usage7_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage7_sn,
                            'alias': items.avg_cpu_usage7_alias,
                            'value': items.avg_cpu_usage7
                        })
                    if items.avg_cpu_usage8_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage8_sn,
                            'alias': items.avg_cpu_usage8_alias,
                            'value': items.avg_cpu_usage8
                        })
                    if items.avg_cpu_usage9_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage9_sn,
                            'alias': items.avg_cpu_usage9_alias,
                            'value': items.avg_cpu_usage9
                        })
                    if items.avg_cpu_usage10_sn is not None:
                        objects.append({
                            'serial_number': items.avg_cpu_usage10_sn,
                            'alias': items.avg_cpu_usage10_alias,
                            'value': items.avg_cpu_usage10
                        })

        elif type == "dp_temp":
            if self.userType is USER_TYPE_ADMIN:
                i = 0
                value_list = {}
                real_list = {}
                items = db.session.query(Cumulative_Top10_Val_Temp).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Val_Temp.time_range == time_range).all()

                if items is not None:
                    for item in items:
                        if item.avg_val_temp1 is not None:
                            value_list[i] = item.avg_val_temp1
                            real_list[i] = [item.avg_val_temp1_alias, item.avg_val_temp1_sn, item.avg_val_temp1_mp_sn]
                            i = i + 1
                        if item.avg_val_temp2 is not None:
                            value_list[i] = item.avg_val_temp2
                            real_list[i] = [item.avg_val_temp2_alias, item.avg_val_temp2_sn, item.avg_val_temp2_mp_sn]
                            i = i + 1
                        if item.avg_val_temp3 is not None:
                            value_list[i] = item.avg_val_temp3
                            real_list[i] = [item.avg_val_temp3_alias, item.avg_val_temp3_sn, item.avg_val_temp3_mp_sn]
                            i = i + 1
                        if item.avg_val_temp4 is not None:
                            value_list[i] = item.avg_val_temp4
                            real_list[i] = [item.avg_val_temp4_alias, item.avg_val_temp4_sn, item.avg_val_temp4_mp_sn]
                            i = i + 1
                        if item.avg_val_temp5 is not None:
                            value_list[i] = item.avg_val_temp5
                            real_list[i] = [item.avg_val_temp5_alias, item.avg_val_temp5_sn, item.avg_val_temp5_mp_sn]
                            i = i + 1
                        if item.avg_val_temp6 is not None:
                            value_list[i] = item.avg_val_temp6
                            real_list[i] = [item.avg_val_temp6_alias, item.avg_val_temp6_sn, item.avg_val_temp6_mp_sn]
                            i = i + 1
                        if item.avg_val_temp7 is not None:
                            value_list[i] = item.avg_val_temp7
                            real_list[i] = [item.avg_val_temp7_alias, item.avg_val_temp7_sn, item.avg_val_temp7_mp_sn]
                            i = i + 1
                        if item.avg_val_temp8 is not None:
                            value_list[i] = item.avg_val_temp8
                            real_list[i] = [item.avg_val_temp8_alias, item.avg_val_temp8_sn, item.avg_val_temp8_mp_sn]
                            i = i + 1
                        if item.avg_val_temp9 is not None:
                            value_list[i] = item.avg_val_temp9
                            real_list[i] = [item.avg_val_temp9_alias, item.avg_val_temp9_sn, item.avg_val_temp9_mp_sn]
                            i = i + 1
                        if item.avg_val_temp10 is not None:
                            value_list[i] = item.avg_val_temp10
                            real_list[i] = [item.avg_val_temp10_alias, item.avg_val_temp10_sn, item.avg_val_temp10_mp_sn]
                            i = i + 1

                    sorted_index = sorted(value_list, key=value_list.get, reverse=True)

                    i = 0
                    for sorted_item in sorted_index:
                        objects.append({
                            'serial_number': real_list[sorted_item][1],
                            'alias': real_list[sorted_item][0],
                            'value': value_list[sorted_item],
                            'mp_serial_number': real_list[sorted_item][2]
                        })

                        i = i + 1
                        if i == 10:
                            break

            elif self.userType is USER_TYPE_MANAGER:

                items = db.session.query(Cumulative_Top10_Val_Temp).filter(or_(*self.filter_group)).filter(Cumulative_Top10_Val_Temp.time_range == time_range).first()

                if items is not None:
                    if items.avg_val_temp1_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp1_sn,
                            'alias': items.avg_val_temp1_alias,
                            'value': items.avg_val_temp1,
                            'mp_serial_number': items.avg_val_temp1_mp_sn
                        })
                    if items.avg_val_temp2_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp2_sn,
                            'alias': items.avg_val_temp2_alias,
                            'value': items.avg_val_temp2,
                            'mp_serial_number': items.avg_val_temp2_mp_sn
                        })
                    if items.avg_val_temp3_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp3_sn,
                            'alias': items.avg_val_temp3_alias,
                            'value': items.avg_val_temp3,
                            'mp_serial_number': items.avg_val_temp3_mp_sn
                        })
                    if items.avg_val_temp4_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp4_sn,
                            'alias': items.avg_val_temp4_alias,
                            'value': items.avg_val_temp4,
                            'mp_serial_number': items.avg_val_temp4_mp_sn
                        })
                    if items.avg_val_temp5_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp5_sn,
                            'alias': items.avg_val_temp5_alias,
                            'value': items.avg_val_temp5,
                            'mp_serial_number': items.avg_val_temp5_mp_sn
                        })
                    if items.avg_val_temp6_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp6_sn,
                            'alias': items.avg_val_temp6_alias,
                            'value': items.avg_val_temp6,
                            'mp_serial_number': items.avg_val_temp6_mp_sn
                        })
                    if items.avg_val_temp7_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp7_sn,
                            'alias': items.avg_val_temp7_alias,
                            'value': items.avg_val_temp7,
                            'mp_serial_number': items.avg_val_temp7_mp_sn
                        })
                    if items.avg_val_temp8_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp8_sn,
                            'alias': items.avg_val_temp8_alias,
                            'value': items.avg_val_temp8,
                            'mp_serial_number': items.avg_val_temp8_mp_sn
                        })
                    if items.avg_val_temp9_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp9_sn,
                            'alias': items.avg_val_temp9_alias,
                            'value': items.avg_val_temp9,
                            'mp_serial_number': items.avg_val_temp9_mp_sn
                        })
                    if items.avg_val_temp10_sn is not None:
                        objects.append({
                            'serial_number': items.avg_val_temp10_sn,
                            'alias': items.avg_val_temp10_alias,
                            'value': items.avg_val_temp10,
                            'mp_serial_number': items.avg_val_temp10_mp_sn
                        })

        else:
            return result(500, "type Error", None, None, "by KSM")


        # [ Case by ] length != 10
        if len(objects) != 10:
            limit_range = 10 - len(objects)
            for i in range(0, limit_range):
                objects.append({
                    'serial_number': 'vertual_key' + str(i),
                    'alias': '',
                    "value": -1,
                    'mp_serial_number': ''
                })
        return result(200, "Get the Report TOP10 List", objects, None, "by KSM")

class ReportMPHistoryAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------

        super(ReportMPHistoryAPI, self).__init__()

    def get(self, serial_number):

        dp = MediaPlayers_History()

        dp_query = "SELECT * FROM %(table_name)s WHERE serial_number = '%(serial_number)s' AND last_updated_date > DATE_ADD(now(), INTERVAL -24 hour) order by last_updated_date" % {
                    "table_name": dp.__tablename__,
                    "serial_number":serial_number
        }

        dp_list = dp.query.from_statement(dp_query).all()

        objects = []

        count = len(dp_list)

        if count == 0:
            return result(404, "No mp history data.", None, None, "by PJH")


        todayDate24 = time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime(time.time()-24*60*60) )
        first_timestamp = time.mktime(time.strptime(json_encoder(todayDate24), '%Y-%m-%dT%H:%M:%S'))

        objects.append({
            "serial_number":dp.serial_number,
            "last_updated_date":first_timestamp,
            "cpu_temp":0,
            "cpu_usage":0,
            "hdd_temp":0,
            "hdd_usage":0,
            "mem_usage":0,
            "fan_speed":0
        })

        for dp in dp_list:
            first_timestamp_item = time.mktime(time.strptime(json_encoder(dp.last_updated_date), '%Y-%m-%dT%H:%M:%S'))
            first_timestamp_item = str(int(first_timestamp_item) - 1)

            objects.append({
                "serial_number":dp.serial_number,
                "last_updated_date":first_timestamp_item,
                "cpu_temp":0,
                "cpu_usage":0,
                "hdd_temp":0,
                "hdd_usage":0,
                "mem_usage":0,
                "fan_speed":0
            })
            break


        pivot = (count / 1000) + 1
        i = pivot

        for dp in dp_list:
            if i == pivot:
                last_timestamp = time.mktime(time.strptime(json_encoder(dp.last_updated_date), '%Y-%m-%dT%H:%M:%S'))
                objects.append({
                    "serial_number":dp.serial_number,
                    "last_updated_date":last_timestamp,
                    "cpu_temp":dp.cpu_temp,
                    "cpu_usage":dp.cpu_usage,
                    "hdd_temp":dp.hdd_temp,
                    "hdd_usage":dp.hdd_usage,
                    "mem_usage":dp.mem_usage,
                    "fan_speed":dp.fan_speed,
                })
                i = 0
                last_timestamp_item = last_timestamp
            else:
                i = i + 1


        last_timestamp_item = str(int(last_timestamp_item) + 1)

        objects.append({
            "serial_number":dp.serial_number,
            "last_updated_date":last_timestamp_item,
            "cpu_temp":0,
            "cpu_usage":0,
            "hdd_temp":0,
            "hdd_usage":0,
            "mem_usage":0,
            "fan_speed":0
        })


        todayDate = time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime(time.time()) )
        last_timestamp = time.mktime(time.strptime(json_encoder(todayDate), '%Y-%m-%dT%H:%M:%S'))

        objects.append({
            "serial_number":dp.serial_number,
            "last_updated_date":last_timestamp,
            "cpu_temp":0,
            "cpu_usage":0,
            "hdd_temp":0,
            "hdd_usage":0,
            "mem_usage":0,
            "fan_speed":0
        })

        return result(200, "Get the Report History data.", objects, None, "by PJH")

class ReportDPHistoryAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------

        super(ReportDPHistoryAPI, self).__init__()

    def get(self, serial_number):
        dp = Displays_History()

        dp_query = "SELECT * FROM %(table_name)s WHERE serial_number = '%(serial_number)s' AND last_updated_date > DATE_ADD(now(), INTERVAL -24 hour) order by last_updated_date" % {
                    "table_name": dp.__tablename__,
                    "serial_number":serial_number
        }

        dp_list = dp.query.from_statement(dp_query).all()

        objects = []

        count = len(dp_list)

        if count == 0:
            return result(404, "No mp history data.", None, None, "by PJH")


        todayDate24 = time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime(time.time()-24*60*60) )
        first_timestamp = time.mktime(time.strptime(json_encoder(todayDate24), '%Y-%m-%dT%H:%M:%S'))

        objects.append({
            "serial_number": dp.serial_number,
            "last_updated_date": first_timestamp,
            "dp_temp": 0
        })

        for dp in dp_list:
            first_timestamp_item = time.mktime(time.strptime(json_encoder(dp.last_updated_date), '%Y-%m-%dT%H:%M:%S'))
            first_timestamp_item = str(int(first_timestamp_item) - 1)

            objects.append({
                "serial_number":dp.serial_number,
                "last_updated_date":first_timestamp_item,
                "dp_temp": 0
            })
            break


        pivot = (count / 1000) + 1
        i = pivot

        for dp in dp_list:
            if i == pivot:
                last_timestamp = time.mktime(time.strptime(json_encoder(dp.last_updated_date), '%Y-%m-%dT%H:%M:%S'))
                objects.append({
                    "serial_number": dp.serial_number,
                    "last_updated_date": last_timestamp,
                    "dp_temp": dp.val_Temp
                })
                i = 0
                last_timestamp_item = last_timestamp
            else:
                i = i + 1


        last_timestamp_item = str(int(last_timestamp_item) + 1)

        objects.append({
            "serial_number": dp.serial_number,
            "last_updated_date": last_timestamp_item,
            "dp_temp": 0
        })


        todayDate = time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime(time.time()) )
        last_timestamp = time.mktime(time.strptime(json_encoder(todayDate), '%Y-%m-%dT%H:%M:%S'))

        objects.append({
            "serial_number": dp.serial_number,
            "last_updated_date": last_timestamp,
            "dp_temp": 0
        })

        return result(200, "Get the Report History data.", objects, None, "by PJH")

class ReportStatsSettingAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        super(ReportStatsSettingAPI, self).__init__()

    def get(self):
        search_target = request.args.get('target')
        param_manager_id = request.args.get('manager_id')
        param_site_name = request.args.get('site_name')
        param_region_name = request.args.get('region_name')
        param_shop_name = request.args.get('shop_name')
        param_dg_name = request.args.get('dg_name')

        if search_target == "manager":
            manager_filter_group = []
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) != 0:
                for mid in manager_ids:
                    manager_filter_group.append(Managers.id == mid.FK_manager_id)
            manager_list = Managers.query.filter_by(isAdmin=False).filter(or_(*manager_filter_group)).all()
            objects = []
            objects.append({
                "id": "All",
                "userID": "All"
            })
            if len(manager_list) != 0:
                for manager in manager_list:
                    objects.append({
                        "id": manager.id,
                        "userID": manager.userID
                    })
            return result(200, "Get the Manager List", objects, None, "by KSM")
        elif search_target == "account":
            account_filter_group = []
            if self.userType is USER_TYPE_ADMIN:
                if param_manager_id != "All":
                    account_filter_group.append(MediaPlayers.FK_managers_id == param_manager_id)
            elif self.userType is USER_TYPE_MANAGER:
                account_filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
            elif self.userType is USER_TYPE_MEMBER:
                group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

                # User doesn't have any device group
                if len(group_ids) is 0:
                    account_filter_group.append(MediaPlayers.id == -1)
                else:
                    for id in group_ids:
                        mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                        if mp_ids is None:
                            account_filter_group.append(MediaPlayers.id == -1)
                        else:
                            for mp_id in mp_ids:
                                account_filter_group.append(MediaPlayers.id == mp_id.id)

            site_list = MediaPlayers.query.filter(or_(*account_filter_group)).group_by(MediaPlayers.site).all()

            objects = []
            objects.append({
                "id": "All",
                "name": "All"
            })

            if len(site_list) != 0:
                for site in site_list:
                    objects.append({
                        "id": site.id,
                        "name": site.site
                    })
            return result(200, "Get the Manager List", objects, None, "by KSM")
        elif search_target == "dg":
            dg_filter_group = []
            if self.userType is USER_TYPE_ADMIN:
                if param_manager_id != "All":
                    dg_filter_group.append(Device_Groups.FK_managers_id == param_manager_id)
            elif self.userType is USER_TYPE_MANAGER:
                dg_filter_group.append(Device_Groups.FK_managers_id == self.user_manager_id)
            elif self.userType is USER_TYPE_MEMBER:
                group_conn_list = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

                # User doesn't have any device group
                if len(group_conn_list) is 0:
                    dg_filter_group.append(Device_Groups.id == -1)
                else:
                    for group_conn in group_conn_list:
                        dg_filter_group.append(Device_Groups.id == group_conn.FK_device_groups_id)

            dg_list = Device_Groups.query.filter(or_(*dg_filter_group)).all()

            objects = []
            objects.append({
                "id": "All",
                "name": "All"
            })

            if len(dg_list) != 0:
                for dg in dg_list:
                    objects.append({
                        "id": dg.id,
                        "name": dg.name
                    })
            return result(200, "Get the Device Group List", objects, None, "by KSM")
        elif search_target == "region":
            location_filter_group = []
            if param_site_name != "All":
                location_filter_group.append(MediaPlayers.site == param_site_name)
            location_list = MediaPlayers.query.filter(or_(*location_filter_group)).group_by(MediaPlayers.location).all()

            objects = []
            objects.append({
                "id": "All",
                "name": "All"
            })

            if len(location_list) != 0:
                for location in location_list:
                    objects.append({
                        "id": location.id,
                        "name": location.location
                    })
            return result(200, "Get the Manager List", objects, None, "by KSM")

        elif search_target == "shop":
            shop_filter_group = []
            if param_region_name != "All":
                shop_filter_group.append(MediaPlayers.location == param_region_name)
            shop_list = MediaPlayers.query.filter(or_(*shop_filter_group)).group_by(MediaPlayers.shop).all()

            objects = []
            objects.append({
                "id": "All",
                "name": "All"
            })

            if len(shop_list) != 0:
                for shop in shop_list:
                    objects.append({
                        "id": shop.id,
                        "name": shop.shop
                    })
            return result(200, "Get the Manager List", objects, None, "by KSM")
        elif search_target == "mp":
            mp_filter_group = []
            if param_shop_name != "All":
                mp_filter_group.append(MediaPlayers.shop == param_shop_name)
            mp_list = MediaPlayers.query.filter(or_(*mp_filter_group)).all()

            objects = []
            objects.append({
                "id": "All",
                "alias": "All",
                "serial_number": "All"
            })

            if len(mp_list) != 0:
                for mp in mp_list:
                    objects.append({
                        "id": mp.id,
                        "alias": mp.alias,
                        "serial_number": mp.serial_number
                    })
            return result(200, "Get the MP List", objects, None, "by KSM")
        elif search_target == "dg_mp":
            dg_mp_filter_group = []
            if param_dg_name != "All":
                dg_mp_filter_group.append(MediaPlayers.dg_name == param_dg_name)
            mp_list = MediaPlayers.query.filter(or_(*dg_mp_filter_group)).all()

            objects = []
            objects.append({
                "id": "All",
                "alias": "All",
                "serial_number": "All"
            })

            if len(mp_list) != 0:
                for mp in mp_list:
                    objects.append({
                        "id": mp.id,
                        "alias": mp.alias,
                        "serial_number": mp.serial_number
                    })
            return result(200, "Get the MP List", objects, None, "by KSM")

class ReportStatsMPAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        super(ReportStatsMPAPI, self).__init__()

    def get(self):
        param = {}
        # type:bool, true or false
        param["device_mp"] = str2bool(request.args.get('device_mp'))

        filter_and_group = []
        filter_or_group = []
        mp_filter_and_group = []
        mp_filter_or_group = []

        if param["device_mp"]:
            # "daily", "weekly", "monthly"
            param["period"] = str(request.args.get('period'))

            param["start_date"] = request.args.get('start_date')
            param["end_date"] = request.args.get('end_date')
            start_date_obj = datetime.strptime(param["start_date"], '%Y-%m-%d')
            end_date_obj = datetime.strptime(param["end_date"], '%Y-%m-%d')

            total_min = 0
            search_start_date = start_date_obj.replace(hour=0, minute=0, second=0, microsecond=0)
            search_end_date = end_date_obj.replace(hour=23, minute=59, second=59, microsecond=999999)

            if param["period"] == "daily":
                if datetime.utcnow().day == search_end_date.day:
                    search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

            elif param["period"] == "weekly":
                if datetime.utcnow().year == search_start_date.year:
                    if datetime.utcnow().month == search_start_date.month:
                        if datetime.utcnow().day >= search_start_date.day and datetime.utcnow().day <= search_end_date.day:
                            search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

            elif param["period"] == "monthly":
                if datetime.utcnow().year == search_start_date.year:
                    if datetime.utcnow().month == search_start_date.month:
                        search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

            filter_and_group.append(DailyMPReports.date>=search_start_date.strftime("%Y-%m-%d %H:%M:%S"))
            filter_and_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
            mp_filter_and_group.append(DailyMPReports.date>=search_start_date.strftime("%Y-%m-%d %H:%M:%S"))
            mp_filter_and_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
            total_min = round((search_end_date - search_start_date).total_seconds() / 60)

            # "account", "dg"
            param["base"] = str(request.args.get('base'))

            if param["base"] == "account":
                # type:int or string id or "all"
                param["manager_id"] = str(request.args.get('manager_id')) if str(request.args.get('manager_id')) == "All" else int(request.args.get('manager_id'))
                if param["manager_id"] != "All":
                    filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
                    mp_filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
                else:
                    if self.userType is USER_TYPE_ADMIN:
                        manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                        if len(manager_ids) is 0:
                            filter_and_group.append(MediaPlayers.id == -1)
                            mp_filter_and_group.append(MediaPlayers.id == -1)
                        # rainmaker
                        #else:
                            #for mid in manager_ids:
                                # filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                                # mp_filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                    elif self.userType is USER_TYPE_MANAGER:
                        filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                        mp_filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                    elif self.userType is USER_TYPE_MEMBER:
                        group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

                        # User doesn't have any device group
                        if len(group_ids) is 0:
                            filter_and_group.append(MediaPlayers.id == -1)
                            mp_filter_and_group.append(MediaPlayers.id == -1)
                        else:
                            for id in group_ids:
                                mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                                if mp_ids is None:
                                    filter_and_group.append(MediaPlayers.id == -1)
                                    mp_filter_and_group.append(MediaPlayers.id == -1)
                                else:
                                    for mp_id in mp_ids:
                                        filter_or_group.append(MediaPlayers.id == mp_id.id)
                                        mp_filter_or_group.append(MediaPlayers.id == mp_id.id)

                # type:string, name or "all"
                param["account"] = str(request.args.get('account'))
                if param["account"] != "All":
                    filter_and_group.append(MediaPlayers.site==param["account"])
                    mp_filter_and_group.append(MediaPlayers.site==param["account"])
                param["region"] = str(request.args.get('region'))
                if param["region"] != "All":
                    filter_and_group.append(MediaPlayers.location==param["region"])
                    mp_filter_and_group.append(MediaPlayers.location==param["region"])
                param["shop"] = str(request.args.get('shop'))
                if param["shop"] != "All":
                    filter_and_group.append(MediaPlayers.shop==param["shop"])
                    mp_filter_and_group.append(MediaPlayers.shop==param["shop"])
            elif param["base"] == "dg":
                param["dg_id"] = str(request.args.get('dg_id')) if str(request.args.get('dg_id')) == "All" else int(request.args.get('dg_id'))
                if param["dg_id"] != "All":
                    filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])
                    mp_filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])

            total_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).count()
            malfunction_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(DailyMPReports.incident_cnt > 0).count()

            malfunction_ratio = 0
            if total_cnt != 0:
                malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

            operation_ratio = 0
            if total_cnt != 0:
                operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

            report_list = db.session.query(DailyMPReports).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).all()

            downtime = 0
            temp_list = []
            if len(report_list) != 0:
                for report in report_list:
                    downtime += report.down_time
                    if report.serial_number is not None and report.incident_cnt > 0:
                        temp_list.append(report.serial_number)

            temp_list = list(set(temp_list))
            malfunction_list = []
            for serial_number in temp_list:
                mp = MediaPlayers.query.filter_by(serial_number=serial_number).one()
                malfunction_list.append({
                    "mp_id": mp.id
                })

            downtime_ratio = 0
            if total_min != 0 and total_cnt != 0:
                downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * 100

            uptime_ratio = 0
            if total_min != 0 and total_cnt != 0:
                uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

            objects = []
            if total_cnt != 0:
                objects.append({
                    "total_cnt": total_cnt,
                    "malfunction_cnt": malfunction_cnt,
                    "malfunction_ratio": malfunction_ratio,
                    "operation_ratio": operation_ratio,
                    "downtime": downtime,
                    "downtime_ratio": downtime_ratio,
                    "uptime_ratio": uptime_ratio,
                    "malfunction_list": malfunction_list
                })
            return result(200, "Get Report Stats MP", objects, None, "by KSM")

class ReportStatsDPAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        super(ReportStatsDPAPI, self).__init__()

    def get(self):
        param = {}
        # type:bool, true or false
        param["device_dp"] = str2bool(request.args.get('device_dp'))

        filter_and_group = []
        filter_or_group = []
        mp_filter_and_group = []
        mp_filter_or_group = []

        if param["device_dp"]:
            # "daily", "weekly", "monthly"
            param["period"] = str(request.args.get('period'))

            param["start_date"] = request.args.get('start_date')
            param["end_date"] = request.args.get('end_date')
            start_date_obj = datetime.strptime(param["start_date"], '%Y-%m-%d')
            end_date_obj = datetime.strptime(param["end_date"], '%Y-%m-%d')

            total_min = 0
            search_start_date = start_date_obj.replace(hour=0, minute=0, second=0, microsecond=0)
            search_end_date = end_date_obj.replace(hour=23, minute=59, second=59, microsecond=999999)

            if param["period"] == "daily":
                if datetime.utcnow().day == search_end_date.day:
                    search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

            elif param["period"] == "weekly":
                if datetime.utcnow().year == search_start_date.year:
                    if datetime.utcnow().month == search_start_date.month:
                        if datetime.utcnow().day >= search_start_date.day and datetime.utcnow().day <= search_end_date.day:
                            search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

            elif param["period"] == "monthly":
                if datetime.utcnow().year == search_start_date.year:
                    if datetime.utcnow().month == search_start_date.month:
                        search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

            filter_and_group.append(DailyDPReports.date>=search_start_date.strftime("%Y-%m-%d %H:%M:%S"))
            filter_and_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
            mp_filter_and_group.append(DailyDPReports.date>=search_start_date.strftime("%Y-%m-%d %H:%M:%S"))
            mp_filter_and_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))

            total_min = round((search_end_date - search_start_date).total_seconds() / 60)

            #"account", "dg"
            param["base"] = str(request.args.get('base'))

            if param["base"] == "account":
                # type:int or string id or "all"
                param["manager_id"] = str(request.args.get('manager_id')) if str(request.args.get('manager_id')) == "All" else int(request.args.get('manager_id'))
                if param["manager_id"] != "All":
                    filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
                    mp_filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
                else:
                    if self.userType is USER_TYPE_ADMIN:
                        manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                        if len(manager_ids) is 0:
                            filter_and_group.append(MediaPlayers.id == -1)
                            mp_filter_and_group.append(MediaPlayers.id == -1)
                        # rainmaker
                        #else:
                            #for mid in manager_ids:
                                #filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                                #mp_filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                    elif self.userType is USER_TYPE_MANAGER:
                        filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                        mp_filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                    elif self.userType is USER_TYPE_MEMBER:
                        group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

                        # User doesn't have any device group
                        if len(group_ids) is 0:
                            filter_and_group.append(MediaPlayers.id == -1)
                            mp_filter_and_group.append(MediaPlayers.id == -1)
                        else:
                            for id in group_ids:
                                mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                                if mp_ids is None:
                                    filter_and_group.append(MediaPlayers.id == -1)
                                    mp_filter_and_group.append(MediaPlayers.id == -1)
                                else:
                                    for mp_id in mp_ids:
                                        filter_or_group.append(MediaPlayers.id == mp_id.id)
                                        mp_filter_or_group.append(MediaPlayers.id == mp_id.id)

                # type:string, name or "all"
                param["account"] = str(request.args.get('account'))
                if param["account"] != "All":
                    filter_and_group.append(MediaPlayers.site==param["account"])
                    mp_filter_and_group.append(MediaPlayers.site==param["account"])
                param["region"] = str(request.args.get('region'))
                if param["region"] != "All":
                    filter_and_group.append(MediaPlayers.location==param["region"])
                    mp_filter_and_group.append(MediaPlayers.location==param["region"])
                param["shop"] = str(request.args.get('shop'))
                if param["shop"] != "All":
                    filter_and_group.append(MediaPlayers.shop==param["shop"])
                    mp_filter_and_group.append(MediaPlayers.shop==param["shop"])
            elif param["base"] == "dg":
                param["dg_id"] = str(request.args.get('dg_id')) if str(request.args.get('dg_id')) == "All" else int(request.args.get('dg_id'))
                if param["dg_id"] != "All":
                    filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])
                    mp_filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])

            total_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).count()
            malfunction_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(DailyDPReports.incident_cnt > 0).count()

            malfunction_ratio = 0
            if total_cnt != 0:
                malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

            operation_ratio = 0
            if total_cnt != 0:
                operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

            report_list = db.session.query(DailyDPReports).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).all()

            downtime = 0
            temp_list = []
            if len(report_list) != 0:
                for report in report_list:
                    downtime += report.down_time
                    if report.serial_number is not None and report.incident_cnt > 0:
                        temp_list.append(report.serial_number)
            temp_list = list(set(temp_list))
            malfunction_list = []
            for serial_number in temp_list:
                dp = Displays.query.filter_by(serial_number=serial_number).one()
                malfunction_list.append({
                    "dp_id": dp.id
                })

            downtime_ratio = 0
            if total_min != 0 and total_cnt != 0:
                downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

            uptime_ratio = 0
            if total_min != 0 and total_cnt != 0:
                uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

            objects = []
            if total_cnt != 0:
                objects.append({
                    "total_cnt": total_cnt,
                    "malfunction_cnt": malfunction_cnt,
                    "malfunction_ratio": malfunction_ratio,
                    "operation_ratio": operation_ratio,
                    "downtime": downtime,
                    "downtime_ratio": downtime_ratio,
                    "uptime_ratio": uptime_ratio,
                    "malfunction_list": malfunction_list
                })

            return result(200, "Get Report Stats MP", objects, None, "by KSM")

class ReportStatsDetailAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        super(ReportStatsDetailAPI, self).__init__()

    def get(self):
        param = {}
        # type:bool, true or false
        param["device_mp"] = str2bool(request.args.get('device_mp'))
        param["device_dp"] = str2bool(request.args.get('device_dp'))

        filter_and_group = []
        filter_or_group = []
        mp_filter_and_group = []
        mp_filter_or_group = []

        mp_type = ""
        dp_type = ""
        if param["device_mp"]:
            mp_type = "MediaPlayer"
        if param["device_dp"]:
            dp_type = "Display"
        #"account", "dg"
        param["base"] = str(request.args.get('base'))

        if param["base"] == "account":
            # type:int or string id or "all"
            param["manager_id"] = str(request.args.get('manager_id')) if str(request.args.get('manager_id')) == "All" else int(request.args.get('manager_id'))
            if param["manager_id"] != "All":
                filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
                mp_filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
            else:
                if self.userType is USER_TYPE_ADMIN:
                    manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                    if len(manager_ids) is 0:
                        filter_and_group.append(MediaPlayers.id == -1)
                        mp_filter_and_group.append(MediaPlayers.id == -1)
                    # rainmaker
                    # else:
                        # for mid in manager_ids:
                            # filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                            # mp_filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                elif self.userType is USER_TYPE_MANAGER:
                    filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                    mp_filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                elif self.userType is USER_TYPE_MEMBER:
                    group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

                    # User doesn't have any device group
                    if len(group_ids) is 0:
                        filter_and_group.append(MediaPlayers.id == -1)
                        mp_filter_and_group.append(MediaPlayers.id == -1)
                    else:
                        for id in group_ids:
                            mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                            if mp_ids is None:
                                filter_and_group.append(MediaPlayers.id == -1)
                                mp_filter_and_group.append(MediaPlayers.id == -1)
                            else:
                                for mp_id in mp_ids:
                                    filter_or_group.append(MediaPlayers.id == mp_id.id)
                                    mp_filter_or_group.append(MediaPlayers.id == mp_id.id)
            # type:string, name or "all"
            param["account"] = str(request.args.get('account'))
            if param["account"] != "All":
                filter_and_group.append(MediaPlayers.site==param["account"])
                mp_filter_and_group.append(MediaPlayers.site==param["account"])
            param["region"] = str(request.args.get('region'))
            if param["region"] != "All":
                filter_and_group.append(MediaPlayers.location==param["region"])
                mp_filter_and_group.append(MediaPlayers.location==param["region"])
            param["shop"] = str(request.args.get('shop'))
            if param["shop"] != "All":
                filter_and_group.append(MediaPlayers.shop==param["shop"])
                mp_filter_and_group.append(MediaPlayers.shop==param["shop"])
        elif param["base"] == "dg":
            param["dg_id"] = str(request.args.get('dg_id')) if str(request.args.get('dg_id')) == "All" else int(request.args.get('dg_id'))
            if param["dg_id"] != "All":
                filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])
                mp_filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])

        # "daily", "weekly", "monthly"
        param["period"] = str(request.args.get('period'))

        param["start_date"] = request.args.get('start_date')
        param["end_date"] = request.args.get('end_date')
        param['excel_create'] = request.args.get('excel_create')
        date = datetime.today()
        date = date.strftime('%Y-%m-%d')

        start_date_obj = datetime.strptime(param["start_date"], '%Y-%m-%d')
        end_date_obj = datetime.strptime(param["end_date"], '%Y-%m-%d')

        total_min = 0
        search_start_date = start_date_obj.replace(hour=0, minute=0, second=0, microsecond=0)
        search_end_date = end_date_obj.replace(hour=23, minute=59, second=59, microsecond=999999)

        if param["period"] == "daily":
            if datetime.utcnow().day == search_end_date.day:
                search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

        elif param["period"] == "weekly":
            if datetime.utcnow().year == search_start_date.year:
                if datetime.utcnow().month == search_start_date.month:
                    if datetime.utcnow().day >= search_start_date.day and datetime.utcnow().day <= search_end_date.day:
                        search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

        elif param["period"] == "monthly":
            if datetime.utcnow().year == search_start_date.year:
                if datetime.utcnow().month == search_start_date.month:
                    search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

        if param["period"] == "daily":
            # rainmaker
            daily_list = range(1, end_date_obj.day+1)
            #daily_list = range(end_date_obj.day, end_date_obj.day+1)

            objects = []
            excel_object = []

            for day in daily_list:
                daily_mp_filter_group = []
                daily_dp_filter_group = []
                daily_start = search_start_date.replace(day=day, hour=0, minute=0, second=0, microsecond=0)
                daily_end = search_end_date.replace(day=day, hour=23, minute=59, second=59, microsecond=999999)

                daily_mp_filter_group.append(DailyMPReports.date>=daily_start.strftime("%Y-%m-%d %H:%M:%S"))
                daily_dp_filter_group.append(DailyDPReports.date>=daily_start.strftime("%Y-%m-%d %H:%M:%S"))

                if day < search_end_date.day:
                    daily_mp_filter_group.append(DailyMPReports.date<=daily_end.strftime("%Y-%m-%d %H:%M:%S"))
                    daily_dp_filter_group.append(DailyDPReports.date<=daily_end.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((daily_end - daily_start).total_seconds() / 60)
                    final_daily_end = daily_end
                else:
                    daily_mp_filter_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    daily_dp_filter_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((search_end_date - daily_start).total_seconds() / 60)
                    final_daily_end = search_end_date

                mp_total_cnt = 0
                mp_malfunction_cnt = 0
                mp_malfunction_ratio = 0
                mp_operation_ratio = 0
                mp_downtime = 0
                mp_downtime_ratio = 0
                mp_uptime_ratio = 0
                mp_malfunction_list = []

                if param["device_mp"]:
                    total_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*daily_mp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_mp_filter_group)).filter(DailyMPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyMPReports).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_mp_filter_group)).all()
                    downtime = 0
                    temp_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time
                            if report.serial_number is not None and report.incident_cnt > 0:
                                temp_list.append(report.serial_number)

                    temp_list = list(set(temp_list))
                    malfunction_list = []
                    for serial_number in temp_list:
                        mp = MediaPlayers.query.filter_by(serial_number=serial_number).one()
                        malfunction_list.append({
                            "mp_id": mp.id
                        })

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    mp_total_cnt = total_cnt
                    mp_malfunction_cnt = malfunction_cnt
                    mp_malfunction_ratio = malfunction_ratio
                    mp_operation_ratio = operation_ratio
                    mp_downtime = downtime
                    mp_downtime_ratio = downtime_ratio
                    mp_uptime_ratio = uptime_ratio
                    mp_malfunction_list = malfunction_list

                dp_total_cnt = 0
                dp_malfunction_cnt = 0
                dp_malfunction_ratio = 0
                dp_operation_ratio = 0
                dp_downtime = 0
                dp_downtime_ratio = 0
                dp_uptime_ratio = 0
                dp_malfunction_list = []

                if param["device_dp"]:
                    total_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*daily_dp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_dp_filter_group)).filter(DailyDPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyDPReports).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_dp_filter_group)).all()
                    downtime = 0
                    temp_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time
                            if report.serial_number is not None and report.incident_cnt > 0:
                                temp_list.append(report.serial_number)
                    temp_list = list(set(temp_list))
                    malfunction_list = []
                    for serial_number in temp_list:
                        dp = Displays.query.filter_by(serial_number=serial_number).one()
                        malfunction_list.append({
                            "dp_id": dp.id
                        })

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    dp_total_cnt = total_cnt
                    dp_malfunction_cnt = malfunction_cnt
                    dp_malfunction_ratio = malfunction_ratio
                    dp_operation_ratio = operation_ratio
                    dp_downtime = downtime
                    dp_downtime_ratio = downtime_ratio
                    dp_uptime_ratio = uptime_ratio
                    dp_malfunction_list = malfunction_list

                objects.append({
                    "search_type": param["period"],
                    "start_date": json_encoder(daily_start.strftime("%Y-%m-%d")),
                    "end_date": json_encoder(final_daily_end.strftime("%Y-%m-%d")),
                    "mp_type": mp_type,
                    "mp_total_cnt": mp_total_cnt,
                    "mp_malfunction_cnt": mp_malfunction_cnt,
                    "mp_operation_ratio": mp_operation_ratio,
                    "mp_malfunction_ratio": mp_malfunction_ratio,
                    "mp_uptime_ratio": mp_uptime_ratio,
                    "mp_downtime_ratio": mp_downtime_ratio,
                    "mp_downtime": mp_downtime,
                    "mp_malfunction_list": mp_malfunction_list,
                    "dp_type": dp_type,
                    "dp_total_cnt": dp_total_cnt,
                    "dp_malfunction_cnt": dp_malfunction_cnt,
                    "dp_operation_ratio": dp_operation_ratio,
                    "dp_malfunction_ratio": dp_malfunction_ratio,
                    "dp_uptime_ratio": dp_uptime_ratio,
                    "dp_downtime_ratio": dp_downtime_ratio,
                    "dp_downtime": dp_downtime,
                    "dp_malfunction_list": dp_malfunction_list,
                    "userID": self.userID,
                    "create_date": date
                })

                excel_object.append({
                    "Day": json_encoder(daily_start.strftime("%Y-%m-%d")),
                    "Account": param["account"],
                    "Location": param["region"],
                    "Shop": param["shop"],
                    "Type": mp_type,
                    "Total": mp_total_cnt,
                    "Operation Ratio":  mp_operation_ratio,
                    "Uptime Ratio": mp_uptime_ratio,
                    "Malfunction Devices": mp_malfunction_cnt,
                    "Downtime (min)": mp_downtime
                })
                excel_object.append(
                {
                    "Day": json_encoder(daily_start.strftime("%Y-%m-%d")),
                    "Account": param["account"],
                    "Location": param["region"],
                    "Shop": param["shop"],
                    "Type": dp_type,
                    "Total": dp_total_cnt,
                    "Operation Ratio":  dp_operation_ratio,
                    "Uptime Ratio": dp_uptime_ratio,
                    "Malfunction Devices": dp_malfunction_cnt,
                    "Downtime (min)": dp_downtime
                })

            if param['excel_create'] == 'true':

                date = datetime.today()
                date = date.strftime('%Y-%m-%d')

                if not os.path.exists('../download/excel/' + self.userID):
                    os.makedirs('../download/excel/' + self.userID)

                workbook = xlsxwriter.Workbook('../download/excel/' + self.userID + '/Statistics-' + date + '.xlsx')
                ordering = {'Day': 0, 'Manager': 1, 'Account': 2, 'Location': 3, 'Shop': 4, 'Type': 5, 'Total': 6, 'Operation Ratio': 7, 'Uptime Ratio': 8, 'Malfunction Devices': 9, 'Downtime (min)': 10}

                worksheet = workbook.add_worksheet()
                worksheet.set_column(0, 0, 15)
                worksheet.set_column(0, 4, 14)
                worksheet.set_column(6, 7, 15)
                worksheet.set_column(8, 9, 18)
                head = workbook.add_format({'bold': True, 'font_color': '#000000', 'bg_color': '#999999', 'align': 'center'})
                body = workbook.add_format({'align': 'center'})

                col = 0
                for data in excel_object:
                    data = sorted(data, key=ordering.__getitem__)
                    for k in data:
                        if col < len(data):
                            worksheet.write(0, col, k, head)
                            col += 1

                row = 1
                col = 0
                for data in excel_object:
                    data = [data[i] for i in sorted(data, key=ordering.__getitem__)]
                    for v in data:
                        if col < len(data):
                            worksheet.write(row, col, v, body)
                            col += 1
                    row += 1
                    col = 0

                workbook.close()

            return result(200, "Get Report Stats Daily", objects, None, "by KSM")

        elif param["period"] == "weekly":
            # rainmaker
            '''
            week_day = 1
            weekly_list = []
            weekly_list.append(week_day)#add 1 day
            while week_day <= search_end_date.day:
                this_week_day = search_end_date.replace(day=week_day)
                if this_week_day.weekday() == 0:
                    weekly_list.append(week_day)
                week_day = week_day + 1
            '''
            weekly_list = []
            weekly_list.append(search_start_date.day)

            objects = []
            excel_object = []

            for week in weekly_list:
                weekly_mp_filter_group = []
                weekly_dp_filter_group = []

                weekly_start = search_start_date.replace(day=week, hour=0, minute=0, second=0, microsecond=0)
                weekly_end = (weekly_start + timedelta(days = 6-weekly_start.weekday())).replace(hour=23, minute=59, second=59, microsecond=999999)

                weekly_mp_filter_group.append(DailyMPReports.date>=weekly_start.strftime("%Y-%m-%d %H:%M:%S"))
                weekly_dp_filter_group.append(DailyDPReports.date>=weekly_start.strftime("%Y-%m-%d %H:%M:%S"))

                if search_end_date.month == weekly_end.month:
                    if search_end_date.day < weekly_end.day:
                        weekly_mp_filter_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                        weekly_dp_filter_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                        total_min = round((search_end_date - weekly_start).total_seconds() / 60)
                        this_week_end_date = search_end_date
                    else:
                        weekly_mp_filter_group.append(DailyMPReports.date<=weekly_end.strftime("%Y-%m-%d %H:%M:%S"))
                        weekly_dp_filter_group.append(DailyDPReports.date<=weekly_end.strftime("%Y-%m-%d %H:%M:%S"))
                        total_min = round((weekly_end - weekly_start).total_seconds() / 60)
                        this_week_end_date = weekly_end
                elif search_end_date.month != weekly_end.month:
                    weekly_mp_filter_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    weekly_dp_filter_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((search_end_date - weekly_start).total_seconds() / 60)
                    this_week_end_date = search_end_date

                mp_total_cnt = 0
                mp_malfunction_cnt = 0
                mp_malfunction_ratio = 0
                mp_operation_ratio = 0
                mp_downtime = 0
                mp_downtime_ratio = 0
                mp_uptime_ratio = 0
                mp_malfunction_list = []

                if param["device_mp"]:
                    total_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*weekly_mp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_mp_filter_group)).filter(DailyMPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyMPReports).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_mp_filter_group)).all()
                    downtime = 0
                    temp_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time
                            if report.serial_number is not None and report.incident_cnt > 0:
                                temp_list.append(report.serial_number)

                    temp_list = list(set(temp_list))
                    malfunction_list = []
                    for serial_number in temp_list:
                        mp = MediaPlayers.query.filter_by(serial_number=serial_number).one()
                        malfunction_list.append({
                            "mp_id": mp.id
                        })

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    mp_total_cnt = total_cnt
                    mp_malfunction_cnt = malfunction_cnt
                    mp_malfunction_ratio = malfunction_ratio
                    mp_operation_ratio = operation_ratio
                    mp_downtime = downtime
                    mp_downtime_ratio = downtime_ratio
                    mp_uptime_ratio = uptime_ratio
                    mp_malfunction_list = malfunction_list

                dp_total_cnt = 0
                dp_malfunction_cnt = 0
                dp_malfunction_ratio = 0
                dp_operation_ratio = 0
                dp_downtime = 0
                dp_downtime_ratio = 0
                dp_uptime_ratio = 0
                dp_malfunction_list = []

                if param["device_dp"]:
                    total_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*weekly_dp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_dp_filter_group)).filter(DailyDPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyDPReports).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_dp_filter_group)).all()
                    downtime = 0
                    temp_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time
                            if report.serial_number is not None and report.incident_cnt > 0:
                                temp_list.append(report.serial_number)
                    temp_list = list(set(temp_list))
                    malfunction_list = []
                    for serial_number in temp_list:
                        dp = Displays.query.filter_by(serial_number=serial_number).one()
                        malfunction_list.append({
                            "dp_id": dp.id
                        })

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    dp_total_cnt = total_cnt
                    dp_malfunction_cnt = malfunction_cnt
                    dp_malfunction_ratio = malfunction_ratio
                    dp_operation_ratio = operation_ratio
                    dp_downtime = downtime
                    dp_downtime_ratio = downtime_ratio
                    dp_uptime_ratio = uptime_ratio
                    dp_malfunction_list = malfunction_list

                objects.append({
                    "search_type": param["period"],
                    "start_date": json_encoder(weekly_start.strftime("%Y-%m-%d")),
                    "end_date": json_encoder(this_week_end_date.strftime("%Y-%m-%d")),
                    "mp_type": mp_type,
                    "mp_total_cnt": mp_total_cnt,
                    "mp_malfunction_cnt": mp_malfunction_cnt,
                    "mp_operation_ratio": mp_operation_ratio,
                    "mp_malfunction_ratio": mp_malfunction_ratio,
                    "mp_uptime_ratio": mp_uptime_ratio,
                    "mp_downtime_ratio": mp_downtime_ratio,
                    "mp_downtime": mp_downtime,
                    "mp_malfunction_list": mp_malfunction_list,
                    "dp_type": dp_type,
                    "dp_total_cnt": dp_total_cnt,
                    "dp_malfunction_cnt": dp_malfunction_cnt,
                    "dp_operation_ratio": dp_operation_ratio,
                    "dp_malfunction_ratio": dp_malfunction_ratio,
                    "dp_uptime_ratio": dp_uptime_ratio,
                    "dp_downtime_ratio": dp_downtime_ratio,
                    "dp_downtime": dp_downtime,
                    "dp_malfunction_list": dp_malfunction_list,
                    "userID": self.userID,
                    "create_date": date
                })

                excel_object.append({
                    "Week ": json_encoder(weekly_start.strftime("%Y-%m-%d")) + '~' + json_encoder(this_week_end_date.strftime("%Y-%m-%d")),
                    "Account": param["account"],
                    "Location": param["region"],
                    "Shop": param["shop"],
                    "Type": mp_type,
                    "Total": mp_total_cnt,
                    "Operation Ratio":  mp_operation_ratio,
                    "Uptime Ratio": mp_uptime_ratio,
                    "Malfunction Devices": mp_malfunction_cnt,
                    "Downtime (min)": mp_downtime
                })
                excel_object.append(
                {
                    "Week ": json_encoder(weekly_start.strftime("%Y-%m-%d")) + '~' + json_encoder(this_week_end_date.strftime("%Y-%m-%d")),
                    "Account": param["account"],
                    "Location": param["region"],
                    "Shop": param["shop"],
                    "Type": dp_type,
                    "Total": dp_total_cnt,
                    "Operation Ratio":  dp_operation_ratio,
                    "Uptime Ratio": dp_uptime_ratio,
                    "Malfunction Devices": dp_malfunction_cnt,
                    "Downtime (min)": dp_downtime
                })

            if param['excel_create'] == 'true':

                date = datetime.today()
                date = date.strftime('%Y-%m-%d')

                if not os.path.exists('../download/excel/' + self.userID):
                    os.makedirs('../download/excel/' + self.userID)

                workbook = xlsxwriter.Workbook('../download/excel/' + self.userID + '/Statistics-' + date + '.xlsx')
                ordering = {'Week ': 0, 'Manager': 1, 'Account': 2, 'Location': 3, 'Shop': 4, 'Type': 5, 'Total': 6, 'Operation Ratio': 7, 'Uptime Ratio': 8, 'Malfunction Devices': 9, 'Downtime (min)': 10}

                worksheet = workbook.add_worksheet()
                worksheet.set_column(0, 0, 15)
                worksheet.set_column(0, 4, 14)
                worksheet.set_column(6, 7, 15)
                worksheet.set_column(8, 9, 18)
                head = workbook.add_format({'bold': True, 'font_color': '#000000', 'bg_color': '#999999', 'align': 'center'})
                body = workbook.add_format({'align': 'center'})

                col = 0
                for data in excel_object:
                    data = sorted(data, key=ordering.__getitem__)
                    for k in data:
                        if col < len(data):
                            worksheet.write(0, col, k, head)
                            col += 1

                row = 1
                col = 0
                for data in excel_object:
                    data = [data[i] for i in sorted(data, key=ordering.__getitem__)]
                    for v in data:
                        if col < len(data):
                            worksheet.write(row, col, v, body)
                            col += 1
                    row += 1
                    col = 0

                workbook.close()

            return result(200, "Get Report Stats Weekly", objects, None, "by KSM")
        elif param["period"] == "monthly":
            # rainmaker
            # monthly_list = range(1, search_start_date.month+1)
            monthly_list = range(search_start_date.month, search_start_date.month+1)

            objects = []
            excel_object = []

            for month in monthly_list:
                monthly_mp_filter_group = []
                monthly_dp_filter_group = []
                monthly_start = search_start_date.replace(month=month, day=1, hour=0, minute=0, second=0, microsecond=0)
                monthly_mp_filter_group.append(DailyMPReports.date>=monthly_start.strftime("%Y-%m-%d %H:%M:%S"))
                monthly_dp_filter_group.append(DailyDPReports.date>=monthly_start.strftime("%Y-%m-%d %H:%M:%S"))

                if month != 12:
                    monthly_end = search_end_date.replace(year=search_end_date.year, month=month + 1, day=1, hour=0, minute=0, second=0, microsecond=0) - timedelta(days = 1)
                elif month == 12:
                    monthly_end = search_end_date.replace(year=search_end_date.year+1, month=1, day=1, hour=23, minute=59, second=59, microsecond=999999) - timedelta(days = 1)

                if monthly_end.month < search_end_date.month:
                    monthly_mp_filter_group.append(DailyMPReports.date<=monthly_end.strftime("%Y-%m-%d %H:%M:%S"))
                    monthly_dp_filter_group.append(DailyDPReports.date<=monthly_end.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((monthly_end - monthly_start).total_seconds() / 60)
                    final_monthly_end = monthly_end
                else:
                    monthly_mp_filter_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    monthly_dp_filter_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((search_end_date - monthly_start).total_seconds() / 60)
                    final_monthly_end = search_end_date

                mp_total_cnt = 0
                mp_malfunction_cnt = 0
                mp_malfunction_ratio = 0
                mp_operation_ratio = 0
                mp_downtime = 0
                mp_downtime_ratio = 0
                mp_uptime_ratio = 0
                mp_malfunction_list = []

                if param["device_mp"]:
                    total_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*monthly_mp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyMPReports.serial_number).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*monthly_mp_filter_group)).filter(DailyMPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyMPReports).distinct().join(MediaPlayers, DailyMPReports.serial_number == MediaPlayers.serial_number).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*monthly_mp_filter_group)).all()
                    downtime = 0
                    temp_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time
                            if report.serial_number is not None and report.incident_cnt > 0:
                                temp_list.append(report.serial_number)

                    temp_list = list(set(temp_list))
                    malfunction_list = []
                    for serial_number in temp_list:
                        mp = MediaPlayers.query.filter_by(serial_number=serial_number).one()
                        malfunction_list.append({
                            "mp_id": mp.id
                        })

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    mp_total_cnt = total_cnt
                    mp_malfunction_cnt = malfunction_cnt
                    mp_malfunction_ratio = malfunction_ratio
                    mp_operation_ratio = operation_ratio
                    mp_downtime = downtime
                    mp_downtime_ratio = downtime_ratio
                    mp_uptime_ratio = uptime_ratio
                    mp_malfunction_list = malfunction_list

                dp_total_cnt = 0
                dp_malfunction_cnt = 0
                dp_malfunction_ratio = 0
                dp_operation_ratio = 0
                dp_downtime = 0
                dp_downtime_ratio = 0
                dp_uptime_ratio = 0
                dp_malfunction_list = []

                if param["device_dp"]:
                    total_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*monthly_dp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyDPReports.serial_number).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*monthly_dp_filter_group)).filter(DailyDPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyDPReports).distinct().join(Displays, DailyDPReports.serial_number == Displays.serial_number).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*monthly_dp_filter_group)).all()
                    downtime = 0
                    temp_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time
                            if report.serial_number is not None and report.incident_cnt > 0:
                                temp_list.append(report.serial_number)
                    temp_list = list(set(temp_list))
                    malfunction_list = []
                    for serial_number in temp_list:
                        dp = Displays.query.filter_by(serial_number=serial_number).one()
                        malfunction_list.append({
                            "dp_id": dp.id
                        })

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    dp_total_cnt = total_cnt
                    dp_malfunction_cnt = malfunction_cnt
                    dp_malfunction_ratio = malfunction_ratio
                    dp_operation_ratio = operation_ratio
                    dp_downtime = downtime
                    dp_downtime_ratio = downtime_ratio
                    dp_uptime_ratio = uptime_ratio
                    dp_malfunction_list = malfunction_list

                objects.append({
                    "search_type": param["period"],
                    "start_date": json_encoder(monthly_start.strftime("%Y-%m-%d")),
                    "end_date": json_encoder(final_monthly_end.strftime("%Y-%m-%d")),
                    "view_start_date": json_encoder(monthly_start.strftime("%Y-%m")),
                    "mp_type": mp_type,
                    "mp_total_cnt": mp_total_cnt,
                    "mp_malfunction_cnt": mp_malfunction_cnt,
                    "mp_operation_ratio": mp_operation_ratio,
                    "mp_malfunction_ratio": mp_malfunction_ratio,
                    "mp_uptime_ratio": mp_uptime_ratio,
                    "mp_downtime_ratio": mp_downtime_ratio,
                    "mp_downtime": mp_downtime,
                    "mp_malfunction_list": mp_malfunction_list,
                    "dp_type": dp_type,
                    "dp_total_cnt": dp_total_cnt,
                    "dp_malfunction_cnt": dp_malfunction_cnt,
                    "dp_operation_ratio": dp_operation_ratio,
                    "dp_malfunction_ratio": dp_malfunction_ratio,
                    "dp_uptime_ratio": dp_uptime_ratio,
                    "dp_downtime_ratio": dp_downtime_ratio,
                    "dp_downtime": dp_downtime,
                    "dp_malfunction_list": dp_malfunction_list,
                    "userID": self.userID,
                    "create_date": date
                })

                excel_object.append({
                    "Month ": json_encoder(monthly_start.strftime("%Y-%m-%d")),
                    "Account": param["account"],
                    "Location": param["region"],
                    "Shop": param["shop"],
                    "Type": mp_type,
                    "Total": mp_total_cnt,
                    "Operation Ratio":  mp_operation_ratio,
                    "Uptime Ratio": mp_uptime_ratio,
                    "Malfunction Devices": mp_malfunction_cnt,
                    "Downtime (min)": mp_downtime
                })
                excel_object.append(
                {
                    "Month ": json_encoder(monthly_start.strftime("%Y-%m-%d")),
                    "Account": param["account"],
                    "Location": param["region"],
                    "Shop": param["shop"],
                    "Type": dp_type,
                    "Total": dp_total_cnt,
                    "Operation Ratio":  dp_operation_ratio,
                    "Uptime Ratio": dp_uptime_ratio,
                    "Malfunction Devices": dp_malfunction_cnt,
                    "Downtime (min)": dp_downtime
                })

            if param['excel_create'] == 'true':

                date = datetime.today()
                date = date.strftime('%Y-%m-%d')

                if not os.path.exists('../download/excel/' + self.userID):
                    os.makedirs('../download/excel/' + self.userID)

                workbook = xlsxwriter.Workbook('../download/excel/' + self.userID + '/Statistics-' + date + '.xlsx')
                ordering = {'Month ': 0, 'Manager': 1, 'Account': 2, 'Location': 3, 'Shop': 4, 'Type': 5, 'Total': 6, 'Operation Ratio': 7, 'Uptime Ratio': 8, 'Malfunction Devices': 9, 'Downtime (min)': 10}

                worksheet = workbook.add_worksheet()
                worksheet.set_column(0, 0, 15)
                worksheet.set_column(0, 4, 14)
                worksheet.set_column(6, 7, 15)
                worksheet.set_column(8, 9, 18)
                head = workbook.add_format({'bold': True, 'font_color': '#000000', 'bg_color': '#999999', 'align': 'center'})
                body = workbook.add_format({'align': 'center'})

                col = 0
                for data in excel_object:
                    data = sorted(data, key=ordering.__getitem__)
                    for k in data:
                        if col < len(data):
                            worksheet.write(0, col, k, head)
                            col += 1

                row = 1
                col = 0
                for data in excel_object:
                    data = [data[i] for i in sorted(data, key=ordering.__getitem__)]
                    for v in data:
                        if col < len(data):
                            worksheet.write(row, col, v, body)
                            col += 1
                    row += 1
                    col = 0

                workbook.close()

            return result(200, "Get Report Stats Monthly", objects, None, "by KSM")

class ReportStatsRatioAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        super(ReportStatsRatioAPI, self).__init__()

    def get(self):
        param = {}
        # type:bool, true or false
        param["device_mp"] = str2bool(request.args.get('device_mp'))
        param["device_dp"] = str2bool(request.args.get('device_dp'))

        filter_and_group = []
        filter_or_group = []
        mp_filter_and_group = []
        mp_filter_or_group = []

        mp_type = ""
        dp_type = ""
        if param["device_mp"]:
            mp_type = "MediaPlayer"
        if param["device_dp"]:
            dp_type = "Display"
        #"account", "dg"
        param["base"] = str(request.args.get('base'))

        if param["base"] == "account":
            # type:int or string id or "all"
            param["manager_id"] = str(request.args.get('manager_id')) if str(request.args.get('manager_id')) == "All" else int(request.args.get('manager_id'))
            if param["manager_id"] != "All":
                filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
                mp_filter_and_group.append(MediaPlayers.FK_managers_id==param["manager_id"])
            else:
                if self.userType is USER_TYPE_ADMIN:
                    manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                    if len(manager_ids) is 0:
                        filter_and_group.append(MediaPlayers.id == -1)
                        mp_filter_and_group.append(MediaPlayers.id == -1)
                    #rainmaker
                    #else:
                        #for mid in manager_ids:
                            #filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                            #mp_filter_or_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
                elif self.userType is USER_TYPE_MANAGER:
                    filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                    mp_filter_and_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
                elif self.userType is USER_TYPE_MEMBER:
                    group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

                    # User doesn't have any device group
                    if len(group_ids) is 0:
                        filter_and_group.append(MediaPlayers.id == -1)
                        mp_filter_and_group.append(MediaPlayers.id == -1)
                    else:
                        for id in group_ids:
                            mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                            if mp_ids is None:
                                filter_and_group.append(MediaPlayers.id == -1)
                                mp_filter_and_group.append(MediaPlayers.id == -1)
                            else:
                                for mp_id in mp_ids:
                                    filter_or_group.append(MediaPlayers.id == mp_id.id)
                                    mp_filter_or_group.append(MediaPlayers.id == mp_id.id)
            # type:string, name or "all"
            param["account"] = str(request.args.get('account'))
            if param["account"] != "All":
                filter_and_group.append(MediaPlayers.site==param["account"])
                mp_filter_and_group.append(MediaPlayers.site==param["account"])
            param["region"] = str(request.args.get('region'))
            if param["region"] != "All":
                filter_and_group.append(MediaPlayers.location==param["region"])
                mp_filter_and_group.append(MediaPlayers.location==param["region"])
            param["shop"] = str(request.args.get('shop'))
            if param["shop"] != "All":
                filter_and_group.append(MediaPlayers.shop==param["shop"])
                mp_filter_and_group.append(MediaPlayers.shop==param["shop"])
        elif param["base"] == "dg":
            param["dg_id"] = str(request.args.get('dg_id')) if str(request.args.get('dg_id')) == "All" else int(request.args.get('dg_id'))
            if param["dg_id"] != "All":
                filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])
                mp_filter_and_group.append(MediaPlayers.FK_device_groups_id==param["dg_id"])

        # "daily", "weekly", "monthly"
        param["period"] = str(request.args.get('period'))

        param["start_date"] = request.args.get('start_date')
        param["end_date"] = request.args.get('end_date')
        start_date_obj = datetime.strptime(param["start_date"], '%Y-%m-%d')
        end_date_obj = datetime.strptime(param["end_date"], '%Y-%m-%d')

        total_min = 0
        search_start_date = start_date_obj.replace(hour=0, minute=0, second=0, microsecond=0)
        search_end_date = end_date_obj.replace(hour=23, minute=59, second=59, microsecond=999999)

        if param["period"] == "weekly":
            if datetime.utcnow().day == search_end_date.day:
                search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

        elif param["period"] == "monthly":
            if datetime.utcnow().year == search_start_date.year:
                if datetime.utcnow().month == search_start_date.month:
                    if datetime.utcnow().day >= search_start_date.day and datetime.utcnow().day <= search_end_date.day:
                        search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)
            #if datetime.utcnow().year == search_start_date.year:
            #    if datetime.utcnow().month == search_start_date.month:
            #        search_end_date = end_date_obj.replace(hour=datetime.utcnow().hour, minute=datetime.utcnow().minute, second=datetime.utcnow().second, microsecond=datetime.utcnow().microsecond)

        if param["period"] == "weekly":
            daily_list = range(start_date_obj.day, end_date_obj.day+1)

            objects = []
            for day in daily_list:
                daily_mp_filter_group = []
                daily_dp_filter_group = []
                daily_start = search_start_date.replace(day=day, hour=0, minute=0, second=0, microsecond=0)
                daily_end = search_end_date.replace(day=day, hour=23, minute=59, second=59, microsecond=999999)

                daily_mp_filter_group.append(DailyMPReports.date>=daily_start.strftime("%Y-%m-%d %H:%M:%S"))
                daily_dp_filter_group.append(DailyDPReports.date>=daily_start.strftime("%Y-%m-%d %H:%M:%S"))

                if day < search_end_date.day:
                    daily_mp_filter_group.append(DailyMPReports.date<=daily_end.strftime("%Y-%m-%d %H:%M:%S"))
                    daily_dp_filter_group.append(DailyDPReports.date<=daily_end.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((daily_end - daily_start).total_seconds() / 60)
                    final_daily_end = daily_end
                else:
                    daily_mp_filter_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    daily_dp_filter_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((search_end_date - daily_start).total_seconds() / 60)
                    final_daily_end = search_end_date

                mp_total_cnt = 0
                mp_malfunction_cnt = 0
                mp_malfunction_ratio = 0
                mp_operation_ratio = 0
                mp_downtime = 0
                mp_downtime_ratio = 0
                mp_uptime_ratio = 0
                mp_malfunction_list = []

                if param["device_mp"]:
                    total_cnt = db.session.query(DailyMPReports.serial_number).distinct().filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*daily_mp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyMPReports.serial_number).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_mp_filter_group)).filter(DailyMPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyMPReports).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_mp_filter_group)).all()
                    downtime = 0
                    malfunction_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    mp_total_cnt = total_cnt
                    mp_malfunction_cnt = malfunction_cnt
                    mp_malfunction_ratio = malfunction_ratio
                    mp_operation_ratio = operation_ratio
                    mp_downtime = downtime
                    mp_downtime_ratio = downtime_ratio
                    mp_uptime_ratio = uptime_ratio
                    mp_malfunction_list = malfunction_list

                dp_total_cnt = 0
                dp_malfunction_cnt = 0
                dp_malfunction_ratio = 0
                dp_operation_ratio = 0
                dp_downtime = 0
                dp_downtime_ratio = 0
                dp_uptime_ratio = 0
                dp_malfunction_list = []

                if param["device_dp"]:
                    total_cnt = db.session.query(DailyDPReports.serial_number).distinct().filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*daily_dp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyDPReports.serial_number).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_dp_filter_group)).filter(DailyDPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyDPReports).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*daily_dp_filter_group)).all()
                    downtime = 0
                    malfunction_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    dp_total_cnt = total_cnt
                    dp_malfunction_cnt = malfunction_cnt
                    dp_malfunction_ratio = malfunction_ratio
                    dp_operation_ratio = operation_ratio
                    dp_downtime = downtime
                    dp_downtime_ratio = downtime_ratio
                    dp_uptime_ratio = uptime_ratio
                    dp_malfunction_list = malfunction_list

                objects.append({
                    "search_type": param["period"],
                    "start_date": json_encoder(daily_start.strftime("%Y-%m-%d")),
                    "end_date": json_encoder(final_daily_end.strftime("%Y-%m-%d")),
                    "week_date": json_encoder(daily_start.strftime("%d")),
                    "week_day": daily_start.weekday(),
                    "mp_type": mp_type,
                    "mp_total_cnt": mp_total_cnt,
                    "mp_malfunction_cnt": mp_malfunction_cnt,
                    "mp_operation_ratio": mp_operation_ratio,
                    "mp_malfunction_ratio": mp_malfunction_ratio,
                    "mp_uptime_ratio": mp_uptime_ratio,
                    "mp_downtime_ratio": mp_downtime_ratio,
                    "mp_downtime": mp_downtime,
                    "mp_malfunction_list": mp_malfunction_list,
                    "dp_type": dp_type,
                    "dp_total_cnt": dp_total_cnt,
                    "dp_malfunction_cnt": dp_malfunction_cnt,
                    "dp_operation_ratio": dp_operation_ratio,
                    "dp_malfunction_ratio": dp_malfunction_ratio,
                    "dp_uptime_ratio": dp_uptime_ratio,
                    "dp_downtime_ratio": dp_downtime_ratio,
                    "dp_downtime": dp_downtime,
                    "dp_malfunction_list": dp_malfunction_list
                })
            return result(200, "Get Report Ratio Weekly", objects, None, "by KSM")

        elif param["period"] == "monthly":
            week_day = 1
            weekly_list = []
            weekly_list.append(week_day)#add 1 day
            while week_day <= search_end_date.day:
                this_week_day = search_end_date.replace(day=week_day)
                if this_week_day.weekday() == 0:
                    weekly_list.append(week_day)
                week_day = week_day + 1

            objects = []
            for week in weekly_list:
                weekly_mp_filter_group = []
                weekly_dp_filter_group = []

                weekly_start = search_start_date.replace(day=week, hour=0, minute=0, second=0, microsecond=0)
                weekly_end = (weekly_start + timedelta(days = 6-weekly_start.weekday())).replace(hour=23, minute=59, second=59, microsecond=999999)

                weekly_mp_filter_group.append(DailyMPReports.date>=weekly_start.strftime("%Y-%m-%d %H:%M:%S"))
                weekly_dp_filter_group.append(DailyDPReports.date>=weekly_start.strftime("%Y-%m-%d %H:%M:%S"))
                if search_end_date.month == weekly_end.month:
                    if search_end_date.day < weekly_end.day:
                        weekly_mp_filter_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                        weekly_dp_filter_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                        total_min = round((search_end_date - weekly_start).total_seconds() / 60)
                        this_week_end_date = search_end_date
                    else:
                        weekly_mp_filter_group.append(DailyMPReports.date<=weekly_end.strftime("%Y-%m-%d %H:%M:%S"))
                        weekly_dp_filter_group.append(DailyDPReports.date<=weekly_end.strftime("%Y-%m-%d %H:%M:%S"))
                        total_min = round((weekly_end - weekly_start).total_seconds() / 60)
                        this_week_end_date = weekly_end
                elif search_end_date.month != weekly_end.month:
                    weekly_mp_filter_group.append(DailyMPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    weekly_dp_filter_group.append(DailyDPReports.date<=search_end_date.strftime("%Y-%m-%d %H:%M:%S"))
                    total_min = round((search_end_date - weekly_start).total_seconds() / 60)
                    this_week_end_date = search_end_date

                mp_total_cnt = 0
                mp_malfunction_cnt = 0
                mp_malfunction_ratio = 0
                mp_operation_ratio = 0
                mp_downtime = 0
                mp_downtime_ratio = 0
                mp_uptime_ratio = 0
                mp_malfunction_list = []

                if param["device_mp"]:
                    total_cnt = db.session.query(DailyMPReports.serial_number).distinct().filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*weekly_mp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyMPReports.serial_number).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_mp_filter_group)).filter(DailyMPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyMPReports).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_mp_filter_group)).all()
                    downtime = 0
                    malfunction_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    mp_total_cnt = total_cnt
                    mp_malfunction_cnt = malfunction_cnt
                    mp_malfunction_ratio = malfunction_ratio
                    mp_operation_ratio = operation_ratio
                    mp_downtime = downtime
                    mp_downtime_ratio = downtime_ratio
                    mp_uptime_ratio = uptime_ratio
                    mp_malfunction_list = malfunction_list

                dp_total_cnt = 0
                dp_malfunction_cnt = 0
                dp_malfunction_ratio = 0
                dp_operation_ratio = 0
                dp_downtime = 0
                dp_downtime_ratio = 0
                dp_uptime_ratio = 0
                dp_malfunction_list = []

                if param["device_dp"]:
                    total_cnt = db.session.query(DailyDPReports.serial_number).distinct().filter(and_(*mp_filter_and_group)).filter(or_(*mp_filter_or_group)).filter(and_(*weekly_dp_filter_group)).count()
                    malfunction_cnt = db.session.query(DailyDPReports.serial_number).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_dp_filter_group)).filter(DailyDPReports.incident_cnt > 0).count()

                    malfunction_ratio = 0
                    if total_cnt != 0:
                        malfunction_ratio = float(malfunction_cnt) / float(total_cnt) * float(100)

                    operation_ratio = 0
                    if total_cnt != 0:
                        operation_ratio = (float(total_cnt) - float(malfunction_cnt)) / float(total_cnt) * float(100)

                    report_list = db.session.query(DailyDPReports).distinct().filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).filter(and_(*weekly_dp_filter_group)).all()
                    downtime = 0
                    malfunction_list = []
                    if len(report_list) != 0:
                        for report in report_list:
                            downtime += report.down_time

                    downtime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        downtime_ratio = float(downtime) / (float(total_min) * float(total_cnt)) * float(100)

                    uptime_ratio = 0
                    if total_min != 0 and total_cnt != 0:
                        uptime_ratio = ((float(total_min) * float(total_cnt)) - float(downtime)) / (float(total_min) * float(total_cnt)) * float(100)

                    dp_total_cnt = total_cnt
                    dp_malfunction_cnt = malfunction_cnt
                    dp_malfunction_ratio = malfunction_ratio
                    dp_operation_ratio = operation_ratio
                    dp_downtime = downtime
                    dp_downtime_ratio = downtime_ratio
                    dp_uptime_ratio = uptime_ratio
                    dp_malfunction_list = malfunction_list

                objects.append({
                    "search_type": param["period"],
                    "start_date": json_encoder(weekly_start.strftime("%Y-%m-%d")),
                    "end_date": json_encoder(this_week_end_date.strftime("%Y-%m-%d")),
                    "mp_type": mp_type,
                    "mp_total_cnt": mp_total_cnt,
                    "mp_malfunction_cnt": mp_malfunction_cnt,
                    "mp_operation_ratio": mp_operation_ratio,
                    "mp_malfunction_ratio": mp_malfunction_ratio,
                    "mp_uptime_ratio": mp_uptime_ratio,
                    "mp_downtime_ratio": mp_downtime_ratio,
                    "mp_downtime": mp_downtime,
                    "mp_malfunction_list": mp_malfunction_list,
                    "dp_type": dp_type,
                    "dp_total_cnt": dp_total_cnt,
                    "dp_malfunction_cnt": dp_malfunction_cnt,
                    "dp_operation_ratio": dp_operation_ratio,
                    "dp_malfunction_ratio": dp_malfunction_ratio,
                    "dp_uptime_ratio": dp_uptime_ratio,
                    "dp_downtime_ratio": dp_downtime_ratio,
                    "dp_downtime": dp_downtime,
                    "dp_malfunction_list": dp_malfunction_list
                })

            return result(200, "Get Report Ratio Monthly", objects, None, "by KSM")


class ReportStatsMalfuncMPListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.API = '/api/v1/report/stats/mp/malfunction/list'
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        super(ReportStatsMalfuncMPListAPI, self).__init__()

    def get(self):
        #param
        param_mp_list = request.args.getlist('mp_list')

        #setting
        order = request.args.get('order')
        by = request.args.get('by')
        #setting - pagination
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))
        pagination = request.args.get('pagination')
        ie_check = request.args.get('excel_create')

        order_by = MediaPlayers.__tablename__ + "." + by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []

        # [ make ] filter
        if param_mp_list is not None:
            for param_mp in param_mp_list:
                filter_or_group.append(MediaPlayers.id == param_mp)

        # [ make ] query
        if pagination == 'pagination':
            mp_list = db.session.query(MediaPlayers).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by).limit(limit).offset(offset)
            total_count = db.session.query(MediaPlayers).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by).count()
            current_count = mp_list.count()
        elif pagination == 'normal':
            mp_list = db.session.query(MediaPlayers).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by)
            total_count = db.session.query(MediaPlayers).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by).count()
            current_count = mp_list.count()

        # [ create ] Pagination
        previous = None
        next = None
        offset_list = {
            "previous": offset - limit,
            "next": offset + limit
        }

        if offset != 0:
            previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
            if (total_count - (offset+current_count)) > limit:
                next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {}
        objects = []
        excel_object = []
        date = datetime.today()
        date = date.strftime('%Y-%m-%d')

        if current_count == 0:

            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({
                "pk": "EMPTY",
            })
            return result(200, "Get Report Stats Monthly", objects, meta, "by KSM")
        else:
            if pagination == 'pagination':
                meta = {
                    "limit": limit,
                    "next": next,
                    "offset": offset,
                    "previous": previous,
                    "total_count": total_count
                }

                for mp in mp_list:
                    objects.append({
                        "id": mp.id,
                        "serial_number": mp.serial_number,
                        "alias": mp.alias,
                        "account": mp.site,
                        "location": mp.location,
                        "shop": mp.shop
                    })

                return result(200, "Get Report Stats Malfunction MP List", objects, meta, "by KSM")
            else:
                meta = {
                    "total_count": total_count
                }
                for mp in mp_list:
                    objects.append({
                        "id": mp.id,
                        "serial_number": mp.serial_number,
                        "alias": mp.alias,
                        "account": mp.site,
                        "location": mp.location,
                        "shop": mp.shop,
                        "userID": self.userID,
                        "create_date": date
                    })

                    excel_object.append({
                        "No": mp.id,
                        "S/N": mp.serial_number,
                        "Alias": mp.alias,
                        "Account": mp.site,
                        "Location": mp.location,
                        "Shop": mp.shop,
                        "Live Status": "View",
                        "Incidents": "View"
                    })

                if ie_check == 'true':

                    date = datetime.today()
                    date = date.strftime('%Y-%m-%d')

                    if not os.path.exists('../download/excel/' + self.userID):
                        os.makedirs('../download/excel/' + self.userID)

                    workbook = xlsxwriter.Workbook('../download/excel/' + self.userID + '/MediaPlayer_Fault_Detail-' + date + '.xlsx')
                    ordering = {'No': 0, 'S/N': 1, 'Alias': 2, 'Account': 3, 'Location': 4, 'Shop': 5, 'Live Status': 6, 'Incidents': 7}

                    worksheet = workbook.add_worksheet()
                    worksheet.set_column(0, 0, 15)
                    worksheet.set_column(0, 4, 14)
                    worksheet.set_column(6, 7, 15)
                    worksheet.set_column(8, 9, 18)
                    head = workbook.add_format({'bold': True, 'font_color': '#000000', 'bg_color': '#999999', 'align': 'center'})
                    body = workbook.add_format({'align': 'center'})

                    col = 0
                    for data in excel_object:
                        data = sorted(data, key=ordering.__getitem__)
                        for k in data:
                            if col < len(data):
                                worksheet.write(0, col, k, head)
                                col += 1

                    row = 1
                    col = 0
                    for data in excel_object:
                        data = [data[i] for i in sorted(data, key=ordering.__getitem__)]
                        for v in data:
                            if col < len(data):
                                worksheet.write(row, col, v, body)
                                col += 1
                        row += 1
                        col = 0

                    workbook.close()

                return result(200, "Get Report Stats Malfunction MP Normal List", objects, meta, "by KSM")

class ReportStatsMalfuncDPListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.API = '/api/v1/report/stats/dp/malfunction/list'
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        super(ReportStatsMalfuncDPListAPI, self).__init__()

    def get(self):
        #param
        param_dp_list = request.args.getlist('dp_list')

        #setting
        order = request.args.get('order')
        by = request.args.get('by')
        #setting - pagination
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))
        pagination = request.args.get('pagination')
        ie_check = request.args.get('excel_create')

        order_by = Displays.__tablename__ + "." + by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []

        # [ make ] filter
        if param_dp_list is not None:
            for param_dp in param_dp_list:
                filter_or_group.append(Displays.id == param_dp)

        # [ make ] query
        if pagination == 'pagination':
            dp_list = db.session.query(Displays).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by).limit(limit).offset(offset)
            total_count = db.session.query(Displays).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by).count()
            current_count = dp_list.count()
        elif pagination == 'normal':
            dp_list = db.session.query(Displays).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by)
            total_count = db.session.query(Displays).join(MediaPlayers, Displays.FK_mp_id == MediaPlayers.id).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by).count()
            current_count = dp_list.count()

        # [ create ] Pagination
        previous = None
        next = None
        offset_list = {
            "previous": offset - limit,
            "next": offset + limit
        }

        if offset != 0:
            previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
            if (total_count - (offset+current_count)) > limit:
                next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {}
        objects = []
        excel_object = []
        date = datetime.today()
        date = date.strftime('%Y-%m-%d')

        if current_count == 0:

            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({
                "pk": "EMPTY",
            })
            return result(200, "Get Report Stats Monthly", objects, meta, "by KSM")
        else:
            if pagination == 'pagination':
                meta = {
                    "limit": limit,
                    "next": next,
                    "offset": offset,
                    "previous": previous,
                    "total_count": total_count
                }

                for dp in dp_list:
                    mp = MediaPlayers.query.filter_by(id=dp.FK_mp_id).first()
                    objects.append({
                        "id": dp.id,
                        "serial_number": dp.serial_number,
                        "dp_group": dp.dp_group,
                        "alias": dp.alias,
                        "mp_serial_number": mp.serial_number,
                        "account": mp.site,
                        "location": mp.location,
                        "shop": mp.shop
                    })

                return result(200, "Get Report Stats Malfunction DP List", objects, meta, "by KSM")
            else:
                meta = {
                    "total_count": total_count
                }
                for dp in dp_list:
                    mp = MediaPlayers.query.filter_by(id=dp.FK_mp_id).first()
                    objects.append({
                        "id": dp.id,
                        "serial_number": dp.serial_number,
                        "dp_group": dp.dp_group,
                        "alias": dp.alias,
                        "mp_serial_number": mp.serial_number,
                        "account": mp.site,
                        "location": mp.location,
                        "shop": mp.shop,
                        "userID": self.userID,
                        "create_date": date
                    })

                    excel_object.append({
                        "No": dp.id,
                        "S/N": dp.serial_number,
                        "Alias": dp.alias,
                        "Device_Group": dp.dp_group,
                        "Account": mp.site,
                        "Location": mp.location,
                        "Shop": mp.shop,
                        "Live Status": "View",
                        "Incidents": "View"
                    })

                if ie_check == 'true':

                    date = datetime.today()
                    date = date.strftime('%Y-%m-%d')

                    if not os.path.exists('../download/excel/' + self.userID):
                        os.makedirs('../download/excel/' + self.userID)

                    workbook = xlsxwriter.Workbook('../download/excel/' + self.userID + '/Display_Fault_Detail-' + date + '.xlsx')
                    ordering = {'No': 0, 'S/N': 1, 'Alias': 2, 'Device_Group':3, 'Account': 4, 'Location': 5, 'Shop': 6, 'Live Status': 7, 'Incidents': 8}

                    worksheet = workbook.add_worksheet()
                    worksheet.set_column(0, 0, 15)
                    worksheet.set_column(0, 4, 14)
                    worksheet.set_column(6, 7, 15)
                    worksheet.set_column(8, 9, 18)
                    head = workbook.add_format({'bold': True, 'font_color': '#000000', 'bg_color': '#999999', 'align': 'center'})
                    body = workbook.add_format({'align': 'center'})

                    col = 0
                    for data in excel_object:
                        data = sorted(data, key=ordering.__getitem__)
                        for k in data:
                            if col < len(data):
                                worksheet.write(0, col, k, head)
                                col += 1

                    row = 1
                    col = 0
                    for data in excel_object:
                        data = [data[i] for i in sorted(data, key=ordering.__getitem__)]
                        for v in data:
                            if col < len(data):
                                worksheet.write(row, col, v, body)
                                col += 1
                        row += 1
                        col = 0

                    workbook.close()

                return result(200, "Get Report Stats Malfunction DP Normal List", objects, meta, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Administration
#----------------------------------------------------------------------------------------------------------------------
class UsersAdminListAPI(Resource):
    """
    [ AdminList ]
    @ GET : Get the Admin Information
    @ POST : Register Admin
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("userID", type=str, location="json")
        self.parser.add_argument("userPW", type=str, location="json")
        self.parser.add_argument("userName", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")
        self.parser.add_argument("userOTP", type=bool, location="json")
        self.parser.add_argument("comp_key", type=str, location="json")
        self.parser.add_argument("time_zone", type=str, location="json")
        self.parser.add_argument("date_format", type=str, location="json")
        self.parser.add_argument("refresh_time", type=str, location="json")
        self.parser.add_argument("isAdmin", type=str, location="json")
        self.parser.add_argument("change_manager_id", type=str, location="json")

        super(UsersAdminListAPI, self).__init__()

    def get(self):
        if self.isValidToken:
            admin_list = Managers.query.filter_by(isAdmin=True, isSuper=False).all()

            objects = []
            if len(admin_list) == 0:
                return result(200, "No Manager information.", None, None, "by KSM")
            else:
                for admin in admin_list:
                    objects.append({
                        'key': admin.id,
                        'userID': admin.userID,
                        'userName': admin.userName,
                        'phone': admin.phone,
                        'email': admin.email,
                        'comp_key': admin.comp_key,
                        'what_key': admin.what_key,
                        'time_zone': admin.time_zone,
                        'date_format': admin.date_format,
                        'refresh_time': admin.refresh_time
                    })
                return result(200, "GET the Admin information.", objects, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

class UsersAdminAPI(Resource):
    """
    [ Admin ]
    @ GET : Get the Admin Information
    @ POST : Register Manager
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        self.isSuper = self.token_manager.get_admin_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("userID", type=str, location="json")
        self.parser.add_argument("userPW", type=str, location="json")
        self.parser.add_argument("userName", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")
        self.parser.add_argument("userOTP", type=bool, location="json")
        self.parser.add_argument("comp_key", type=str, location="json")
        self.parser.add_argument("time_zone", type=str, location="json")
        self.parser.add_argument("date_format", type=str, location="json")
        self.parser.add_argument("refresh_time", type=str, location="json")
        self.parser.add_argument("isAdmin", type=str, location="json")
        self.parser.add_argument("change_manager_id", type=str, location="json")

        super(UsersAdminAPI, self).__init__()

    def get(self):
        search_key_admin_id = request.args.get('key')

        if self.isValidToken:
            if search_key_admin_id:
                admin = Managers.query.filter_by(id=search_key_admin_id).first()
            else:
                admin = Managers.query.filter_by(userID=self.userID).first()

            objects = []

            if admin is not None:
                objects.append({
                    'key': admin.id,
                    'userID': admin.userID,
                    'userName': admin.userName,
                    'phone': admin.phone,
                    'email': admin.email,
                    'userOTP': admin.userOTP,
                    'comp_key': admin.comp_key,
                    'what_key': admin.what_key,
                    'time_zone': admin.time_zone,
                    'date_format': admin.date_format,
                    'refresh_time': admin.refresh_time,
                    'isAdmin': admin.isAdmin
                })
                return result(200, "GET the Admin Information", objects, None, "by KSM")
            else:
                return result(400, "Admin account does not exist.", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def post(self):
        input = self.parser.parse_args()

        if self.userType == USER_TYPE_SUPERADMIN or USER_TYPE_ADMIN:
            admin = Managers.query.filter_by(userID=self.userID).first()
            if admin is not None:
                new_manager = Managers()

                # [ Check ] input
                for k, v in input.items():
                    if v is None:
                        input[k] = ''

                new_manager.userID = input['userID']
                new_manager.userPW = password_encoder(input['userPW'])
                new_manager.userName = input['userName']
                new_manager.phone = input['phone']
                new_manager.email = input['email']
                new_manager.userOTP = 0
                new_manager.createOTP = 0
                new_manager.otp_key = random.random()
                new_manager.comp_key = input['comp_key']
                new_manager.time_zone = input['time_zone']
                new_manager.date_format = input['date_format']
                new_manager.refresh_time = input['refresh_time']

                if self.isSuper:
                    new_manager.isAdmin = 1
                else:
                    new_manager.isAdmin = 0
                new_manager.isSuper = 0

                db.session.add(new_manager)
                db.session.commit()

                if self.isSuper:
                    return result(200, "create Admin", None, None, "by KSM")
                else:
                    creatmanager = Managers.query.filter_by(userID=input['userID']).first()

                    new_connect = CONN_Admins_N_Managers()
                    new_connect.FK_admin_id = admin.id
                    new_connect.FK_manager_id = creatmanager.id

                    db.session.add(new_connect)
                    db.session.commit()
                    return result(200, "create Manager", None, None, "by KSM")
            else:
                return result(401, "No Auth", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def put(self):
        input = self.parser.parse_args()

        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_SUPERADMIN:
             # [ Case by ] manager Info change
            if input['change_manager_id'] != None:
                change_manager_id = input['change_manager_id']
                del input['change_manager_id']
                manager = Managers.query.filter_by(id=change_manager_id).first()

                if manager is None:
                    return result(500, "Can not find an edit manager.", None, None, "by KSM")
                else:
                    # [ encoder ] password
                    if input['userPW'] is not None:
                        input['userPW'] = password_encoder(input['userPW'])

                    # [ Filter ] input
                    for k, v in input.items():
                        if v is None:
                            del input[k]

                    del input['token']
                    db.session.query(Managers).filter_by(userID=manager.userID).update(input)
                    db.session.commit()

                    return result(200, "Update the Manager Information", None, None, "by KSM")
            else:
                del input['change_manager_id']
                admin = Managers.query.filter_by(userID=self.userID).first()

                # [ encoder ] password
                if input['userPW'] is not None:
                    input['userPW'] = password_encoder(input['userPW'])

                # [ Filter ] input
                for k, v in input.items():
                    if v is None:
                        del input[k]

                if admin is not None:
                    del input['token']
                    db.session.query(Managers).filter_by(userID=admin.userID).update(input)
                    db.session.commit()

                    return result(200, "Update the Admin Information", None, None, "by KSM")
                else:
                    return result(400, "Admin account does not exist.", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def delete(self):
        change_manager_id = request.args.get('change_manager_id')

        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_SUPERADMIN:
            # [ Case by ] manager Info change
            if change_manager_id is not None:
                manager = Managers.query.filter_by(id=change_manager_id).first()

                if manager is None:
                    return result(500, "Can not find an delete manager.", None, None, "by KSM")
                else:
                    db.session.delete(manager)
                    db.session.commit()

                    return result(200, "manager information deleted.", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

class UsersManagerListAPI(Resource):
    """
    [ Manager List ]
    @ GET : Get the Manager List Information
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("userID", type=str, location="json")
        self.parser.add_argument("userName", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")
        self.parser.add_argument("key", type=str, location="json")
        self.parser.add_argument("manager_list", type=list, location="json")

        super(UsersManagerListAPI, self).__init__()

    def get(self):
        superAdminKey = request.args.get('key')
        if self.isValidToken:
            self.filter_group = []
            if self.userType is USER_TYPE_ADMIN:
                manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                if len(manager_ids) is 0:
                    self.filter_group.append(Managers.id == -1)
                else:
                    for mid in manager_ids:
                        self.filter_group.append(Managers.id == mid.FK_manager_id)

            manager_list = Managers.query.filter_by(isAdmin=False).filter(or_(*self.filter_group)).all()

            objects = []
            if len(manager_list) == 0:
                return result(200, "No Manager information.", None, None, "by KSM")
            else:
                for manager in manager_list:
                    checked = False
                    if superAdminKey:
                        conn_check = CONN_Admins_N_Managers.query.filter_by(FK_admin_id=superAdminKey, FK_manager_id=manager.id).first()
                        if conn_check is not None:
                            checked = True

                    objects.append({
                        'key': manager.id,
                        'userID': manager.userID,
                        'userName': manager.userName,
                        'phone': manager.phone,
                        'email': manager.email,
                        'comp_key': manager.comp_key,
                        'what_key': manager.what_key,
                        'time_zone': manager.time_zone,
                        'date_format': manager.date_format,
                        'refresh_time': manager.refresh_time,
                        'checked': checked
                    })
                return result(200, "GET the Manager information.", objects, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def put(self):
        input = self.parser.parse_args()

        #clear
        conn_list = CONN_Admins_N_Managers.query.filter_by(FK_admin_id=input['key']).all()
        if len(conn_list) != 0:
            for conn in conn_list:
                db.session.delete(conn)
                db.session.commit()

        #insert
        for managerKey in input['manager_list']:
            conn_A_n_M = CONN_Admins_N_Managers()
            conn_A_n_M.FK_admin_id = input['key']
            conn_A_n_M.FK_manager_id = managerKey
            db.session.add(conn_A_n_M)
            db.session.commit()

        return result(200, "OK", None, None, "by KSM")

class UsersManagerAPI(Resource):
    """
    [ Manager ]
    @ GET : Get the Manager Information
    @ POST : Register User
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("userID", type=str, location="json")
        self.parser.add_argument("userPW", type=str, location="json")
        self.parser.add_argument("userName", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")
        self.parser.add_argument("userOTP", type=bool, location="json")
        self.parser.add_argument("auth", type=str, location="json")
        self.parser.add_argument("what_key", type=str, location="json")
        self.parser.add_argument("time_zone", type=str, location="json")
        self.parser.add_argument("date_format", type=str, location="json")
        self.parser.add_argument("refresh_time", type=str, location="json")
        self.parser.add_argument("isAdmin", type=str, location="json")
        self.parser.add_argument("change_member_id", type=str, location="json")

        super(UsersManagerAPI, self).__init__()

    def get(self):
        search_key_manager_id = request.args.get('key')

        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
            # [ Case by ] manager id search
            if search_key_manager_id is not None:
                manager = Managers.query.filter_by(id=search_key_manager_id).first()
                objects = []

                if manager is not None:
                    objects.append({
                        'key': manager.id,
                        'userID': manager.userID,
                        'userName': manager.userName,
                        'phone': manager.phone,
                        'email': manager.email,
                        'userOTP': manager.userOTP,
                        'comp_key': manager.comp_key,
                        'what_key': manager.what_key,
                        'time_zone': manager.time_zone,
                        'date_format': manager.date_format,
                        'refresh_time': manager.refresh_time,
                        'isAdmin': manager.isAdmin
                    })
                    return result(200, "GET the Manager Information", objects, None, "by KSM")
                else:
                    return result(400, "Manager account does not exist.", None, None, "by KSM")
            # [ Case by ] All Manager
            else:
                manager = Managers.query.filter_by(userID=self.userID).first()

                objects = []

                if manager is not None:
                    objects.append({
                        'key': manager.id,
                        'userID': manager.userID,
                        'userName': manager.userName,
                        'phone': manager.phone,
                        'email': manager.email,
                        'userOTP': manager.userOTP,
                        'comp_key': manager.comp_key,
                        'what_key': manager.what_key,
                        'time_zone': manager.time_zone,
                        'date_format': manager.date_format,
                        'refresh_time': manager.refresh_time,
                        'isAdmin': manager.isAdmin
                    })
                    return result(200, "GET the Manager Information", objects, None, "by KSM")
                else:
                    return result(400, "Manager account does not exist.", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def post(self):
        input = self.parser.parse_args()

        if self.isValidToken:
            manager = Managers.query.filter_by(userID=self.userID).first()

            if manager is not None:
                new_user = Users()

                # [ Check ] input
                for k, v in input.items():
                    if v is None:
                        input[k] = ''

                new_user.userID = input['userID']
                new_user.userPW = password_encoder(input['userPW'])
                new_user.userName = input['userName']
                new_user.phone = input['phone']
                new_user.email = input['email']
                new_user.userOTP = 0
                new_user.createOTP = 0
                new_user.otp_key = random.random()
                new_user.auth = input['auth']
                new_user.FK_user_groups_id = -1
                new_user.user_group_name = 'empty'
                new_user.time_zone = input['time_zone']
                new_user.date_format = input['date_format']
                new_user.refresh_time = input['refresh_time']
                new_user.FK_managers_id = manager.id

                db.session.add(new_user)
                db.session.commit()

                return result(200, "create User", None, None, "by KSM")
            else:
                return result(401, "No Auth", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def put(self):
        input = self.parser.parse_args()

        if self.userType is not USER_TYPE_GUEST:
            if input['change_member_id'] is None:
                manager = Managers.query.filter_by(userID=self.userID).first()

                # [ encoder ] password
                if input['userPW'] is not None:
                   input['userPW'] = password_encoder(input['userPW'])

                # [ Filter ] input
                for k, v in input.items():
                    if v is None:
                        del input[k]

                if manager is not None:
                    del input['token']
                    db.session.query(Managers).filter_by(userID=manager.userID).update(input)
                    db.session.commit()

                    return result(200, "Update the Manager Information", None, None, "by KSM")
                else:
                    return result(400, "Manager account does not exist.", None, None, "by KSM")
            else:
                change_member_id = input['change_member_id']
                del input['change_member_id']

                # [ encoder ] password
                if input['userPW'] is not None:
                    input['userPW'] = password_encoder(input['userPW'])

                # [ Filter ] input
                for k, v in input.items():
                    if v is None:
                        del input[k]

                member = Users.query.filter_by(id=change_member_id).first()
                if member is not None:
                    del input['token']
                    db.session.query(Users).filter_by(id=change_member_id).update(input)
                    db.session.commit()

                    return result(200, "Update the Manager Information", None, None, "by KSM")
                else:
                    return result(400, "Manager account does not exist.", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def delete(self):
        change_member_id = request.args.get('change_member_id')
        if self.userType is not USER_TYPE_GUEST:
            # [ Case by ] member Info change
            if change_member_id is not None:
                member = Users.query.filter_by(id=change_member_id).first()

                if member is None:
                    return result(500, "Can not find an delete member.", None, None, "by KSM")
                else:
                    db.session.delete(member)
                    db.session.commit()
                    return result(200, "member information deleted.", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

class UsersMemberListAPI(Resource):
    """
    [ Member List ]
    @ GET : Get the Member List Information
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("userID", type=str, location="json")
        self.parser.add_argument("userName", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")

        super(UsersMemberListAPI, self).__init__()

    def get(self):
        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
            manager = Managers.query.filter_by(userID=self.userID).first()

            objects = []
            if manager is None:
                return result(200, "No Manager information.", None, None, "by KSM")
            else:
                member_list = Users.query.filter_by(FK_managers_id=manager.id).all()
                ug_list = User_Groups.query.filter_by(FK_managers_id=manager.id).all()

                userGroupObj = []
                userGroupObj.append({
                    'key': -1,
                    'name': 'empty'
                })

                for ug in ug_list:
                    userGroupObj.append({
                        'key': ug.id,
                        'name': ug.name
                    })

                for member in member_list:
                    objects.append({
                        'key': member.id,
                        'userID': member.userID,
                        'userName': member.userName,
                        'userGroup': userGroupObj,
                        'phone': member.phone,
                        'email': member.email,
                        'time_zone': member.time_zone,
                        'date_format': member.date_format,
                        'refresh_time': member.refresh_time,
                        'userGroupKey': member.FK_user_groups_id,
                        'userGroupName': member.user_group_name
                    })
                return result(200, "GET the User information.", objects, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

class UsersMemberAPI(Resource):
    """
    [ Member ]
    @ GET : Get the Member Information
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("userID", type=str, location="json")
        self.parser.add_argument("userPW", type=str, location="json")
        self.parser.add_argument("userName", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")
        self.parser.add_argument("userOTP", type=bool, location="json")
        self.parser.add_argument("auth", type=str, location="json")
        self.parser.add_argument("time_zone", type=str, location="json")
        self.parser.add_argument("date_format", type=str, location="json")
        self.parser.add_argument("refresh_time", type=str, location="json")

        super(UsersMemberAPI, self).__init__()

    def get(self):
        input = self.parser.parse_args()

        search_key_user_id = request.args.get('key')

        if self.userType is not USER_TYPE_GUEST:
            # [ Case by ] Token
            if search_key_user_id is None:
                auth = Tokens.query.filter_by(token=input['token']).first()
                member = Users.query.filter_by(userID=auth.userID).first()

                objects = []
                if member is not None:

                    objects.append({
                        'key': member.id,
                        'userID': member.userID,
                        'userName': member.userName,
                        'phone': member.phone,
                        'email': member.email,
                        'userOTP': member.userOTP,
                        'auth': check_member_auth(member.auth),
                        'time_zone': member.time_zone,
                        'date_format': member.date_format,
                        'refresh_time': member.refresh_time
                    })
                    return result(200, "GET the Member Information", objects, None, "by KSM")
                else:
                    return result(400, "Member account does not exist.", None, None, "by KSM")
            # [ Case by ] search key user ID
            else:
                member = Users.query.filter_by(id=search_key_user_id).first()

                objects = []

                if member is None:
                    return result(400, "No User", None, None, "by KSM")
                else:
                    objects.append({
                        'key': member.id,
                        'userID': member.userID,
                        'userName': member.userName,
                        'phone': member.phone,
                        'email': member.email,
                        'userOTP': member.userOTP,
                        'auth': check_member_auth(member.auth),
                        'time_zone': member.time_zone,
                        'date_format': member.date_format,
                        'refresh_time': member.refresh_time
                    })
                    return result(200, "GET the Member Information", objects, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def post(self):
        if self.isValidToken:
            pass
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def put(self):
        input = self.parser.parse_args()

        if self.isValidToken:
            auth = Tokens.query.filter_by(token=input['token']).first()
            user = Users.query.filter_by(userID=auth.userID).first()

            # [ encoder ] password
            if input['userPW'] is not None:
               input['userPW'] = password_encoder(input['userPW'])

            # [ Filter ] input
            for k, v in input.items():
                if v is None:
                    del input[k]

            if user is not None:
                del input['token']
                db.session.query(Users).filter_by(userID=user.userID).update(input)
                db.session.commit()

                return result(200, "Update the User Information", None, None, "by KSM")
            else:
                return result(400, "User account does not exist.", None, None, "by KSM")
        else:
            return result(401, "No Auth", None, None, "by KSM")

    def delete(self):
        if self.isValidToken:
            pass
        else:
            return result(401, "No Auth", None, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Silent Installation
#----------------------------------------------------------------------------------------------------------------------
class SilentInstallListAPI(Resource):
    """
    [ Silent Install Data List ]
    @ GET : All List
    @ POST : the Install Data List Add
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("key", type=str, location="json")
        self.parser.add_argument("search_key", type=str, location="json")
        self.parser.add_argument("site", type=str, location="json")
        self.parser.add_argument("location", type=str, location="json")
        self.parser.add_argument("shop", type=str, location="json")

        self.parser.add_argument("mp_model", type=str, location="json")
        self.parser.add_argument("mp_sn", type=str, location="json")
        self.parser.add_argument("mp_fw_ver", type=str, location="json")
        self.parser.add_argument("mp_alias", type=str, location="json")
        self.parser.add_argument("mp_activate_date", type=str, location="json")
        self.parser.add_argument("mp_expire_date", type=str, location="json")

        self.parser.add_argument("dp_alias", type=str, location="json")
        self.parser.add_argument("dp_activate_date", type=str, location="json")
        self.parser.add_argument("dp_expire_date", type=str, location="json")
        self.parser.add_argument("dp_method", type=str, location="json")
        self.parser.add_argument("dp_connect_to", type=str, location="json")
        self.parser.add_argument("dp_set_id", type=str, location="json")

        super(SilentInstallListAPI, self).__init__()

    def get(self):
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))

        # [ Make ] ORDER BY
        order_by = request.args.get('order_by')
        order_by_str = ''
        if order_by is not None:
            order_by_str = 'install_data.' + str(order_by)

        order = request.args.get('order')
        order_str = ''
        if order == '-':
            order_str = 'DESC'
        else:
            order_str = 'ASC'

        result_order_by = order_by_str + ' ' + order_str

        objects = []

        meta = {}

        if self.userType is USER_TYPE_MANAGER:
            total_count = InstallData.query.filter_by(FK_managers_id=self.user_manager_id).count()
            result_install = InstallData.query.order_by(result_order_by).filter_by(FK_managers_id=self.user_manager_id).limit(limit).offset(offset)
        elif self.userType is USER_TYPE_ADMIN:
            total_count = InstallData.query.count()
            result_install = InstallData.query.order_by(result_order_by).limit(limit).offset(offset)

        current_count = result_install.count()

        next = ''
        previous = ''

        previous_offset = offset-limit
        next_offset = offset+limit
        # Creating a Pagenation
        if offset == 0:
            previous = None
            next = None
        elif offset != 0:
            previous = "/api/v1/silent-install?limit="+str(limit)+"&offset="+str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next = "/api/v1/silent-install?limit="+str(limit)+"&offset="+str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        objects = []

        if current_count == 0:
            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({

            })
            return result(404, "No Install Info", None, meta, "by KSM")
        else:
            for data in result_install:
                objects.append({
                    'rkey': data.id,
                    'key': data.key,
                    'site': data.site,
                    'location': data.location,
                    'shop': data.shop,
                    'mp_model': data.mp_model,
                    'mp_sn': data.mp_sn,
                    'mp_fw_ver': data.mp_fw_ver,
                    'mp_alias': data.mp_alias,
                    'mp_activate_date': json_encoder(data.mp_activate_date),
                    'mp_expire_date': json_encoder(data.mp_expire_date),
                    'dp_alias': data.dp_alias,
                    'dp_activate_date': json_encoder(data.dp_activate_date),
                    'dp_expire_date': json_encoder(data.dp_expire_date),
                    'dp_method': data.dp_method,
                    'dp_connect_to': data.dp_connect_to,
                    'dp_set_id': data.dp_set_id
                })
            return result(200, "GET the Install Data List", objects, meta, "by KSM")

    def post(self):
        input = self.parser.parse_args()
        # [ Check ] Key
        install = InstallData.query.filter_by(key=input['key']).first()

        if install is None:
            data = InstallData()

            data.key = input['key']
            data.site = input['site']
            data.location = input['location']
            data.shop = input['shop']

            data.mp_model = input['mp_model']
            data.mp_sn = input['mp_sn']
            data.mp_fw_ver = input['mp_fw_ver']
            data.mp_alias = input['mp_alias']
            data.mp_activate_date = input['mp_activate_date']
            data.mp_expire_date = input['mp_expire_date']

            data.dp_alias = input['dp_alias']
            data.dp_activate_date = input['dp_activate_date']
            data.dp_expire_date = input['dp_expire_date']
            data.dp_method = input['dp_method']
            data.dp_connect_to = input['dp_connect_to']
            data.dp_set_id = input['dp_set_id']

            data.FK_managers_id = self.user_manager_id

            db.session.add(data)
            db.session.commit()

            return result(200, "Register Install Data Successful", None, None, "by KSM")
        else:
            return result(400, "Install Data Duplicate", None, None, "by KSM")

class SilentInstallAPI(Resource):
    """
    [ Install Data ]
    For Web
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("key", type=str, location="json")
        self.parser.add_argument("site", type=str, location="json")
        self.parser.add_argument("location", type=str, location="json")
        self.parser.add_argument("shop", type=str, location="json")

        self.parser.add_argument("mp_model", type=str, location="json")
        self.parser.add_argument("mp_sn", type=str, location="json")
        self.parser.add_argument("mp_fw_ver", type=str, location="json")
        self.parser.add_argument("mp_alias", type=str, location="json")
        self.parser.add_argument("mp_activate_date", type=str, location="json")
        self.parser.add_argument("mp_expire_date", type=str, location="json")

        self.parser.add_argument("dp_alias", type=str, location="json")
        self.parser.add_argument("dp_activate_date", type=str, location="json")
        self.parser.add_argument("dp_expire_date", type=str, location="json")
        self.parser.add_argument("dp_method", type=str, location="json")
        self.parser.add_argument("dp_connect_to", type=str, location="json")
        self.parser.add_argument("dp_set_id", type=str, location="json")

        super(SilentInstallAPI, self).__init__()

    def get(self, install_id):
        data = InstallData.query.filter_by(id=install_id).first()

        objects = []

        if data is None:
            return result(404, "No Install Data",  None, None, "by KSM")
        else:
            objects.append({
                "rkey": data.id,
                "key": data.key,
                "site": data.site,
                "location": data.location,
                "shop": data.shop,
                "mp_model": data.mp_model,
                "mp_sn": data.mp_sn,
                "mp_fw_ver": data.mp_fw_ver,
                "mp_alias": data.mp_alias,
                "mp_act_date": data.mp_activate_date,
                "mp_exp_date": data.mp_expire_date,
                "dp_alias": data.dp_alias,
                "dp_act_date": data.dp_activate_date,
                "dp_exp_date": data.dp_expire_date,
                "dp_method": data.dp_method,
                "dp_conn": data.dp_connect_to,
                "dp_set_id": data.dp_set_id
            })

            return result(200, "GET the Install Data", objects, None, "by KSM")

    def put(self, install_id):
        input = self.parser.parse_args()

        # [ Check ] input
        for k, v in input.items():
            if v is None:
                del input[k]

        data = InstallData.query.filter_by(id=install_id).first()

        if data is None:
            return result(404, "No Install Data",  None, None, "by KSM")
        else:
            db.session.query(InstallData).filter_by(id=install_id).update(input)
            db.session.commit()
            return result(200, "Update Install Data",  None, None, "by KSM")

    def delete(self, install_id):
        data = InstallData.query.filter_by(id=install_id).first()
        if data is None:
            return result(404, "No Install Data", None, None, "by KSM")
        else:
            db.session.delete(data)
            db.session.commit()
            return result(200, "Remove Install Data",  None, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Site
#----------------------------------------------------------------------------------------------------------------------
class CONN_SiteListAPI(Resource):
    """
    [ My Site List ]
    @ GET: Get the My Site List
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("site_key", type=str, location="json")
        self.parser.add_argument("manager_key", type=str, location="json")

        super(CONN_SiteListAPI, self).__init__()

    def get(self):
        manager_id = request.args.get('manager_id')

        if self.userType == USER_TYPE_GUEST:
            return result(503, "No Auth", None, None, "by KSM")
        else:
            # [ Case by ] All
            if manager_id is None:
                if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
                    manager = Managers.query.filter_by(userID=self.userID).first()
                elif self.userType == USER_TYPE_MEMBER:
                    user = Users.query.filter_by(userID=self.userID).first()
                    manager = Managers.query.filter_by(id=user.FK_managers_id).first()

                site_conn_list = CONN_Managers_N_Sites.query.filter_by(FK_managers_id=manager.id).all()

                if len(site_conn_list) == 0:
                    return result(404, "Empty Assinged Site List", None, None, "by KSM")
                else:
                    objects = []
                    for site_conn in site_conn_list:
                        site = Sites.query.filter_by(id=site_conn.FK_sites_id).first()
                        objects.append({
                            "key": site.id,
                            "name": site.name
                        })
                    return result(200, "Get the Assinged Site List", objects, None, "by KSM")
            # [ Case by ] manager_id
            else:
                site_conn_list = CONN_Managers_N_Sites.query.filter_by(FK_managers_id=manager_id).all()

                if len(site_conn_list) == 0:
                    return result(404, "Empty Assinged Site List", None, None, "by KSM")
                else:
                    objects = []
                    for site_conn in site_conn_list:
                        site = Sites.query.filter_by(id=site_conn.FK_sites_id).first()
                        if site is None:
                            return result(404, "Empty Site", None, None, "by KSM")
                        else:
                            objects.append({
                                "key": site.id,
                                "name": site.name
                            })
                    return result(200, "Get the Assinged Site List", objects, None, "by KSM")
    def post(self):
        input = self.parser.parse_args()

        # [ Check ] Site
        site = Sites.query.filter_by(id=input['site_key']).first()

        if site is None:
            return result(404, "Can not find your Account.", None, None, "by KSM")
        else:
            # [ Check ] connector
            conn_list = CONN_Managers_N_Sites.query.filter_by(FK_managers_id=input['manager_key'], FK_sites_id=input['site_key']).all()
            if len(conn_list) == 0:
                conn = CONN_Managers_N_Sites()
                conn.FK_managers_id = input['manager_key']
                conn.FK_sites_id = input['site_key']
                db.session.add(conn)
                db.session.commit()
                return result(200, "register Manager & Site Connector", None, None, "by KSM")
            else:
                return result(400, "Duplicate Property", None, None, "by KSM")

    def delete(self):
        manager_key = request.args.get('manager_key')
        site_key = request.args.get('site_key')

        conn = CONN_Managers_N_Sites.query.filter_by(FK_managers_id=manager_key, FK_sites_id=site_key).first()
        db.session.delete(conn)
        db.session.commit()

        return result(200, "remove Manager & Site Connector", None, None, "by KSM")

class SiteListAPI(Resource):
    """
    [ Site List ]
    @ GET: Get the Site List
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        super(SiteListAPI, self).__init__()

    def get(self):
        site_list = MediaPlayers.query.group_by(MediaPlayers.site).all()
        if len(site_list) == 0:
            return result(404, "Empty Account List", None, None, "by KSM")
        else:
            objects = []
            for site in site_list:
                objects.append({
                    "key": site.id,
                    "name": site.site
                })
            return result(200, "GET the Account List", objects, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Device Group
#----------------------------------------------------------------------------------------------------------------------
class DeviceGroupListAPI(Resource):
    """
    [ Device Group List ]
    @ GET: Get the Device Group List
    @ POST: Register the Device Group to the List
    @ DEL: Delete the Device Group List
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("start_time", type=str, location="json")
        self.parser.add_argument("end_time", type=str, location="json")
        self.parser.add_argument("setting_flag", type=bool, location="json")
        self.parser.add_argument("mp_serial_number", type=str, location="json")
        self.parser.add_argument("polling_time", type=str, location="json")

        super(DeviceGroupListAPI, self).__init__()

    def get(self):

        if self.isValidToken == False or self.userType == USER_TYPE_GUEST:
            return result(401, "You do not have permission.", None, None, "by KSM")
        else:
            # [ Case by ] mp_serial number input
            mp_serial_number = request.args.get('mp_serial_number')

            if mp_serial_number is not None:
                mp = MediaPlayers.query.filter_by(serial_number=mp_serial_number).first()

                objects=[]

                # [ Check ] Media Player
                if mp is None:
                    return result(500, "The Media Player does not exist.", None, None, "by KSM")
                else:

                    # [ Set ] Device Group
                    if mp.FK_device_groups_id is None or mp.FK_device_groups_id == '':
                        dg = Device_Groups.query.filter_by(name=DEFAULT_GROUP).first()
                    else:
                        dg = Device_Groups.query.filter_by(id=mp.FK_device_groups_id).first()
                    objects.append({
                        'name': dg.name,
                        'th_cpu_usage_error': dg.th_cpu_usage_error,
                        'th_cpu_usage_warning': dg.th_cpu_usage_warning,
                        'th_mem_usage_error': dg.th_mem_usage_error,
                        'th_mem_usage_warning': dg.th_mem_usage_warning,
                        'th_hdd_usage_error': dg.th_hdd_usage_error,
                        'th_hdd_usage_warning': dg.th_hdd_usage_warning,
                        'th_cpu_temp_error': dg.th_cpu_temp_error,
                        'th_cpu_temp_warning': dg.th_cpu_temp_warning,
                        'th_hdd_temp_error': dg.th_hdd_temp_error,
                        'th_hdd_temp_warning': dg.th_hdd_temp_warning,
                        'th_fan_speed_error': dg.th_fan_speed_error,
                        'th_fan_speed_warning': dg.th_fan_speed_warning,
                        'th_res_time_error': dg.th_res_time_error,
                        'th_res_time_warning': dg.th_res_time_warning,
                        'th_app1': dg.th_app1,
                        'th_app2': dg.th_app2,
                        'th_app3': dg.th_app3,
                        'th_app4': dg.th_app4,
                        'th_dp_temp_error': dg.th_dp_temp_error,
                        'th_dp_temp_warning': dg.th_dp_temp_warning,
                        'timer_onoff_sun': dg.timer_onoff_sun,
                        'timer_begin_sun': json_encoder(dg.timer_begin_sun),
                        'timer_end_sun': json_encoder(dg.timer_end_sun),
                        'timer_onoff_mon': dg.timer_onoff_mon,
                        'timer_begin_mon': json_encoder(dg.timer_begin_mon),
                        'timer_end_mon': json_encoder(dg.timer_end_mon),
                        'timer_onoff_tue': dg.timer_onoff_tue,
                        'timer_begin_tue': json_encoder(dg.timer_begin_tue),
                        'timer_end_tue': json_encoder(dg.timer_end_tue),
                        'timer_onoff_wed': dg.timer_onoff_wed,
                        'timer_begin_wed': json_encoder(dg.timer_begin_wed),
                        'timer_end_wed': json_encoder(dg.timer_end_wed),
                        'timer_onoff_thu': dg.timer_onoff_thu,
                        'timer_begin_thu': json_encoder(dg.timer_begin_thu),
                        'timer_end_thu': json_encoder(dg.timer_end_thu),
                        'timer_onoff_fri': dg.timer_onoff_fri,
                        'timer_begin_fri': json_encoder(dg.timer_begin_fri),
                        'timer_end_fri': json_encoder(dg.timer_end_fri),
                        'timer_onoff_sat': dg.timer_onoff_sat,
                        'timer_begin_sat': json_encoder(dg.timer_begin_sat),
                        'timer_end_sat': json_encoder(dg.timer_end_sat),
                        "polling_time": dg.polling_time
                    })

                    return result(200, "GET the Device Group Information", objects, None, "by KSM")
            # [ Case by ] All
            else:
                if self.userType == USER_TYPE_MEMBER:
                    user = Users.query.filter_by(userID=self.userID).first()
                    # [ Check ] Device Group
                    dg_list = Device_Groups.query.filter_by(FK_managers_id=user.FK_managers_id).all()
                elif self.userType == USER_TYPE_MANAGER:
                    manager = Managers.query.filter_by(userID=self.userID).first()
                    # [ Check ] Device Group
                    dg_list = Device_Groups.query.filter_by(FK_managers_id=manager.id).all()
                elif self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_SUPERADMIN:
                    return result(404, "Administrator does not need device group information.", None, None, "by KSM")

                if len(dg_list) == 0:
                    return result(404, "The device group does not exist.", None, None, "by KSM")
                else:
                    objects = []

                    for dg in dg_list:
                        # [ Merge ]
                        objects.append({
                            "key": dg.id,
                            "group": dg.name,
                            "polling_time": dg.polling_time
                        })

                    return result(200, "Get the Device Group List Info", objects, None, "by KSM")

    def post(self):
        input = self.parser.parse_args()

        if self.userType == USER_TYPE_GUEST:
            return result(401, "You do not have permission.", None, None, "by KSM")
        else:
            manager = Managers.query.filter_by(userID=self.userID).first()
            dg = Device_Groups()

            dg.name = input['name']
            dg.timer_onoff_sun = input['setting_flag']
            dg.timer_onoff_mon = input['setting_flag']
            dg.timer_onoff_tue = input['setting_flag']
            dg.timer_onoff_wed = input['setting_flag']
            dg.timer_onoff_thu = input['setting_flag']
            dg.timer_onoff_fri = input['setting_flag']
            dg.timer_onoff_sat = input['setting_flag']
            dg.timer_begin_sun = input['start_time']
            dg.timer_begin_mon = input['start_time']
            dg.timer_begin_tue = input['start_time']
            dg.timer_begin_wed = input['start_time']
            dg.timer_begin_thu = input['start_time']
            dg.timer_begin_fri = input['start_time']
            dg.timer_begin_sat = input['start_time']
            dg.timer_end_sun = input['end_time']
            dg.timer_end_mon = input['end_time']
            dg.timer_end_tue = input['end_time']
            dg.timer_end_wed = input['end_time']
            dg.timer_end_thu = input['end_time']
            dg.timer_end_fri = input['end_time']
            dg.timer_end_sat = input['end_time']
            dg.polling_time = 0
            #Incident
            dg.fp_send_email = 0
            dg.fp_send_phone = 0
            dg.ic_send_email = 0
            dg.ic_send_phone = 0
            dg.ic_R01_enable = 0
            dg.ic_R02_enable = 0
            dg.ic_R11_enable = 0
            dg.ic_R12_enable = 0
            dg.ic_R13_enable = 0
            dg.ic_R14_enable = 0
            dg.ic_O01_enable = 0
            dg.ic_O02_enable = 0
            dg.ic_O03_enable = 0
            dg.ic_O04_enable = 0
            dg.ic_O05_enable = 0
            dg.ic_O06_enable = 0
            dg.ic_O07_enable = 0
            dg.ic_O08_enable = 0
            dg.ic_O09_enable = 0
            dg.ic_Y01_enable = 0
            dg.ic_Y02_enable = 0
            dg.ic_Y03_enable = 0
            dg.ic_Y04_enable = 0
            dg.ic_Y05_enable = 0
            dg.ic_Y06_enable = 0
            dg.ic_Y07_enable = 0
            dg.ic_Y08_enable = 0
            #send mail, sms
            dg.send_method = 1
            dg.ic_R01_email_enable = 0
            dg.ic_R01_sms_enable = 0
            dg.ic_R02_email_enable = 0
            dg.ic_R02_sms_enable = 0
            dg.ic_R11_email_enable = 0
            dg.ic_R11_sms_enable = 0
            dg.ic_R12_email_enable = 0
            dg.ic_R12_sms_enable = 0
            dg.ic_R13_email_enable = 0
            dg.ic_R13_sms_enable = 0
            dg.ic_R14_email_enable = 0
            dg.ic_R14_sms_enable = 0
            dg.ic_O01_email_enable = 0
            dg.ic_O01_sms_enable = 0
            dg.ic_O02_email_enable = 0
            dg.ic_O02_sms_enable = 0
            dg.ic_O03_email_enable = 0
            dg.ic_O03_sms_enable = 0
            dg.ic_Y01_email_enable = 0
            dg.ic_Y01_sms_enable = 0
            dg.ic_Y02_email_enable = 0
            dg.ic_Y02_sms_enable = 0
            dg.ic_Y03_email_enable = 0
            dg.ic_Y03_sms_enable = 0
            dg.ic_Y04_email_enable = 0
            dg.ic_Y04_sms_enable = 0
            dg.ic_Y05_email_enable = 0
            dg.ic_Y05_sms_enable = 0
            dg.ic_Y06_email_enable = 0
            dg.ic_Y06_sms_enable = 0
            dg.ic_Y07_email_enable = 0
            dg.ic_Y07_sms_enable = 0
            dg.ic_Y08_email_enable = 0
            dg.ic_Y08_sms_enable = 0

            dg.FK_managers_id = manager.id

            db.session.add(dg)
            db.session.commit()

            return result(200, "Generate Device Group", None, None, "by KSM")

class DeviceGroupAPI(Resource):
    """
    [ Device Group ]
    @ param: key
    @ GET: Get the Device Group Info
    @ PUT: Touch the Device Group Info
    @ DEL: Delete the Device Group Info
    - by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("key", type=str, location="json")
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("th_cpu_usage_error", type=str, location="json")
        self.parser.add_argument("th_cpu_usage_warning", type=str, location="json")
        self.parser.add_argument("th_mem_usage_error", type=str, location="json")
        self.parser.add_argument("th_mem_usage_warning", type=str, location="json")
        self.parser.add_argument("th_hdd_usage_error", type=str, location="json")
        self.parser.add_argument("th_hdd_usage_warning", type=str, location="json")
        self.parser.add_argument("th_cpu_temp_error", type=str, location="json")
        self.parser.add_argument("th_cpu_temp_warning", type=str, location="json")
        self.parser.add_argument("th_hdd_temp_error", type=str, location="json")
        self.parser.add_argument("th_hdd_temp_warning", type=str, location="json")
        self.parser.add_argument("th_fan_speed_error", type=str, location="json")
        self.parser.add_argument("th_fan_speed_warning", type=str, location="json")
        self.parser.add_argument("th_res_time_error", type=str, location="json")
        self.parser.add_argument("th_res_time_warning", type=str, location="json")
        self.parser.add_argument("th_dp_temp_error", type=str, location="json")
        self.parser.add_argument("th_dp_temp_warning", type=str, location="json")
        self.parser.add_argument("th_app1", type=bool, location="json")
        self.parser.add_argument("th_app2", type=bool, location="json")
        self.parser.add_argument("th_app3", type=bool, location="json")
        self.parser.add_argument("th_app4", type=bool, location="json")
        self.parser.add_argument('group_timer_flag', type=bool, location="json")
        self.parser.add_argument('time_zone', type=str, location="json")
        self.parser.add_argument('timer_onoff_sun', type=bool, location="json")
        self.parser.add_argument('timer_begin_sun', type=str, location="json")
        self.parser.add_argument('timer_end_sun', type=str, location="json")
        self.parser.add_argument('timer_onoff_mon', type=bool, location="json")
        self.parser.add_argument('timer_begin_mon', type=str, location="json")
        self.parser.add_argument('timer_end_mon', type=str, location="json")
        self.parser.add_argument('timer_onoff_tue', type=bool, location="json")
        self.parser.add_argument('timer_begin_tue', type=str, location="json")
        self.parser.add_argument('timer_end_tue', type=str, location="json")
        self.parser.add_argument('timer_onoff_wed', type=bool, location="json")
        self.parser.add_argument('timer_begin_wed', type=str, location="json")
        self.parser.add_argument('timer_end_wed', type=str, location="json")
        self.parser.add_argument('timer_onoff_thu', type=bool, location="json")
        self.parser.add_argument('timer_begin_thu', type=str, location="json")
        self.parser.add_argument('timer_end_thu', type=str, location="json")
        self.parser.add_argument('timer_onoff_fri', type=bool, location="json")
        self.parser.add_argument('timer_begin_fri', type=str, location="json")
        self.parser.add_argument('timer_end_fri', type=str, location="json")
        self.parser.add_argument('timer_onoff_sat', type=bool, location="json")
        self.parser.add_argument('timer_begin_sat', type=str, location="json")
        self.parser.add_argument('timer_end_sat', type=str, location="json")
        self.parser.add_argument('polling_time', type=str, location="json")
        self.parser.add_argument('email', type=str, location="json")
        self.parser.add_argument('phone', type=str, location="json")
        self.parser.add_argument('test_email', type=str, location="json")
        self.parser.add_argument('test_phone', type=str, location="json")
        self.parser.add_argument('test_country_code', type=str, location="json")
        self.parser.add_argument('fp_send_email', type=bool, location="json")
        self.parser.add_argument('fp_send_phone', type=bool, location="json")
        self.parser.add_argument('ic_send_email', type=bool, location="json")
        self.parser.add_argument('ic_send_phone', type=bool, location="json")
        self.parser.add_argument('send_method', type=bool, location="json")
        # [ Code ] RED
        self.parser.add_argument('ic_R01_enable', type=bool, location="json")
        self.parser.add_argument('ic_R01_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_R01_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_R01_limit', type=str, location="json")
        self.parser.add_argument('ic_R01_timeout', type=str, location="json")
        self.parser.add_argument('ic_R02_enable', type=bool, location="json")
        self.parser.add_argument('ic_R02_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_R02_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_R02_limit', type=str, location="json")
        self.parser.add_argument('ic_R02_timeout', type=str, location="json")
        self.parser.add_argument('ic_R11_enable', type=bool, location="json")
        self.parser.add_argument('ic_R11_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_R11_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_R11_limit', type=str, location="json")
        self.parser.add_argument('ic_R11_timeout', type=str, location="json")
        self.parser.add_argument('ic_R12_enable', type=bool, location="json")
        self.parser.add_argument('ic_R12_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_R12_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_R12_limit', type=str, location="json")
        self.parser.add_argument('ic_R12_timeout', type=str, location="json")
        self.parser.add_argument('ic_R13_enable', type=bool, location="json")
        self.parser.add_argument('ic_R13_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_R13_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_R13_limit', type=str, location="json")
        self.parser.add_argument('ic_R13_timeout', type=str, location="json")
        self.parser.add_argument('ic_R14_enable', type=bool, location="json")
        self.parser.add_argument('ic_R14_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_R14_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_R14_limit', type=str, location="json")
        self.parser.add_argument('ic_R14_timeout', type=str, location="json")
        # [ Code ] ORANGE
        self.parser.add_argument('ic_O01_enable', type=bool, location="json")
        self.parser.add_argument('ic_O01_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_O01_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_O01_limit', type=str, location="json")
        self.parser.add_argument('ic_O01_timeout', type=str, location="json")
        self.parser.add_argument('ic_O02_enable', type=bool, location="json")
        self.parser.add_argument('ic_O02_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_O02_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_O02_limit', type=str, location="json")
        self.parser.add_argument('ic_O02_timeout', type=str, location="json")
        self.parser.add_argument('ic_O03_enable', type=bool, location="json")
        self.parser.add_argument('ic_O03_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_O03_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_O03_limit', type=str, location="json")
        self.parser.add_argument('ic_O03_timeout', type=str, location="json")
        self.parser.add_argument('ic_O04_enable', type=bool, location="json")
        self.parser.add_argument('ic_O04_limit', type=str, location="json")
        self.parser.add_argument('ic_O04_timeout', type=str, location="json")
        self.parser.add_argument('ic_O05_enable', type=bool, location="json")
        self.parser.add_argument('ic_O05_limit', type=str, location="json")
        self.parser.add_argument('ic_O05_timeout', type=str, location="json")
        self.parser.add_argument('ic_O06_enable', type=bool, location="json")
        self.parser.add_argument('ic_O06_limit', type=str, location="json")
        self.parser.add_argument('ic_O06_timeout', type=str, location="json")
        self.parser.add_argument('ic_O07_enable', type=bool, location="json")
        self.parser.add_argument('ic_O07_limit', type=str, location="json")
        self.parser.add_argument('ic_O07_timeout', type=str, location="json")
        self.parser.add_argument('ic_O08_enable', type=bool, location="json")
        self.parser.add_argument('ic_O08_limit', type=str, location="json")
        self.parser.add_argument('ic_O08_timeout', type=str, location="json")
        self.parser.add_argument('ic_O09_enable', type=bool, location="json")
        self.parser.add_argument('ic_O09_limit', type=str, location="json")
        self.parser.add_argument('ic_O09_timeout', type=str, location="json")
        # [ Code ] YELLOW
        self.parser.add_argument('ic_Y01_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y01_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y01_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y01_limit', type=str, location="json")
        self.parser.add_argument('ic_Y01_timeout', type=str, location="json")
        self.parser.add_argument('ic_Y02_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y02_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y02_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y02_limit', type=str, location="json")
        self.parser.add_argument('ic_Y02_timeout', type=str, location="json")
        self.parser.add_argument('ic_Y03_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y03_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y03_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y03_limit', type=str, location="json")
        self.parser.add_argument('ic_Y03_timeout', type=str, location="json")
        self.parser.add_argument('ic_Y04_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y04_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y04_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y04_limit', type=str, location="json")
        self.parser.add_argument('ic_Y04_timeout', type=str, location="json")
        self.parser.add_argument('ic_Y05_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y05_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y05_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y05_limit', type=str, location="json")
        self.parser.add_argument('ic_Y05_timeout', type=str, location="json")
        self.parser.add_argument('ic_Y06_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y06_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y06_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y06_limit', type=str, location="json")
        self.parser.add_argument('ic_Y06_timeout', type=str, location="json")
        self.parser.add_argument('ic_Y07_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y07_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y07_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y07_limit', type=str, location="json")
        self.parser.add_argument('ic_Y07_timeout', type=str, location="json")
        self.parser.add_argument('ic_Y08_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y08_email_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y08_sms_enable', type=bool, location="json")
        self.parser.add_argument('ic_Y08_limit', type=str, location="json")
        self.parser.add_argument('ic_Y08_timeout', type=str, location="json")

        super(DeviceGroupAPI, self).__init__()

    def get(self, key):
        if self.userType is not USER_TYPE_MANAGER:
            return result(400, 'You do not have permission', None, None, "by KSM")
        if key is None:
            return result(400, 'There is no essential parameters.', None, None, "by KSM")
        else:
            objects = []
            # [ Check ] Device Group
            dg = Device_Groups.query.filter_by(id=key).first()
            if dg is None:
                return result(404, 'Can not find the Device group.', None, None, "by KSM")
            else:
                objects.append({
                    "key": dg.id,
                    "name": dg.name,
                    "cpu_usage_error": dg.th_cpu_usage_error,
                    "cpu_usage_warning": dg.th_cpu_usage_warning,
                    "mem_usage_error": dg.th_mem_usage_error,
                    "mem_usage_warning": dg.th_mem_usage_warning,
                    "hdd_usage_error": dg.th_hdd_usage_error,
                    "hdd_usage_warning": dg.th_hdd_usage_warning,
                    "cpu_temp_error": dg.th_cpu_temp_error,
                    "cpu_temp_warning": dg.th_cpu_temp_warning,
                    "hdd_temp_error": dg.th_hdd_temp_error,
                    "hdd_temp_warning": dg.th_hdd_temp_warning,
                    "fan_speed_error": dg.th_fan_speed_error,
                    "fan_speed_warning": dg.th_fan_speed_warning,
                    "res_time_error": dg.th_res_time_error,
                    "res_time_warning": dg.th_res_time_warning,
                    "dp_temp_error": dg.th_dp_temp_error,
                    "dp_temp_warning": dg.th_dp_temp_warning,
                    "app1": dg.th_app1,
                    "app2": dg.th_app2,
                    "app3": dg.th_app3,
                    "app4": dg.th_app4,
                    "group_timer_flag": dg.group_timer_flag,
                    "time_zone": dg.time_zone,
                    'timer_onoff_sun': dg.timer_onoff_sun,
                    'timer_begin_sun': json_encoder(dg.timer_begin_sun),
                    'timer_end_sun': json_encoder(dg.timer_end_sun),
                    'timer_onoff_mon': dg.timer_onoff_mon,
                    'timer_begin_mon': json_encoder(dg.timer_begin_mon),
                    'timer_end_mon': json_encoder(dg.timer_end_mon),
                    'timer_onoff_tue': dg.timer_onoff_tue,
                    'timer_begin_tue': json_encoder(dg.timer_begin_tue),
                    'timer_end_tue': json_encoder(dg.timer_end_tue),
                    'timer_onoff_wed': dg.timer_onoff_wed,
                    'timer_begin_wed': json_encoder(dg.timer_begin_wed),
                    'timer_end_wed': json_encoder(dg.timer_end_wed),
                    'timer_onoff_thu': dg.timer_onoff_thu,
                    'timer_begin_thu': json_encoder(dg.timer_begin_thu),
                    'timer_end_thu': json_encoder(dg.timer_end_thu),
                    'timer_onoff_fri': dg.timer_onoff_fri,
                    'timer_begin_fri': json_encoder(dg.timer_begin_fri),
                    'timer_end_fri': json_encoder(dg.timer_end_fri),
                    'timer_onoff_sat': dg.timer_onoff_sat,
                    'timer_begin_sat': json_encoder(dg.timer_begin_sat),
                    'timer_end_sat': json_encoder(dg.timer_end_sat),
                    "polling_time": dg.polling_time,
                    "email": dg.email,
                    "phone": dg.phone,
                    "test_email": dg.test_email,
                    "test_phone": dg.test_phone,
                    "test_country_code": dg.test_country_code,
                    "fp_send_email": dg.fp_send_email,
                    "fp_send_phone": dg.fp_send_phone,
                    "ic_send_email": dg.ic_send_email,
                    "ic_send_phone": dg.ic_send_phone,
                    "send_method": dg.send_method,
                    # [ Code ] RED
                    "ic_R01_enable": dg.ic_R01_enable,
                    "ic_R01_email_enable": dg.ic_R01_email_enable,
                    "ic_R01_sms_enable": dg.ic_R01_sms_enable,
                    "ic_R01_limit": dg.ic_R01_limit,
                    "ic_R01_timeout": dg.ic_R01_timeout,
                    "ic_R02_enable": dg.ic_R02_enable,
                    "ic_R02_email_enable": dg.ic_R02_email_enable,
                    "ic_R02_sms_enable": dg.ic_R02_sms_enable,
                    "ic_R02_limit": dg.ic_R02_limit,
                    "ic_R02_timeout": dg.ic_R02_timeout,
                    "ic_R11_enable": dg.ic_R11_enable,
                    "ic_R11_email_enable": dg.ic_R11_email_enable,
                    "ic_R11_sms_enable": dg.ic_R11_sms_enable,
                    "ic_R11_limit": dg.ic_R11_limit,
                    "ic_R11_timeout": dg.ic_R11_timeout,
                    "ic_R12_enable": dg.ic_R12_enable,
                    "ic_R12_email_enable": dg.ic_R12_email_enable,
                    "ic_R12_sms_enable": dg.ic_R12_sms_enable,
                    "ic_R12_limit": dg.ic_R12_limit,
                    "ic_R12_timeout": dg.ic_R12_timeout,
                    "ic_R13_enable": dg.ic_R13_enable,
                    "ic_R13_email_enable": dg.ic_R13_email_enable,
                    "ic_R13_sms_enable": dg.ic_R13_sms_enable,
                    "ic_R13_limit": dg.ic_R13_limit,
                    "ic_R13_timeout": dg.ic_R13_timeout,
                    "ic_R14_enable": dg.ic_R14_enable,
                    "ic_R14_email_enable": dg.ic_R14_email_enable,
                    "ic_R14_sms_enable": dg.ic_R14_sms_enable,
                    "ic_R14_limit": dg.ic_R14_limit,
                    "ic_R14_timeout": dg.ic_R14_timeout,
                    # [ Code ] ORANGE
                    "ic_O01_enable": dg.ic_O01_enable,
                    "ic_O01_email_enable": dg.ic_O01_email_enable,
                    "ic_O01_sms_enable": dg.ic_O01_sms_enable,
                    "ic_O01_limit": dg.ic_O01_limit,
                    "ic_O01_timeout": dg.ic_O01_timeout,
                    "ic_O02_enable": dg.ic_O02_enable,
                    "ic_O02_email_enable": dg.ic_O02_email_enable,
                    "ic_O02_sms_enable": dg.ic_O02_sms_enable,
                    "ic_O02_limit": dg.ic_O02_limit,
                    "ic_O02_timeout": dg.ic_O02_timeout,
                    "ic_O03_enable": dg.ic_O03_enable,
                    "ic_O03_email_enable": dg.ic_O03_email_enable,
                    "ic_O03_sms_enable": dg.ic_O03_sms_enable,
                    "ic_O03_limit": dg.ic_O03_limit,
                    "ic_O03_timeout": dg.ic_O03_timeout,
                    "ic_O04_enable": dg.ic_O04_enable,
                    "ic_O04_limit": dg.ic_O04_limit,
                    "ic_O04_timeout": dg.ic_O04_timeout,
                    "ic_O05_enable": dg.ic_O05_enable,
                    "ic_O05_limit": dg.ic_O05_limit,
                    "ic_O05_timeout": dg.ic_O05_timeout,
                    "ic_O06_enable": dg.ic_O06_enable,
                    "ic_O06_limit": dg.ic_O06_limit,
                    "ic_O06_timeout": dg.ic_O06_timeout,
                    "ic_O07_enable": dg.ic_O07_enable,
                    "ic_O07_limit": dg.ic_O07_limit,
                    "ic_O07_timeout": dg.ic_O07_timeout,
                    "ic_O08_enable": dg.ic_O08_enable,
                    "ic_O08_limit": dg.ic_O08_limit,
                    "ic_O08_timeout": dg.ic_O08_timeout,
                    "ic_O09_enable": dg.ic_O09_enable,
                    "ic_O09_limit": dg.ic_O09_limit,
                    "ic_O09_timeout": dg.ic_O09_timeout,
                    # [ Code ] YELLOW
                    "ic_Y01_enable": dg.ic_Y01_enable,
                    "ic_Y01_email_enable": dg.ic_Y01_email_enable,
                    "ic_Y01_sms_enable": dg.ic_Y01_sms_enable,
                    "ic_Y01_limit": dg.ic_Y01_limit,
                    "ic_Y01_timeout": dg.ic_Y01_timeout,
                    "ic_Y02_enable": dg.ic_Y02_enable,
                    "ic_Y02_email_enable": dg.ic_Y02_email_enable,
                    "ic_Y02_sms_enable": dg.ic_Y02_sms_enable,
                    "ic_Y02_limit": dg.ic_Y02_limit,
                    "ic_Y02_timeout": dg.ic_Y02_timeout,
                    "ic_Y03_enable": dg.ic_Y03_enable,
                    "ic_Y03_email_enable": dg.ic_Y03_email_enable,
                    "ic_Y03_sms_enable": dg.ic_Y03_sms_enable,
                    "ic_Y03_limit": dg.ic_Y03_limit,
                    "ic_Y03_timeout": dg.ic_Y03_timeout,
                    "ic_Y04_enable": dg.ic_Y04_enable,
                    "ic_Y04_email_enable": dg.ic_Y04_email_enable,
                    "ic_Y04_sms_enable": dg.ic_Y04_sms_enable,
                    "ic_Y04_limit": dg.ic_Y04_limit,
                    "ic_Y04_timeout": dg.ic_Y04_timeout,
                    "ic_Y05_enable": dg.ic_Y05_enable,
                    "ic_Y05_email_enable": dg.ic_Y05_email_enable,
                    "ic_Y05_sms_enable": dg.ic_Y05_sms_enable,
                    "ic_Y05_limit": dg.ic_Y05_limit,
                    "ic_Y05_timeout": dg.ic_Y05_timeout,
                    "ic_Y06_enable": dg.ic_Y06_enable,
                    "ic_Y06_email_enable": dg.ic_Y06_email_enable,
                    "ic_Y06_sms_enable": dg.ic_Y06_sms_enable,
                    "ic_Y06_limit": dg.ic_Y06_limit,
                    "ic_Y06_timeout": dg.ic_Y06_timeout,
                    "ic_Y07_enable": dg.ic_Y07_enable,
                    "ic_Y07_email_enable": dg.ic_Y07_email_enable,
                    "ic_Y07_sms_enable": dg.ic_Y07_sms_enable,
                    "ic_Y07_limit": dg.ic_Y07_limit,
                    "ic_Y07_timeout": dg.ic_Y07_timeout,
                    "ic_Y08_enable": dg.ic_Y08_enable,
                    "ic_Y08_email_enable": dg.ic_Y08_email_enable,
                    "ic_Y08_sms_enable": dg.ic_Y08_sms_enable,
                    "ic_Y08_limit": dg.ic_Y08_limit,
                    "ic_Y08_timeout": dg.ic_Y08_timeout
                })
                return result(200, "GET the Device Group Info", objects, None, "by KSM")

    def put(self, key):
        if self.userType is not USER_TYPE_MANAGER:
            return result(400, 'You do not have permission', None, None, "by KSM")

        input = self.parser.parse_args()

        # [ Check ] polling time
        if input['polling_time'] is not None:
            if int(input['polling_time']) > 0 and int(input['polling_time']) < 30:
                return result(400, 'The polling time value is not between 1 and 29.', None, None, "by KSM")

        # [ Check ] input
        var_name = input['name']
        del input['token']
        for k, v in input.items():
            if v is None:
                del input[k]

        if key is None:
            return result(400, 'There is no essential parameters.', None, None, "by KSM")
        else:
            # [ Check ] Device Group
            dg = Device_Groups.query.filter_by(id=key).first()

            if dg is None:
                return result(404, 'Can not find the Device group.', None, None, "by KSM")
            else:
                old_dg_name = dg.name
                db.session.query(Device_Groups).filter_by(id=key).update(input)
                db.session.commit()

                if var_name is not None:
                    db.session.query(MediaPlayers).filter(or_(*self.filter_group)).filter(MediaPlayers.dg_name == old_dg_name).update(dict(dg_name=var_name))
                    db.session.commit()
                return result(200, "Touch the Device Group Info", None, None, "by KSM")

    def delete(self, key):
        if key is None:
            return result(400, 'There is no essential parameters.', None, None, "by KSM")
        else:
            # [ Check ] Device Group
            dg = Device_Groups.query.filter_by(id=key).first()

            if dg is not None:
                db.session.delete(dg)
                db.session.commit()
                return result(200, "Delete the Device Group Info", None, None, "by KSM")
        return result(200, "Delete the Device Group Info", None, None, "by KSM")

class FaultPredictListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("FK_device_groups_id", type=int, location="json")
        self.parser.add_argument("category_id", type=int, location="json")
        self.parser.add_argument("ruleset", type=str, location="json")
        self.parser.add_argument("active", type=int, location="json")
        self.parser.add_argument("mail_active", type=int, location="json")
        self.parser.add_argument("sms_active", type=int, location="json")
        self.parser.add_argument("th1_device_id", type=int, location="json")
        self.parser.add_argument("th1_compare_id", type=int, location="json")
        self.parser.add_argument("th1_num", type=int, location="json")
        self.parser.add_argument("th1_limit", type=int, location="json")
        self.parser.add_argument("th1_timeout", type=int, location="json")
        self.parser.add_argument("th1_co_id", type=int, location="json")
        self.parser.add_argument("th2_device_id", type=int, location="json")
        self.parser.add_argument("th2_compare_id", type=int, location="json")
        self.parser.add_argument("th2_num", type=int, location="json")
        self.parser.add_argument("th2_limit", type=int, location="json")
        self.parser.add_argument("th2_timeout", type=int, location="json")
        self.parser.add_argument("th2_co_id", type=int, location="json")
        self.parser.add_argument("th3_device_id", type=int, location="json")
        self.parser.add_argument("th3_compare_id", type=int, location="json")
        self.parser.add_argument("th3_num", type=int, location="json")
        self.parser.add_argument("th3_limit", type=int, location="json")
        self.parser.add_argument("th3_timeout", type=int, location="json")
        self.parser.add_argument("th3_co_id", type=int, location="json")
        self.parser.add_argument("th4_device_id", type=int, location="json")
        self.parser.add_argument("th4_compare_id", type=int, location="json")
        self.parser.add_argument("th4_num", type=int, location="json")
        self.parser.add_argument("th4_limit", type=int, location="json")
        self.parser.add_argument("th4_timeout", type=int, location="json")

        super(FaultPredictListAPI, self).__init__()

    def get(self):
        param = {}
        param["FK_device_groups_id"] = int(request.args.get('FK_device_groups_id'))

        predict_list = db.session.query(FaultPredicts).filter_by(FK_device_groups_id=param["FK_device_groups_id"]).order_by(FaultPredicts.create_time)

        objects = []
        for predict in predict_list:
            objects.append({
                "id": predict.id,
                "category_id": predict.category_id,
                "ruleset": predict.ruleset,
                "active": predict.active,
                "mail_active": predict.mail_active,
                "sms_active": predict.sms_active,
                "FK_device_groups_id": predict.FK_device_groups_id
            })

        return result(200, "Get Predict", objects, None, "by KSM")

    def post(self):
        input = self.parser.parse_args()

        fp = FaultPredicts()
        now = time.gmtime()
        current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
        fp.create_time = current_time

        if input['FK_device_groups_id'] is not None:
            fp.FK_device_groups_id = input['FK_device_groups_id']
        if input['category_id'] is not None:
            fp.category_id = input['category_id']
        if input['ruleset'] is not None:
            fp.ruleset = input['ruleset']
        if input['active'] is not None:
            fp.active = input['active']
        if input['mail_active'] is not None:
            fp.mail_active = input['mail_active']
        if input['sms_active'] is not None:
            fp.sms_active = input['sms_active']

        if input["th1_device_id"] is not None:
            fp.th1_device_id = input["th1_device_id"]
        if input["th1_compare_id"] is not None:
            fp.th1_compare_id = input["th1_compare_id"]
        if input["th1_num"] is not None:
            fp.th1_num = input["th1_num"]
        if input["th1_limit"] is not None:
            fp.th1_limit = input["th1_limit"]
        if input["th1_timeout"] is not None:
            fp.th1_timeout = input["th1_timeout"]
        if input["th1_co_id"] is not None:
            fp.th1_co_id = input["th1_co_id"]

            if input["th1_co_id"] == 0:
                fp.th2_device_id = 0
                fp.th2_compare_id = 0
                fp.th2_num = 0
                fp.th2_limit = 0
                fp.th2_timeout = 0
                fp.th2_co_id = 0

                fp.th3_device_id = 0
                fp.th3_compare_id = 0
                fp.th3_num = 0
                fp.th3_limit = 0
                fp.th3_timeout = 0
                fp.th3_co_id = 0

                fp.th4_device_id = 0
                fp.th4_compare_id = 0
                fp.th4_num = 0
                fp.th4_limit = 0
                fp.th4_timeout = 0
                db.session.add(fp)
                db.session.commit()

                return result(200, "Register Predict", None, None, "by KSM")

        if input["th2_device_id"] is not None:
            fp.th2_device_id = input["th2_device_id"]
        if input["th2_compare_id"] is not None:
            fp.th2_compare_id = input["th2_compare_id"]
        if input["th2_num"] is not None:
            fp.th2_num = input["th2_num"]
        if input["th2_limit"] is not None:
            fp.th2_limit = input["th2_limit"]
        if input["th2_timeout"] is not None:
            fp.th2_timeout = input["th2_timeout"]
        if input["th2_co_id"] is not None:
            fp.th2_co_id = input["th2_co_id"]
            if input["th2_co_id"] == 0:
                fp.th3_device_id = 0
                fp.th3_compare_id = 0
                fp.th3_num = 0
                fp.th3_limit = 0
                fp.th3_timeout = 0
                fp.th3_co_id = 0

                fp.th4_device_id = 0
                fp.th4_compare_id = 0
                fp.th4_num = 0
                fp.th4_limit = 0
                fp.th4_timeout = 0
                db.session.add(fp)
                db.session.commit()

                return result(200, "Register Predict", None, None, "by KSM")

        if input["th3_device_id"] is not None:
            fp.th3_device_id = input["th3_device_id"]
        if input["th3_compare_id"] is not None:
            fp.th3_compare_id = input["th3_compare_id"]
        if input["th3_num"] is not None:
            fp.th3_num = input["th3_num"]
        if input["th3_limit"] is not None:
            fp.th3_limit = input["th3_limit"]
        if input["th3_timeout"] is not None:
            fp.th3_timeout = input["th3_timeout"]
        if input["th3_co_id"] is not None:
            fp.th3_co_id = input["th3_co_id"]
            if input["th3_co_id"] == 0:
                fp.th4_device_id = 0
                fp.th4_compare_id = 0
                fp.th4_num = 0
                fp.th4_limit = 0
                fp.th4_timeout = 0
                db.session.add(fp)
                db.session.commit()

                return result(200, "Register Predict", None, None, "by KSM")

        if input["th4_device_id"] is not None:
            fp.th4_device_id = input["th4_device_id"]
        if input["th4_compare_id"] is not None:
            fp.th4_compare_id = input["th4_compare_id"]
        if input["th4_num"] is not None:
            fp.th4_num = input["th4_num"]
        if input["th4_limit"] is not None:
            fp.th4_limit = input["th4_limit"]
        if input["th4_timeout"] is not None:
            fp.th4_timeout = input["th4_timeout"]

        db.session.add(fp)
        db.session.commit()

        return result(200, "Register Predict", None, None, "by KSM")

class FaultPredictAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("active", type=int, location="json")
        self.parser.add_argument("mail_active", type=int, location="json")
        self.parser.add_argument("sms_active", type=int, location="json")
        self.parser.add_argument("th1_device_id", type=int, location="json")
        self.parser.add_argument("th1_compare_id", type=int, location="json")
        self.parser.add_argument("th1_num", type=int, location="json")
        self.parser.add_argument("th1_limit", type=int, location="json")
        self.parser.add_argument("th1_timeout", type=int, location="json")
        self.parser.add_argument("th1_co_id", type=int, location="json")
        self.parser.add_argument("th2_device_id", type=int, location="json")
        self.parser.add_argument("th2_compare_id", type=int, location="json")
        self.parser.add_argument("th2_num", type=int, location="json")
        self.parser.add_argument("th2_limit", type=int, location="json")
        self.parser.add_argument("th2_timeout", type=int, location="json")
        self.parser.add_argument("th2_co_id", type=int, location="json")
        self.parser.add_argument("th3_device_id", type=int, location="json")
        self.parser.add_argument("th3_compare_id", type=int, location="json")
        self.parser.add_argument("th3_num", type=int, location="json")
        self.parser.add_argument("th3_limit", type=int, location="json")
        self.parser.add_argument("th3_timeout", type=int, location="json")
        self.parser.add_argument("th3_co_id", type=int, location="json")
        self.parser.add_argument("th4_device_id", type=int, location="json")
        self.parser.add_argument("th4_compare_id", type=int, location="json")
        self.parser.add_argument("th4_num", type=int, location="json")
        self.parser.add_argument("th4_limit", type=int, location="json")
        self.parser.add_argument("th4_timeout", type=int, location="json")

        super(FaultPredictAPI, self).__init__()

    def get(self, key):
        param = {}

        predict = db.session.query(FaultPredicts).filter_by(id=key).one()

        objects = []

        objects.append({
            "id": predict.id,
            "category_id": predict.category_id,
            "ruleset": predict.ruleset,
            "active": predict.active,
            "mail_active": predict.mail_active,
            "sms_active": predict.sms_active,
            "th1_device_id": predict.th1_device_id,
            "th1_compare_id": predict.th1_compare_id,
            "th1_num": predict.th1_num,
            "th1_limit": predict.th1_limit,
            "th1_timeout": predict.th1_timeout,
            "th1_co_id": predict.th1_co_id,
            "th2_device_id": predict.th2_device_id,
            "th2_compare_id": predict.th2_compare_id,
            "th2_num": predict.th2_num,
            "th2_limit": predict.th2_limit,
            "th2_timeout": predict.th2_timeout,
            "th2_co_id": predict.th2_co_id,
            "th3_device_id": predict.th3_device_id,
            "th3_compare_id": predict.th3_compare_id,
            "th3_num": predict.th3_num,
            "th3_limit": predict.th3_limit,
            "th3_timeout": predict.th3_timeout,
            "th3_co_id": predict.th3_co_id,
            "th4_device_id": predict.th4_device_id,
            "th4_compare_id": predict.th4_compare_id,
            "th4_num": predict.th4_num,
            "th4_limit": predict.th4_limit,
            "th4_timeout": predict.th4_timeout,

            "FK_device_groups_id": predict.FK_device_groups_id
        })

        return result(200, "Get Predict", objects, None, "by KSM")

    def put(self, key):
        input = self.parser.parse_args()

        update_params = {}

        if input["active"] is not None:
            update_params["active"] = input["active"]
        if input['mail_active'] is not None:
            update_params['mail_active'] = input['mail_active']
        if input['sms_active'] is not None:
            update_params['sms_active'] = input['sms_active']

        if input["th1_device_id"] is not None:
            update_params["th1_device_id"] = input["th1_device_id"]
        if input["th1_compare_id"] is not None:
            update_params["th1_compare_id"] = input["th1_compare_id"]
        if input["th1_num"] is not None:
            update_params["th1_num"] = input["th1_num"]
        if input["th1_limit"] is not None:
            update_params["th1_limit"] = input["th1_limit"]
        if input["th1_timeout"] is not None:
            update_params["th1_timeout"] = input["th1_timeout"]
        if input["th1_co_id"] is not None:
            update_params["th1_co_id"] = input["th1_co_id"]
            if input["th1_co_id"] == 0:
                update_params["th2_device_id"] = 0
                update_params["th2_compare_id"] = 0
                update_params["th2_num"] = 0
                update_params["th2_limit"] = 0
                update_params["th2_timeout"] = 0
                update_params["th2_co_id"] = 0

                update_params["th3_device_id"] = 0
                update_params["th3_compare_id"] = 0
                update_params["th3_num"] = 0
                update_params["th3_limit"] = 0
                update_params["th3_timeout"] = 0
                update_params["th3_co_id"] = 0

                update_params["th4_device_id"] = 0
                update_params["th4_compare_id"] = 0
                update_params["th4_num"] = 0
                update_params["th4_limit"] = 0
                update_params["th4_timeout"] = 0
                db.session.query(FaultPredicts).filter_by(id=key).update(update_params)
                db.session.commit()

                return result(200, "Modified Predict", None, None, "by KSM")

        if input["th2_device_id"] is not None:
            update_params["th2_device_id"] = input["th2_device_id"]
        if input["th2_compare_id"] is not None:
            update_params["th2_compare_id"] = input["th2_compare_id"]
        if input["th2_num"] is not None:
            update_params["th2_num"] = input["th2_num"]
        if input["th2_limit"] is not None:
            update_params["th2_limit"] = input["th2_limit"]
        if input["th2_timeout"] is not None:
            update_params["th2_timeout"] = input["th2_timeout"]
        if input["th2_co_id"] is not None:
            update_params["th2_co_id"] = input["th2_co_id"]
            if input["th2_co_id"] == 0:
                update_params["th3_device_id"] = 0
                update_params["th3_compare_id"] = 0
                update_params["th3_num"] = 0
                update_params["th3_limit"] = 0
                update_params["th3_timeout"] = 0
                update_params["th3_co_id"] = 0

                update_params["th4_device_id"] = 0
                update_params["th4_compare_id"] = 0
                update_params["th4_num"] = 0
                update_params["th4_limit"] = 0
                update_params["th4_timeout"] = 0
                db.session.query(FaultPredicts).filter_by(id=key).update(update_params)
                db.session.commit()

                return result(200, "Modified Predict", None, None, "by KSM")

        if input["th3_device_id"] is not None:
            update_params["th3_device_id"] = input["th3_device_id"]
        if input["th3_compare_id"] is not None:
            update_params["th3_compare_id"] = input["th3_compare_id"]
        if input["th3_num"] is not None:
            update_params["th3_num"] = input["th3_num"]
        if input["th3_limit"] is not None:
            update_params["th3_limit"] = input["th3_limit"]
        if input["th3_timeout"] is not None:
            update_params["th3_timeout"] = input["th3_timeout"]
        if input["th3_co_id"] is not None:
            update_params["th3_co_id"] = input["th3_co_id"]
            if input["th3_co_id"] == 0:
                update_params["th4_device_id"] = 0
                update_params["th4_compare_id"] = 0
                update_params["th4_num"] = 0
                update_params["th4_limit"] = 0
                update_params["th4_timeout"] = 0
                db.session.query(FaultPredicts).filter_by(id=key).update(update_params)
                db.session.commit()

                return result(200, "Modified Predict", None, None, "by KSM")

        if input["th4_device_id"] is not None:
            update_params["th4_device_id"] = input["th4_device_id"]
        if input["th4_compare_id"] is not None:
            update_params["th4_compare_id"] = input["th4_compare_id"]
        if input["th4_num"] is not None:
            update_params["th4_num"] = input["th4_num"]
        if input["th4_limit"] is not None:
            update_params["th4_limit"] = input["th4_limit"]
        if input["th4_timeout"] is not None:
            update_params["th4_timeout"] = input["th4_timeout"]

        db.session.query(FaultPredicts).filter_by(id=key).update(update_params)
        db.session.commit()

        return result(200, "Modified Predict", None, None, "by KSM")

    def delete(self, key):
        fp = FaultPredicts.query.filter_by(id=key).one()
        db.session.delete(fp)
        db.session.commit()
        return result(200, "Delete Predict", None, None, "by KSM")

class CONN_DeviceGroupAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("dg_list", type=list, location="json")

        super(CONN_DeviceGroupAPI, self).__init__()

    def get(self, key):
        # [ Check ] user
        user = Users.query.filter_by(id=key).first()
        if user is None:
            return result(404, "No User", None, None, "by KSM")
        else:
            # [ Check ] device_group
            dg_list = Device_Groups.query.filter_by(FK_managers_id=user.FK_managers_id).all()
            if len(dg_list) == 0:
                return result(404, "No Device Group", None, None, "by KSM")
            else:
                objects = []
                for dg in dg_list:
                    # [ Check ] CONN
                    conn = CONN_Users_N_Device_Groups.query.filter_by(FK_users_id=user.id, FK_device_groups_id=dg.id).first()

                    user_conn = False
                    if conn is not None:
                        user_conn = True

                    objects.append({
                        "key": dg.id,
                        "name": dg.name,
                        "checked": user_conn
                    })
                return result(200, "GET the all the information of the Device Group generated by the Manager", objects, None, "by KSM")

    def put(self, key):
        input = self.parser.parse_args()

        # [ Check ] user
        user = Users.query.filter_by(id=key).first()

        # [ Check ] conn
        conn_list = CONN_Users_N_Device_Groups.query.filter_by(FK_users_id=user.id).all()
        if len(conn_list) != 0:
            # [ Clear ] conn
            for conn in conn_list:
                db.session.delete(conn)
                db.session.commit()

        if input['dg_list'] is not None:
            for dg_key in input['dg_list']:
                # [ New ] conn
                new_conn = CONN_Users_N_Device_Groups()

                new_conn.FK_users_id = user.id
                new_conn.FK_device_groups_id = dg_key

                db.session.add(new_conn)
                db.session.commit()

        return result(200, "Connected Devie_Groups And Users", None, None, "by KSM")

class AssignedTagListAPI(Resource):
    """
    [ Assigned Tag List ]
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------

        super(AssignedTagListAPI, self).__init__()

    def get(self):
        objects = []

        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
            # [ Check ] MANAGER
            manager = Managers.query.filter_by(userID=self.userID).first()

            conn_list = CONN_Users_N_MediaPlayers.query.filter_by(FK_managers_id=manager.id).all()
            if len(conn_list) == 0:
                return result(404, "No Data", None, None, "by KSM")
            else:
                for conn in conn_list:
                    mp = MediaPlayers.query.filter_by(id=conn.FK_mp_id).first()
                    if mp is None:
                        return result(500, "No mp Data", None, None, "by KSM")
                    else:
                        objects.append({
                            "key": mp.id,
                            "serial_number": mp.serial_number,
                            "alias": mp.alias
                        })

        elif self.userType == USER_TYPE_MEMBER:
            # [ Check ] USER
            user = Users.query.filter_by(userID=self.userID).first()

            conn_list = CONN_Users_N_MediaPlayers.query.filter_by(FK_users_id=user.id).all()
            if len(conn_list) == 0:
                return result(404, "No Data", None, None, "by KSM")
            else:
                for conn in conn_list:
                    mp = MediaPlayers.query.filter_by(id=conn.FK_mp_id).first()
                    if mp is None:
                        return result(500, "No mp Data", None, None, "by KSM")
                    else:
                        objects.append({
                            "key": mp.id,
                            "serial_number": mp.serial_number,
                            "alias": mp.alias
                        })
        elif self.userType == USER_TYPE_GUEST:
            return result(401, "No Auth", None, None, "by KSM")

        return result(200, "GET the Assigned Tag List", objects, None, "by KSM")

class TagListAPI(Resource):
    """
    [ Tag List ]
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("check_list", type=list, location="json")

        super(TagListAPI, self).__init__()

    def get(self):

        if self.userType == USER_TYPE_GUEST:
            return result(401, "No Auth", None, None, "by KSM")

        param = {}
        param["search_mode"] = request.args.get('search_mode')

        order_by = MediaPlayers.id
        if param["search_mode"] == 'pagination':
            #pagination setting
            limit = int(request.args.get('limit'))
            offset = int(request.args.get('offset'))
            order = request.args.get('order')
            by = request.args.get('by')
            order_by = by + " " + order.upper()

        mp_list = MediaPlayers.query.filter(or_(*self.filter_group)).order_by(order_by)
        total_count = mp_list.count()
        if param["search_mode"] == 'pagination':
            mp_list = mp_list.limit(limit).offset(offset)
        current_count = mp_list.count()

        if param["search_mode"] == 'pagination':
            # [ create ] Pagination
            previous = None
            next = None
            offset_list = {}
            offset_list["previous"] = offset - limit
            offset_list["next"] = offset + limit

            if offset != 0:
                previous = "/api/v1/tagging?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
                if (total_count - (offset+current_count)) > limit:
                    next = "/api/v1/tagging?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
                elif (total_count - (offset+current_count)) <= limit:
                    next = None

        meta = {}
        objects = []
        if current_count == 0:
            meta["total_count"] = 0
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = 0
                meta["previous"] = 0

            objects.append({
                "pk": "EMPTY",
            })

            return result(404, "No Device Info", objects, meta, "by KSM")
        else:
            meta["total_count"] = total_count
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = next
                meta["previous"] = previous

            for mp in mp_list:
                check_tag = False
                # [ Check ] conn
                if self.userType is USER_TYPE_SUPERADMIN or self.userType is USER_TYPE_ADMIN or self.userType is USER_TYPE_MANAGER:
                    conn = CONN_Users_N_MediaPlayers.query.filter_by(FK_managers_id=self.user_manager_id, FK_mp_id=mp.id).first()
                elif self.userType is USER_TYPE_MEMBER:
                    conn = CONN_Users_N_MediaPlayers.query.filter_by(FK_users_id=self.user_manager_id, FK_mp_id=mp.id).first()
                if conn is not None:
                    check_tag = True

                objects.append({
                    "key": mp.id,
                    "tag": check_tag,
                    "alias": mp.alias,
                    "serial_number": mp.serial_number,
                    "site": mp.site,
                    "location": mp.location,
                    "shop": mp.shop
                })

            msg = "GET the Tag List"

            return result(200, msg, objects, meta, "by KSM")

    def put(self):
        input = self.parser.parse_args()

        # [ Case by ] ADMIN or MANAGER
        if self.userType == USER_TYPE_ADMIN or self.userType == USER_TYPE_MANAGER:
            # [ Check ] Manager
            manager = Managers.query.filter_by(userID=self.userID).first()
            # [ Clear ] conn
            conn_list = CONN_Users_N_MediaPlayers.query.filter_by(FK_managers_id=manager.id).all()
            if len(conn_list) != 0:
                for conn in conn_list:
                    db.session.delete(conn)
                    db.session.commit()

            # [ New ] conn
            for key in input['check_list']:
                new_conn = CONN_Users_N_MediaPlayers()
                new_conn.FK_managers_id = manager.id
                new_conn.FK_mp_id = key

                db.session.add(new_conn)
                db.session.commit()

        elif self.userType == USER_TYPE_MEMBER:
            # [ Check ] User ID
            user = Users.query.filter_by(userID=self.userID).first()
            # [ Clear ] conn
            conn_list = CONN_Users_N_MediaPlayers.query.filter_by(FK_users_id=user.id).all()
            if len(conn_list) != 0:
                for conn in conn_list:
                    db.session.delete(conn)
                    db.session.commit()

            # [ New ] conn
            for key in input['check_list']:
                new_conn = CONN_Users_N_MediaPlayers()
                new_conn.FK_users_id = user.id
                new_conn.FK_mp_id = key

                db.session.add(new_conn)
                db.session.commit()

        elif self.userType == USER_TYPE_GUEST:
            pass
            #return result(403, "No Auth", None, None, "by KSM")

class DeviceManageListAPI(Resource):
    """
    [ Device Manage ]
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("serial_number", type=str, location="json")
        self.parser.add_argument("site", type=str, location="json")
        self.parser.add_argument("location", type=str, location="json")
        self.parser.add_argument("shop", type=str, location="json")

        super(DeviceManageListAPI, self).__init__()

    def get(self):
        if self.userType == USER_TYPE_GUEST:
            return result(403, "No Auth", None, None, "by KSM")
        else:
            if self.userType is USER_TYPE_ADMIN:
                self.filter_group = []
                manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
                if len(manager_ids) is 0:
                    self.filter_group.append(MediaPlayers.FK_managers_id == -1)
                else:
                    for mid in manager_ids:
                        self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)

                total_mp = MediaPlayers.query.filter(or_(*self.filter_group)).all()
            else:
                total_mp = MediaPlayers.query.filter_by(FK_managers_id=self.user_manager_id).all()

            total_count = len(total_mp)

            limit = int(request.args.get('limit'))
            offset = int(request.args.get('offset'))

            # [ Make ] ORDER BY
            order_by = request.args.get('order_by')
            order_by_str = ''
            if order_by is not None:
                order_by_str = 'mediaplayers.' + str(order_by)

            order = request.args.get('order')
            order_str = ''
            if order == '-':
                order_str = 'DESC'
            else:
                order_str = 'ASC'

            result_order_by = order_by_str + ' ' + order_str

            if self.userType is USER_TYPE_ADMIN:
                result_mp = MediaPlayers.query.filter(or_(*self.filter_group)).order_by(result_order_by).limit(limit).offset(offset)
            else:
                result_mp = MediaPlayers.query.order_by(result_order_by).filter_by(FK_managers_id=self.user_manager_id).limit(limit).offset(offset)
            current_count = result_mp.count()

            next = ''
            previous = ''

            # Creating a Pagenation
            if offset == 0:
                previous = None
                if current_count < total_count:
                    next_offset = offset+limit
                    next = "/api/v1/device/?limit="+str(limit)+"&offset="+str(next_offset)
                elif current_count == total_count:
                    next = None
                else:
                    return result(500, "Media Player Count Error", None, None, "by KSM")
            elif offset != 0:
                previous_offset = offset-limit
                previous = "/api/v1/device/?limit="+str(limit)+"&offset="+str(previous_offset)
                if (total_count - (offset+current_count)) > limit:
                    next_offset = offset+limit
                    next = "/api/v1/device/?limit="+str(limit)+"&offset="+str(next_offset)
                elif (total_count - (offset+current_count)) <= limit:
                    next = None

            meta = {
                "limit": limit,
                "next": next,
                "offset": offset,
                "previous": previous,
                "total_count": total_count
            }

            objects = []

            if current_count == 0:
                meta = {
                    "limit": limit,
                    "next": 0,
                    "offset": offset,
                    "previous": 0,
                    "total_count": 0
                }

                objects.append({

                })
                return result(404, "No Device Info", None, meta, "by KSM")
            else:
                for mp in result_mp:
                    # TODO : must be modified
                    manager = Managers.query.filter_by(id=mp.FK_managers_id).first()
                    if manager is None:
                        manager_id = ""
                    else:
                        manager_id = manager.userID

                    objects.append({
                        "key": mp.id,
                        "dg": mp.FK_device_groups_id,
                        "dg_name": mp.dg_name,
                        "alias": mp.alias,
                        "site": mp.site,
                        "location": mp.location,
                        "shop": mp.shop,
                        "created_date": json_encoder(mp.created_date),
                        "email": mp.email,
                        "phone": mp.phone,
                        "addr": mp.addr,
                        "memo": mp.memo,
                        "manager_id" : manager_id,
                        "manager_key" : mp.FK_managers_id
                    })
                return result(200, "GET the Deviec Manage Info", objects, meta, "by KSM")

    def post(self):
        input = self.parser.parse_args()

        mp = MediaPlayers()

        mp.alias = input['alias']
        mp.serial_number = input['serial_number']
        mp.manage_serial_number = input['serial_number']
        mp.site = input['site']
        mp.location = input['location']
        mp.shop = input['shop']
        mp.FK_managers_id = self.user_manager_id

        db.session.add(mp)
        db.session.commit()

        return result(200, "Register Device", None, None, "by KSM")

class DeviceManageAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("dg_name", type=str, location="json")
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("serial_number", type=str, location="json")
        self.parser.add_argument("site", type=str, location="json")
        self.parser.add_argument("location", type=str, location="json")
        self.parser.add_argument("shop", type=str, location="json")
        self.parser.add_argument("created_date", type=str, location="json")
        self.parser.add_argument("addr", type=str, location="json")
        self.parser.add_argument("memo", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")
        self.parser.add_argument("manager_id", type=str, location="json")

        super(DeviceManageAPI, self).__init__()

    def put(self, key):
        input = self.parser.parse_args()

        if input['dg_name'] is not None:
            dg = Device_Groups.query.filter_by(name=input['dg_name']).filter_by(FK_managers_id=self.user_manager_id).first()

            mp_update = {
                "FK_device_groups_id": dg.id,
                "dg_name": input['dg_name'],
                "alias": input['alias'],
                "site": input['site'],
                "location": input['location'],
                "shop": input['shop'],
                "created_date": input['created_date'],
                "addr": input['addr'],
                "memo": input['memo'],
                "phone": input['phone'],
                "email": input['email']
            }
        elif input['manager_id'] is not None:
            manager = Managers.query.filter_by(userID=input['manager_id']).first()

            mp_update = {
                "FK_managers_id": manager.id,
                "alias": input['alias'],
                "site": input['site'],
                "location": input['location'],
                "shop": input['shop'],
                "created_date": input['created_date'],
                "addr": input['addr'],
                "memo": input['memo'],
                "phone": input['phone'],
                "email": input['email']
            }
        else:
            mp_update = {
                "alias": input['alias'],
                "site": input['site'],
                "location": input['location'],
                "shop": input['shop'],
                "created_date": input['created_date'],
                "addr": input['addr'],
                "memo": input['memo'],
                "phone": input['phone'],
                "email": input['email']
            }

        # [ Check ] None
        for k, v in mp_update.items():
            if v is None:
                del mp_update[k]

        if len(mp_update) != 0:
            db.session.query(MediaPlayers).filter_by(id=key).update(mp_update)
            db.session.commit()

        return result(200, "Device Info Update", None, None, "by KSM")

    def delete(self, key):
        mp = MediaPlayers.query.filter_by(id=key).first()
        if mp is None:
            return result(404, "Not Found Device Info", None, None, "by KSM")
        else:
            dp_list = Displays.query.filter_by(FK_mp_id=mp.id).all()
            if len(dp_list) != 0:
                for dp in dp_list:
                    db.session.delete(dp)
                    db.session.commit()

            db.session.delete(mp)
            db.session.commit()

            return result(200, "Remove Device Info", None, None, "by KSM")

class DeviceManageDisplayListAPI(Resource):
    """
    [ Device Manage ]
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.userID == '':
            self.isValidToken = False
            self.userType = USER_TYPE_GUEST
        else:
            self.isValidToken = True
            self.userType, self.user_manager_id, self.manager_FK_id = self.token_manager.get_user_type(self.userID)

        #---------------------------------------------------------------------------------------------------------------
        # Filtering
        #---------------------------------------------------------------------------------------------------------------
        self.filter_group = []
        if self.userType is USER_TYPE_ADMIN:
            manager_ids = CONN_Admins_N_Managers.query.filter(CONN_Admins_N_Managers.FK_admin_id == self.user_manager_id).all()
            if len(manager_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for mid in manager_ids:
                    self.filter_group.append(MediaPlayers.FK_managers_id == mid.FK_manager_id)
        elif self.userType is USER_TYPE_MANAGER:
            self.filter_group.append(MediaPlayers.FK_managers_id == self.user_manager_id)
        elif self.userType is USER_TYPE_MEMBER:
            group_ids = CONN_Users_N_Device_Groups.query.join(Users, CONN_Users_N_Device_Groups.FK_users_id == Users.id).filter(Users.id == self.user_manager_id).all()

            # User doesn't have any device group
            if len(group_ids) is 0:
                self.filter_group.append(MediaPlayers.id == -1)
            else:
                for id in group_ids:
                    mp_ids = MediaPlayers.query.filter(MediaPlayers.FK_device_groups_id == id.FK_device_groups_id).filter(MediaPlayers.FK_managers_id == self.manager_FK_id)
                    if mp_ids is None:
                        self.filter_group.append(MediaPlayers.id == -1)
                    else:
                        for mp_id in mp_ids:
                            self.filter_group.append(MediaPlayers.id == mp_id.id)

        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("port_number", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("email", type=str, location="json")
        self.parser.add_argument("dp_group", type=str, location="json")
        self.parser.add_argument("set_id", type=str, location="json")

        super(DeviceManageDisplayListAPI, self).__init__()

    def get(self, key):
        if self.userType == USER_TYPE_GUEST:
            return result(403, "No Auth", None, None, "by KSM")
        else:
            total_dp = Displays.query.filter_by(FK_mp_id=key).all()

            total_count = len(total_dp)

            limit = int(request.args.get('limit'))
            offset = int(request.args.get('offset'))

            # [ Make ] ORDER BY
            order_by = request.args.get('order_by')
            order_by_str = ''
            if order_by is not None:
                order_by_str = 'displays.' + str(order_by)

            order = request.args.get('order')
            order_str = ''
            if order == '-':
                order_str = 'DESC'
            else:
                order_str = 'ASC'

            result_order_by = order_by_str + ' ' + order_str

            result_dp = Displays.query.filter_by(FK_mp_id=key).order_by(result_order_by).limit(limit).offset(offset)
            current_count = result_dp.count()

            next = ''
            previous = ''

            # Creating a Pagenation
            if offset == 0:
                previous = None
                if current_count < total_count:
                    next_offset = offset+limit
                    next = "/api/v1/device/?limit="+str(limit)+"&offset="+str(next_offset)
                elif current_count == total_count:
                    next = None
                else:
                    return result(500, "Media Player Count Error", None, None, "by KSM")
            elif offset != 0:
                previous_offset = offset-limit
                previous = "/api/v1/device/display/"+key+"?limit="+str(limit)+"&offset="+str(previous_offset)
                if (total_count - (offset+current_count)) > limit:
                    next_offset = offset+limit
                    next = "/api/v1/device/display/"+key+"?limit="+str(limit)+"&offset="+str(next_offset)
                elif (total_count - (offset+current_count)) <= limit:
                    next = None

            meta = {
                "limit": limit,
                "next": next,
                "offset": offset,
                "previous": previous,
                "total_count": total_count
            }

            objects = []

            if current_count == 0:
                meta = {
                    "limit": limit,
                    "next": 0,
                    "offset": offset,
                    "previous": 0,
                    "total_count": 0
                }

                objects.append({

                })
                return result(404, "No Display Info", None, meta, "by KSM")
            else:
                for dp in result_dp:
                    objects.append({
                        "key": dp.id,
                        "idx": dp.number,
                        "serial_number": dp.serial_number,
                        "alias": dp.alias,
                        "model": dp.model,
                        "port_number": dp.port_number,
                        "created_date": json_encoder(dp.created_date),
                        "email": dp.se_email,
                        "phone": dp.se_mobile,
                        "dp_group": dp.dp_group,
                        "set_id": dp.set_id
                    })
                return result(200, "GET the Device Manage(Display) List", objects, meta, "by KSM")

    def put(self, key):
        input = self.parser.parse_args()

        dp_update = {
            "alias": input['alias'],
            "port_number": input['port_number'],
            "se_mobile": input['phone'],
            "se_email": input['email'],
            "dp_group": input['dp_group'],
            "set_id": input['set_id']
        }

        # [ Check ] None
        for k, v in dp_update.items():
            if v is None:
                del dp_update[k]

        if len(dp_update) != 0:
            db.session.query(Displays).filter_by(id=key).update(dp_update)
            db.session.commit()

        return result(200, "Display Info Update", None, None, "by KSM")

    def delete(self, key):

        dp = Displays.query.filter_by(id=key).first()

        if dp is not None:
            db.session.delete(dp)
            db.session.commit()

        return result(200, "Remove Display Info", None, None, "by KSM")


#----------------------------------------------------------------------------------------------------------------------
#                                            FileUpload
#----------------------------------------------------------------------------------------------------------------------

class FileUploadAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

        super(FileUploadAPI, self).__init__()

    def allowed_file(self, filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    def post(self):
        file = request.files['file']
        if file and self.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return "YES"
        return "NO"

#----------------------------------------------------------------------------------------------------------------------
#                                            Test URI
#----------------------------------------------------------------------------------------------------------------------
class AlarmAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Token
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.token_manager = TokenManager.instance()
        self.userID = self.token_manager.validate_token(self.parser.parse_args()["token"])

        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("type", type=str, location="json")
        self.parser.add_argument("group_key", type=int, location="json")

        super(AlarmAPI, self).__init__()

    def post(self):
        input = self.parser.parse_args()
        #print input

        dg = Device_Groups.query.filter_by(id=input["group_key"]).first()
        if dg is not None:
            if input["type"] == "test_email":
                from_email = "system@adamssuite.com"
                to_email = dg.test_email
                headers = [
                    "From: " + from_email,
                    "Subject: " + "Test E-mail from adamssuite",
                    "To: " + to_email,
                    "MIME-Version: 1.0",
                    "Content-Type: text/html"]

                headers = "\r\n".join(headers)
                #print headers
                message = headers + '\r\n\r\n\r\n' + "Thank you for testing."

                # Send mail
                server = smtplib.SMTP('smtp.mandrillapp.com', 587)
                server.login('hchkim@unet.kr', 'T238oVg4BMzaqAbSEOsQaQ')
                server.sendmail(from_email, to_email, message)
                server.quit()
            elif input["type"] == "test_phone":
                phone = re.sub('[=.^#/!?%@:&$*}()+-]', '', dg.test_phone)

                sendsms = Clickatell('rainmaker', 'CAOMgbAedaVNZA', '3525182', sendmsg_defaults={'callback': cc.YES,'msg_type': cc.SMS_DEFAULT,
                                                                                 'deliv_ack': cc.YES,
                                                                                 'req_feat': cc.FEAT_ALPHA + cc.FEAT_NUMER + cc.FEAT_DELIVACK})

                resp = sendsms.sendmsg(recipients=[phone], text="Test SMS from adamssuite")

        return result(200, "Alarm TEST", None, None, "by KSM")

#----------------------------------------------------------------------------------------------------------------------
#                                            Agent URI
#----------------------------------------------------------------------------------------------------------------------
class MediaPlayersPollingTimeAPI(Resource):
    """
    [ Agent Polling Time ]
    For Agent
    @ GET : Returns the Polling Time of Media Player Table
    by KSM
    """
    def __init__(self):
        super(MediaPlayersPollingTimeAPI, self).__init__()

    def get(self, serial_number):
        # [ Check ] MP
        mp = MediaPlayers.query.join(Device_Groups, MediaPlayers.FK_device_groups_id == Device_Groups.id).filter(MediaPlayers.serial_number == serial_number).first()

        send_polling_time = DEFAULT_POLLING_TIME

        # [ SET ] make send polling time
        if mp is not None:
            send_polling_time = mp.device_group.polling_time

        dict_data = dict()
        dict_data["polling_time"] = send_polling_time

        return result(200, "GET the MediaPlayer Polling Time", dict_data, None, "by KSM")

class MediaPlayersAppAPI(Resource):
    """
    [ Agent Polling Time ]
    For Agent
    @ GET : Returns the App Info of Media Player Table
    by KSM
    """
    def __init__(self):
        super(MediaPlayersAppAPI, self).__init__()

    def get(self, serial_number):
        # [ Check ] MP
        mp = MediaPlayers.query.filter_by(serial_number=serial_number).first()

        if mp is None:
            return result(404, "Not Found Media Player Info", None, None, "by KSM")
        else:
            dict_data = dict()
            dict_data["app_path1"] = mp.app_path1
            dict_data["app_path2"] = mp.app_path2
            dict_data["app_path3"] = mp.app_path3
            dict_data["app_path4"] = mp.app_path4
            return result(200, "GET the APP Info", dict_data, None, "by KSM")

class MediaPlayerRegisterAPI(Resource):
    """
    [ Media Players Register ]
    @ PUT : MediaPlayer config
        - Used to create a URI to give to Agent.
        [ Case by ] MediaPlayer Register
        [ Case by ] MediaPlayer Update
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("shop", type=str, location="json")
        self.parser.add_argument("activation_date", type=str, location="json")
        self.parser.add_argument("location", type=str, location="json")
        self.parser.add_argument("expire_date", type=str, location="json")
        self.parser.add_argument("serial_number", type=str, location="json")
        self.parser.add_argument("manage_serial_number", type=str, location="json")
        self.parser.add_argument("model", type=str, location="json")
        self.parser.add_argument("manufact", type=str, location="json")
        self.parser.add_argument("version", type=str, location="json")
        self.parser.add_argument("uuid", type=str, location="json")
        self.parser.add_argument("wakeup", type=str, location="json")
        self.parser.add_argument("sku", type=str, location="json")
        self.parser.add_argument("family", type=str, location="json")
        self.parser.add_argument("processor", type=str, location="json")
        self.parser.add_argument("bios", type=str, location="json")
        self.parser.add_argument("os", type=str, location="json")
        self.parser.add_argument("physical_memory", type=float, location="json")
        self.parser.add_argument("physical_hdd", type=float, location="json")
        self.parser.add_argument("media_player_alias", type=str, location="json")
        self.parser.add_argument("firmware_version", type=str, location="json")
        self.parser.add_argument("site", type=str, location="json")
        self.parser.add_argument("comp_key", type=str, location="json")

        super(MediaPlayerRegisterAPI, self).__init__()

    def put(self):
        input = self.parser.parse_args()

        # Check properties
        check = ["serial_number", "model", "media_player_alias", "comp_key"]
        for i in range(len(check)):
            if not str(check[i]) in input:
                return result(400, "Missing required properties of MediaPlayer", None, None, "by KSM")

        # Search manager by comp_key
        manager = Managers.query.filter_by(comp_key=input["comp_key"]).first()

        if manager is not None:
            manager_id = manager.id
        else:
            manager_id = 0

        # Check if same serial number exists
        mp = MediaPlayers.query.filter_by(serial_number=input["serial_number"]).first()

        if mp is None:
            # Check if same alias exists
            mp = MediaPlayers.query.filter_by(alias=input["media_player_alias"]).first()

            # Create New Item
            if mp is None:
                mp_new = MediaPlayers()

                # [ TIME ] UTC
                now = time.gmtime()
                current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
                mp_new.created_date = current_time

                mp_new.enabled = True
                mp_new.activation_date = input["activation_date"]
                mp_new.expiration_date = input["expire_date"]
                mp_new.model = input["model"]
                mp_new.serial_number = input["serial_number"]
                mp_new.manage_serial_number = input["manage_serial_number"]
                mp_new.alias = input["media_player_alias"]
                mp_new.firmware_version = input["firmware_version"]

                mp_new.site = input["site"]
                mp_new.location = input["location"]
                mp_new.shop = input["shop"]

                mp_new.manufact = input["manufact"]
                mp_new.version = input["version"]
                mp_new.uuid = input["uuid"]
                mp_new.wakeup = input["wakeup"]
                mp_new.sku = input["sku"]
                mp_new.family = input["family"]

                mp_new.processor = input["processor"]
                mp_new.bios = input["bios"]
                mp_new.os = input["os"]
                mp_new.physical_memory = input["physical_memory"]
                mp_new.physical_hdd = input["physical_hdd"]

                # [ DEFAULT ] Threshold Values
                mp_new.status = DEFAULT_STATUS
                mp_new.group_status = DEFAULT_STATUS
                mp_new.status_cpu_usage = DEFAULT_STATUS
                mp_new.status_mem_usage = DEFAULT_STATUS
                mp_new.status_hdd_usage = DEFAULT_STATUS
                mp_new.status_cpu_temp = DEFAULT_STATUS
                mp_new.status_hdd_temp = DEFAULT_STATUS
                mp_new.status_fan_speed = DEFAULT_STATUS
                mp_new.status_res_time = DEFAULT_STATUS

                mp_new.FK_managers_id = manager_id

                db.session.add(mp_new)
                db.session.commit()

                return result(200, "New MediaPlayer registration is successful", None, None, "by KSM")

            else:
                db.session.query(MediaPlayers).filter_by(alias=input["media_player_alias"]).update(dict(
                    activation_date=input["activation_date"],
                    expiration_date=input["expire_date"],
                    firmware_version=input["firmware_version"],
                    model=input["model"],
                    serial_number=input["serial_number"],
                    manage_serial_number=input["manage_serial_number"],
                    site=input["site"],
                    location=input["location"],
                    shop=input["shop"],
                    manufact=input["manufact"],
                    version=input["version"],
                    uuid=input["uuid"],
                    wakeup=input["wakeup"],
                    sku=input["sku"],
                    family=input["family"],
                    processor=input["processor"],
                    bios=input["bios"],
                    os=input["os"],
                    physical_memory=input["physical_memory"],
                    physical_hdd=input["physical_hdd"],
                    FK_managers_id=manager_id
                ))

                db.session.commit()
                return result(200, "Updated the existing MediaPlayer by alias", None, None, "by KSM")
        else:
            db.session.query(MediaPlayers).filter_by(serial_number=mp.serial_number).update(dict(
                activation_date=input["activation_date"],
                expiration_date=input["expire_date"],
                alias=input["media_player_alias"],
                firmware_version=input["firmware_version"],
                model=input["model"],
                manage_serial_number=input["manage_serial_number"],
                site=input["site"],
                location=input["location"],
                shop=input["shop"],
                manufact=input["manufact"],
                version=input["version"],
                uuid=input["uuid"],
                wakeup=input["wakeup"],
                sku=input["sku"],
                family=input["family"],
                processor=input["processor"],
                bios=input["bios"],
                os=input["os"],
                physical_memory=input["physical_memory"],
                physical_hdd=input["physical_hdd"],
                FK_managers_id=manager_id
            ))

            db.session.commit()

            return result(200, "Updated the existing MediaPlayer by serial number", None, None, "by KSM")

class MediaPlayerPollingAPI(Resource):
    """
    [ Media Player Polling ]
    @ PUT : The Update information for one of the MediaPlayer - by rainmaker
    """
    mp_global_logger = Helper.get_file_logger("mediaplayers", "mediaplayers.log")

    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("put_type", type=str, location="json")

        self.parser.add_argument("manufact", type=str, location="json")
        self.parser.add_argument("version", type=str, location="json")
        self.parser.add_argument("uuid", type=str, location="json")
        self.parser.add_argument("wakeup", type=str, location="json")
        self.parser.add_argument("sku", type=str, location="json")
        self.parser.add_argument("family", type=str, location="json")

        self.parser.add_argument("model", type=str, location="json")
        self.parser.add_argument("os", type=str, location="json")
        self.parser.add_argument("firmware_version", type=str, location="json")
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("ip", type=str, location="json")
        self.parser.add_argument("netmask", type=str, location="json")
        self.parser.add_argument("mac", type=str, location="json")
        self.parser.add_argument("s_bytes", type=str, location="json")
        self.parser.add_argument("r_bytes", type=str, location="json")
        self.parser.add_argument("gateway", type=str, location="json")
        self.parser.add_argument("dns", type=str, location="json")
        self.parser.add_argument("interface", type=str, location="json")

        self.parser.add_argument("agent_version", type=str, location="json")

        self.parser.add_argument("cpu_temp", type=float, location="json")
        self.parser.add_argument("cpu_usage", type=float, location="json")
        self.parser.add_argument("hdd_temp", type=float, location="json")
        self.parser.add_argument("hdd_usage", type=float, location="json")
        self.parser.add_argument("mem_usage", type=float, location="json")
        self.parser.add_argument("fan_speed", type=float, location="json")
        self.parser.add_argument("app_path1", type=str, location="json")
        self.parser.add_argument("app_path2", type=str, location="json")
        self.parser.add_argument("app_path3", type=str, location="json")
        self.parser.add_argument("app_path4", type=str, location="json")
        self.parser.add_argument("app_status1", type=str, location="json")
        self.parser.add_argument("app_status2", type=str, location="json")
        self.parser.add_argument("app_status3", type=str, location="json")
        self.parser.add_argument("app_status4", type=str, location="json")
        self.parser.add_argument("app_cpu1", type=float, location="json")
        self.parser.add_argument("app_cpu2", type=float, location="json")
        self.parser.add_argument("app_cpu3", type=float, location="json")
        self.parser.add_argument("app_cpu4", type=float, location="json")
        self.parser.add_argument("app_memory1", type=float, location="json")
        self.parser.add_argument("app_memory2", type=float, location="json")
        self.parser.add_argument("app_memory3", type=float, location="json")
        self.parser.add_argument("app_memory4", type=float, location="json")
        self.parser.add_argument("running_time", type=str, location="json")
        self.parser.add_argument("system_time", type=str, location="json")
        self.parser.add_argument("utc_time", type=str, location="json")
        self.parser.add_argument("system_time_zone", type=str, location="json")
        self.parser.add_argument("polling_time", type=str, location="json")
        self.parser.add_argument("response_time", type=str, location="json")
        self.parser.add_argument("fbgrab", type=str, location="json")
        self.parser.add_argument("svr_conn", type=str, location="json")

        self.logger = MediaPlayerPollingAPI.mp_global_logger

        super(MediaPlayerPollingAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()
        put_type = input["put_type"]
        del input["put_type"]

        for k, v in input.items():
            if v is None:
                del input[k]

        # [ Check ] serial_number
        mp = MediaPlayers.query.filter(MediaPlayers.serial_number == serial_number).first()

        # [ Case by ] MediaPlayer update
        if mp is not None:
            if put_type == "0":
                db.session.query(MediaPlayers).filter_by(serial_number=serial_number).update(input)
                db.session.commit()

            elif put_type == "1":
                # [ TIME ] UTC
                now = time.gmtime()
                current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
                input["last_updated_date"] = current_time

                encoded = input["fbgrab"]
                del input["fbgrab"]

                img_file_name = '{0}{1}.jpg'.format(LIVE_VIEW_ROOT, serial_number)
                if encoded:
                    data = base64.b64decode(encoded)
                    out = open(img_file_name, 'wb')
                    try:
                        out.write(data)
                    finally:
                        out.close()

                db.session.query(MediaPlayers).filter_by(serial_number=serial_number).update(input)
                db.session.commit()

                mph = MediaPlayers_History()
                mph.serial_number = serial_number
                mph.alias = mp.alias
                mph.FK_managers_id = mp.FK_managers_id
                if "last_updated_date" in input:
                    mph.last_updated_date = input["last_updated_date"]
                if "cpu_temp" in input:
                    mph.cpu_temp = input["cpu_temp"]
                if "cpu_usage" in input:
                    mph.cpu_usage = input["cpu_usage"]
                if "hdd_temp" in input:
                    mph.hdd_temp = input["hdd_temp"]
                if "hdd_usage" in input:
                    mph.hdd_usage = input["hdd_usage"]
                if "mem_usage" in input:
                    mph.mem_usage = input["mem_usage"]
                if "fan_speed" in input:
                    mph.fan_speed = input["fan_speed"]
                if "app_path1" in input:
                    mph.app_path1 = input["app_path1"]
                if "app_path2" in input:
                    mph.app_path2 = input["app_path2"]
                if "app_path3" in input:
                    mph.app_path3 = input["app_path3"]
                if "app_path4" in input:
                    mph.app_path4 = input["app_path4"]
                if "app_status1" in input:
                    mph.app_status1 = input["app_status1"]
                if "app_status2" in input:
                    mph.app_status2 = input["app_status2"]
                if "app_status3" in input:
                    mph.app_status3 = input["app_status3"]
                if "app_status4" in input:
                    mph.app_status4 = input["app_status4"]
                if "app_cpu1" in input:
                    mph.app_cpu1 = input["app_cpu1"]
                if "app_cpu2" in input:
                    mph.app_cpu2 = input["app_cpu2"]
                if "app_cpu3" in input:
                    mph.app_cpu3 = input["app_cpu3"]
                if "app_cpu4" in input:
                    mph.app_cpu4 = input["app_cpu4"]
                if "app_memory1" in input:
                    mph.app_memory1 = input["app_memory1"]
                if "app_memory2" in input:
                    mph.app_memory2 = input["app_memory2"]
                if "app_memory3" in input:
                    mph.app_memory3 = input["app_memory3"]
                if "app_memory4" in input:
                    mph.app_memory4 = input["app_memory4"]

                db.session.add(mph)
                db.session.commit()
                '''
                dict_log = dict()

                dict_log["group_id"] = 1
                dict_log["account"] = mp.site
                dict_log["location"] = mp.location
                dict_log["shop"] = mp.shop
                dict_log["serial_number"] = serial_number
                dict_log["alias"] = mp.alias
                if "cpu_temp" in input:
                    dict_log["cpu_temp"] = input["cpu_temp"]
                if "cpu_usage" in input:
                    dict_log["cpu_usage"] = input["cpu_usage"]
                if "hdd_temp" in input:
                    dict_log["hdd_temp"] = input["hdd_temp"]
                if "hdd_usage" in input:
                    dict_log["hdd_usage"] = input["hdd_usage"]
                if "mem_usage" in input:
                    dict_log["mem_usage"] = input["mem_usage"]
                if "fan_speed" in input:
                    dict_log["fan_speed"] = input["fan_speed"]
                if "app_status1" in input:
                    dict_log["app_status1"] = input["app_status1"]
                if "app_status2" in input:
                    dict_log["app_status2"] = input["app_status2"]
                if "app_status3" in input:
                    dict_log["app_status3"] = input["app_status3"]
                if "app_status4" in input:
                    dict_log["app_status4"] = input["app_status4"]
                if "app_cpu1" in input:
                    dict_log["app_cpu1"] = input["app_cpu1"]
                if "app_cpu2" in input:
                    dict_log["app_cpu2"] = input["app_cpu2"]
                if "app_cpu3" in input:
                    dict_log["app_cpu3"] = input["app_cpu3"]
                if "app_cpu4" in input:
                    dict_log["app_cpu4"] = input["app_cpu4"]
                if "app_memory1" in input:
                    dict_log["app_memory1"] = input["app_memory1"]
                if "app_memory2" in input:
                    dict_log["app_memory2"] = input["app_memory2"]
                if "app_memory3" in input:
                    dict_log["app_memory3"] = input["app_memory3"]
                if "app_memory4" in input:
                    dict_log["app_memory4"] = input["app_memory4"]

                json_log = json.dumps(dict_log)
                self.logger.info(json_log)
            '''

            return result(200, "MediaPlayer update successful", None, None, "by KSM")

        # [ Case by ] MediaPlayer does not exist
        else:
            return result(400, "MediaPlayer does not exist", None, None, None)

class DisplayRegisterAPI(Resource):
    """
    [ Displays Register ]
    @ PUT : Display config
        - Used to create a URI to give to Agent.
        [ Case by ] Display Register
        [ Case by ] Display Update
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("se_email", type=str, location="json")
        self.parser.add_argument("memo", type=str, location="json")
        self.parser.add_argument("activation_date", type=str, location="json")
        self.parser.add_argument("serial_number", type=str, location="json")
        self.parser.add_argument("se_name", type=str, location="json")
        self.parser.add_argument("firmware_version", type=str, location="json")
        self.parser.add_argument("port_number", type=str, location="json")
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("si_company", type=str, location="json")
        self.parser.add_argument("se_mobile", type=str, location="json")
        self.parser.add_argument("display_index", type=str, location="json")
        self.parser.add_argument("media_player", type=str, location="json")
        self.parser.add_argument("expire_date", type=str, location="json")
        self.parser.add_argument("organization", type=str, location="json")
        self.parser.add_argument("model", type=str, location="json")
        self.parser.add_argument("set_id", type=str, location="json")

        super(DisplayRegisterAPI, self).__init__()

    def put(self):
        input = self.parser.parse_args()

        # [ Check ] property
        check = ["se_email", "memo", "activation_date", "serial_number", "se_name",
                 "firmware_version", "alias", "si_company", "se_mobile", "display_index",
                 "media_player", "expire_date", "organization", "model"]
        for i in range(len(check)):
            if not str(check[i]) in input:
                return result(400, "Missing required properties of Display", None, None, "by KSM")

        # [ Check ] parents MediaPlayer
        mp = MediaPlayers.query.filter_by(serial_number=input["media_player"]).first()
        if mp is None:
            return result(500, "Can not find the parents MediaPlayer", None, None, "by KSM")
        else:

            # [ Check ] If the number is registered in the mediaplayer
            exist_dp = Displays.query.filter_by(mp_serial_number=mp.serial_number).filter(Displays.serial_number!=input['serial_number']).filter_by(number=input['display_index']).first()

            if exist_dp is not None:
                db.session.delete(exist_dp)
                db.session.commit()

            dp = Displays.query.filter_by(serial_number=input["serial_number"]).first()

            if input["set_id"] is None:
                input["set_id"] = "1"

            # [ Case by ] Display Register
            if dp is None:
                dp = Displays()

                # [ DEFAULT ] Threshold Values
                dp.status = DEFAULT_STATUS
                dp.status_temp = DEFAULT_STATUS

                # [ TIME ] UTC
                now = time.gmtime()
                current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
                dp.created_date = current_time
                dp.se_email = input["se_email"]
                dp.se_name = input["se_name"]
                dp.si_company = input["si_company"]
                dp.se_mobile = input["se_mobile"]
                dp.number = input["display_index"]
                dp.set_id = input["set_id"]
                dp.memo = input["memo"]
                dp.activation_date = input["activation_date"]
                dp.serial_number = input["serial_number"]
                dp.port_number = input['port_number']
                dp.firmware_version = input["firmware_version"]
                dp.alias = input["alias"]
                dp.expiration_date = input["expire_date"]
                dp.organization = input["organization"]
                dp.display_timer_flag = False
                dp.timer_onoff_mon = True
                dp.timer_begin_mon = TIMER_BEGIN
                dp.timer_end_mon = TIMER_END
                dp.timer_onoff_tue = True
                dp.timer_begin_tue = TIMER_BEGIN
                dp.timer_end_tue = TIMER_END
                dp.timer_onoff_wed = True
                dp.timer_begin_wed = TIMER_BEGIN
                dp.timer_end_wed = TIMER_END
                dp.timer_onoff_thu = True
                dp.timer_begin_thu = TIMER_BEGIN
                dp.timer_end_thu = TIMER_END
                dp.timer_onoff_fri = True
                dp.timer_begin_fri = TIMER_BEGIN
                dp.timer_end_fri = TIMER_END
                dp.timer_onoff_sat = True
                dp.timer_begin_sat = TIMER_BEGIN
                dp.timer_end_sat = TIMER_END
                dp.timer_onoff_sun = True
                dp.timer_begin_sun = TIMER_BEGIN
                dp.timer_end_sun = TIMER_END

                dp.FK_mp_id = mp.id
                dp.mp_serial_number = mp.serial_number

                # [ Check ] model
                dp_model = Display_Models.query.filter_by(name=input["model"]).first()

                FK_models_id = 0
                temp_model = ''
                # [ Case by ] Basic Model inheritance
                if dp_model is None:
                    default_dp_model = Display_Models.query.filter_by(name=DEFAULT_DP_MODEL).first()
                    FK_models_id = default_dp_model.id
                    temp_model = input["model"]
                # [ Case by ] The Model inheritance
                else:
                    FK_models_id = dp_model.id
                    temp_model = dp_model.name

                dp.FK_models_id = FK_models_id
                dp.model = temp_model

                db.session.add(dp)
                db.session.commit()


                return result(200, "New Display registration is successful", None, None, "by KSM")
            # [ Case by ] Display Update
            else:
                # [ Check ] model
                dp_model = Display_Models.query.filter_by(name=input["model"]).first()

                FK_models_id = 0
                temp_model = ''
                # [ Case by ] Basic Model inheritance
                if dp_model is None:
                    default_dp_model = Display_Models.query.filter_by(name=DEFAULT_DP_MODEL).first()
                    FK_models_id = default_dp_model.id
                    temp_model = input["model"]
                # [ Case by ] The Model inheritance
                else:
                    FK_models_id = dp_model.id
                    temp_model = dp_model.name

                db.session.query(Displays).filter_by(serial_number=dp.serial_number).update(dict(
                    alias=input["alias"],
                    firmware_version=input["firmware_version"],
                    activation_date=input["activation_date"],
                    expiration_date=input["expire_date"],
                    number=input["display_index"],
                    FK_models_id=FK_models_id,
                    FK_mp_id=mp.id,
                    mp_serial_number=mp.serial_number,
                    model=temp_model,
                    port_number=input['port_number'],
                    status=DEFAULT_STATUS,
                    status_temp=DEFAULT_STATUS,
                ))
                db.session.commit()

                return result(200, "Updated the existing Display", None, None, "by KSM")

class DisplayPollingAPI(Resource):
    """
    [ Display Polling ]
    @ PUT : The Update information for one of the Display - by rainmaker
    """
    dp_global_logger = Helper.get_file_logger("displays", "displays.log")

    def __init__(self):
        self.parser = reqparse.RequestParser()
        #---------------------------------------------------------------------------------------------------------------
        # Param
        #---------------------------------------------------------------------------------------------------------------
        self.parser.add_argument("serial_number", type=str, location="json")

        self.parser.add_argument("media_player", type=str, location="json")
        self.parser.add_argument("expire_date", type=str, location="json")
        self.parser.add_argument("firmware_version", type=str, location="json")

        self.parser.add_argument("val_PowerStatus", type=str, location="json")
        self.parser.add_argument("val_InputSelect", type=str, location="json")
        self.parser.add_argument("val_AspectRatio", type=str, location="json")
        self.parser.add_argument("val_PictureMode", type=str, location="json")
        self.parser.add_argument("val_Temp", type=str, location="json")
        self.parser.add_argument("val_Fan", type=str, location="json")
        self.parser.add_argument("val_Signal", type=str, location="json")
        self.parser.add_argument("val_Lamp", type=str, location="json")
        self.parser.add_argument("val_FirmwareVer", type=str, location="json")
        self.parser.add_argument("val_MonitorStatus", type=str, location="json")

        self.parser.add_argument("val_EnergySaving", type=str, location="json")
        self.parser.add_argument("val_Backlight", type=str, location="json")
        self.parser.add_argument("val_Constrast", type=str, location="json")
        self.parser.add_argument("val_Brightness", type=str, location="json")
        self.parser.add_argument("val_Sharpness", type=str, location="json")
        self.parser.add_argument("val_Color", type=str, location="json")
        self.parser.add_argument("val_Tint", type=str, location="json")
        self.parser.add_argument("val_ColorTemp", type=str, location="json")

        self.parser.add_argument("val_ismMode", type=str, location="json")
        self.parser.add_argument("val_dpmSelect", type=str, location="json")
        self.parser.add_argument("val_osdSelect", type=str, location="json")
        self.parser.add_argument("val_Lang", type=str, location="json")
        self.parser.add_argument("val_Remote", type=str, location="json")
        self.parser.add_argument("val_PowerOnDelay", type=str, location="json")

        self.parser.add_argument("val_Date", type=str, location="json")
        self.parser.add_argument("val_Time", type=str, location="json")
        self.parser.add_argument("val_SleepTime", type=str, location="json")
        self.parser.add_argument("val_AutoStandby", type=str, location="json")
        self.parser.add_argument("val_AutoOff", type=str, location="json")

        self.parser.add_argument("val_OnTimer", type=str, location="json")
        self.parser.add_argument("val_OffTimer", type=str, location="json")
        self.parser.add_argument("val_OnTimerInput", type=str, location="json")

        self.parser.add_argument("val_Speaker", type=str, location="json")
        self.parser.add_argument("val_SoundMode", type=str, location="json")
        self.parser.add_argument("val_Balance", type=str, location="json")
        self.parser.add_argument("val_VolumControl", type=str, location="json")
        self.parser.add_argument("val_VolumMute", type=str, location="json")
        self.parser.add_argument("val_Treble", type=str, location="json")
        self.parser.add_argument("val_Bass", type=str, location="json")
        self.parser.add_argument("val_AutoVolume", type=str, location="json")

        self.parser.add_argument("val_TileModeCheck", type=str, location="json")
        self.parser.add_argument("val_TileID", type=str, location="json")
        self.parser.add_argument("val_TileNaturalMode", type=str, location="json")
        self.parser.add_argument("val_SizeH", type=str, location="json")
        self.parser.add_argument("val_SizeV", type=str, location="json")
        self.parser.add_argument("val_PositionH", type=str, location="json")
        self.parser.add_argument("val_PositionV", type=str, location="json")

        self.parser.add_argument("val_RedGain", type=str, location="json")
        self.parser.add_argument("val_RedOffset", type=str, location="json")
        self.parser.add_argument("val_GreenGain", type=str, location="json")
        self.parser.add_argument("val_GreenOffset", type=str, location="json")
        self.parser.add_argument("val_BlueGain", type=str, location="json")
        self.parser.add_argument("val_BlueOffset", type=str, location="json")

        self.logger = DisplayPollingAPI.dp_global_logger

        super(DisplayPollingAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()

        # [ Temp ] delete
        input['expiration_date'] = input['expire_date']
        del input['expire_date']

        # [ Check ] serial_number
        dp = Displays.query.filter_by(serial_number=serial_number).first()

        if dp is not None:
            # [ TIME ] UTC
            now = time.gmtime()
            current_time = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
            input["last_updated_date"] = current_time

            media_player_serial_number = input["media_player"]
            del input["media_player"]

            change_to_decimal_list = ["val_Temp", "val_Backlight", "val_Constrast", "val_Brightness", "val_Sharpness", "val_Color", "val_Tint", "val_ColorTemp", "val_Balance",
                    "val_VolumControl", "val_Treble", "val_Bass", "val_SizeH", "val_SizeV", "val_PositionH", "val_PositionV", "val_RedGain", "val_RedOffset", "val_GreenGain",
                    "val_GreenOffset", "val_BlueGain", "val_BlueOffset"]

            for item in change_to_decimal_list:
                try:
                    if input[item] != "NG":
                        input[item] = int(input[item], 16)
                    else:
                        input[item] = ""
                except:
                    input[item] = ""

            try:
                result_date = ""
                str_date_yy = input["val_Date"][:2]
                date_yy = int(str_date_yy, 16) + 2010
                result_date += str(date_yy) + '-'
                str_date_mm = input["val_Date"][2:4]
                date_mm = int(str_date_mm, 16)
                result_date += str(date_mm) + '-'
                str_date_dd = input["val_Date"][4:]
                date_dd = int(str_date_dd, 16)
                result_date += str(date_dd)
                input["val_Date"] = result_date
            except:
                input["val_Date"] = "00-00-00"

            try:
                result_time = ""
                str_time_hh = input["val_Time"][:2]
                time_hh = int(str_time_hh, 16)
                result_time += str(time_hh) + ':'
                str_time_mm = input["val_Time"][2:4]
                time_mm = int(str_time_mm, 16)
                result_time += str(time_mm)
                #+ ':'
                #str_time_ss = input["val_Time"][4:]
                #time_ss = int(str_time_ss, 16)
                #result_time += str(time_ss)
                input["val_Time"] = result_time
            except:
                input["val_Time"] = "00:00:00"


            if input["val_Temp"] > 255:
                input["val_Temp"] = input["val_Temp"] % 256


            input["mp_serial_number"] = media_player_serial_number

            db.session.query(Displays).filter_by(serial_number=serial_number).update(input)
            db.session.commit()

            dph = Displays_History()
            dph.serial_number = input["serial_number"]
            dph.alias = dp.alias
            dph.last_updated_date = input["last_updated_date"]
            dph.val_PowerStatus = input["val_PowerStatus"]
            dph.val_InputSelect = input["val_InputSelect"]
            dph.val_AspectRatio = input["val_AspectRatio"]
            dph.val_PictureMode = input["val_PictureMode"]
            dph.val_Temp = input["val_Temp"]
            dph.val_Fan = input["val_Fan"]
            dph.val_Signal = input["val_Signal"]
            dph.val_Lamp = input["val_Lamp"]
            dph.val_FirmwareVer = input["val_FirmwareVer"]
            dph.val_MonitorStatus = input["val_MonitorStatus"]
            dph.val_EnergySaving = input["val_EnergySaving"]
            dph.val_Backlight = input["val_Backlight"]
            dph.val_Constrast = input["val_Constrast"]
            dph.val_Brightness = input["val_Brightness"]
            dph.val_Sharpness = input["val_Sharpness"]
            dph.val_Color = input["val_Color"]
            dph.val_Tint = input["val_Tint"]
            dph.val_ColorTemp = input["val_ColorTemp"]
            dph.val_Speaker = input["val_Speaker"]
            dph.val_SoundMode = input["val_SoundMode"]
            dph.val_Balance = input["val_Balance"]
            dph.val_VolumControl = input["val_VolumControl"]
            dph.val_VolumMute = input["val_VolumMute"]
            dph.val_Treble = input["val_Treble"]
            dph.val_Bass = input["val_Bass"]
            dph.val_AutoVolume = input["val_AutoVolume"]
            dph.val_Date = input["val_Date"]
            dph.val_Time = input["val_Time"]
            dph.val_SleepTime = input["val_SleepTime"]
            dph.val_AutoStandby = input["val_AutoStandby"]
            dph.val_AutoOff = input["val_AutoOff"]
            dph.val_ismMode = input["val_ismMode"]
            dph.val_dpmSelect = input["val_dpmSelect"]
            dph.val_osdSelect = input["val_osdSelect"]
            dph.val_Lang = input["val_Lang"]
            dph.val_Remote = input["val_Remote"]
            dph.val_PowerOnDelay = input["val_PowerOnDelay"]
            dph.val_TileModeCheck = input["val_TileModeCheck"]
            dph.val_TileID = input["val_TileID"]
            dph.val_TileNaturalMode = input["val_TileNaturalMode"]
            dph.val_SizeH = input["val_SizeH"]
            dph.val_SizeV = input["val_SizeV"]
            dph.val_PositionH = input["val_PositionH"]
            dph.val_PositionV = input["val_PositionV"]
            dph.val_RedGain = input["val_RedGain"]
            dph.val_RedOffset = input["val_RedOffset"]
            dph.val_GreenGain = input["val_GreenGain"]
            dph.val_GreenOffset = input["val_GreenOffset"]
            dph.val_BlueGain = input["val_BlueGain"]
            dph.val_BlueOffset = input["val_BlueOffset"]
            dph.val_OnTimer = input["val_OnTimer"]
            dph.val_OffTimer = input["val_OffTimer"]
            dph.val_OnTimerInput = input["val_OnTimerInput"]
            dph.mp_serial_number = media_player_serial_number

            mp = MediaPlayers.query.filter(MediaPlayers.serial_number == media_player_serial_number).first()
            dph.FK_managers_id = mp.FK_managers_id

            db.session.add(dph)
            db.session.commit()


            '''
            if mp is not None:
                dict_log = dict()

                dict_log["group_id"] = 1
                dict_log["account"] = mp.site
                dict_log["location"] = mp.location
                dict_log["shop"] = mp.shop
                dict_log["serial_number"] = media_player_serial_number
                dict_log["alias"] = mp.alias

                dict_log["val_Signal"] = input["val_Signal"]
                dict_log["val_Temp"] = input["val_Temp"]

                json_log = json.dumps(dict_log)
                self.logger.info(json_log)
            '''
            return result(200, "Display update successful", None, None, "by KSM")
        else:
            return result(400, "Display does not exist", None, None, "by KSM")

class DisplayAliveAPI(Resource):
    """
    [ Display Alive Check ]
    For Agent
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("alive", type=str, location="json")

        super(DisplayAliveAPI, self).__init__()

    def put(self, serial_number):
        input = self.parser.parse_args()

        dp = Displays.query.filter_by(serial_number=serial_number).first()
        if dp is None:
            return result(404, "No Display", None, None, "by KSM")
        else:
            db.session.query(Displays).filter_by(serial_number=serial_number).update(input)
            db.session.commit()

            return result(200, "Display Alive Update", None, None, "by KSM")

class DisplayModelsAPI(Resource):
    """
    [ Display Models ]
    - Used to give a URI to the daemon.
    by rainmaker
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()

        super(DisplayModelsAPI, self).__init__()

    def get(self, model_name):


        dpm = Display_Models.query.filter_by(name=model_name).first()

        if dpm is None:
            dpm = Display_Models.query.filter_by(name=DEFAULT_DP_MODEL).first()

        command = dict()

        command["tag_PowerStatus"] = dpm.tag_PowerStatus
        command["tag_InputSelect"] = dpm.tag_InputSelect
        command["tag_AspectRatio"] = dpm.tag_AspectRatio
        command["tag_PictureMode"] = dpm.tag_PictureMode
        command["tag_Temp"] = dpm.tag_Temp
        command["tag_Fan"] = dpm.tag_Fan
        command["tag_Signal"] = dpm.tag_Signal
        command["tag_Lamp"] = dpm.tag_Lamp
        command["tag_SerialNo"] = dpm.tag_SerialNo
        command["tag_FirmwareVer"] = dpm.tag_FirmwareVer
        command["tag_MonitorStatus"] = dpm.tag_MonitorStatus

        # picture
        command["tag_EnergySaving"] = dpm.tag_EnergySaving
        command["tag_Backlight"] = dpm.tag_Backlight
        command["tag_Constrast"] = dpm.tag_Constrast
        command["tag_Brightness"] = dpm.tag_Brightness
        command["tag_Sharpness"] = dpm.tag_Sharpness
        command["tag_Color"] = dpm.tag_Color
        command["tag_Tint"] = dpm.tag_Tint
        command["tag_ColorTemp"] = dpm.tag_ColorTemp

        # Option
        command["tag_ismMode"] = dpm.tag_ismMode
        command["tag_dpmSelect"] = dpm.tag_dpmSelect
        command["tag_osdSelect"] = dpm.tag_osdSelect
        command["tag_Lang"] = dpm.tag_Lang
        command["tag_Remote"] = dpm.tag_Remote
        command["tag_PowerOnDelay"] = dpm.tag_PowerOnDelay

        # Support
        command["tag_FirmwareUpdate"] = dpm.tag_FirmwareUpdate

        # Time
        command["tag_Date"] = dpm.tag_Date
        command["tag_Time"] = dpm.tag_Time
        command["tag_SleepTime"] = dpm.tag_SleepTime
        command["tag_AutoStandby"] = dpm.tag_AutoStandby
        command["tag_AutoOff"] = dpm.tag_AutoOff

        # Schedule
        command["tag_OnTimer"] = dpm.tag_OnTimer
        command["tag_OffTimer"] = dpm.tag_OffTimer
        command["tag_OnTimerInput"] = dpm.tag_OnTimerInput

        # Audio
        command["tag_Speaker"] = dpm.tag_Speaker
        command["tag_SoundMode"] = dpm.tag_SoundMode
        command["tag_Balance"] = dpm.tag_Balance
        command["tag_VolumControl"] = dpm.tag_VolumControl
        command["tag_VolumMute"] = dpm.tag_VolumMute
        command["tag_Treble"] = dpm.tag_Treble
        command["tag_Bass"] = dpm.tag_Bass
        command["tag_AutoVolume"] = dpm.tag_AutoVolume

        # Tile
        command["tag_TileMode"] = dpm.tag_TileMode
        command["tag_TileModeCheck"] = dpm.tag_TileModeCheck
        command["tag_TileID"] = dpm.tag_TileID
        command["tag_TileNaturalMode"] = dpm.tag_TileNaturalMode
        command["tag_SizeH"] = dpm.tag_SizeH
        command["tag_SizeV"] = dpm.tag_SizeV
        command["tag_PositionH"] = dpm.tag_PositionH
        command["tag_PositionV"] = dpm.tag_PositionV

        # White Balance
        command["tag_RedGain"] = dpm.tag_RedGain
        command["tag_RedOffset"] = dpm.tag_RedOffset
        command["tag_GreenGain"] = dpm.tag_GreenGain
        command["tag_GreenOffset"] = dpm.tag_GreenOffset
        command["tag_BlueGain"] = dpm.tag_BlueGain
        command["tag_BlueOffset"] = dpm.tag_BlueOffset

        # Remote Controller
        command["tag_RemoteControl"] = dpm.tag_RemoteControl

        return result(200, "GET the Display Command", command, None, "by rainmaker")

class DisplayModelsNoneAPI(Resource):
    """
    [ Display Models ]
    - Used to give a URI to the daemon.
    by rainmaker
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()

        super(DisplayModelsNoneAPI, self).__init__()

    def get(self):
        dpm = Display_Models.query.filter_by(name=DEFAULT_DP_MODEL).first()

        command = dict()

        command["tag_PowerStatus"] = dpm.tag_PowerStatus
        command["tag_InputSelect"] = dpm.tag_InputSelect
        command["tag_AspectRatio"] = dpm.tag_AspectRatio
        command["tag_PictureMode"] = dpm.tag_PictureMode
        command["tag_Temp"] = dpm.tag_Temp
        command["tag_Fan"] = dpm.tag_Fan
        command["tag_Signal"] = dpm.tag_Signal
        command["tag_Lamp"] = dpm.tag_Lamp
        command["tag_SerialNo"] = dpm.tag_SerialNo
        command["tag_FirmwareVer"] = dpm.tag_FirmwareVer
        command["tag_MonitorStatus"] = dpm.tag_MonitorStatus

        # picture
        command["tag_EnergySaving"] = dpm.tag_EnergySaving
        command["tag_Backlight"] = dpm.tag_Backlight
        command["tag_Constrast"] = dpm.tag_Constrast
        command["tag_Brightness"] = dpm.tag_Brightness
        command["tag_Sharpness"] = dpm.tag_Sharpness
        command["tag_Color"] = dpm.tag_Color
        command["tag_Tint"] = dpm.tag_Tint
        command["tag_ColorTemp"] = dpm.tag_ColorTemp

        # Option
        command["tag_ismMode"] = dpm.tag_ismMode
        command["tag_dpmSelect"] = dpm.tag_dpmSelect
        command["tag_osdSelect"] = dpm.tag_osdSelect
        command["tag_Lang"] = dpm.tag_Lang
        command["tag_Remote"] = dpm.tag_Remote
        command["tag_PowerOnDelay"] = dpm.tag_PowerOnDelay

        # Support
        command["tag_FirmwareUpdate"] = dpm.tag_FirmwareUpdate

        # Time
        command["tag_Date"] = dpm.tag_Date
        command["tag_Time"] = dpm.tag_Time
        command["tag_SleepTime"] = dpm.tag_SleepTime
        command["tag_AutoStandby"] = dpm.tag_AutoStandby
        command["tag_AutoOff"] = dpm.tag_AutoOff

        # Schedule
        command["tag_OnTimer"] = dpm.tag_OnTimer
        command["tag_OffTimer"] = dpm.tag_OffTimer
        command["tag_OnTimerInput"] = dpm.tag_OnTimerInput

        # Audio
        command["tag_Speaker"] = dpm.tag_Speaker
        command["tag_SoundMode"] = dpm.tag_SoundMode
        command["tag_Balance"] = dpm.tag_Balance
        command["tag_VolumControl"] = dpm.tag_VolumControl
        command["tag_VolumMute"] = dpm.tag_VolumMute
        command["tag_Treble"] = dpm.tag_Treble
        command["tag_Bass"] = dpm.tag_Bass
        command["tag_AutoVolume"] = dpm.tag_AutoVolume

        # Tile
        command["tag_TileMode"] = dpm.tag_TileMode
        command["tag_TileModeCheck"] = dpm.tag_TileModeCheck
        command["tag_TileID"] = dpm.tag_TileID
        command["tag_TileNaturalMode"] = dpm.tag_TileNaturalMode
        command["tag_SizeH"] = dpm.tag_SizeH
        command["tag_SizeV"] = dpm.tag_SizeV
        command["tag_PositionH"] = dpm.tag_PositionH
        command["tag_PositionV"] = dpm.tag_PositionV

        # White Balance
        command["tag_RedGain"] = dpm.tag_RedGain
        command["tag_RedOffset"] = dpm.tag_RedOffset
        command["tag_GreenGain"] = dpm.tag_GreenGain
        command["tag_GreenOffset"] = dpm.tag_GreenOffset
        command["tag_BlueGain"] = dpm.tag_BlueGain
        command["tag_BlueOffset"] = dpm.tag_BlueOffset

        # Remote Controller
        command["tag_RemoteControl"] = dpm.tag_RemoteControl

        return result(200, "GET the Display Command", command, None, "by rainmaker")

class DisplaysTimerAPI(Resource):
    """
    [ Display Timer ]
    @ GET : TV Control Timer Data
    by KSM
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()

        super(DisplaysTimerAPI, self).__init__()

    def get(self, serial_number):
        dp = Displays.query.filter_by(serial_number = serial_number).first()

        if dp is None:
            return result(404, "display does not exist.", None, None, "by KSM")
        else:
            dp_flag = dp.display_timer_flag

            if dp_flag is False:
                return result(404, "Do not use the TV Control function", None, None, "by KSM")

            mp = MediaPlayers.query.join(Device_Groups, MediaPlayers.FK_device_groups_id == Device_Groups.id).filter(MediaPlayers.id == dp.FK_mp_id).first()

            dict_data = dict()

            if mp is None:
                data = dp
            else:
                # [ Case by ] Device Group : ON
                if mp.device_group.group_timer_flag is True:
                    data = mp.device_group
                # [ Case by ] Device Group : OFF
                else:
                    data = dp

            dict_data["timer_onoff_sun"] = bool2int(data.timer_onoff_sun)
            dict_data["timer_begin_sun"] = json_encoder(data.timer_begin_sun)
            dict_data["timer_end_sun"] = json_encoder(data.timer_end_sun)
            dict_data["timer_onoff_mon"] = bool2int(data.timer_onoff_mon)
            dict_data["timer_begin_mon"] = json_encoder(data.timer_begin_mon)
            dict_data["timer_end_mon"] = json_encoder(data.timer_end_mon)
            dict_data["timer_onoff_tue"] = bool2int(data.timer_onoff_tue)
            dict_data["timer_begin_tue"] = json_encoder(data.timer_begin_tue)
            dict_data["timer_end_tue"] = json_encoder(data.timer_end_tue)
            dict_data["timer_onoff_wed"] = bool2int(data.timer_onoff_wed)
            dict_data["timer_begin_wed"] = json_encoder(data.timer_begin_wed)
            dict_data["timer_end_wed"] = json_encoder(data.timer_end_wed)
            dict_data["timer_onoff_thu"] = bool2int(data.timer_onoff_thu)
            dict_data["timer_begin_thu"] = json_encoder(data.timer_begin_thu)
            dict_data["timer_end_thu"] = json_encoder(data.timer_end_thu)
            dict_data["timer_onoff_fri"] = bool2int(data.timer_onoff_fri)
            dict_data["timer_begin_fri"] = json_encoder(data.timer_begin_fri)
            dict_data["timer_end_fri"] = json_encoder(data.timer_end_fri)
            dict_data["timer_onoff_sat"] = bool2int(data.timer_onoff_sat)
            dict_data["timer_begin_sat"] = json_encoder(data.timer_begin_sat)
            dict_data["timer_end_sat"] = json_encoder(data.timer_end_sat)

            return result(200, "TV Control Timer Data", dict_data, None, "by KSM")

class InstallDataAPI(Resource):
    """
    [ Install Data ]
    For Agent
    - by KSM
    """
    def __init__(self):
        super(InstallDataAPI, self).__init__()

    def get(self, comp_key):
        alias = request.args.get('alias')
        ip = request.args.get('ip')
        mac = request.args.get('mac')

        manager = Managers.query.filter_by(comp_key=comp_key).first()

        if manager is not None:
            if manager.what_key == WHAT_KEY_ALIAS:
                data = InstallData.query.filter_by(FK_managers_id=manager.id).filter_by(key=alias).first()
            elif manager.what_key == WHAT_KEY_IP:
                data = InstallData.query.filter_by(FK_managers_id=manager.id).filter_by(key=ip).first()
            elif manager.what_key == WHAT_KEY_MAC:
                data = InstallData.query.filter_by(FK_managers_id=manager.id).filter_by(key=mac).first()
            else:
                data = InstallData.query.filter_by(FK_managers_id=manager.id).filter_by(key=alias).first()

            if data is not None:
                dict_data = dict()
                dict_data["site"] = data.site
                dict_data["location"] = data.location
                dict_data["shop"] = data.shop
                dict_data["mp_model"] = data.mp_model
                dict_data["mp_sn"] = data.mp_sn
                dict_data["mp_fw_ver"] = data.mp_fw_ver
                dict_data["mp_alias"] = data.mp_alias
                dict_data["mp_activate_date"] = data.mp_activate_date
                dict_data["mp_expire_date"] = data.mp_expire_date
                dict_data["dp_alias"] = data.dp_alias
                dict_data["dp_activate_date"] = data.dp_activate_date
                dict_data["dp_expire_date"] = data.dp_expire_date
                dict_data["dp_method"] = data.dp_method
                dict_data["dp_connect_to"] = data.dp_connect_to
                dict_data["dp_set_id"] = data.dp_set_id

                return result(200, "InstallData", dict_data, None, None)
            else:
                return result(404, "InstallData empty", None, None, None)

        return result(404, "Invalid company key", None, None, None)


class ServersAPI(Resource):
    """
    [ servers ]
    For Agent
    - by Park
    """
    def __init__(self):
        super(ServersAPI, self).__init__()

    def get(self):

        data = Servers.query.first()

        if data is not None:
            dict_data = dict()
            dict_data["rc_address"] = data.rc_address
            dict_data["rc_port"] = data.rc_port
            dict_data["server_address"] = data.server_address

            return result(200, "Servers", dict_data, None, 'Park')
        else:
            return result(404, "Servers empty", None, None, 'Park')


class ServerListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("rc_address", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(ServerListAPI, self).__init__()

    def get(self):
        servers = Servers.query.all()

        if len(servers) == 0:
            return

        objects = []
        for server in servers:

            objects.append({
                "id": server.id,
                "rc_address": server.rc_address,
                "server_address": server.server_address,
                "rc_port": server.rc_port,
                "server_label": server.server_label,
                "server_key": server.server_key
            })

        return {"objects": objects, "status": 200}

    def post(self):
        input = self.parser.parse_args()

        new_server = Servers()
        new_server.id = 2

        db.session.add(new_server)
        db.session.commit()

        return {"status": 200}


class ServerAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("id", type=str, location="json")
        self.parser.add_argument("server_address", type=str, location="json")
        self.parser.add_argument("server_key", type=str, location="json")
        self.parser.add_argument("server_label", type=str, location="json")
        self.parser.add_argument("rc_address", type=str, location="json")
        self.parser.add_argument("rc_port", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(ServerAPI, self).__init__()

    def get(self, pk):
        info = request.args.get("server_address")

        if info is not None:
            server = Servers.query.filter_by(id=pk).first()
        else:
            server = Servers.query.filter_by(id=pk).first()

        if server is None:
            return

        object = {
            "server_address": server.server_address
        }

        return {"object": object, "status": 200}

    def put(self, pk):
        input = self.parser.parse_args()

        var_update_list = {}
        check = []
        for key in input:
            check.append(key)

        for i in range(len(check)):
            if input[check[i]] is None:
                del input[check[i]]

        if input is not None:
            var_update_list = input

        db.session.query(Servers).filter_by(id=pk).update(var_update_list)
        db.session.commit()

        return {"status": 200}

    def delete(self, pk):
        input = self.parser.parse_args()

        server = Servers.query.first()

        db.session.delete(server)
        db.session.commit()

        return {"status": 200}


class MediaPlayerModelListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

     #------------------------------------------------------------------------------------------------------------------
        super(MediaPlayerModelListAPI, self).__init__()

    def get(self):
        #pagination setting
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))

        # [ Make ] ORDER BY
        order_by = request.args.get('order_by')
        order_by_str = ''
        if order_by is not None:
            order_by_str = 'mediaplayer_models.' + str(order_by)

        order = request.args.get('order')
        order_str = ''
        if order == '-':
            order_str = 'DESC'
        else:
            order_str = 'ASC'

        result_order_by = order_by_str + ' ' + order_str

        total_count = MediaPlayer_Models.query.count()

        result_mediaplayers = MediaPlayer_Models.query.order_by(result_order_by).limit(limit).offset(offset)
        current_count = result_mediaplayers.count()

        previous = None
        next = None
        previous_offset = offset - limit
        next_offset = offset + limit
        if offset != 0:
            previous = "mediaplayer_model?limit=" + str(limit) + "&offset=" + str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next = "mediaplayer_model?limit=" + str(limit) + "&offset=" + str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {}
        objects = []

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        if current_count == 0:
            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({

            })
            return result(404, "No Incidents Info", None, meta, "by PWB")
        else:
            for mediaplayer in result_mediaplayers:

                objects.append({
                    "id": mediaplayer.id,
                    "name": mediaplayer.name,
                    "img_location": mediaplayer.img_location,
                    "specification": mediaplayer.specification
                })
            return result(200, "Get the MediaPlayer list", objects, meta, "by PWB")


class MediaPlayerModelAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("id", type=str, location="json")
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("img_location", type=str, location="json")
        self.parser.add_argument("specification", type=str, location="json")
    #-------------------------------------------------------------------------------------------------------------------
        super(MediaPlayerModelAPI, self).__init__()

    def get(self, pk):
        info = request.args.get("name")

        if info is not None:
            mediaplayer = MediaPlayer_Models.query.filter_by(id=pk).filter_by(name=info).first()
        else:
            mediaplayer = MediaPlayer_Models.query.filter_by(id=pk).first()

        if mediaplayer is None:
            return

        object = {
            "name": mediaplayer.name
        }

        return {"object": object, "status": 200}

    def put(self, pk):
        input = self.parser.parse_args()

        var_update_list = {}

        check = []
        for key in input:
            check.append(key)

        for i in range(len(check)):
            if input[check[i]] is None:
                del input[check[i]]

        if input is not None:
            var_update_list = input

        db.session.query(MediaPlayer_Models).filter_by(id=pk).update(var_update_list)
        db.session.commit()

        return {"status": 200}

    def delete(self, pk):
        input = self.parser.parse_args()

        mediaplayer = MediaPlayer_Models.query.first()

        db.session.delete(mediaplayer)
        db.session.commit()

        return {"status": 200}


class DisplayModelListAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
    #-------------------------------------------------------------------------------------------------------------------
        super(DisplayModelListAPI, self).__init__()

    def get(self):
        #pagination setting
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))

        # [ Make ] ORDER BY
        order_by = request.args.get('order_by')
        order_by_str = ''
        if order_by is not None:
            order_by_str = 'display_models.' + str(order_by)

        order = request.args.get('order')
        order_str = ''
        if order == '-':
            order_str = 'DESC'
        else:
            order_str = 'ASC'

        result_order_by = order_by_str + ' ' + order_str

        result_display = Display_Models.query.order_by(result_order_by).limit(limit).offset(offset)
        total_count = Display_Models.query.count()

        current_count = result_display.count()

        previous = None
        next = None
        previous_offset = offset - limit
        next_offset = offset + limit

        if offset != 0:
            previous = "display_model?limit=" + str(limit) + "&offset=" + str(previous_offset)
            if (total_count - (offset+current_count)) > limit:
                next = "display_model?limit=" + str(limit) + "&offset=" + str(next_offset)
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {}
        objects = []

        meta = {
            "limit": limit,
            "next": next,
            "offset": offset,
            "previous": previous,
            "total_count": total_count
        }

        if current_count == 0:
            meta = {
                "limit": limit,
                "next": 0,
                "offset": offset,
                "previous": 0,
                "total_count": 0
            }

            objects.append({

            })
            return result(404, "No Incidents Info", None, meta, "by PWB")
        else:
            for display in result_display:
                objects.append({
                    "id": display.id,
                    "name": display.name,
                    "img_location": display.img_location,
                    "specification": display.specification
                })
            return result(200, "Get the MediaPlayer list", objects, meta, "by PWB")


class DisplayModelAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("id", type=str, location="json")
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("tag_PowerStatus", type=str, location="json")
        self.parser.add_argument("tag_InputSelect", type=str, location="json")
        self.parser.add_argument("tag_AspectRatio", type=str, location="json")
        self.parser.add_argument("tag_PictureMode", type=str, location="json")
        self.parser.add_argument("tag_Temp", type=str, location="json")
        self.parser.add_argument("tag_Fan", type=str, location="json")
        self.parser.add_argument("tag_Signal", type=str, location="json")
        self.parser.add_argument("tag_Lamp", type=str, location="json")
        self.parser.add_argument("tag_FirmwareVer", type=str, location="json")
        self.parser.add_argument("tag_SerialNo", type=str, location="json")
        self.parser.add_argument("tag_EnergySaving", type=str, location="json")
        self.parser.add_argument("tag_Backlight", type=str, location="json")
        self.parser.add_argument("tag_Constrast", type=str, location="json")
        self.parser.add_argument("tag_Brightness", type=str, location="json")
        self.parser.add_argument("tag_Sharpness", type=str, location="json")
        self.parser.add_argument("tag_Color", type=str, location="json")
        self.parser.add_argument("tag_Tint", type=str, location="json")
        self.parser.add_argument("tag_ColorTemp", type=str, location="json")
        self.parser.add_argument("tag_MonitorStatus", type=str, location="json")
        self.parser.add_argument("tag_RemoteControl", type=str, location="json")
        self.parser.add_argument("tag_Speaker", type=str, location="json")
        self.parser.add_argument("tag_SoundMode", type=str, location="json")
        self.parser.add_argument("tag_Balance", type=str, location="json")
        self.parser.add_argument("tag_VolumControl", type=str, location="json")
        self.parser.add_argument("tag_VolumMute", type=str, location="json")
        self.parser.add_argument("tag_Treble", type=str, location="json")
        self.parser.add_argument("tag_Bass", type=str, location="json")
        self.parser.add_argument("tag_AutoVolume", type=str, location="json")
        self.parser.add_argument("tag_Date", type=str, location="json")
        self.parser.add_argument("tag_Time", type=str, location="json")
        self.parser.add_argument("tag_SleepTime", type=str, location="json")
        self.parser.add_argument("tag_AutoStandby", type=str, location="json")
        self.parser.add_argument("tag_AutoOff", type=str, location="json")
        self.parser.add_argument("tag_ismMode", type=str, location="json")
        self.parser.add_argument("tag_dpmSelect", type=str, location="json")
        self.parser.add_argument("tag_osdSelect", type=str, location="json")
        self.parser.add_argument("tag_Lang", type=str, location="json")
        self.parser.add_argument("tag_Remote", type=str, location="json")
        self.parser.add_argument("tag_PowerOnDelay", type=str, location="json")
        self.parser.add_argument("tag_TileMode", type=str, location="json")
        self.parser.add_argument("tag_TileModeCheck", type=str, location="json")
        self.parser.add_argument("tag_TileID", type=str, location="json")
        self.parser.add_argument("tag_TileNaturalMode", type=str, location="json")
        self.parser.add_argument("tag_SizeH", type=str, location="json")
        self.parser.add_argument("tag_SizeV", type=str, location="json")
        self.parser.add_argument("tag_PositionH", type=str, location="json")
        self.parser.add_argument("tag_PositionV", type=str, location="json")
        self.parser.add_argument("tag_RedGain", type=str, location="json")
        self.parser.add_argument("tag_RedOffset", type=str, location="json")
        self.parser.add_argument("tag_GreenGain", type=str, location="json")
        self.parser.add_argument("tag_GreenOffset", type=str, location="json")
        self.parser.add_argument("tag_BlueGain", type=str, location="json")
        self.parser.add_argument("tag_BlueOffset", type=str, location="json")
        self.parser.add_argument("tag_OnTimer", type=str, location="json")
        self.parser.add_argument("tag_OffTimer", type=str, location="json")
        self.parser.add_argument("tag_OnTimerInput", type=str, location="json")
        self.parser.add_argument("tag_FirmwareUpdate", type=str, location="json")
        self.parser.add_argument("img_location", type=str, location="json")
        self.parser.add_argument("specification", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(DisplayModelAPI, self).__init__()

    def get(self, pk):
        info = request.args.get("name")

        if info is not None:
            display = Display_Models.query.filter_by(id=pk).first()
        else:
            display = Display_Models.query.filter_by(id=pk).first()

        if display is None:
            return

        object = {
            "id": display.id,
            "name": display.name,
            "tag_PowerStatus": display.tag_PowerStatus,
            "tag_InputSelect": display.tag_InputSelect,
            "tag_AspectRatio": display.tag_AspectRatio,
            "tag_PictureMode": display.tag_PictureMode,
            "tag_Temp": display.tag_Temp,
            "tag_Fan": display.tag_Fan,
            "tag_Signal": display.tag_Signal,
            "tag_Lamp": display.tag_Lamp,
            "tag_FirmwareVer": display.tag_FirmwareVer,
            "tag_SerialNo": display.tag_SerialNo,
            "tag_EnergySaving": display.tag_EnergySaving,
            "tag_Backlight": display.tag_Backlight,
            "tag_Constrast": display.tag_Constrast,
            "tag_Brightness": display.tag_Brightness,
            "tag_Sharpness": display.tag_Sharpness,
            "tag_Color": display.tag_Color,
            "tag_Tint": display.tag_Tint,
            "tag_ColorTemp": display.tag_ColorTemp,
            "tag_MonitorStatus": display.tag_MonitorStatus,
            "tag_RemoteControl": display.tag_RemoteControl,
            "tag_Speaker": display.tag_Speaker,
            "tag_SoundMode": display.tag_SoundMode,
            "tag_Balance": display.tag_Balance,
            "tag_VolumControl": display.tag_VolumControl,
            "tag_VolumMute": display.tag_VolumMute,
            "tag_Treble": display.tag_Treble,
            "tag_Bass": display.tag_Bass,
            "tag_AutoVolume": display.tag_AutoVolume,
            "tag_Date": display.tag_Date,
            "tag_Time": display.tag_Time,
            "tag_SleepTime": display.tag_SleepTime,
            "tag_AutoStandby": display.tag_AutoStandby,
            "tag_AutoOff": display.tag_AutoOff,
            "tag_ismMode": display.tag_ismMode,
            "tag_dpmSelect": display.tag_dpmSelect,
            "tag_osdSelect": display.tag_osdSelect,
            "tag_Lang": display.tag_Lang,
            "tag_Remote": display.tag_Remote,
            "tag_PowerOnDelay": display.tag_PowerOnDelay,
            "tag_TileMode": display.tag_TileMode,
            "tag_TileModeCheck": display.tag_TileModeCheck,
            "tag_TileID": display.tag_TileID,
            "tag_TileNaturalMode": display.tag_TileNaturalMode,
            "tag_SizeH": display.tag_SizeH,
            "tag_SizeV": display.tag_SizeV,
            "tag_PositionH": display.tag_PositionH,
            "tag_PositionV": display.tag_PositionV,
            "tag_RedGain": display.tag_RedGain,
            "tag_RedOffset": display.tag_RedOffset,
            "tag_GreenGain": display.tag_GreenGain,
            "tag_GreenOffset": display.tag_GreenOffset,
            "tag_BlueGain": display.tag_BlueGain,
            "tag_BlueOffset": display.tag_BlueOffset,
            "tag_OnTimer": display.tag_OnTimer,
            "tag_OffTimer": display.tag_OffTimer,
            "tag_OnTimerInput": display.tag_OnTimerInput,
            "tag_FirmwareUpdate": display.tag_FirmwareUpdate,
            "img_location": display.img_location,
            "specification": display.specification
        }

        return {"object": object, "status": 200}

    def put(self, pk):
        input = self.parser.parse_args()

        var_update_list = {}
        check = []
        for key in input:
            check.append(key)

        for i in range(len(check)):
            if input[check[i]] is None:
                del input[check[i]]

        update_list = input

        display_model = Display_Models.query.filter_by(id=pk).first()

        if display_model is not None:
            db.session.query(Display_Models).filter_by(id=pk).update(update_list)
            db.session.commit()
            return {"status": 200}
        else:
            return {"status": 400}

    def delete(self, pk):
        input = self.parser.parse_args()

        display = Display_Models.query.filter_by(id=pk).first()

        db.session.delete(display)
        db.session.commit()

        return {"status": 200}


#----------------------------------------------------------------------------------------------------------------------
#                                            URI Area
#----------------------------------------------------------------------------------------------------------------------
api = Api(app)

# Basic URI
api.add_resource(BasicAPI, '/api/v1/server-check')
api.add_resource(AccessAPI, '/api/v1/access_history')

# Auth URI
api.add_resource(AuthAPI, '/api/v1/auth')
api.add_resource(OtpAPI, '/api/v1/otp')

# User URI
api.add_resource(UserAPI, '/api/v1/users')
api.add_resource(CheckUserAPI, '/api/v1/users/check/<string:user_id>')
api.add_resource(CheckGroupAPI, '/api/v1/groups/check/<string:user_id>')
api.add_resource(UserGroupListAPI, '/api/v1/user-group')
api.add_resource(UserGroupAPI, '/api/v1/user-group/<string:key>')
api.add_resource(CONN_UserGroupAPI, '/api/v1/user-group/conn')

# Display List URI
api.add_resource(DisplayListByMediaPlayerAPI, '/api/v1/display_list')

api.add_resource(IncidentListAPI, '/api/v1/incident-list')

# Media Players URI
api.add_resource(MediaPlayerListAPI, '/api/v1/media-players')
api.add_resource(MediaPlayerAPI, '/api/v1/media-players/<string:serial_number>')
api.add_resource(MediaPlayersTop10API, '/api/v1/media-players/top10')
api.add_resource(MediaPlayersControllerAPI, '/api/v1/media-players/controller/<string:serial_number>')
api.add_resource(MediaPlayersReviseAPI, '/api/v1/media-players/revise/<string:serial_number>')
api.add_resource(MediaPlayersAliveAPI, '/api/v1/media-players/alive/<string:serial_number>')
api.add_resource(MediaPlayersCountAPI, '/api/v1/media-players/count')

# Displays URI
api.add_resource(DisplayListAPI, '/api/v1/displays')
api.add_resource(DisplayAPI, '/api/v1/displays/<string:serial_number>')
api.add_resource(DisplaysTop10API, '/api/v1/displays/top10')
api.add_resource(DisplaysControllerAPI, '/api/v1/displays/controller/<string:serial_number>')
api.add_resource(DisplaysReviseAPI, '/api/v1/displays/revise/<string:serial_number>')

# Trees URI
api.add_resource(TreeListAPI, '/api/v1/trees')
api.add_resource(TreeAPI, '/api/v1/trees/<string:shop>')

# History URI
api.add_resource(HistoryListAPI, '/api/v1/history')

# auto_complete URI
api.add_resource(AUTO_MediaPlayerAliasesAPI, '/api/v1/auto_complete/media-player-aliases')
api.add_resource(AUTO_MediaPlayerSerialNumberAPI, '/api/v1/auto_complete/media-player-serial-number')
api.add_resource(AUTO_DisplayAliasesAPI, '/api/v1/auto_complete/display-aliases')
api.add_resource(AUTO_SitesAPI, '/api/v1/auto_complete/sites')
api.add_resource(AUTO_LocationsAPI, '/api/v1/auto_complete/locations')
api.add_resource(AUTO_ShopsAPI, '/api/v1/auto_complete/shops')

# For Report URI
api.add_resource(ReportIncidentListAPI, '/api/v1/report/incident-list')
api.add_resource(ReportTop10API, '/api/v1/report/top10')
api.add_resource(ReportMPHistoryAPI, '/api/v1/report/mp/history/<string:serial_number>')
api.add_resource(ReportDPHistoryAPI, '/api/v1/report/dp/history/<string:serial_number>')
api.add_resource(ReportStatsSettingAPI, '/api/v1/report/stats/setting')
api.add_resource(ReportStatsMPAPI, '/api/v1/report/stats/mp')
api.add_resource(ReportStatsDPAPI, '/api/v1/report/stats/dp')
api.add_resource(ReportStatsDetailAPI, '/api/v1/report/stats/detail')
api.add_resource(ReportStatsRatioAPI, '/api/v1/report/stats/ratio')
api.add_resource(ReportStatsMalfuncMPListAPI, '/api/v1/report/stats/mp/malfunction/list')
api.add_resource(ReportStatsMalfuncDPListAPI, '/api/v1/report/stats/dp/malfunction/list')

# For Administrator
api.add_resource(UsersAdminAPI, '/api/v1/users/admin')
api.add_resource(UsersAdminListAPI, '/api/v1/users/admins')
api.add_resource(UsersManagerListAPI, '/api/v1/users/managers')
api.add_resource(UsersManagerAPI, '/api/v1/users/manager')
api.add_resource(UsersMemberListAPI, '/api/v1/users/members')
api.add_resource(UsersMemberAPI, '/api/v1/users/member')
# Device Group URI
api.add_resource(DeviceGroupListAPI, '/api/v1/device-group')
api.add_resource(DeviceGroupAPI, '/api/v1/device-group/<string:key>')
api.add_resource(DeviceManageListAPI, '/api/v1/device')
api.add_resource(DeviceManageAPI, '/api/v1/device/<string:key>')
api.add_resource(DeviceManageDisplayListAPI, '/api/v1/device/display/<string:key>')

api.add_resource(FaultPredictListAPI, '/api/v1/fault-predict')
api.add_resource(FaultPredictAPI, '/api/v1/fault-predict/<string:key>')

# Assigned URI
api.add_resource(CONN_DeviceGroupAPI, '/api/v1/member-device-group/<string:key>')
api.add_resource(TagListAPI, '/api/v1/tagging')
api.add_resource(AssignedTagListAPI, '/api/v1/my-tag')

# Site
api.add_resource(SiteListAPI, '/api/v1/sites')
api.add_resource(CONN_SiteListAPI, '/api/v1/my-sites')

# For Silent Install
api.add_resource(SilentInstallListAPI, '/api/v1/silent-install')
api.add_resource(SilentInstallAPI, '/api/v1/silent-install/<string:install_id>')

# File Upload
api.add_resource(FileUploadAPI, '/api/v1/file_upload')

# TEST
api.add_resource(AlarmAPI, '/api/v1/alarm')

# For Agent
api.add_resource(MediaPlayersPollingTimeAPI, '/api/v1/agent/media-players/polling_time/<string:serial_number>')
api.add_resource(MediaPlayersAppAPI, '/api/v1/agent/media-players/app/<string:serial_number>')
api.add_resource(MediaPlayerRegisterAPI, '/api/v1/agent/media-players')
api.add_resource(MediaPlayerPollingAPI, '/api/v1/agent/media-players/<string:serial_number>')
api.add_resource(DisplayRegisterAPI, '/api/v1/agent/displays')
api.add_resource(DisplayPollingAPI, '/api/v1/agent/displays/<string:serial_number>')
api.add_resource(DisplayAliveAPI, '/api/v1/agent/displays/alive/<string:serial_number>')
api.add_resource(DisplayModelsAPI, '/api/v1/agent/display_models/<string:model_name>')
api.add_resource(DisplayModelsNoneAPI, '/api/v1/agent/display_models/')
api.add_resource(DisplaysTimerAPI, '/api/v1/agent/displays/timer/<string:serial_number>')
api.add_resource(InstallDataAPI, '/api/v1/agent/install_data/<string:comp_key>')
api.add_resource(ServersAPI, '/api/v1/agent/servers/info')

api.add_resource(ServerListAPI, '/api/v1/admin/servers')
api.add_resource(ServerAPI, '/api/v1/admin/servers/<int:pk>')
api.add_resource(MediaPlayerModelListAPI, '/api/v1/admin/mediaplayer_model')
api.add_resource(MediaPlayerModelAPI, '/api/v1/admin/mediaplayer_model/<int:pk>')
api.add_resource(DisplayModelListAPI, '/api/v1/admin/display_model')
api.add_resource(DisplayModelAPI, '/api/v1/admin/display_model/<int:pk>')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8088)
