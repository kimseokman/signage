__author__ = 'pjh@koino.net'
# -*- coding: utf-8 -*-

import os
import sys
import time
import signal
import platform
import logging
from logging import handlers
import MySQLdb
from MySQLdb import Error
from datetime import datetime
import re

# Multilanguages
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

current_dir = os.path.dirname(os.path.realpath(__file__))

g_platform = platform.system()

if g_platform == "Linux":
    LOG_DEFAULT_DIR = '/var/log/dstcs/'
elif g_platform == "Windows":
    LOG_DEFAULT_DIR = '.'
elif g_platform == "Darwin":
    LOG_DEFAULT_DIR = '.'


class HistoryBackup(object):
    def __init__(self, logger, db_info, intervals):
        # Properties for Daemon
        self.stdin_path = '/dev/null'
        self.stdout_path = current_dir + '/history-backup.out'
        self.stderr_path = current_dir + '/history-backup.err'
        self.pidfile_path = current_dir + '/history-backup.pid'
        self.pidfile_timeout = 5

        self.db_info = db_info
        self.db = None

        self.logger = logger
        self.is_running = True
        self.intervals = 60

        self.history_delete_period = 10
        self.history_delete_counter = 0

    def connect_to_db(self, db_info=None):
        if db_info is None:
            info = self.db_info
        else:
            info = db_info

        try:
            self.db = MySQLdb.connect(
                info['host'],
                info['username'],
                info['password'],
                info['database'])

            self.logger.info('Database connected.')
            self.db.autocommit(True)

            return True

        except Error, e:
            self.logger.error('Database connection failed: %s' % str(e))

            return False

    def disconnect(self):
        if self.db:
            self.db.close()


    def cursor(self):
        if self.db is None:
            return None

        return self.db.cursor(MySQLdb.cursors.DictCursor)


    def check(self):

        check_mp_sqls = []
        check_dp_sqls = []
        create_mp_sqls = []
        create_dp_sqls = []
        mp_sqls = []
        dp_sqls = []
        delete_mp_sqls = []
        delete_dp_sqls = []
        cursor = None

        utc_table_date = datetime.utcnow()
        utc_table_date = utc_table_date.strftime('%Y%m')
        utc_t_date = str(utc_table_date)

        utc_datetime_date = datetime.utcnow()
        utc_datetime_date.strftime("%Y-%m-%d %H:%M:%S")
        utc_datetime = str(utc_datetime_date)

        check_mp_sqls.append("SHOW TABLES LIKE 'mp_history_" + utc_t_date + "'")
        check_dp_sqls.append("SHOW TABLES LIKE 'dp_history_" + utc_t_date + "'")

        create_mp_sqls.append("CREATE TABLE IF NOT EXISTS `mp_history_" + utc_t_date + "` ("
                            "`id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
                            "`serial_number` varchar(256) DEFAULT NULL,"
                            "`alias` varchar(256) DEFAULT NULL,"
                            "`last_updated_date` datetime DEFAULT NULL,"
                            "`cpu_temp` float DEFAULT NULL,"
                            "`cpu_usage` float DEFAULT NULL,"
                            "`hdd_temp` float DEFAULT NULL,"
                            "`hdd_usage` float DEFAULT NULL,"
                            "`mem_usage` float DEFAULT NULL,"
                            "`fan_speed` float DEFAULT NULL,"
                            "`app_status1` varchar(20) DEFAULT NULL,"
                            "`app_status2` varchar(20) DEFAULT NULL,"
                            "`app_status3` varchar(20) DEFAULT NULL,"
                            "`app_status4` varchar(20) DEFAULT NULL,"
                            "`app_cpu1` float DEFAULT NULL,"
                            "`app_cpu2` float DEFAULT NULL,"
                            "`app_cpu3` float DEFAULT NULL,"
                            "`app_cpu4` float DEFAULT NULL,"
                            "`app_memory1` float DEFAULT NULL,"
                            "`app_memory2` float DEFAULT NULL,"
                            "`app_memory3` float DEFAULT NULL,"
                            "`app_memory4` float DEFAULT NULL,"
                            "`FK_managers_id` int(11) unsigned DEFAULT NULL,"
                            "PRIMARY KEY (`id`)"
                            ") ENGINE=InnoDB  DEFAULT CHARSET=utf8")

        create_dp_sqls.append("CREATE TABLE IF NOT EXISTS `dp_history_" + utc_t_date + "` ("
                            "`id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
                            "`serial_number` varchar(256) DEFAULT NULL,"
                            "`alias` varchar(256) DEFAULT NULL,"
                            "`last_updated_date` datetime DEFAULT NULL,"
                            "`val_PowerStatus` varchar(48) DEFAULT NULL,"
                            "`val_InputSelect` varchar(48) DEFAULT NULL,"
                            "`val_AspectRatio` varchar(48) DEFAULT NULL,"
                            "`val_PictureMode` varchar(48) DEFAULT NULL,"
                            "`val_Temp` varchar(48) DEFAULT NULL,"
                            "`val_Fan` varchar(48) DEFAULT NULL,"
                            "`val_Signal` varchar(48) DEFAULT NULL,"
                            "`val_Lamp` varchar(48) DEFAULT NULL,"
                            "`val_SerialNo` varchar(48) DEFAULT NULL,"
                            "`val_FirmwareVer` varchar(48) DEFAULT NULL,"
                            "`val_MonitorStatus` varchar(48) DEFAULT NULL,"
                            "`val_EnergySaving` varchar(48) DEFAULT NULL,"
                            "`val_Backlight` varchar(48) DEFAULT NULL,"
                            "`val_Constrast` varchar(48) DEFAULT NULL,"
                            "`val_Brightness` varchar(48) DEFAULT NULL,"
                            "`val_Sharpness` varchar(48) DEFAULT NULL,"
                            "`val_Color` varchar(48) DEFAULT NULL,"
                            "`val_Tint` varchar(48) DEFAULT NULL,"
                            "`val_ColorTemp` varchar(48) DEFAULT NULL,"
                            "`val_Speaker` varchar(48) DEFAULT NULL,"
                            "`val_SoundMode` varchar(48) DEFAULT NULL,"
                            "`val_Balance` varchar(48) DEFAULT NULL,"
                            "`val_VolumControl` varchar(48) DEFAULT NULL,"
                            "`val_VolumMute` varchar(48) DEFAULT NULL,"
                            "`val_Treble` varchar(48) DEFAULT NULL,"
                            "`val_Bass` varchar(48) DEFAULT NULL,"
                            "`val_AutoVolume` varchar(48) DEFAULT NULL,"
                            "`val_Date` varchar(48) DEFAULT NULL,"
                            "`val_Time` varchar(48) DEFAULT NULL,"
                            "`val_SleepTime` varchar(48) DEFAULT NULL,"
                            "`val_AutoStandby` varchar(48) DEFAULT NULL,"
                            "`val_AutoOff` varchar(48) DEFAULT NULL,"
                            "`val_ismMode` varchar(48) DEFAULT NULL,"
                            "`val_dpmSelect` varchar(48) DEFAULT NULL,"
                            "`val_osdSelect` varchar(48) DEFAULT NULL,"
                            "`val_Lang` varchar(48) DEFAULT NULL,"
                            "`val_Remote` varchar(48) DEFAULT NULL,"
                            "`val_PowerOnDelay` varchar(48) DEFAULT NULL,"
                            "`val_TileModeCheck` varchar(48) DEFAULT NULL,"
                            "`val_TileID` varchar(48) DEFAULT NULL,"
                            "`val_TileNaturalMode` varchar(48) DEFAULT NULL,"
                            "`val_SizeH` varchar(48) DEFAULT NULL,"
                            "`val_SizeV` varchar(48) DEFAULT NULL,"
                            "`val_PositionH` varchar(48) DEFAULT NULL,"
                            "`val_PositionV` varchar(48) DEFAULT NULL,"
                            "`val_RedGain` varchar(48) DEFAULT NULL,"
                            "`val_RedOffset` varchar(48) DEFAULT NULL,"
                            "`val_GreenGain` varchar(48) DEFAULT NULL,"
                            "`val_GreenOffset` varchar(48) DEFAULT NULL,"
                            "`val_BlueGain` varchar(48) DEFAULT NULL,"
                            "`val_BlueOffset` varchar(48) DEFAULT NULL,"
                            "`val_OnTimer` varchar(48) DEFAULT NULL,"
                            "`val_OffTimer` varchar(48) DEFAULT NULL,"
                            "`val_OnTimerInput` varchar(48) DEFAULT NULL,"
                            "`mp_serial_number` varchar(256) DEFAULT NULL,"
                            "`FK_managers_id` int(11) unsigned DEFAULT NULL,"
                            "PRIMARY KEY (`id`)"
                            ") ENGINE=InnoDB  DEFAULT CHARSET=utf8")


        mp_sqls.append("SELECT * FROM dstcs.mediaplayers_history WHERE last_updated_date < DATE_ADD(DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), INTERVAL -3 DAY) limit 1")
        dp_sqls.append("SELECT * FROM dstcs.displays_history WHERE last_updated_date < DATE_ADD(DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), INTERVAL -3 DAY) limit 1")

        delete_mp_sqls.append("DELETE FROM dstcs.mediaplayers_history WHERE last_updated_date < DATE_ADD(DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), INTERVAL -3 DAY)")
        delete_dp_sqls.append("DELETE FROM dstcs.displays_history WHERE last_updated_date < DATE_ADD(DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), INTERVAL -3 DAY)")

        try:
            cursor = self.cursor()
            strMpDate = ""
            strDpDate = ""

            for csql in check_mp_sqls:
                cursor.execute(csql)
                sm_data = cursor.fetchone()

                if sm_data is None:
                    for sql in create_mp_sqls:
                        cursor.execute(sql)

            for csql in check_dp_sqls:
                cursor.execute(csql)
                sd_data = cursor.fetchone()

                if sd_data is None:
                    for sql in create_dp_sqls:
                        cursor.execute(sql)

            for sql in mp_sqls:
                rows_count = cursor.execute(sql)

                if rows_count > 0:
                    rm_data = cursor.fetchone()
                else:
                    continue

                if rm_data is None or rm_data['last_updated_date'] is None:
                    continue

                strMpDate = str(rm_data['last_updated_date'])[0:7]
                strMpDate = re.sub('[=.^#/!?%@:&$*}()+-]', '', strMpDate)

                insert_mp_sqls = "INSERT INTO dstcs_history.mp_history_"+strMpDate+" " \
                                "SELECT * FROM dstcs.mediaplayers_history WHERE last_updated_date < DATE_ADD(DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), INTERVAL -3 DAY)"

                # print insert_mp_sqls
                mp_row_cnt = cursor.execute(insert_mp_sqls)

                if mp_row_cnt != 0:
                    for delete_mp_sql in delete_mp_sqls:
                        cursor.execute(delete_mp_sql)
                # print mp_row_cnt

            for sql in dp_sqls:
                rows_count = cursor.execute(sql)

                if rows_count > 0:
                    rd_data = cursor.fetchone()
                else:
                    continue

                if rd_data is None or rd_data['last_updated_date'] is None:
                    continue

                strDpDate = str(rd_data['last_updated_date'])[0:7]
                strDpDate = re.sub('[=.^#/!?%@:&$*}()+-]', '', strDpDate)

                insert_dp_sqls = "INSERT INTO dstcs_history.dp_history_"+strDpDate+" " \
                                "SELECT * FROM dstcs.displays_history WHERE last_updated_date < DATE_ADD(DATE_FORMAT('"+utc_datetime+"','%Y-%m-%d 00:00:00'), INTERVAL -3 DAY)"

                # print insert_dp_sqls
                dp_row_cnt = cursor.execute(insert_dp_sqls)

                if dp_row_cnt != 0:
                    for delete_dp_sql in delete_dp_sqls:
                        cursor.execute(delete_dp_sql)
                # print dp_row_cnt

            # for delete_sql in delete_sqls:
            #     cursor.execute(delete_sql)

            return []

        except Error, e:
            self.logger.error("Check sql statement failed: %s" % str(e))
            return []

        finally:
            if not cursor:
                cursor.close()

    def check_cumulative_top10(self):

        time_slot = ['3', '6', '12', '24']
        mp_case_slot = ['cpu_usage', 'mem_usage', 'hdd_usage', 'cpu_temp']
        dp_case_slot = ['val_temp']
        insert_sqls = []

        sql_query = "SELECT id FROM dstcs.managers"
        cursor = self.cursor()

        utc_datetime_date = datetime.utcnow()
        utc_datetime_date.strftime("%Y-%m-%d %H:%M:%S")
        utc_datetime = str(utc_datetime_date)

        cursor.execute(sql_query)
        mgr_list = cursor.fetchall()

        for mgr in mgr_list:

            for slot_case in mp_case_slot:
                sql_query = "SELECT id FROM dstcs.cumulative_top10_" + slot_case + " WHERE FK_managers_id = " + str(mgr['id'])
                cursor.execute(sql_query)
                ret_data = cursor.fetchone()

                if ret_data is None:
                    for slot_num in time_slot:
                        # insert average top10
                        insert_sqls.append("INSERT INTO dstcs.cumulative_top10_" + slot_case + "(last_updated_date, time_range, FK_managers_id)" \
                                        " VALUES('%s', '%s', %d)" % (utc_datetime, slot_num, mgr['id']))

            for slot_case in dp_case_slot:
                sql_query = "SELECT id FROM dstcs.cumulative_top10_" + slot_case + " WHERE FK_managers_id = " + str(mgr['id'])
                cursor.execute(sql_query)
                ret_data = cursor.fetchone()

                if ret_data is None:
                    for slot_num in time_slot:
                        # insert average top10
                        insert_sqls.append("INSERT INTO dstcs.cumulative_top10_" + slot_case + "(last_updated_date, time_range, FK_managers_id)" \
                                        " VALUES('%s', '%s', %d)" % (utc_datetime, slot_num, mgr['id']))

            for insert_sql in insert_sqls:
                # print insert_sql
                cursor.execute(insert_sql)

            for slot_case in mp_case_slot:
                for slot_num in time_slot:
                    # update average top10
                    cu_s_query = "SELECT alias, serial_number, avg(" + slot_case + ") as avg" \
                            " FROM dstcs.mediaplayers_history" \
                            " WHERE FK_managers_id = '" + str(mgr['id']) + "' and last_updated_date > DATE_ADD('"+utc_datetime+"', INTERVAL -"+ slot_num +" HOUR) group by serial_number order by avg desc limit 10"

                    # print cu_s_query

                    cursor.execute(cu_s_query)
                    cu_top10_list = cursor.fetchall()

                    if len(cu_top10_list) > 0:
                        i = 1
                        cu_u_query = "UPDATE dstcs.cumulative_top10_" + slot_case + " SET last_updated_date = '"+ utc_datetime +"',"

                        for cu_top10 in cu_top10_list:
                            cu_u_query = cu_u_query + " avg_" + slot_case + str(i) + " = '" + str(cu_top10['avg']) + "'," \
                                        " avg_" + slot_case + str(i) + "_alias = '" + cu_top10['alias'] + "'," \
                                        " avg_" + slot_case + str(i) + "_sn = '" + cu_top10['serial_number'] + "'"

                            if len(cu_top10_list) != i:
                                cu_u_query = cu_u_query + ","

                            i = i + 1

                        cu_u_query = cu_u_query + " WHERE time_range = '" + slot_num + "' and FK_managers_id = '" + str(mgr['id']) + "'"

                        cursor.execute(cu_u_query)
                        # print cu_u_query

            for slot_case in dp_case_slot:
                for slot_num in time_slot:
                    cu_s_query = "SELECT alias, serial_number, mp_serial_number, avg('" + slot_case + "') as avg" \
                            " FROM dstcs.displays_history" \
                            " WHERE FK_managers_id = '" + str(mgr['id']) + "' and last_updated_date > DATE_ADD('"+utc_datetime+"', INTERVAL -"+ slot_num +" HOUR) group by serial_number order by avg desc limit 10"

                    # print cu_s_query

                    cursor.execute(cu_s_query)
                    cu_top10_list = cursor.fetchall()

                    if len(cu_top10_list) > 0:
                        i = 1
                        cu_u_query = "UPDATE dstcs.cumulative_top10_" + slot_case + " SET last_updated_date = '"+ utc_datetime +"',"

                        for cu_top10 in cu_top10_list:
                            cu_u_query = cu_u_query + " avg_" + slot_case + str(i) + " = '" + str(cu_top10['avg']) + "'," \
                                        " avg_" + slot_case + str(i) + "_alias = '" + cu_top10['alias'] + "'," \
                                        " avg_" + slot_case + str(i) + "_sn = '" + cu_top10['serial_number'] + "'," \
                                        " avg_" + slot_case + str(i) + "_mp_sn = '" + cu_top10['mp_serial_number'] + "'"

                            if len(cu_top10_list) != i:
                                cu_u_query = cu_u_query + ","

                            i = i + 1

                        cu_u_query = cu_u_query + " WHERE time_range = '" + slot_num + "' and FK_managers_id = '" + str(mgr['id']) + "'"

                        cursor.execute(cu_u_query)
                        # print cu_u_query

            # print "check_cumulative_top10 End"
            insert_sqls = []

    def run(self):
        self.init_signal_handler()

        while self.is_running:

            for _ in range(self.intervals):
                if self.is_running:
                    time.sleep(1)

            now = datetime.utcnow()
            nowTime = now.strftime('%H%M')
            nowMinute = now.strftime('%M')

            print nowTime
            if nowTime == '0200':
                if not self.connect_to_db():
                    self.logger.error('Database initialize failed.')
                    continue

                checked_users = self.check()
                self.disconnect()

            if nowMinute == '00':
                if not self.connect_to_db():
                    self.logger.error('Database initialize failed.')
                    continue

                self.check_cumulative_top10()
                self.disconnect()

    def init_signal_handler(self):
        def signal_term_handler(signum, frame):
            self.logger.info("Got TERM signal. Stopping application.")
            self.is_running = False

        signal.signal(signal.SIGTERM, signal_term_handler)

if __name__ == '__main__':
    file_logger = logging.getLogger("HistoryBackup")
    file_logger.setLevel(logging.INFO)

    file_handler = handlers.RotatingFileHandler(
        "history-backup.log",
        maxBytes=(1024 * 1024 * 1),
        backupCount=3
    )

    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
    file_handler.setFormatter(formatter)
    file_logger.addHandler(file_handler)

    app = HistoryBackup(file_logger, {
        'host': '127.0.0.1',
        'username': 'root',
        'password': '1234',
        'database': 'dstcs_history'
    }, 60)

    if g_platform == "Linux":
    #if False:
        from daemon import runner
        try:
            daemon_runner = runner.DaemonRunner(app)
            daemon_runner.daemon_context.files_preserve = [file_handler.stream]
            daemon_runner.do_action()

        except runner.DaemonRunnerStopFailureError:
            print("Daemon already stopped.")

        except runner.DaemonRunnerStartFailureError:
            print("Daemon start failed.")
    else:
        app.logger.info('history-backup initialize.')
        app.run()


