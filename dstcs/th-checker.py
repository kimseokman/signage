__author__ = 'rainmaker'
# -*- coding: utf-8 -*-

import os
import sys
import time
import signal
import platform
import logging
from logging import handlers
import MySQLdb
from MySQLdb import Error
import smtplib
from datetime import datetime, timedelta
from clickatell.api import Clickatell
from clickatell import constants as cc
import re

# Multilanguages
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

current_dir = os.path.dirname(os.path.realpath(__file__))

g_platform = platform.system()

if g_platform == "Linux":
    LOG_DEFAULT_DIR = '/var/log/dstcs/'
elif g_platform == "Windows":
    LOG_DEFAULT_DIR = '.'
elif g_platform == "Darwin":
    LOG_DEFAULT_DIR = '.'


class ThresholdChecker(object):
    def __init__(self, logger, db_info, intervals):
        # Properties for Daemon
        self.stdin_path = '/dev/null'
        self.stdout_path = current_dir + '/th-checker.out'
        self.stderr_path = current_dir + '/th-checker.err'
        self.pidfile_path = current_dir + '/th-checker.pid'
        self.pidfile_timeout = 5

        self.db_info = db_info
        self.db = None

        self.logger = logger
        self.is_running = True
        self.intervals = intervals

        self.from_email = "system@adamssuite.com"
        self.message = []

    def connect_to_db(self, db_info=None):
        if db_info is None:
            info = self.db_info
        else:
            info = db_info

        try:
            self.db = MySQLdb.connect(
                info['host'],
                info['username'],
                info['password'],
                info['database'])

            self.logger.info('Database connected.')
            self.db.autocommit(True)

            return True

        except Error, e:
            self.logger.error('Database connection failed: %s' % str(e))

            return False

    def disconnect(self):
        if self.db:
            self.db.close()

    def cursor(self):
        if self.db is None:
            return None

        return self.db.cursor(MySQLdb.cursors.DictCursor)

    def check(self):
        sqls = []

        # cpu_usage 1
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_cpu_usage = 0" \
                    " WHERE (dg.th_cpu_usage_warning > mp.cpu_usage) or ((dg.th_cpu_usage_warning is null) and (dg.th_cpu_usage_error > mp.cpu_usage)) or ((dg.th_cpu_usage_warning is null) and (dg.th_cpu_usage_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_cpu_usage = 1" \
                    " WHERE ((dg.th_cpu_usage_warning <= mp.cpu_usage) and (dg.th_cpu_usage_error > mp.cpu_usage)) or ((dg.th_cpu_usage_warning <= mp.cpu_usage) and (dg.th_cpu_usage_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_cpu_usage = 2" \
                    " WHERE (dg.th_cpu_usage_error <= mp.cpu_usage)")

        # mem_usage 3
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_mem_usage = 0" \
                    " WHERE (dg.th_mem_usage_warning > mp.mem_usage) or ((dg.th_mem_usage_warning is null) and (dg.th_mem_usage_error > mp.mem_usage)) or ((dg.th_mem_usage_warning is null) and (dg.th_mem_usage_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_mem_usage = 1" \
                    " WHERE ((dg.th_mem_usage_warning <= mp.mem_usage) and (dg.th_mem_usage_error > mp.mem_usage)) or ((dg.th_mem_usage_warning <= mp.mem_usage) and (dg.th_mem_usage_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_mem_usage = 2" \
                    " WHERE (dg.th_mem_usage_error <= mp.mem_usage)")

        # hdd_usage 5
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_hdd_usage = 0" \
                    " WHERE (dg.th_hdd_usage_warning > mp.hdd_usage) or ((dg.th_hdd_usage_warning is null) and (dg.th_hdd_usage_error > mp.hdd_usage)) or ((dg.th_hdd_usage_warning is null) and (dg.th_hdd_usage_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_hdd_usage = 1" \
                    " WHERE ((dg.th_hdd_usage_warning <= mp.hdd_usage) and (dg.th_hdd_usage_error > mp.hdd_usage)) or ((dg.th_hdd_usage_warning <= mp.hdd_usage) and (dg.th_hdd_usage_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_hdd_usage = 2" \
                    " WHERE (dg.th_hdd_usage_error <= mp.hdd_usage)")

        # cpu_temp 7
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_cpu_temp = 0" \
                    " WHERE (dg.th_cpu_temp_warning > mp.cpu_temp) or ((dg.th_cpu_temp_warning is null) and (dg.th_cpu_temp_error > mp.cpu_temp)) or ((dg.th_cpu_temp_warning is null) and (dg.th_cpu_temp_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_cpu_temp = 1" \
                    " WHERE ((dg.th_cpu_temp_warning <= mp.cpu_temp) and (dg.th_cpu_temp_error > mp.cpu_temp)) or ((dg.th_cpu_temp_warning <= mp.cpu_temp) and (dg.th_cpu_temp_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_cpu_temp = 2" \
                    " WHERE (dg.th_cpu_temp_error <= mp.cpu_temp)")

        # hdd_temp 9
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_hdd_temp = 0" \
                    " WHERE (dg.th_hdd_temp_warning > mp.hdd_temp) or ((dg.th_hdd_temp_warning is null) and (dg.th_hdd_temp_error > mp.hdd_temp)) or ((dg.th_hdd_temp_warning is null) and (dg.th_hdd_temp_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_hdd_temp = 1" \
                    " WHERE ((dg.th_hdd_temp_warning <= mp.hdd_temp) and (dg.th_hdd_temp_error > mp.hdd_temp)) or ((dg.th_hdd_temp_warning <= mp.hdd_temp) and (dg.th_hdd_temp_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_hdd_temp = 2" \
                    " WHERE (dg.th_hdd_temp_error <= mp.hdd_temp)")

        # fan_speed 11
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_fan_speed = 0" \
                    " WHERE (dg.th_fan_speed_warning > mp.fan_speed) or ((dg.th_fan_speed_warning is null) and (dg.th_fan_speed_error > mp.fan_speed)) or ((dg.th_fan_speed_warning is null) and (dg.th_fan_speed_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_fan_speed = 1" \
                    " WHERE ((dg.th_fan_speed_warning <= mp.fan_speed) and (dg.th_fan_speed_error > mp.fan_speed)) or ((dg.th_fan_speed_warning <= mp.fan_speed) and (dg.th_fan_speed_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_fan_speed = 2" \
                    " WHERE (dg.th_fan_speed_error <= mp.fan_speed)")

        # display temperature 13
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET dp.status_temp = 0" \
                    " WHERE (dg.th_dp_temp_warning > dp.val_Temp) or ((dg.th_dp_temp_warning is null) and (dg.th_dp_temp_error > dp.val_Temp)) or ((dg.th_dp_temp_warning is null) and (dg.th_dp_temp_error is null))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET dp.status_temp = 1" \
                    " WHERE ((dg.th_dp_temp_warning <= dp.val_Temp) and (dg.th_dp_temp_error > dp.val_Temp)) or ((dg.th_dp_temp_warning <= dp.val_Temp) and (dg.th_dp_temp_error is null))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET dp.status_temp = 2" \
                    " WHERE (dg.th_dp_temp_error <= dp.val_Temp)")

        # response_time 15
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_res_time = 0" \
                    " WHERE (dg.th_res_time_warning > mp.response_time) or ((dg.th_res_time_warning is null) and (dg.th_res_time_error > mp.response_time)) or ((dg.th_res_time_warning is null) and (dg.th_res_time_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_res_time = 1" \
                    " WHERE ((dg.th_res_time_warning <= mp.response_time) and (dg.th_res_time_error > mp.response_time)) or ((dg.th_res_time_warning <= mp.response_time) and (dg.th_res_time_error is null))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.status_res_time = 2" \
                    " WHERE (dg.th_res_time_error <= mp.response_time)")

        # display power when tv control time set

        # display setting(contrast, brightness, etc.)

         # synchronize status, group_status 17
        # mediaplayers don't have displays
        sqls.append("UPDATE mediaplayers as mp SET mp.group_status=0, mp.status=0" \
                    " WHERE (mp.status_cpu_usage=0 and mp.status_mem_usage=0 and mp.status_hdd_usage=0 and mp.status_cpu_temp=0 and mp.status_hdd_temp=0 and mp.status_fan_speed=0 and mp.status_res_time=0) and mp.alive=1 and mp.group_status!=-1")

        sqls.append("UPDATE mediaplayers as mp SET mp.group_status=1, mp.status=1" \
                    " WHERE ((mp.status_cpu_usage=1 or mp.status_mem_usage=1 or mp.status_hdd_usage=1 or mp.status_cpu_temp=1 or mp.status_hdd_temp=1 or mp.status_fan_speed=1 or mp.status_res_time=1) and mp.alive=1 and mp.group_status!=-1)")

        sqls.append("UPDATE mediaplayers as mp SET mp.group_status=2, mp.status=2" \
                    " WHERE (mp.status_cpu_usage=2 or mp.status_mem_usage=2 or mp.status_hdd_usage=2 or mp.status_cpu_temp=2 or mp.status_hdd_temp=2 or mp.status_fan_speed=2 or mp.status_res_time=2) and mp.alive=1 and mp.group_status!=-1")


        # synchronize status, group_status 20
        # mediaplayers have displays
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id SET mp.group_status=0, mp.status=0, dp.status=0" \
                    " WHERE (mp.status_cpu_usage=0 and mp.status_mem_usage=0 and mp.status_hdd_usage=0 and mp.status_cpu_temp=0 and mp.status_hdd_temp=0 and mp.status_fan_speed=0 and mp.status_res_time=0 and dp.status_temp=0 and dp.val_Signal!='NG' and dp.val_Lamp!='NG') and mp.alive=1 and mp.group_status!=-1")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id SET mp.group_status=1, mp.status=1, dp.status=1" \
                    " WHERE ((mp.status_cpu_usage=1 or mp.status_mem_usage=1 or mp.status_hdd_usage=1 or mp.status_cpu_temp=1 or mp.status_hdd_temp=1 or mp.status_fan_speed=1 or mp.status_res_time=1 or dp.status_temp=1) and mp.alive=1 and mp.group_status!=-1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id SET mp.group_status=2, mp.status=2, dp.status=2" \
                    " WHERE (mp.status_cpu_usage=2 or mp.status_mem_usage=2 or mp.status_hdd_usage=2 or mp.status_cpu_temp=2 or mp.status_hdd_temp=2 or mp.status_fan_speed=2 or mp.status_res_time=2 or dp.status_temp=2 or dp.val_Signal='NG' or dp.val_Lamp='NG' or mp.svr_conn='Disconnected') and mp.alive=1 and mp.group_status!=-1")


        # Incident List
        # R01
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_status=1, mp.ic_R01_time=utc_timestamp()" \
                    " WHERE (dg.ic_R01_enable = 1) and (mp.alive = 0) and (mp.ic_R01_status = 0) and (dg.timer_operate_enable = 1) and (dg.timer_operate_reverse_flag = 0)" \
                    " and ( ((DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) and (DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_end, '%H-%i')))"
                    " or ((DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) or (DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_end, '%H-%i'))) and (TIMESTAMPDIFF(MINUTE, mp.utc_time, utc_timestamp()) > 20) )")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_status=1, mp.ic_R01_time=utc_timestamp()" \
                    " WHERE (dg.ic_R01_enable = 1) and (mp.alive = 0) and (mp.ic_R01_status = 0) and (dg.timer_operate_enable = 1) and (dg.timer_operate_reverse_flag = 1)" \
                    " and ( ((DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) or (DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_end, '%H-%i')))"
                    " or ((DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) or (DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_end, '%H-%i'))) and (TIMESTAMPDIFF(MINUTE, mp.utc_time, utc_timestamp()) > 20) )")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_status=1, mp.ic_R01_time=utc_timestamp()" \
                    " WHERE (dg.ic_R01_enable = 1) and (mp.alive = 0) and (mp.ic_R01_status = 0) and (dg.timer_operate_enable = 0)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_status=0, mp.ic_R01_logging=0" \
                    " WHERE (dg.ic_R01_enable = 0) or (mp.alive = 1)")

        sqls.append("UPDATE incidents as ic JOIN mediaplayers as mp JOIN device_groups as dg ON ic.mp_serial_number=mp.serial_number and mp.FK_device_groups_id=dg.id" \
                    " SET ic.solved_time=utc_timestamp()" \
                    " WHERE ((dg.ic_R01_enable = 0) or (mp.alive = 1)) and (ic.error_code = 'R01') and (ic.solved_time is null)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_status=2" \
                    " WHERE (dg.ic_R01_enable = 1) and (mp.alive = 0) and (mp.ic_R01_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R01_time, utc_timestamp()) = dg.ic_R01_timeout) and (dg.timer_operate_enable = 1) and (dg.timer_operate_reverse_flag = 0)" \
                    " and ( ((DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) and (DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_end, '%H-%i')))" \
                    " or ((DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) or (DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_end, '%H-%i'))) and (TIMESTAMPDIFF(MINUTE, mp.utc_time, utc_timestamp()) > 20) )")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_status=2" \
                    " WHERE (dg.ic_R01_enable = 1) and (mp.alive = 0) and (mp.ic_R01_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R01_time, utc_timestamp()) = dg.ic_R01_timeout) and (dg.timer_operate_enable = 1) and (dg.timer_operate_reverse_flag = 1)" \
                    " and ( ((DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) or (DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_end, '%H-%i')))" \
                    " or ((DATE_FORMAT(mp.system_time,'%H-%i') >= DATE_FORMAT(dg.timer_operate_begin, '%H-%i')) or (DATE_FORMAT(mp.system_time,'%H-%i') < DATE_FORMAT(dg.timer_operate_end, '%H-%i'))) and (TIMESTAMPDIFF(MINUTE, mp.utc_time, utc_timestamp()) > 20) )")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_status=2" \
                    " WHERE (dg.ic_R01_enable = 1) and (mp.alive = 0) and (mp.ic_R01_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R01_time, utc_timestamp()) = dg.ic_R01_timeout) and (dg.timer_operate_enable = 0)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'R01', 2, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_R01_status = 2) and (ic_R01_logging = 0) and (alive = 0)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id"
                    " SET mp.ic_R01_time=utc_timestamp()" \
                    " WHERE (dg.ic_R01_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R01_time, utc_timestamp()) >= dg.ic_R01_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R01_logging=1" \
                    " WHERE (dg.ic_R01_enable = 1) and (mp.ic_R01_status = 2)")

        # R02
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_R02_status=1, dp.ic_R02_time=utc_timestamp()" \
                    " WHERE (dg.ic_R02_enable = 1) and (mp.alive = 1) and (dp.alive = 0) and (dp.ic_R02_status = 0)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_R02_status=0, dp.ic_R02_logging=0" \
                    " WHERE (dg.ic_R02_enable = 0) or ((dp.alive = 1) and (mp.alive = 1))")

        sqls.append("UPDATE incidents as ic JOIN displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id and ic.dp_serial_number=dp.serial_number" \
                    " SET ic.solved_time=utc_timestamp()" \
                    " WHERE ((dg.ic_R02_enable = 0) or ((dp.alive = 1) and (mp.alive = 1))) and (ic.error_code = 'R02') and (ic.solved_time is null)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_R02_status=2" \
                    " WHERE (dg.ic_R02_enable = 1) and (dp.alive = 0) and (mp.alive = 1) and (dp.ic_R02_status = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_R02_time, utc_timestamp()) = dg.ic_R02_timeout)")

        sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'R02', 2, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
                    " WHERE (dp.ic_R02_status = 2) and (dp.ic_R02_logging = 0) and (dp.alive = 0) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_R02_time=utc_timestamp()" \
                    " WHERE (dg.ic_R02_enable = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_R02_time, utc_timestamp()) >= dg.ic_R02_timeout) and (dp.alive = 1) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_R02_logging=1" \
                    " WHERE (dg.ic_R02_enable = 1) and (dp.ic_R02_status = 2)")

        # R11
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R11_status=1, mp.ic_R11_time=utc_timestamp()" \
                    " WHERE (dg.ic_R11_enable = 1) and (mp.app_status1 = 'stop') and (mp.ic_R11_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R11_status=0, mp.ic_R11_logging=0" \
                    " WHERE (dg.ic_R11_enable = 0) or (mp.app_status1 != 'stop')")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R11_status=2" \
                    " WHERE (dg.ic_R11_enable = 1) and (mp.app_status1 = 'stop')  and (mp.alive = 1) and (mp.ic_R11_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R11_time, utc_timestamp()) = dg.ic_R11_timeout)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'R11', 2, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_R11_status = 2) and (ic_R11_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R11_time=utc_timestamp()" \
                    " WHERE (dg.ic_R11_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R11_time, utc_timestamp()) >= dg.ic_R11_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R11_logging=1" \
                    " WHERE (dg.ic_R11_enable = 1) and (mp.ic_R11_status = 2)")

        # R12
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R12_status=1, mp.ic_R12_time=utc_timestamp()" \
                    " WHERE (dg.ic_R12_enable = 1) and (mp.app_status2 = 'stop') and (mp.ic_R12_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R12_status=0, mp.ic_R12_logging=0" \
                    " WHERE (dg.ic_R12_enable = 0) or (mp.app_status2 != 'stop')")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R12_status=2" \
                    " WHERE (dg.ic_R12_enable = 1) and (mp.app_status2 = 'stop')  and (mp.alive = 1) and (mp.ic_R12_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R12_time, utc_timestamp()) = dg.ic_R12_timeout)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'R12', 2, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_R12_status = 2) and (ic_R12_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R12_time=utc_timestamp()" \
                    " WHERE (dg.ic_R12_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R12_time, utc_timestamp()) >= dg.ic_R12_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R12_logging=1" \
                    " WHERE (dg.ic_R12_enable = 1) and (mp.ic_R12_status = 2)")

        # R13
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R13_status=1, mp.ic_R13_time=utc_timestamp()" \
                    " WHERE (dg.ic_R13_enable = 1) and (mp.app_status3 = 'stop') and (mp.ic_R13_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R13_status=0, mp.ic_R13_logging=0" \
                    " WHERE (dg.ic_R13_enable = 0) or (mp.app_status3 != 'stop')")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R13_status=2" \
                    " WHERE (dg.ic_R13_enable = 1) and (mp.app_status3 = 'stop')  and (mp.alive = 1) and (mp.ic_R13_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R13_time, utc_timestamp()) = dg.ic_R13_timeout)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'R13', 2, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_R13_status = 2) and (ic_R13_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R13_time=utc_timestamp()" \
                    " WHERE (dg.ic_R13_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R13_time, utc_timestamp()) >= dg.ic_R13_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R13_logging=1" \
                    " WHERE (dg.ic_R13_enable = 1) and (mp.ic_R13_status = 2)")

        # R14
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R14_status=1, mp.ic_R14_time=utc_timestamp()" \
                    " WHERE (dg.ic_R14_enable = 1) and (mp.app_status4 = 'stop') and (mp.ic_R14_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R14_status=0, mp.ic_R14_logging=0" \
                    " WHERE (dg.ic_R14_enable = 0) or (mp.app_status4 != 'stop')")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R14_status=2" \
                    " WHERE (dg.ic_R14_enable = 1) and (mp.app_status4 = 'stop')  and (mp.alive = 1) and (mp.ic_R14_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R14_time, utc_timestamp()) = dg.ic_R14_timeout)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'R14', 2, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_R14_status = 2) and (ic_R14_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R14_time=utc_timestamp()" \
                    " WHERE (dg.ic_R14_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_R14_time, utc_timestamp()) >= dg.ic_R14_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_R14_logging=1" \
                    " WHERE (dg.ic_R14_enable = 1) and (mp.ic_R14_status = 2)")

        # O01
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O01_count=1, dp.ic_O01_status=1, dp.ic_O01_time=utc_timestamp()" \
                    " WHERE (dg.ic_O01_enable = 1) and (mp.alive = 1) and (dp.alive = 1) and (dp.val_Signal = 'NG') and (dp.ic_O01_status = 0)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O01_count = dp.ic_O01_count + 1" \
                    " WHERE (dg.ic_O01_enable = 1) and (dp.val_Signal = 'NG') and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_O01_status != 0) and (TIMESTAMPDIFF(MINUTE, dp.ic_O01_time, utc_timestamp()) <= dg.ic_O01_timeout)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O01_status=0, dp.ic_O01_count=0, dp.ic_O01_logging=0" \
                    " WHERE ((dg.ic_O01_enable = 0) or (dp.val_Signal != 'NG')) or ((dg.ic_O01_enable = 1) and (dp.val_Signal = 'NG') and (dp.ic_O01_status = 2) and (TIMESTAMPDIFF(MINUTE, dp.ic_O01_time, utc_timestamp()) = dg.ic_O01_timeout) and (dp.ic_O01_count < dg.ic_O01_limit))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O01_status=2" \
                    " WHERE (dg.ic_O01_enable = 1) and (dp.alive = 1) and (mp.alive = 1) and (dp.val_Signal = 'NG') and (dp.ic_O01_status = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_O01_time, utc_timestamp()) = dg.ic_O01_timeout) and (dp.ic_O01_count >= dg.ic_O01_limit)")

        sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'O01', 1, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id"\
                    " WHERE (dp.ic_O01_status = 2) and (dp.ic_O01_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O01_count=0, dp.ic_O01_time=utc_timestamp()" \
                    " WHERE (dg.ic_O01_enable = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_O01_time, utc_timestamp()) >= dg.ic_O01_timeout) and (mp.alive = 1) and (dp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O01_logging=1" \
                    " WHERE (dg.ic_O01_enable = 1) and (dp.ic_O01_status = 2)")

        # O02
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_O02_count=1, mp.ic_O02_status=1, mp.ic_O02_time=utc_timestamp()" \
                    " WHERE (dg.ic_O02_enable = 1) and (mp.cpu_temp >= dg.th_cpu_temp_error) and (mp.ic_O02_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_O02_count = mp.ic_O02_count + 1" \
                    " WHERE (dg.ic_O02_enable = 1) and (mp.cpu_temp >= dg.th_cpu_temp_error) and (mp.alive = 1) and (mp.ic_O02_status != 0) and (TIMESTAMPDIFF(MINUTE, mp.ic_O02_time, utc_timestamp()) <= dg.ic_O02_timeout)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_O02_status=0, mp.ic_O02_count=0, mp.ic_O02_logging=0" \
                    " WHERE ((dg.ic_O02_enable = 0) or (mp.cpu_temp < dg.th_cpu_temp_error)) or ((dg.ic_O02_enable = 1) and (mp.cpu_temp >= dg.th_cpu_temp_error) and (mp.ic_O02_status = 2) and (TIMESTAMPDIFF(MINUTE, mp.ic_O02_time, utc_timestamp()) = dg.ic_O02_timeout) and (mp.ic_O02_count < dg.ic_O02_limit))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_O02_status=2" \
                    " WHERE (dg.ic_O02_enable = 1) and (mp.cpu_temp >= dg.th_cpu_temp_error)  and (mp.alive = 1) and (mp.ic_O02_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_O02_time, utc_timestamp()) = dg.ic_O02_timeout) and (mp.ic_O02_count >= dg.ic_O02_limit)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'O02', 1, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_O02_status = 2) and (ic_O02_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_O02_count=0, mp.ic_O02_time=utc_timestamp()" \
                    " WHERE (dg.ic_O02_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_O02_time, utc_timestamp()) >= dg.ic_O02_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_O02_logging=1" \
                    " WHERE (dg.ic_O02_enable = 1) and (mp.ic_O02_status = 2)")

        # O03
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O03_count=1, dp.ic_O03_status=1, dp.ic_O03_time=utc_timestamp()" \
                    " WHERE (dg.ic_O03_enable = 1) and (mp.alive = 1) and (dp.alive = 1) and (dp.val_Temp >= dg.th_dp_temp_error) and (dp.ic_O03_status = 0)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id"\
                    " SET dp.ic_O03_count = dp.ic_O03_count + 1" \
                    " WHERE (dg.ic_O03_enable = 1) and (dp.val_Temp >= dg.th_dp_temp_error) and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_O03_status != 0) and (TIMESTAMPDIFF(MINUTE, dp.ic_O03_time, utc_timestamp()) <= dg.ic_O03_timeout)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id"\
                    " SET dp.ic_O03_status=0, dp.ic_O03_count=0, dp.ic_O03_logging=0" \
                    " WHERE ((dg.ic_O03_enable = 0) or (dp.val_Temp < dg.th_dp_temp_error)) or ((dg.ic_O03_enable = 1) and (dp.val_Temp >= dg.th_dp_temp_error) and (dp.ic_O03_status = 2) and (TIMESTAMPDIFF(MINUTE, dp.ic_O03_time, utc_timestamp()) = dg.ic_O03_timeout) and (dp.ic_O03_count < dg.ic_O03_limit))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id"\
                    " SET dp.ic_O03_status=2" \
                    " WHERE (dg.ic_O03_enable = 1) and (dp.alive = 1) and (mp.alive = 1) and (dp.val_Temp >= dg.th_dp_temp_error) and (dp.ic_O03_status = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_O03_time, utc_timestamp()) = dg.ic_O03_timeout) and (dp.ic_O03_count >= dg.ic_O03_limit)")

        sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'O03', 1, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
                    " WHERE (dp.ic_O03_status = 2) and (dp.ic_O03_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O03_count=0, dp.ic_O03_time=utc_timestamp()" \
                    " WHERE (dg.ic_O03_enable = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_O03_time, utc_timestamp()) >= dg.ic_O03_timeout) and (mp.alive = 1) and (dp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_O03_logging=1" \
                    " WHERE (dg.ic_O03_enable = 1) and (dp.ic_O03_status = 2)")

        # Y01
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y01_count=1, mp.ic_Y01_status=1, mp.ic_Y01_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y01_enable = 1) and (mp.cpu_usage >= dg.th_cpu_usage_error) and (mp.ic_Y01_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y01_count = mp.ic_Y01_count + 1" \
                    " WHERE (dg.ic_Y01_enable = 1) and (mp.cpu_usage >= dg.th_cpu_usage_error) and (mp.alive = 1) and (mp.ic_Y01_status != 0) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y01_time, utc_timestamp()) <= dg.ic_Y01_timeout)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y01_status=0, mp.ic_Y01_count=0, mp.ic_Y01_logging=0" \
                    " WHERE ((dg.ic_Y01_enable = 0) or (mp.cpu_usage < dg.th_cpu_usage_error)) or ((dg.ic_Y01_enable = 1) and (mp.cpu_usage >= dg.th_cpu_usage_error) and (mp.ic_Y01_status = 2) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y01_time, utc_timestamp()) = dg.ic_Y01_timeout) and (mp.ic_Y01_count < dg.ic_Y01_limit))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y01_status=2" \
                    " WHERE (dg.ic_Y01_enable = 1) and (mp.cpu_usage >= dg.th_cpu_usage_error)  and (mp.alive = 1) and (mp.ic_Y01_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y01_time, utc_timestamp()) = dg.ic_Y01_timeout) and (mp.ic_Y01_count >= dg.ic_Y01_limit)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'Y01', 0, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_Y01_status = 2) and (ic_Y01_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y01_count=0, mp.ic_Y01_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y01_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y01_time, utc_timestamp()) >= dg.ic_Y01_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y01_logging=1" \
                    " WHERE (dg.ic_Y01_enable = 1) and (mp.ic_Y01_status = 2)")

        # Y02
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y02_count=1, mp.ic_Y02_status=1, mp.ic_Y02_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y02_enable = 1) and (mp.mem_usage >= dg.th_mem_usage_error) and (mp.ic_Y02_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y02_count = mp.ic_Y02_count + 1" \
                    " WHERE (dg.ic_Y02_enable = 1) and (mp.mem_usage >= dg.th_mem_usage_error) and (mp.alive = 1) and (mp.ic_Y02_status != 0) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y02_time, utc_timestamp()) <= dg.ic_Y02_timeout)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y02_status=0, mp.ic_Y02_count=0, mp.ic_Y02_logging=0" \
                    " WHERE ((dg.ic_Y02_enable = 0) or (mp.mem_usage < dg.th_mem_usage_error)) or ((dg.ic_Y02_enable = 1) and (mp.mem_usage >= dg.th_mem_usage_error) and (mp.ic_Y02_status = 2) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y02_time, utc_timestamp()) = dg.ic_Y02_timeout) and (mp.ic_Y02_count < dg.ic_Y02_limit))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y02_status=2" \
                    " WHERE (dg.ic_Y02_enable = 1) and (mp.mem_usage >= dg.th_mem_usage_error)  and (mp.alive = 1) and (mp.ic_Y02_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y02_time, utc_timestamp()) = dg.ic_Y02_timeout) and (mp.ic_Y02_count >= dg.ic_Y02_limit)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'Y02', 0, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_Y02_status = 2) and (ic_Y02_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y02_count=0, mp.ic_Y02_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y02_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y02_time, utc_timestamp()) >= dg.ic_Y02_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y02_logging=1" \
                    " WHERE (dg.ic_Y02_enable = 1) and (mp.ic_Y02_status = 2)")

        # Y03
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y03_count=1, mp.ic_Y03_status=1, mp.ic_Y03_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y03_enable = 1) and (mp.hdd_usage >= dg.th_hdd_usage_error) and (mp.ic_Y03_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y03_count = mp.ic_Y03_count + 1" \
                    " WHERE (dg.ic_Y03_enable = 1) and (mp.hdd_usage >= dg.th_hdd_usage_error) and (mp.alive = 1) and (mp.ic_Y03_status != 0) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y03_time, utc_timestamp()) <= dg.ic_Y03_timeout)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y03_status=0, mp.ic_Y03_count=0, mp.ic_Y03_logging=0" \
                    " WHERE ((dg.ic_Y03_enable = 0) or (mp.hdd_usage < dg.th_hdd_usage_error)) or ((dg.ic_Y03_enable = 1) and (mp.hdd_usage >= dg.th_hdd_usage_error) and (mp.ic_Y03_status = 2) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y03_time, utc_timestamp()) = dg.ic_Y03_timeout) and (mp.ic_Y03_count < dg.ic_Y03_limit))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y03_status=2" \
                    " WHERE (dg.ic_Y03_enable = 1) and (mp.hdd_usage >= dg.th_hdd_usage_error)  and (mp.alive = 1) and (mp.ic_Y03_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y03_time, utc_timestamp()) = dg.ic_Y03_timeout) and (mp.ic_Y03_count >= dg.ic_Y03_limit)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'Y03', 0, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_Y03_status = 2) and (ic_Y03_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y03_count=0, mp.ic_Y03_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y03_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y03_time, utc_timestamp()) >= dg.ic_Y03_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y03_logging=1" \
                    " WHERE (dg.ic_Y03_enable = 1) and (mp.ic_Y03_status = 2)")

        # Y04
        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y04_count=1, mp.ic_Y04_status=1, mp.ic_Y04_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y04_enable = 1) and (mp.response_time >= dg.th_res_time_error) and (mp.ic_Y04_status = 0) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y04_count = mp.ic_Y04_count + 1" \
                    " WHERE (dg.ic_Y04_enable = 1) and (mp.response_time >= dg.th_res_time_error) and (mp.alive = 1) and (mp.ic_Y04_status != 0) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y04_time, utc_timestamp()) <= dg.ic_Y04_timeout)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y04_status=0, mp.ic_Y04_count=0, mp.ic_Y04_logging=0" \
                    " WHERE ((dg.ic_Y04_enable = 0) or (mp.response_time < dg.th_res_time_error)) or ((dg.ic_Y04_enable = 1) and (mp.response_time >= dg.th_res_time_error) and (mp.ic_Y04_status = 2) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y04_time, utc_timestamp()) = dg.ic_Y04_timeout) and (mp.ic_Y04_count < dg.ic_Y04_limit))")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y04_status=2" \
                    " WHERE (dg.ic_Y04_enable = 1) and (mp.response_time >= dg.th_res_time_error)  and (mp.alive = 1) and (mp.ic_Y04_status = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y04_time, utc_timestamp()) = dg.ic_Y04_timeout) and (mp.ic_Y04_count >= dg.ic_Y04_limit)")

        sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 0, serial_number, alias, 'Y04', 0, FK_device_groups_id, serial_number FROM mediaplayers" \
                    " WHERE (ic_Y04_status = 2) and (ic_Y04_logging = 0) and (alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y04_count=0, mp.ic_Y04_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y04_enable = 1) and (TIMESTAMPDIFF(MINUTE, mp.ic_Y04_time, utc_timestamp()) >= dg.ic_Y04_timeout) and (mp.alive = 1)")

        sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
                    " SET mp.ic_Y04_logging=1" \
                    " WHERE (dg.ic_Y04_enable = 1) and (mp.ic_Y04_status = 2)")

        # Y05
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y05_count=1, dp.ic_Y05_status=1, dp.ic_Y05_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y05_enable = 1) and (mp.alive = 1) and (dp.alive = 1) and (dp.val_EnergySaving != '00') and (dp.ic_Y05_status = 0)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y05_count = dp.ic_Y05_count + 1" \
                    " WHERE (dg.ic_Y05_enable = 1) and (dp.val_EnergySaving != '00') and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_Y05_status != 0) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y05_time, utc_timestamp()) <= dg.ic_Y05_timeout)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y05_status=0, dp.ic_Y05_count=0, dp.ic_Y05_logging=0" \
                    " WHERE ((dg.ic_Y05_enable = 0) or (dp.val_EnergySaving = '00')) or ((dg.ic_Y05_enable = 1) and (dp.val_EnergySaving != '00') and (dp.ic_Y05_status = 2) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y05_time, utc_timestamp()) = dg.ic_Y05_timeout) and (dp.ic_Y05_count < dg.ic_Y05_limit))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y05_status=2" \
                    " WHERE (dg.ic_Y05_enable = 1) and (dp.alive = 1) and (mp.alive = 1) and (dp.val_EnergySaving != '00') and (dp.ic_Y05_status = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y05_time, utc_timestamp()) = dg.ic_Y05_timeout) and (dp.ic_Y05_count >= dg.ic_Y05_limit)")

        sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'Y05', 0, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
                    " WHERE (dp.ic_Y05_status = 2) and (dp.ic_Y05_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y05_count=0, dp.ic_Y05_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y05_enable = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y05_time, utc_timestamp()) >= dg.ic_Y05_timeout) and (mp.alive = 1) and (dp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y05_logging=1" \
                    " WHERE (dg.ic_Y05_enable = 1) and (dp.ic_Y05_status = 2)")

        # Y06
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y06_count=1, dp.ic_Y06_status=1, dp.ic_Y06_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y06_enable = 1) and (mp.alive = 1) and (dp.alive = 1) and (dp.val_SleepTime != '00') and (dp.ic_Y06_status = 0)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y06_count = dp.ic_Y06_count + 1" \
                    " WHERE (dg.ic_Y06_enable = 1) and (dp.val_SleepTime != '00') and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_Y06_status != 0) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y06_time, utc_timestamp()) <= dg.ic_Y06_timeout)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y06_status=0, dp.ic_Y06_count=0, dp.ic_Y06_logging=0" \
                    " WHERE ((dg.ic_Y06_enable = 0) or (dp.val_SleepTime = '00')) or ((dg.ic_Y06_enable = 1) and (dp.val_SleepTime != '00') and (dp.ic_Y06_status = 2) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y06_time, utc_timestamp()) = dg.ic_Y06_timeout) and (dp.ic_Y06_count < dg.ic_Y06_limit))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y06_status=2" \
                    " WHERE (dg.ic_Y06_enable = 1) and (dp.alive = 1) and (mp.alive = 1) and (dp.val_SleepTime != '00') and (dp.ic_Y06_status = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y06_time, utc_timestamp()) = dg.ic_Y06_timeout) and (dp.ic_Y06_count >= dg.ic_Y06_limit)")

        sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'Y06', 0, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
                    " WHERE (dp.ic_Y06_status = 2) and (dp.ic_Y06_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y06_count=0, dp.ic_Y06_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y06_enable = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y06_time, utc_timestamp()) >= dg.ic_Y06_timeout) and (mp.alive = 1) and (dp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y06_logging=1" \
                    " WHERE (dg.ic_Y06_enable = 1) and (dp.ic_Y06_status = 2)")

        # Y07
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y07_count=1, dp.ic_Y07_status=1, dp.ic_Y07_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y07_enable = 1) and (mp.alive = 1) and (dp.alive = 1) and (dp.val_AutoOff != '00') and (dp.ic_Y07_status = 0)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y07_count = dp.ic_Y07_count + 1" \
                    " WHERE (dg.ic_Y07_enable = 1) and (dp.val_AutoOff != '00') and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_Y07_status != 0) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y07_time, utc_timestamp()) <= dg.ic_Y07_timeout)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y07_status=0, dp.ic_Y07_count=0, dp.ic_Y07_logging=0" \
                    " WHERE ((dg.ic_Y07_enable = 0) or (dp.val_AutoOff = '00')) or ((dg.ic_Y07_enable = 1) and (dp.val_AutoOff != '00') and (dp.ic_Y07_status = 2) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y07_time, utc_timestamp()) = dg.ic_Y07_timeout) and (dp.ic_Y07_count < dg.ic_Y07_limit))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y07_status=2" \
                    " WHERE (dg.ic_Y07_enable = 1) and (dp.alive = 1) and (mp.alive = 1) and (dp.val_AutoOff != '00') and (dp.ic_Y07_status = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y07_time, utc_timestamp()) = dg.ic_Y07_timeout) and (dp.ic_Y07_count >= dg.ic_Y07_limit)")

        sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'Y07', 0, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
                    " WHERE (dp.ic_Y07_status = 2) and (dp.ic_Y07_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y07_count=0, dp.ic_Y07_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y07_enable = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y07_time, utc_timestamp()) >= dg.ic_Y07_timeout) and (mp.alive = 1) and (dp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y07_logging=1" \
                    " WHERE (dg.ic_Y07_enable = 1) and (dp.ic_Y07_status = 2)")

        # Y08
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y08_count=1, dp.ic_Y08_status=1, dp.ic_Y08_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y08_enable = 1) and (mp.alive = 1) and (dp.alive = 1) and (dp.val_AutoStandby != '00') and (dp.ic_Y08_status = 0)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y08_count = dp.ic_Y08_count + 1" \
                    " WHERE (dg.ic_Y08_enable = 1) and (dp.val_AutoStandby != '00') and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_Y08_status != 0) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y08_time, utc_timestamp()) <= dg.ic_Y08_timeout)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y08_status=0, dp.ic_Y08_count=0, dp.ic_Y08_logging=0" \
                    " WHERE ((dg.ic_Y08_enable = 0) or (dp.val_AutoStandby = '00')) or ((dg.ic_Y08_enable = 1) and (dp.val_AutoStandby != '00') and (dp.ic_Y08_status = 2) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y08_time, utc_timestamp()) = dg.ic_Y08_timeout) and (dp.ic_Y08_count < dg.ic_Y08_limit))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y08_status=2" \
                    " WHERE (dg.ic_Y08_enable = 1) and (dp.alive = 1) and (mp.alive = 1) and (dp.val_AutoStandby != '00') and (dp.ic_Y08_status = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y08_time, utc_timestamp()) = dg.ic_Y08_timeout) and (dp.ic_Y08_count >= dg.ic_Y08_limit)")

        sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
                    " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'Y08', 0, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
                    " WHERE (dp.ic_Y08_status = 2) and (dp.ic_Y08_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y08_count=0, dp.ic_Y08_time=utc_timestamp()" \
                    " WHERE (dg.ic_Y08_enable = 1) and (TIMESTAMPDIFF(MINUTE, dp.ic_Y08_time, utc_timestamp()) >= dg.ic_Y08_timeout) and (mp.alive = 1) and (dp.alive = 1)")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET dp.ic_Y08_logging=1" \
                    " WHERE (dg.ic_Y08_enable = 1) and (dp.ic_Y08_status = 2)")

        # # O04
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O04_status=1, mp.ic_O04_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O04_enable = 1) and (mp.cpu_usage >= 80) and (mp.mem_usage >= 80) and (mp.ic_O04_status = 0) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O04_status=0, mp.ic_O04_logging=0" \
        #             " WHERE (dg.ic_O04_enable = 0) or (mp.cpu_usage < 80) or (mp.mem_usage < 80)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O04_status=2" \
        #             " WHERE (dg.ic_O04_enable = 1) and (mp.cpu_usage >= 80) and (mp.mem_usage >= 80) and (mp.alive = 1) and (mp.ic_O04_status = 1) and (TIMESTAMPDIFF(HOUR, mp.ic_O04_time, utc_timestamp()) = 6)")
        #
        # sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
        #             " SELECT utc_timestamp(), 0, serial_number, alias, 'O04', 1, FK_device_groups_id, serial_number FROM mediaplayers" \
        #             " WHERE (ic_O04_status = 2) and (ic_O04_logging = 0) and (alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O04_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O04_enable = 1) and (TIMESTAMPDIFF(HOUR, mp.ic_O04_time, utc_timestamp()) >= 6) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O04_logging=1" \
        #             " WHERE (dg.ic_O04_enable = 1) and (mp.ic_O04_status = 2)")
        #
        # # 005
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O05_status=1, mp.ic_O05_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O05_enable = 1) and (mp.cpu_usage >= 90) and (mp.mem_usage >= 90) and (mp.ic_O05_status = 0) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O05_status=0, mp.ic_O05_logging=0" \
        #             " WHERE (dg.ic_O05_enable = 0) or (mp.cpu_usage < 90) or (mp.mem_usage < 90)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O05_status=2" \
        #             " WHERE (dg.ic_O05_enable = 1) and (mp.cpu_usage >= 90) and (mp.mem_usage >= 90) and (mp.alive = 1) and (mp.ic_O05_status = 1) and (TIMESTAMPDIFF(HOUR, mp.ic_O05_time, utc_timestamp()) = 6)")
        #
        # sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
        #             " SELECT utc_timestamp(), 0, serial_number, alias, 'O05', 1, FK_device_groups_id, serial_number FROM mediaplayers" \
        #             " WHERE (ic_O05_status = 2) and (ic_O05_logging = 0) and (alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O05_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O05_enable = 1) and (TIMESTAMPDIFF(HOUR, mp.ic_O05_time, utc_timestamp()) >= 6) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O05_logging=1" \
        #             " WHERE (dg.ic_O05_enable = 1) and (mp.ic_O05_status = 2)")
        #
        #
        # # O06
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O06_status=1, dp.ic_O06_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O06_enable = 1) and (dp.val_Temp >= 65) and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_O06_status = 0)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O06_status=0, dp.ic_O06_logging=0" \
        #             " WHERE (dg.ic_O06_enable = 0) or (dp.val_Temp < 65)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O06_status=2" \
        #             " WHERE (dg.ic_O06_enable = 1) and (dp.val_Temp >= 65) and (dp.alive = 1) and (mp.alive = 1) and (dp.ic_O06_status = 1) and (TIMESTAMPDIFF(HOUR, dp.ic_O06_time, utc_timestamp()) = 6)")
        #
        # sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
        #             " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'O06', 1, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
        #             " WHERE (dp.ic_O06_status = 2) and (dp.ic_O06_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O06_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O06_enable = 1) and (TIMESTAMPDIFF(HOUR, dp.ic_O06_time, utc_timestamp()) >= 6) and (dp.alive = 1) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O06_logging=1" \
        #             " WHERE (dg.ic_O06_enable = 1) and (dp.ic_O06_status = 2)")
        #
        # # O07
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O07_status=1, dp.ic_O07_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O07_enable = 1) and (dp.val_Temp >= 70) and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_O07_status = 0)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O07_status=0, dp.ic_O07_logging=0" \
        #             " WHERE (dg.ic_O07_enable = 0) or (dp.val_Temp < 70)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O07_status=2" \
        #             " WHERE (dg.ic_O07_enable = 1) and (dp.val_Temp >= 70) and (dp.alive = 1) and (mp.alive = 1) and (dp.ic_O07_status = 1) and (TIMESTAMPDIFF(HOUR, dp.ic_O07_time, utc_timestamp()) = 6)")
        #
        # sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
        #             " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'O07', 1, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
        #             " WHERE (dp.ic_O07_status = 2) and (dp.ic_O07_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O07_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O07_enable = 1) and (TIMESTAMPDIFF(HOUR, dp.ic_O07_time, utc_timestamp()) >= 6) and (dp.alive = 1) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O07_logging=1" \
        #             " WHERE (dg.ic_O07_enable = 1) and (dp.ic_O07_status = 2)")
        #
        # # O08
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O08_status=1, mp.ic_O08_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O08_enable = 1) and (mp.cpu_temp is not null) and (mp.cpu_temp >= 50) and (mp.ic_O08_status = 0) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O08_status=0, mp.ic_O08_logging=0" \
        #             " WHERE (dg.ic_O08_enable = 0) or (mp.cpu_temp is null) or (mp.cpu_temp < 50)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O08_status=2" \
        #             " WHERE (dg.ic_O08_enable = 1) and (mp.cpu_temp is not null) and (mp.cpu_temp >= 50) and (mp.alive = 1) and (mp.ic_O08_status = 1) and (TIMESTAMPDIFF(HOUR, mp.ic_O08_time, utc_timestamp()) = 12)")
        #
        # sqls.append("INSERT INTO incidents(created, type, mp_serial_number, alias, error_code, level, FK_device_groups_id, serial_number)" \
        #             " SELECT utc_timestamp(), 0, serial_number, alias, 'O08', 1, FK_device_groups_id, serial_number FROM mediaplayers" \
        #             " WHERE (ic_O08_status = 2) and (ic_O08_logging = 0) and (alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O08_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O08_enable = 1) and (TIMESTAMPDIFF(HOUR, mp.ic_O08_time, utc_timestamp()) >= 12) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE mediaplayers as mp JOIN device_groups as dg ON mp.FK_device_groups_id=dg.id" \
        #             " SET mp.ic_O08_logging=1" \
        #             " WHERE (dg.ic_O08_enable = 1) and (mp.ic_O08_status = 2)")
        #
        #
        # # O09
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O09_status=1, dp.ic_O09_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O09_enable = 1) and (dp.val_Temp >= 65) and (mp.alive = 1) and (dp.alive = 1) and (dp.ic_O09_status = 0)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O09_status=0, dp.ic_O09_logging=0" \
        #             " WHERE (dg.ic_O09_enable = 0) or (dp.val_Temp < 65)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O09_status=2" \
        #             " WHERE (dg.ic_O09_enable = 1) and (dp.val_Temp >= 65) and (dp.alive = 1) and (mp.alive = 1) and (dp.ic_O09_status = 1) and (TIMESTAMPDIFF(HOUR, dp.ic_O09_time, utc_timestamp()) = 12)")
        #
        # sqls.append("INSERT INTO incidents(created, type, dp_serial_number, alias, error_code, level, mp_serial_number, FK_device_groups_id, serial_number)" \
        #             " SELECT utc_timestamp(), 1, dp.serial_number, dp.alias, 'O09', 1, dp.mp_serial_number, mp.FK_device_groups_id, dp.serial_number FROM displays as dp JOIN mediaplayers as mp ON dp.FK_mp_id=mp.id" \
        #             " WHERE (dp.ic_O09_status = 2) and (dp.ic_O09_logging = 0) and (dp.alive = 1) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O09_time=utc_timestamp()" \
        #             " WHERE (dg.ic_O09_enable = 1) and (TIMESTAMPDIFF(HOUR, dp.ic_O09_time, utc_timestamp()) >= 12) and (dp.alive = 1) and (mp.alive = 1)")
        #
        # sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
        #             " SET dp.ic_O09_logging=1" \
        #             " WHERE (dg.ic_O09_enable = 1) and (dp.ic_O09_status = 2)")

        # revolved check
        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET mp.resolved_flag=0" \
                    " WHERE (mp.resolved_flag = 1) and ((mp.ic_R01_status = 2) or (dp.ic_R02_status = 2) or (mp.ic_R11_status = 2) or (mp.ic_R12_status = 2) or (mp.ic_R13_status = 2) or (mp.ic_R14_status = 2) or" \
                    " (dp.ic_O01_status = 2) or (mp.ic_O02_status = 2) or (dp.ic_O03_status = 2) or (mp.ic_Y01_status = 2) or (mp.ic_Y02_status = 2) or (mp.ic_Y03_status = 2) or (mp.ic_Y04_status = 2) or" \
                    " (dp.ic_Y05_status = 2) or (dp.ic_Y06_status = 2) or (dp.ic_Y07_status = 2) or (dp.ic_Y08_status = 2))")

        sqls.append("UPDATE displays as dp JOIN mediaplayers as mp JOIN device_groups as dg ON dp.FK_mp_id=mp.id and mp.FK_device_groups_id=dg.id" \
                    " SET mp.resolved_date=utc_timestamp(), mp.resolved_flag=1" \
                    " WHERE (mp.resolved_flag = 0) and (mp.ic_R01_status = 0) and (dp.ic_R02_status = 0) and (mp.ic_R11_status = 0) and (mp.ic_R12_status = 0) and (mp.ic_R13_status = 0) and (mp.ic_R14_status = 0) and" \
                    " (dp.ic_O01_status = 0) and (mp.ic_O02_status = 0) and (dp.ic_O03_status = 0) and (mp.ic_Y01_status = 0) and (mp.ic_Y02_status = 0) and (mp.ic_Y03_status = 0) and (mp.ic_Y04_status = 0) and" \
                    " (dp.ic_Y05_status = 0) and (dp.ic_Y06_status = 0) and (dp.ic_Y07_status = 0) and (dp.ic_Y08_status = 0)")

        # 179 Query

        cursor = None

        try:
            cursor = self.cursor()
            i = 1
            for sql in sqls:
                # print i
                # print sql
                #self.logger.info(sql)
                cursor.execute(sql)
                i = i + 1

            #self.mail_to_managers()
            self.mail_to_managers_incident()
            self.sms_to_managers_incident()

            return []

        except Error, e:
            self.logger.error("Check sql statement failed: %s" % str(e))
            if not self.connect_to_db():
                self.logger.error('Database initialize failed.')
            return []

        finally:
            if cursor is not None:
                cursor.close()

    def mail_to_managers_incident(self):
        cursor = None

        try:
            utc_datetime_date = datetime.utcnow()
            utc_datetime_date_hour_ago = datetime.utcnow() - timedelta(hours = 1)
            utc_datetime_date.strftime("%Y-%m-%d %H:%M:%S")
            utc_datetime_date_hour_ago.strftime("%Y-%m-%d %H:%M:%S")
            utc_datetime = str(utc_datetime_date)
            utc_datetime_ago = str(utc_datetime_date_hour_ago)

            cursor = self.cursor()
            # print "Send_email_incident START"

            cursor.execute("SELECT id, email FROM device_groups WHERE (ic_send_email = 1) and ((send_email_time < DATE_ADD('%s', INTERVAL -60 minute)) or (send_email_time is null))" % utc_datetime)
            email_flag = cursor.fetchall()

            for info in email_flag:

                cursor.execute("SELECT * FROM device_groups WHERE id = '%s'" % info['id'])
                dg_data = cursor.fetchall()

                cursor.execute("SELECT * FROM incidents WHERE (created > DATE_ADD('%s', INTERVAL -60 minute)) and (FK_device_groups_id = '%s')" % (utc_datetime, info['id']))
                ic_data = cursor.fetchall()

                try:
                    if ic_data[0] is not None:
                        message = ["LG Signage Device Warning<br><br>"]
                        error_level_zero = 0
                        error_level_one = 0
                        error_level_tue = 0

                        for i_data in ic_data:

                            if dg_data[0]['ic_R01_email_enable'] == 0 and i_data['error_code'] == 'R01':
                                continue
                            elif dg_data[0]['ic_R02_email_enable'] == 0 and i_data['error_code'] == 'R02':
                                continue
                            elif dg_data[0]['ic_R11_email_enable'] == 0 and i_data['error_code'] == 'R11':
                                continue
                            elif dg_data[0]['ic_R12_email_enable'] == 0 and i_data['error_code'] == 'R12':
                                continue
                            elif dg_data[0]['ic_R13_email_enable'] == 0 and i_data['error_code'] == 'R13':
                                continue
                            elif dg_data[0]['ic_R14_email_enable'] == 0 and i_data['error_code'] == 'R14':
                                continue
                            elif dg_data[0]['ic_O01_email_enable'] == 0 and i_data['error_code'] == 'O01':
                                continue
                            elif dg_data[0]['ic_O02_email_enable'] == 0 and i_data['error_code'] == 'O02':
                                continue
                            elif dg_data[0]['ic_O03_email_enable'] == 0 and i_data['error_code'] == 'O03':
                                continue
                            elif dg_data[0]['ic_Y01_email_enable'] == 0 and i_data['error_code'] == 'Y01':
                                continue
                            elif dg_data[0]['ic_Y02_email_enable'] == 0 and i_data['error_code'] == 'Y02':
                                continue
                            elif dg_data[0]['ic_Y03_email_enable'] == 0 and i_data['error_code'] == 'Y03':
                                continue
                            elif dg_data[0]['ic_Y04_email_enable'] == 0 and i_data['error_code'] == 'Y04':
                                continue
                            elif dg_data[0]['ic_Y05_email_enable'] == 0 and i_data['error_code'] == 'Y05':
                                continue
                            elif dg_data[0]['ic_Y06_email_enable'] == 0 and i_data['error_code'] == 'Y06':
                                continue
                            elif dg_data[0]['ic_Y07_email_enable'] == 0 and i_data['error_code'] == 'Y07':
                                continue
                            elif dg_data[0]['ic_Y08_email_enable'] == 0 and i_data['error_code'] == 'Y08':
                                continue

                            if dg_data[0]['send_method'] == 1:
                                if i_data['level'] == 0:
                                    error_level_zero += 1
                                elif i_data['level'] == 1:
                                    error_level_one += 1
                                else:
                                    error_level_tue += 1
                            else:
                                if int(i_data['type']) is 0:
                                    message.append('MP-Alias : %s<br>' % i_data['alias'])
                                    message.append('MP-Serial Number : %s<br>' % i_data['serial_number'])
                                    message.append('Error code : %s<br>' % i_data['error_code'])
                                    message.append('Time of occurrence : %s<br>' % i_data['created'])
                                    message.append('Link : https://lge.adamssuite.com/#/media-players/%s<br><br>' % i_data['serial_number'])
                                else:
                                    message.append('DP-Alias : %s<br>' % i_data['alias'])
                                    message.append('DP-Serial Number : %s<br>' % i_data['serial_number'])
                                    message.append('MP-Serial Number : %s<br>' % i_data['mp_serial_number'])
                                    message.append('Error code : %s<br>' % i_data['error_code'])
                                    message.append('Time of occurrence : %s<br>' % i_data['created'])
                                    message.append('Link : https://lge.adamssuite.com/#/media-players/%s<br><br>' % i_data['mp_serial_number'])

                        if dg_data[0]['send_method'] != 1 and message == ["LG Signage Device Warning<br><br>"]:
                            continue

                        if dg_data[0]['send_method'] == 1:
                            if error_level_zero == 0 and error_level_one == 0 and error_level_tue == 0:
                                continue
                            else:
                                message.append('High    : %d<br>' % error_level_tue)
                                message.append('Medium  : %d<br>' % error_level_one)
                                message.append('Low     : %d<br>' % error_level_zero)
                                message.append('Time of occurrence : %s ~ %s<br>' % (utc_datetime_ago[:19], utc_datetime[:19]))

                        message = "\r\n".join(message)
                        self.send_message(self.from_email, info['email'], 'LG Signage Device Warning Information', message)
                        cursor.execute("UPDATE device_groups SET send_email_time = '%s' WHERE id = '%s'" % (utc_datetime, info['id']))
                        # print '----------------------------------------------------------------------------------'
                except:
                    continue

        except Error, e:
            print 'error: %s' % str(e)
            self.logger.error("Check sql statement failed: %s" % str(e))

        finally:
            if cursor is not None:
                cursor.close()

    def sms_to_managers_incident(self):
        cursor = None

        try:
            utc_datetime_date = datetime.utcnow()
            utc_datetime_date_hour_ago = datetime.utcnow() - timedelta(hours = 10)
            utc_datetime_date.strftime("%Y-%m-%d %H:%M:%S")
            utc_datetime_date_hour_ago.strftime("%Y-%m-%d %H:%M:%S")
            utc_datetime = str(utc_datetime_date)
            utc_datetime_ago = str(utc_datetime_date_hour_ago)

            cursor = self.cursor()
            #self.logger.info("Send_sms_incident START")

            cursor.execute("SELECT id, phone FROM device_groups WHERE (ic_send_phone = 1) and ((send_sms_time < DATE_ADD('%s', INTERVAL -600 minute)) or (send_sms_time is null))" % utc_datetime)
            sms_flag = cursor.fetchall()

            for info in sms_flag:

                cursor.execute("SELECT * FROM device_groups WHERE id = '%s'" % info['id'])
                dg_data = cursor.fetchall()

                cursor.execute("SELECT * FROM incidents WHERE (created > DATE_ADD('%s', INTERVAL -600 minute)) and (FK_device_groups_id = '%s')" % (utc_datetime, info['id']))
                ic_data = cursor.fetchall()

                try:
                    if ic_data[0] is not None:

                        error_level_zero = 0
                        error_level_one = 0
                        error_level_tue = 0

                        for i_data in ic_data:

                            message = ["LG Signage Device Warning"]

                            if dg_data[0]['ic_R01_sms_enable'] == 0 and i_data['error_code'] == 'R01':
                                continue
                            elif dg_data[0]['ic_R02_sms_enable'] == 0 and i_data['error_code'] == 'R02':
                                continue
                            elif dg_data[0]['ic_R11_sms_enable'] == 0 and i_data['error_code'] == 'R11':
                                continue
                            elif dg_data[0]['ic_R12_sms_enable'] == 0 and i_data['error_code'] == 'R12':
                                continue
                            elif dg_data[0]['ic_R13_sms_enable'] == 0 and i_data['error_code'] == 'R13':
                                continue
                            elif dg_data[0]['ic_R14_sms_enable'] == 0 and i_data['error_code'] == 'R14':
                                continue
                            elif dg_data[0]['ic_O01_sms_enable'] == 0 and i_data['error_code'] == 'O01':
                                continue
                            elif dg_data[0]['ic_O02_sms_enable'] == 0 and i_data['error_code'] == 'O02':
                                continue
                            elif dg_data[0]['ic_O03_sms_enable'] == 0 and i_data['error_code'] == 'O03':
                                continue
                            elif dg_data[0]['ic_Y01_sms_enable'] == 0 and i_data['error_code'] == 'Y01':
                                continue
                            elif dg_data[0]['ic_Y02_sms_enable'] == 0 and i_data['error_code'] == 'Y02':
                                continue
                            elif dg_data[0]['ic_Y03_sms_enable'] == 0 and i_data['error_code'] == 'Y03':
                                continue
                            elif dg_data[0]['ic_Y04_sms_enable'] == 0 and i_data['error_code'] == 'Y04':
                                continue
                            elif dg_data[0]['ic_Y05_sms_enable'] == 0 and i_data['error_code'] == 'Y05':
                                continue
                            elif dg_data[0]['ic_Y06_sms_enable'] == 0 and i_data['error_code'] == 'Y06':
                                continue
                            elif dg_data[0]['ic_Y07_sms_enable'] == 0 and i_data['error_code'] == 'Y07':
                                continue
                            elif dg_data[0]['ic_Y08_sms_enable'] == 0 and i_data['error_code'] == 'Y08':
                                continue

                            if dg_data[0]['send_method'] == 1:
                                if i_data['level'] == 0:
                                    error_level_zero += 1
                                elif i_data['level'] == 1:
                                    error_level_one += 1
                                else:
                                    error_level_tue += 1
                            else:
                                if int(i_data['type']) is 0:
                                    message.append('MediaPlayer(%s, %s)' % (i_data['error_code'], i_data['alias']))
                                    message.append('Time : %s' % i_data['created'])
                                else:
                                    message.append('Display(%s, %s)' % (i_data['error_code'], i_data['serial_number']))
                                    message.append('Time : %s' % i_data['created'])

                                message = "\r\n".join(message)
                                self.send_sms(info['phone'], message)
                                continue

                        if dg_data[0]['send_method'] != 1:
                            cursor.execute("UPDATE device_groups SET send_sms_time = utc_timestamp() WHERE id = '%s'" % info['id'])
                            continue

                        if dg_data[0]['send_method'] == 1:
                            if error_level_zero == 0 and error_level_one == 0 and error_level_tue == 0:
                                continue
                            else:

                                # if error_level_tue != 0:
                                message.append('High : %d' % error_level_tue)
                                # if error_level_one != 0:
                                message.append('Medium : %d' % error_level_one)
                                # if error_level_zero != 0:
                                message.append('Low : %d' % error_level_zero)

                                message.append('Time : %s ~ %s' % (utc_datetime_ago[:19], utc_datetime[:19]))

                                message = "\r\n".join(message)
                                self.send_sms(info['phone'], message)

                        cursor.execute("UPDATE device_groups SET send_sms_time = utc_timestamp() WHERE id = '%s'" % info['id'])
                        # print '----------------------------------------------------------------------------------'
                except:
                    continue

        except Error, e:
            print 'error: %s' % str(e)
            self.logger.error("Check sql statement failed: %s" % str(e))

        finally:
            if cursor is not None:
                cursor.close()



    def mail_to_managers(self):
        cursor = self.cursor()
        #self.logger.info("Send_email START")
        cursor.execute("SELECT * from `mediaplayers` WHERE group_status=2 AND alive=1")
        data = cursor.fetchall()

        for info in data:
            #print '--------------------------------------------------------------------------------------'
            #print info
            #print info['alias']
            now = datetime.utcnow()
            #print info['send_email_time']

            if info["send_email_time"] is None:
                check_send_time = 1801
            else:
                # Check Send time
                check_send_time = now - info['send_email_time']
                check_send_time = check_send_time.total_seconds()
            #print check_send_time

            if check_send_time > 1800:
                # recipient_email info
                cursor.execute("SELECT device_groups.email, device_groups.phone FROM mediaplayers inner join device_groups on mediaplayers.FK_device_groups_id = device_groups.id WHERE mediaplayers.alias=%s", info['alias'])
                recipient_info = cursor.fetchone()

                try:
                    if recipient_info['email'] is None:
                        continue
                except:
                    continue

                # Input Message
                message = ["LG Signage Device warning Information<br><br>"]
                message.append("MP-Alias : %s<br>" % info['alias'])
                message.append("Link : https://lge.adamssuite.com/#/media-players/%s<br>" % info['serial_number'])

                if info['status_cpu_usage'] == 2:
                    message.append('CPU Usage : %s%%<br>' % info['cpu_usage'])
                else:
                    pass
                if info['status_hdd_usage'] == 2:
                    message.append('HDD Usage : %s%%<br>' % info['hdd_usage'])
                else:
                    pass
                if info['status_mem_usage'] == 2:
                    message.append('Memory Usage : %s%%<br>' % info['mem_usage'])
                else:
                    pass
                if info['status_cpu_temp'] == 2:
                    message.append('CPU TEMP : %s%%<br>' % info['cpu_temp'])
                else:
                    pass
                if info['status_hdd_temp'] == 2:
                    message.append('HDD TEMP : %s%%<br>' % info['cpu_temp'])
                else:
                    pass
                if info['status_fan_speed'] == 2:
                    message.append('FAN Speed : %s RPM<br>' % info['fan_speed'])
                else:
                    pass

                message = "\r\n".join(message)
                #print message

                # login gmail
                #self.login_gmail('wonbae1989@gmail.com', '3126980a')

                # Send_message  recipient, subject, message
                self.send_message(self.from_email, recipient_info['email'], 'LG Signage Device Warning Information', message)

                # Send message phone
                #self.send_sms(recipient_info['phone'])

                # Send mail time'
                send_mail_time = datetime.utcnow()
                #print 'update time'
                cursor.execute("UPDATE mediaplayers SET send_email_time=%s WHERE group_status=2 AND alive=1 AND alias=%s", (send_mail_time, info['alias']))
                #print '=================================================================================='

            #self.logger.info("Send_email END")

    def action(self, threshold, checked_list):
        self.logger.info('Items over threshold(%s) [%d]' % (threshold, len(checked_list)))

        for user in checked_list:
            self.logger.info('  %s - %d' % (user['username'], user['value']))

        return True

    def run(self):
        self.init_signal_handler()

        if not self.connect_to_db():
            self.logger.error('Database initialize failed. Exit.')
            return False

        while self.is_running:
            checked_users = self.check()
            #if len(checked_users) > 0:
            #    self.action(10, checked_users)

            for _ in range(self.intervals):
                if self.is_running:
                    time.sleep(1)

        self.disconnect()

    def init_signal_handler(self):
        def signal_term_handler(signum, frame):
            self.logger.info("Got TERM signal. Stopping application.")
            self.is_running = False

        signal.signal(signal.SIGTERM, signal_term_handler)

    def login_gmail(self, email, password):
        self.login_email = email
        self.login_password = password
        self.server = 'smtp.gmail.com'
        self.port = 587
        session = smtplib.SMTP(self.server, self.port)
        session.ehlo()
        session.starttls()
        session.ehlo()
        # Gmail Login
        session.login(self.login_email, self.login_password)
        self.session = session

    def send_message(self, sender, recipient, subject, message):

        headers = [
            "From: " + sender,
            "Subject: " + subject,
            "To: " + recipient,
            "MIME-Version: 1.0",
            "Content-Type: text/html"]
        headers = "\r\n".join(headers)
        #print headers

        message = headers + '\r\n\r\n\r\n' + message

        # Send mail
        #self.session.sendmail(sender, [recipient], message)
        #self.session.quit()
        server = smtplib.SMTP('smtp.mandrillapp.com', 587)
        server.login('hchkim@unet.kr', 'T238oVg4BMzaqAbSEOsQaQ')
        server.sendmail(self.from_email, recipient, message)
        server.quit()

    def send_sms(self, phone, message):
        phone = re.sub('[=.^#/!?%@:&$*}()+-]', '', phone)

        sendsms = Clickatell('rainmaker', 'CAOMgbAedaVNZA', '3525182', sendmsg_defaults={'callback': cc.YES,'msg_type': cc.SMS_DEFAULT,
                                                                                 'deliv_ack': cc.YES,
                                                                                 'req_feat': cc.FEAT_ALPHA + cc.FEAT_NUMER + cc.FEAT_DELIVACK})

        resp = sendsms.sendmsg(recipients=[phone], text=message)

if __name__ == '__main__':
    file_logger = logging.getLogger("ThresholdChecker")
    file_logger.setLevel(logging.INFO)

    file_handler = handlers.RotatingFileHandler(
        "th-checker.log",
        maxBytes=(1024 * 1024 * 1),
        backupCount=3
    )

    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
    file_handler.setFormatter(formatter)
    file_logger.addHandler(file_handler)

    app = ThresholdChecker(file_logger, {
        'host': '127.0.0.1',
        'username': 'root',
        'password': '1234',
        'database': 'dstcs'
    }, 10)

    if g_platform == "Linux":
    #if False:
        from daemon import runner
        try:
            daemon_runner = runner.DaemonRunner(app)
            daemon_runner.daemon_context.files_preserve = [file_handler.stream]
            daemon_runner.do_action()

        except runner.DaemonRunnerStopFailureError:
            print("Daemon already stopped.")

        except runner.DaemonRunnerStartFailureError:
            print("Daemon start failed.")
    else:
        app.run()


