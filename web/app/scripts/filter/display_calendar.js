/**
 * Created by 박원배 on 2015-05-21.
 */

'use strict';

angular.module('signageApp')
  .filter('display_Calendar', function(){
      return function(value, format, tz){

          var getDateTimeSplitChar = function(str_fmt){
              var fmt = str_fmt;
              var split_char = "EMPTY";

              if(fmt.indexOf("T") != -1){
                  split_char = "T";
              }else{
                  split_char = "EMPTY";
              }

              return split_char;
          };

          var getDateSplitChar = function(str_date_fmt){
              var fmt = str_date_fmt;
              var split_char = "EMPTY";

              if(fmt.indexOf("-") != -1){
                  split_char = "-";
              }else if(fmt.indexOf("/") != -1){
                  split_char = "/";
              }else{
                  split_char = "EMPTY";
              }
              return split_char;
          };

          // validate ------------------------------------------------------------------------------------------------
          if(value == 'None' || value == 'undefined' || value == 'Null' ||
              value == 'none' || value == 'null' ||
              value == '' || value == undefined || value == null ||
              value == '0000-00-00'){
              return value;
          }
          // default setting -----------------------------------------------------------------------------------------
          var param = {
              date_time: value,
              fmt: format,
              fmt_define: {
                year: "YYYY",
                month: "MM",
                day: "DD",
                hour: "hh",
                min: "mm",
                sec: "ss"
              },
              tz: tz
          };

          var result = "";

          //apply time zone ------------------------------------------------------------------------------------------
          var to_date = new Date(value);
          var to_time_stamp = to_date.getTime();
          var time_zone_result = to_date;

          Date.prototype.date_dd = function() {
              var dd = this.getDate().toString();
              return (dd[1]?dd:"0"+dd[0]);
          };

          var str_fmt = param.fmt.toString();
          var date_time_split_char = getDateTimeSplitChar(str_fmt);
          var date_split_char = null;

          if(date_time_split_char == "EMPTY") {//case by Date || Time Format
              date_split_char = getDateSplitChar(str_fmt);

              //case by Date Format
              if (date_split_char != "EMPTY") {
                  var date_fmt_list = str_fmt.split(date_split_char);

                  for (var i = 0; i < date_fmt_list.length; i++) {
                      if (date_fmt_list[i].indexOf(param.fmt_define.year) != -1) {
                          result += time_zone_result.date_utc_yyyy();
                      }

                      if (date_fmt_list[i].indexOf(param.fmt_define.month) != -1) {
                          result += time_zone_result.date_utc_mm();
                      }

                      if (date_fmt_list[i].indexOf(param.fmt_define.day) != -1) {
                          result += time_zone_result.date_dd();
                      }

                      if (i != (date_fmt_list.length - 1)) {
                          result += date_split_char;
                      }
                  }
              }
          }
          return result;
      }
  });
