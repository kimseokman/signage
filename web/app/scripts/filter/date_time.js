/**
 * Created by ksm on 2014-12-10.
 */
'use strict';

angular.module('signageApp')
    .filter('date_time', function(){

        Date.prototype.date_utc_yyyy = function() {
            var yyyy = this.getUTCFullYear().toString();
            return yyyy;
        };

        Date.prototype.date_utc_mm = function() {
            var mm = (this.getUTCMonth()+parseInt(1)).toString(); // getMonth() is zero-based
            return (mm[1]?mm:"0"+mm[0]);
        };

        Date.prototype.date_utc_dd = function() {
            var dd  = this.getUTCDate().toString();
            return (dd[1]?dd:"0"+dd[0]);
        };
        Date.prototype.date_dd = function() {
            var dd  = this.getDate().toString();
            return (dd[1]?dd:"0"+dd[0]);
        };

        Date.prototype.time_utc_hh = function() {
            var hh  = this.getUTCHours().toString();
            return (hh[1]?hh:"0"+hh[0]);
        };
        Date.prototype.time_hh = function() {
            var hh  = this.getHours().toString();
            return (hh[1]?hh:"0"+hh[0]);
        };


        Date.prototype.time_utc_mm = function() {
            var mm  = this.getUTCMinutes().toString();
            return (mm[1]?mm:"0"+mm[0]);
        };

        Date.prototype.time_utc_ss = function() {
            var ss  = this.getUTCSeconds().toString();
            return (ss[1]?ss:"0"+ss[0]);
        };

        var getDateTimeSplitChar = function(str_fmt){
            var fmt = str_fmt;
            var split_char = "EMPTY";

            if(fmt.indexOf("T") != -1){
                split_char = "T";
            }else{
                split_char = "EMPTY";
            }

            return split_char;
        };

        var getDateSplitChar = function(str_date_fmt){
            var fmt = str_date_fmt;
            var split_char = "EMPTY";

            if(fmt.indexOf("-") != -1){
                split_char = "-";
            }else if(fmt.indexOf("/") != -1){
                split_char = "/";
            }else{
                split_char = "EMPTY";
            }

            return split_char;
        };

        var getTimeSplitChar = function(str_time_fmt){
            var fmt = str_time_fmt;
            var split_char = "EMPTY";

            if(fmt.indexOf(":") != -1){
                split_char = ":";
            }else{
                split_char = "EMPTY";
            }
            return split_char;
        };

        var getBrowerFlag = function(){
            var brower_flag = false;
            // IE11(7.0), IE10(6.0), IE8(4.0)
            if(navigator.appVersion.indexOf('Trident/7.0') != -1 || navigator.appVersion.indexOf('Trident/6.0') != -1 || navigator.appVersion.indexOf('Trident/4.0') != -1){
                brower_flag =  true;
            }
            else{
                brower_flag = false;
            }
            return brower_flag;
        };

        return function(param_date_time, param_fmt, param_tz){
            // validate ------------------------------------------------------------------------------------------------
            if(param_date_time == 'None' || param_date_time == 'undefined' || param_date_time == 'Null' ||
                param_date_time == 'none' || param_date_time == 'null' ||
                param_date_time == '' || param_date_time == undefined || param_date_time == null ||
                param_date_time == '0000-00-00'){
                return param_date_time;
            }

            // default setting -----------------------------------------------------------------------------------------
            var param = {
                date_time: param_date_time,
                fmt: param_fmt,
                fmt_define: {
                    year: "YYYY",
                    month: "MM",
                    day: "DD",
                    hour: "hh",
                    min: "mm",
                    sec: "ss"
                },
                tz: param_tz
            };

            var result = "";

            var to_date = new Date(param.date_time);

            //apply time zone ------------------------------------------------------------------------------------------
            var time_zone_result = to_date;
            //console.log(time_zone_result);//GMT + 0900
            if(param_tz != undefined){
                var to_time_stamp = to_date.getTime();
                var tz_apply = to_time_stamp + param.tz;
                var tz_apply_date = new Date(tz_apply);
                time_zone_result = tz_apply_date;
            }
            //console.log(time_zone_result);//GMT + 0900
            //console.log(time_zone_result.toUTCString());//UTC + TimeZoneOffset

            //apply date format ----------------------------------------------------------------------------------------
            var str_fmt = param.fmt.toString();

            var date_time_split_char = getDateTimeSplitChar(str_fmt);

            var date_split_char = null;
            var time_split_char = null;

            if(date_time_split_char == "EMPTY"){//case by Date || Time Format
                date_split_char = getDateSplitChar(str_fmt);
                time_split_char = getTimeSplitChar(str_fmt);

                //case by Date Format
                if(date_split_char != "EMPTY"){
                    var date_fmt_list = str_fmt.split(date_split_char);

                    for (var i=0; i < date_fmt_list.length; i++) {
                        if(date_fmt_list[i].indexOf(param.fmt_define.year) != -1){
                            result += time_zone_result.date_utc_yyyy();
                        }

                        if(date_fmt_list[i].indexOf(param.fmt_define.month) != -1){
                            result += time_zone_result.date_utc_mm();
                        }

                        if(getBrowerFlag() == true){
                            if(date_fmt_list[i].indexOf(param.fmt_define.day) != -1){
                                result += time_zone_result.date_dd();
                            }
                        }else{
                            if(date_fmt_list[i].indexOf(param.fmt_define.day) != -1){
                                result += time_zone_result.date_utc_dd();
                            }
                        }

                        if(i != (date_fmt_list.length-1)){
                            result += date_split_char;
                        }
                    }
                }

                //case by Time Format
                if(time_split_char != "EMPTY"){
                    var time_fmt_list = str_fmt.split(time_split_char);

                    for (var i=0; i < time_fmt_list.length; i++) {
                        if(getBrowerFlag() == true){
                            if(time_fmt_list[i].indexOf(param.fmt_define.hour) != -1){
                                result += time_zone_result.time_hh();
                            }
                        }else{
                            if(time_fmt_list[i].indexOf(param.fmt_define.hour) != -1){
                                result += time_zone_result.time_utc_hh();
                            }
                        }

                        if(time_fmt_list[i].indexOf(param.fmt_define.min) != -1){
                            result += time_zone_result.time_utc_mm();
                        }

                        if(time_fmt_list[i].indexOf(param.fmt_define.sec) != -1){
                            result += time_zone_result.time_utc_ss();
                        }

                        if(i != (time_fmt_list.length-1)){
                            result += time_split_char;
                        }
                    }
                }
            } else {//case by Date & Time Format
                var date_time_split = str_fmt.split(date_time_split_char);
                date_split_char = getDateSplitChar(date_time_split[0]);
                time_split_char = getTimeSplitChar(date_time_split[1]);
                var date_fmt_list = date_time_split[0].split(date_split_char);
                var time_fmt_list = date_time_split[1].split(time_split_char);

                for (var i=0; i < date_fmt_list.length; i++) {
                    if(date_fmt_list[i].indexOf(param.fmt_define.year) != -1){
                        result += time_zone_result.date_utc_yyyy();
                    }

                    if(date_fmt_list[i].indexOf(param.fmt_define.month) != -1){
                        result += time_zone_result.date_utc_mm();
                    }

                    if(getBrowerFlag() == true){
                        if(date_fmt_list[i].indexOf(param.fmt_define.day) != -1){
                            result += time_zone_result.date_dd();
                        }
                    }else{
                        if(date_fmt_list[i].indexOf(param.fmt_define.day) != -1){
                            result += time_zone_result.date_utc_dd();
                        }
                    }

                    if(i != (date_fmt_list.length-1)){
                        result += date_split_char;
                    }
                }

                result += " ";

                for (var i=0; i < time_fmt_list.length; i++) {
                    if(getBrowerFlag() == true){
                        if(time_fmt_list[i].indexOf(param.fmt_define.hour) != -1){
                            result += time_zone_result.time_hh();
                        }
                    }else{
                        if(time_fmt_list[i].indexOf(param.fmt_define.hour) != -1){
                            result += time_zone_result.time_utc_hh();
                        }
                    }

                    if(time_fmt_list[i].indexOf(param.fmt_define.min) != -1){
                        result += time_zone_result.time_utc_mm();
                    }

                    if(time_fmt_list[i].indexOf(param.fmt_define.sec) != -1){
                        result += time_zone_result.time_utc_ss();
                    }

                    if(i != (time_fmt_list.length-1)){
                        result += time_split_char;
                    }
                }
            }
            return result;
        };
    });
