'use strict';

angular
    .module('signageApp', [
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'ngTable',
        'xeditable',
        'ui.select2',
        'checklist-model',
        'ngCookies',
        'angularFileUpload',
        'highcharts-ng',
        'angular-loading-bar',
        'pascalprecht.translate'
    ])
    .config(function ($routeProvider, $translateProvider) {
        $routeProvider
            .when('/dash', {
                templateUrl: 'views/mediaplayerlist.html',
                controller: 'MediaPlayerListCtrl'
            })
            .when('/live/media-players/:mediaPlayerSerialNumber', {
                templateUrl: 'views/live_status.html',
                controller: 'LiveCtrl',
                reloadOnSearch: false
            })
            .when('/live/media-players/:mediaPlayerSerialNumber/displays/:displaySerialNumber', {
                templateUrl: 'views/live_status.html',
                controller: 'LiveCtrl',
                reloadOnSearch: false
            })
            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            })
            .when('/login-otp', {
                templateUrl: 'views/login_otp.html',
                controller: 'OtpCtrl'
            })
            .when('/logout', {
                templateUrl: 'views/logout.html',
                controller: 'LogoutCtrl'
            })
            .when('/reports/stats', {
                templateUrl: 'views/reports_stats.html',
                controller: 'ReportsCtrl'
            })
            .when('/reports/top10', {
                templateUrl: 'views/reports_top10.html',
                controller: 'ReportsCtrl'
            })
            .when('/reports/incident', {
                templateUrl: 'views/reports_incident.html',
                controller: 'ReportsCtrl'
            })
            .when('/reports/summary', {
              templateUrl: 'views/reports_summary.html',
              controller: 'ReportsCtrl'
            })
            .when('/admin', {
                templateUrl: 'views/admin_info.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/access/list', {
                templateUrl: 'views/admin_access_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/install/list', {
                templateUrl: 'views/admin_install_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/install/info', {
                templateUrl: 'views/admin_install_info.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/install/create', {
                templateUrl: 'views/admin_create_install.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/administrator/list', {
                templateUrl: 'views/admin_administrator_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/administrator/create', {
                templateUrl: 'views/admin_create_administrator.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/administrator/info', {
                templateUrl: 'views/admin_administrator_info.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/manager/list', {
                templateUrl: 'views/admin_manager_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/manager/create', {
                templateUrl: 'views/admin_create_manager.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/manager/info', {
                templateUrl: 'views/admin_manager_info.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/device/list', {
                templateUrl: 'views/admin_device_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/device/create', {
                templateUrl: 'views/admin_create_device.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/group/list', {
                templateUrl: 'views/admin_group_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/group/create', {
                templateUrl: 'views/admin_create_group.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/group/info', {
                templateUrl: 'views/admin_group_info.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/user-manage/list', {
                templateUrl: 'views/admin_user_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/user-manage/create', {
                templateUrl: 'views/admin_create_user.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/user-manage/info', {
                templateUrl: 'views/admin_user_info.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/user-group/list', {
                templateUrl: 'views/admin_userGroup_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/user-group/create', {
                templateUrl: 'views/admin_userGroup_create.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/user-group/info', {
                templateUrl: 'views/admin_userGroup_info.html',
                controller: 'AdminCtrl'
            })
            /* wonbae */
            .when('/admin/device_mp/list', {
                templateUrl: 'views/admin_mp_Model_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/device_dp/list', {
                templateUrl: 'views/admin_dp_Model_list.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/device_dp/info', {
                templateUrl: 'views/admin_dp_Model_info.html',
                controller: 'AdminCtrl'
            })
            .when('/admin/server/list', {
                templateUrl: 'views/admin_server_list.html',
                controller: 'AdminCtrl'
            })
            .when('/m.dash', {
                templateUrl: 'views/mobile_dash.html',
                controller: 'MediaPlayerListCtrl'
            })
            .when('/m.live_status/media-players/:mediaPlayerSerialNumber', {
                templateUrl: 'views/mobile_live_status.html',
                controller: 'LiveCtrl',
                reloadOnSearch: true
            })
            .when('/m.live_status/media-players/:mediaPlayerSerialNumber/displays/:displaySerialNumber', {
                templateUrl: 'views/mobile_live_status.html',
                controller: 'LiveCtrl',
                reloadOnSearch: true
            })
            .when('/m.reports/top10', {
                templateUrl: 'views/mobile_report_top10.html',
                controller: 'ReportsCtrl'
            })
            .when('/m.reports/incident', {
                templateUrl: 'views/mobile_incident.html',
                controller: 'ReportsCtrl'
            })
            .otherwise({
                redirectTo: '/dash'
            });

        $translateProvider.useStaticFilesLoader({
            prefix: 'l10n/',
            suffix: '.json'
        });

        var language = "en_US";

        $translateProvider.preferredLanguage(language);

    })
    .run(function ($route, Session, $rootScope, $location, AUTH_EVENTS, AuthService, editableOptions) {
        var original = $location.path;

        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }

            return original.apply($location, [path]);
        };

        $rootScope.$on('$locationChangeStart', function (event, next) {
            var accessToken = Session.get('accessToken');
            var passOTP = Session.get('passOTP');
            var current_url = $location.absUrl();

            //----------------------------------------------------------------------------------------------------------
            // valide token
            //----------------------------------------------------------------------------------------------------------
            if (!accessToken) {
                if (next.templateUrl != 'views/login.html') {
                    $location.path('/login');
                }
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }

            //----------------------------------------------------------------------------------------------------------
            // valide otp
            //----------------------------------------------------------------------------------------------------------
            if (accessToken && (passOTP == 'false')) {
                if (next.templateUrl != 'views/login_otp.html') {
                    $location.path('/login-otp');
                }
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }
            //----------------------------------------------------------------------------------------------------------
            // mobile location change
            //----------------------------------------------------------------------------------------------------------
            //if (current_url.indexOf("/m") == -1){
            //    if (next.templateUrl != 'views/m.live_status.html' || next.templateUrl != 'views/m.report.html'){
            //        $location.path('/m.dash');
            //    }
            //}
        });

        editableOptions.theme = 'bs3';
    });
