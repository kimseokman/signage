'use strict';

angular.module('signageApp')
    .controller('LoginCtrl', function ($http, $scope, $rootScope, AUTH_EVENTS, AuthService) {
        $scope.credentials = {
        };

        /* auto focus */
        angular.element('.inp_id').trigger('focus');

        $scope.login = function (credentials) {
            AuthService.login(credentials);
        };
    });

