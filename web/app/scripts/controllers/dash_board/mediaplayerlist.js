'use strict';

angular.module('signageApp')
    .controller('MediaPlayerListCtrl', function ($scope, $rootScope, $cookieStore, SharedService, $translate, AuthService, $window, $timeout, $location, $http, $route, ngTableParams, MediaPlayer, User, SICompany, Organization, Site, Shop, Location, MediaPlayerAlias, MediaPlayerSerialNumber, DisplayAlias, Top10MediaPlayer, Top10Display, Session) {
        $scope.isAuthenticated = AuthService.isAuthenticated();
        $scope.$parent.updateInformation();

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });
        // DEFAULT -----------------------------------------------------------------------------------------------------
        var params = $location.search();

        $scope.STATUS_MESSAGE = ['Detail', 'Check', 'Resolve', 'Check'];
        $scope.STATUS_IMAGE = ['icon_green.png', 'icon_yellow.png', 'icon_red.png'];
        $scope.query = "";
        $scope.MediaPlayerListHeader = [
            {name: "No", class_name: "no", sortable: false},
            {name: "Status", class_name: "status", field: 'group_status', sortable: true},
            {name: "Location", class_name: "location", field: 'location', sortable: true},
            {name: "Shop", class_name: "shop", field: 'shop', sortable: true},
            {name: "MP Alias", class_name: "mp_alias", field: 'alias', sortable: true},
            {name: "DP Alias", class_name: "dp_alias", field: 'display_aliases', sortable: false},
            {name: "Action", class_name: "act", field: 'group_status', sortable: false}
        ];

        // mobile search table header
        $scope.mobile_MediaPlayerListHeader = [
          {name: "No", class_name: "no", sortable: false},
          {name: "Status", class_name: "status", field: 'group_status', sortable: true},
          {name: "Location", class_name: "location", field: 'location', sortable: true},
          {name: "Shop", class_name: "shop", field: 'shop', sortable: true},
          {name: "MP Alias", class_name: "mp_alias", field: 'alias', sortable: true},
          {name: "Action", class_name: "act", field: 'group_status', sortable: false}
        ];

        if($rootScope.IE_reload){
            $rootScope.IE_reload = false;
            $window.location.reload();
        }

        var cpu_usage_params = {};
        cpu_usage_params['item'] = "cpu_usage";
        cpu_usage_params['order'] = "-";
        Top10MediaPlayer.get(cpu_usage_params, function (data) {
            $scope.cpuTop10 = data.objects;
        });

        var mem_usage_params = {};
        mem_usage_params['item'] = "mem_usage";
        mem_usage_params['order'] = "-";
        Top10MediaPlayer.get(mem_usage_params, function (data) {
            $scope.memoryTop10 = data.objects;
        });

        var cpu_temp_params = {};
        cpu_temp_params['item'] = "cpu_temp";
        cpu_temp_params['order'] = "-";
        Top10MediaPlayer.get(cpu_temp_params, function (data) {
            $scope.cpuTempTop10 = data.objects;
        });

        var val_Temp_params = {};
        val_Temp_params['item'] = "val_Temp";
        val_Temp_params['order'] = "-";
        Top10Display.get(val_Temp_params, function (data) {
            $scope.DPTop10 = data.objects;
        });

        $scope.top_field1 = [
            {name: 'Name'},
            {name: 'Usage'},
            {name: 'Action'}
        ];

        $scope.top_field2 = [
            {name: 'Name'},
            {name: 'Temp.'},
            {name: 'Action'}
        ];

        $scope.limit_lines = [
            {value: '25'},
            {value: '50'},
            {value: '100'}
        ];

        $scope.limit_line = $scope.limit_lines[0];  //default line number set 25
        $scope.default_line_auto_fixed = $scope.limit_line.value;   //table 생성 코드에 모두 집어넣어 자동으로 패치 //140922

        $scope.search_columns = [
            {index: 0, name: 'Status'},
            {index: 1, name: 'MediaPlayer Alias'},
            {index: 2, name: 'MediaPlayer S/N'},
            {index: 3, name: 'Account'},
            {index: 4, name: 'Location'},
            {index: 5, name: 'Shop'},
            {index: 6, name: 'Registered date'},
            {index: 7, name: 'Activation date'},
            {index: 8, name: 'Latest resolved date'}
        ];

        $scope.search_column = $scope.search_columns[0];  //default search status
        $scope.search_index = params['index'] || 0;

        $scope.setSearchIndex = function(index){
            $scope.search_index = index;
        };

        $scope.table_style_flag = true;
        $scope.onStyleChangeTable = function(){
            $scope.table_style_flag = !$scope.table_style_flag;
        };

        $scope.moveToLiveFromDash = function(mp_serial_number, dp_serial_number){
            //dash
            $location.search('member', null);
            $location.search('group_status', null);
            $location.search('resolved_date', null);
            $location.search('activation_date', null);
            //admin
            $location.search('install', null);
            $location.search('device-group', null);
            $location.search('member', null);
            $location.search('manager', null);

            if(!dp_serial_number){
                var change_uri = '/live/media-players/'+mp_serial_number;
            }else{
                var change_uri = '/live/media-players/'+mp_serial_number+'/displays/'+dp_serial_number;
            }

            $location.path(change_uri, true);
        };
        //------------------------------------------------------------------------------
        $scope.moveToLive_mobile = function(mp_serial_number, dp_serial_number){
            //dash
            $location.search('member', null);
            $location.search('group_status', null);
            $location.search('resolved_date', null);
            $location.search('activation_date', null);
            //admin
            $location.search('install', null);
            $location.search('device-group', null);
            $location.search('member', null);
            $location.search('manager', null);

            if(!dp_serial_number){
                var change_uri = '/m.live_status/media-players/'+mp_serial_number;
            }else{
                var change_uri = '/m.live_status/media-players/'+mp_serial_number+'/displays/'+dp_serial_number;
            }

            $location.path(change_uri, true);
        };
        //------------------------------------------------------------------------------
        var d = new Date();
        var threeMonthsAgo = new Date(d.setMonth(d.getMonth() - 3));
        var monthAgo = new Date(d.setMonth(d.getMonth() + 2));
        d.setMonth(d.getMonth() + 1);
        var yesterday = new Date(d.setDate(d.getDate() - 1));

        var language = $cookieStore.get("lang");
        $scope.setText = function(language, text_en, text_ko){
            if(language == "en_US"){
                return text_en;
            }else{
                if(text_ko){
                    return text_ko;
                }
            }
        };

        //-------------------- Related [ $scope.selected ] Sequence --------------------
        $scope.FILTERS = [
            {
                field: 'group_status', placeholder: $scope.setText(language, 'Status', '상태'),
                data: [
                    {id: 'all', text: $scope.setText(language, 'All', '전체')},
                    {id: 0, text: $scope.setText(language, 'Normal', '정상 장비')},
                    {id: 1, text: $scope.setText(language, 'Check required', '확인 필요')},
                    {id: 2, text: $scope.setText(language, 'Unresolved', '미해결 장비')},
                    {id: -1, text: $scope.setText(language, 'Disconnected', '연결 끊김')}
                ],
                filter_show_flag: true
            },
            {field: 'mp_alias', placeholder: $scope.setText(language, 'MediaPlayer Alias', 'MediaPlayer 별칭'), resource: MediaPlayerAlias, filter_show_flag: true},
            {field: 'mp_serial_number', placeholder: $scope.setText(language, 'MediaPlayer S/N', 'MediaPlayer 일련 번호'), resource: MediaPlayerSerialNumber, filter_show_flag: true},
            {field: 'site', placeholder: $scope.setText(language, 'Account', '계정'), resource: Site, filter_show_flag: true},
            {field: 'location', placeholder: $scope.setText(language, 'Location', '위치'), resource: Location, filter_show_flag: true},
            {field: 'shop', placeholder: $scope.setText(language, 'Shop', '지점'), resource: Shop, filter_show_flag: true},
            {
                field: 'created_date', placeholder: $scope.setText(language, 'Registered date', '등록 날짜'),
                data: [
                    {id: yesterday.getFullYear() + '-' + (yesterday.getMonth() + 1) + '-' + yesterday.getDate(), text: $scope.setText(language, 'Since yesterday', '하루 전')},
                    {id: monthAgo.getFullYear() + '-' + (monthAgo.getMonth() + 1) + '-' + monthAgo.getDate(), text: $scope.setText(language, 'In the last month', '한달 전')},
                    {id: threeMonthsAgo.getFullYear() + '-' + (threeMonthsAgo.getMonth() + 1) + '-' + threeMonthsAgo.getDate(), text: $scope.setText(language, 'In the last 3 months', '최근 3개월')}
                ],
                filter_show_flag: true
            },
            {
                field: 'activation_date', placeholder: $scope.setText(language, 'Activation date', '활성화 날짜'),
                data: [
                    {id: yesterday.getFullYear() + '-' + (yesterday.getMonth() + 1) + '-' + yesterday.getDate(), text: $scope.setText(language, 'Since yesterday', '하루 전')},
                    {id: monthAgo.getFullYear() + '-' + (monthAgo.getMonth() + 1) + '-' + monthAgo.getDate(), text: $scope.setText(language, 'In the last month', '한달 전')},
                    {id: threeMonthsAgo.getFullYear() + '-' + (threeMonthsAgo.getMonth() + 1) + '-' + threeMonthsAgo.getDate(), text: $scope.setText(language, 'In the last 3 months', '최근 3개월')}
                ],
                filter_show_flag: true
            },
            {
                field: 'resolved_date', placeholder: $scope.setText(language, 'Latest resolved date', '최근 해결 날짜'),
                data: [
                    {id: yesterday.getFullYear() + '-' + (yesterday.getMonth() + 1) + '-' + yesterday.getDate(), text: $scope.setText(language, 'Since yesterday', '하루 전')},
                    {id: monthAgo.getFullYear() + '-' + (monthAgo.getMonth() + 1) + '-' + monthAgo.getDate(), text: $scope.setText(language, 'In the last month', '한달 전')},
                    {id: threeMonthsAgo.getFullYear() + '-' + (threeMonthsAgo.getMonth() + 1) + '-' + threeMonthsAgo.getDate(), text: $scope.setText(language, 'In the last 3 months', '최근 3개월')}
                ],
                filter_show_flag: true
            },
            {
                field: 'alive', placeholder: 'Alive check',
                data: [
                    {id: 0, text: 'Disconnected'},
                    {id: 1, text: 'Alive'}
                ],
                filter_show_flag: false
            },
            {
                field: 'all', placeholder: 'All Search',
                data: [
                    {id: 0, text: 'Default Search'},
                    {id: 1, text: 'All Search'}
                ],
                filter_show_flag: false
            },
            {field: 'display_alias', placeholder: 'Display Alias', resource: DisplayAlias, filter_show_flag: false},
            {field: 'si_company', placeholder: 'SI Company', resource: SICompany, filter_show_flag: false},
            {field: 'organization', placeholder: 'Organization', resource: Organization, filter_show_flag: false}
        ];
        //-------------------- Related [ $scope.selected ] Sequence --------------------


        //-------------------- Related [ $scope.Filter ] Sequence --------------------
        if(params['group_status'] == 0){
            params['group_status'] = '0';
        }
        $scope.selected = [
                params['group_status'] || null,
                null,
                null,
                null,
                null,
                null,
                params['created'] || null,
                params['activation_date'] || null,
                params['resolved_date'] || null,
                params['alive'] || null,
                params['all'] || null,
                null,
                null,
                null
        ];
        //-------------------- Related [ $scope.Filter ] Sequence --------------------
        $scope.order = params['order'] || '-';
        $scope.item = params['item'] || 'group_status'; //default order by

        $scope.select = function (filter) {
            var options = {
                placeholder: filter.placeholder,
                allowClear: true
            };
            //console.log(options.placeholder);
            if (filter.data) {
                options['minimumResultsForSearch'] = -1;
                options['data'] = filter.data;
            }else if (filter.resource != undefined) {
                options['minimumInputLength'] = 2;
                options['ajax'] = {
                    quietMillis: 500,
                    data: function (term, page) {
                        return {
                            text: term
                        };
                    },
                    transport: function (queryParams) {
                        return filter.resource.query(queryParams.data).$promise.then(queryParams.success);
                    },
                    results: function (data, page, query) {
                        return { results: data }
                    }
                };
            }
            return options;
        };
        $scope.isSelected = false;
        $scope.$watch('search_index', function(newVal, oldVal){
            $scope.search_index = newVal;
            $scope.search_column = $scope.search_columns[$scope.search_index];  //default search status
        });

        $scope.$watch('selected', function (newVal, oldVal) {
            $scope.isSelected = true;
            $scope.isSearch = true;
            if ($scope.dashTableParams == undefined) {
                var parameters = {
                    page: 1,
                    count: $scope.default_line_auto_fixed,
                    sorting: {}
                };

                var item = $scope.item;
                var order = '';
                if($scope.order == '-'){
                    order = 'desc';
                }else{
                    order = 'asc';
                }

                parameters['sorting'][item] = order;

                $scope.dashTableParams = new ngTableParams(parameters, {
                    counts: [],
                    total: 0,
                    getData: function ($defer, params) {
                        var api_params = {};
                        api_params['enabled'] = 1;
                        api_params['limit'] = params.count();

                        if($scope.isSelected){//dashTableParams status init
                            api_params['offset'] = 0;
                            params.page(1);
                        }else{
                            api_params['offset'] = (params.page() - 1) * api_params['limit'];
                        }

                        var query = $scope.query.split('=');
                        api_params[query[0]] = query[1];

                        var sorting = params.sorting();
                        for (var key in sorting) {
                            var value = sorting[key];
                            if (value == 'desc') {
                                api_params['order'] = '-';
                            }
                            api_params['item'] = key;
                        }

                        if(params['index']) {
                            $scope.isSearch = true;
                        }
                        for (var i=-1; i <= $scope.selected.length; i++) {
                            if ($scope.selected[i]) {
                                api_params[$scope.FILTERS[i].field] = $scope.selected[i].id;
                                if($scope.selected[i].id != undefined){
                                    $scope.isSearch = true;
                                }else{
                                    $scope.isSearch = false;
                                }
                            }
                        }
                        if($scope.isSearch){
                            MediaPlayer.get(api_params, function (data) {
                                $timeout(function () {
                                    params.total(data.meta.total_count);
                                    $defer.resolve(data.objects);
                                    if(!!data.objects[0]){// 2014 10 08 liveView auto mapping by ksm
                                        $scope.media_player_serial_number = data.objects[0]['serial_number'];
                                        SharedService.prepForMediaPlayerSerialNumberBroadcast($scope.media_player_serial_number);
                                    }
                                }, 500);
                            });
                        }
                    }
                });
            } else {
                $scope.dashTableParams.reload();
            }

            $scope.isSelected = false;
        }, true);

        $scope.more_show_flag = true;

        $scope.onClickTableList = function(table_display){
            if ($scope.selectedDisplayList && $scope.selectedDisplayList != table_display) $scope.selectedDisplayList.clicked = false;
            table_display.clicked = !table_display.clicked;
            $scope.selectedDisplayList = table_display;
        };

        $scope.name_match = function(searchString, searchStringValue){
            if(searchString == searchStringValue)
                return true;
            else
                return false;
        };

        $scope.contains = function (searchString, searchTerm) {
            return searchString.indexOf(searchTerm) != -1;
        };

        $scope.show_detail_search = false;
        $scope.showDetailSearch = function(detail_search_flag){
            $scope.show_detail_search = !detail_search_flag;
        };

        $scope.tempConverter = function(to_temp_flag, temp){
            var F = 0.0;
            var C = 0.0;
            if(to_temp_flag == 'F'){//to fahrenheit ℉
                F = temp * 9.0 / 5.0 + 32;
                return F;
            }else if(to_temp_flag == 'C'){//to celsius ℃
                C = (temp - 32) * 5.0 / 9.0;
                return C;
            }
        };

        /* status filter*/
        $scope.status_type_NA = function(input_value){
            if(input_value == 0) return false;
            if(input_value == undefined || input_value == '' || input_value == 'None'){
                return true;
            }
            return false;
        };

        $scope.status_type_normal = function(input_value){
            if(input_value == 0) return true;
            if(input_value != undefined && input_value != '' && input_value != 'None'){
                return true;
            }
            return false;
        };

        $scope.status_type_zero_NA = function(input_value){
            if(input_value == undefined || input_value == '' || input_value == 'None'){
                return true;
            }
            return false;
        };

        $scope.status_type_zero = function(input_value){
            if(input_value != undefined && input_value != '' && input_value != 'None'){
                return true;
            }
            return false;
        };
    });
