/**
 * Created by ksm on 2014-10-13.
 */
'use strict';

angular.module('signageApp')
    .controller('HelpCtrl', function ($scope, $modalInstance, Modal) {
        $scope.close = function(){
            $modalInstance.dismiss('cancel');
        };
    });
