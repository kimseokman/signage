/**
 * Created by ksm on 2015-04-28.
 */
'use strict';

angular.module('signageApp')
    .controller('PredictChangeCtrl', function ($scope, $modalInstance, $filter, predict_id, FaultPredict, COM, code_num) {
        $scope.setting = COM.setting;

        $scope.edit_predict_th1_headers = [
            {name: "Code", class_name:"code"},
            {name: "Threshold", class_name:"th"},
            {name: "Th1 Limit", class_name:"th_limit"},
            {name: "Th1 Timeout", class_name:"th_timeout"},
            {name: "CO1", class_name:"co"}
        ];
        $scope.co1_value = 0;
        $scope.edit_predict_th2_headers = [
            {name: "No.", class_name:"th_num"},
            {name: "Threshold", class_name:"th"},
            {name: "Th2 Limit", class_name:"th_limit"},
            {name: "Th2 Timeout", class_name:"th_timeout"},
            {name: "CO2", class_name:"co"}
        ];
        $scope.co2_value = 0;
        $scope.edit_predict_th3_headers = [
            {name: "No.", class_name:"th_num"},
            {name: "Threshold", class_name:"th"},
            {name: "Th3 Limit", class_name:"th_limit"},
            {name: "Th3 Timeout", class_name:"th_timeout"},
            {name: "CO3", class_name:"co"}
        ];
        $scope.co3_value = 0;
        $scope.edit_predict_th4_headers = [
            {name: "No.", class_name:"th_num"},
            {name: "Threshold", class_name:"th"},
            {name: "Th4 Limit", class_name:"th_limit"},
            {name: "Th4 Timeout", class_name:"th_timeout"},
            {name: "", class_name:"co"}
        ];

        FaultPredict.get({key: predict_id}, function (data) {
            if (data.status == 200) {
                $scope.edit_predict = data.objects[0];
                angular.forEach($scope.edit_predict, function(v, k){
                    if(k == "th1_co_id"){
                        $scope.co1_value = v;
                    }

                    if(k == "th2_co_id"){
                        $scope.co2_value = v;
                    }

                    if(k == "th3_co_id"){
                        $scope.co3_value = v;
                    }
                });
            }else{
                alert("Predict Information Error");
            }
        });

        // functions ---------------------------------------------------------------------------------------------------
        $scope.showEditPredictTable = function(value){
            var show_flag = false;

            if(value != 0){
                show_flag = true;
            }

            return show_flag;
        };

        $scope.showPredictCode = function(){
            var code = code_num;
            var show_code = "P";//prediction
            if (code < 10){
                show_code += "0" + code;
            }else{
                show_code += code;
            }

            return show_code;
        };

        $scope.showPredictCoString = function(value){
            var selected = $filter('filter')($scope.setting.predict.co_string.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showPredictDevice = function(value){
            if(value == 0){
                return 'Not Set';
            }

            var selected = $filter('filter')($scope.setting.predict.th_device.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                if(selected.length == 0) {
                    return 'Not Set';
                }else{
                    return selected[0].text;
                }
            }
        };

        $scope.showPredictDevice_unit = function(value){
            var selected = $filter('filter')($scope.setting.predict.th_device.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                if(selected[0].unit)
                    return selected[0].unit;
            }
        };

        $scope.showPredictCompare = function(value){
            var selected = $filter('filter')($scope.setting.predict.th_compare.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                if(selected.length == 0){
                    return 'Not Set';
                }else{
                    return selected[0].text;
                }
            }
        };

        $scope.updatePredictByDB = function(field, data, key){
            var update_predict_arg = {};
            update_predict_arg[field] = data;
            if(field == "th1_co_id"){
                $scope.co1_value = data;
            }else if(field == "th2_co_id"){
                $scope.co2_value = data;
            }else if(field == "th3_co_id"){
                $scope.co3_value = data;
            }
            FaultPredict.update({key: key}, update_predict_arg, function(data){
                if (data.status == 200) {
                }else{
                    alert("Predict Information Modified Error");
                }
            });
        };

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
    });