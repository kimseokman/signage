/**
 * Created by ksm on 2014-11-19.
 */
'use strict';

angular.module('signageApp')
    .controller('AdminCtrl', function ($scope, $templateCache, AuthService, $route, $rootScope, $location, Modal, $timeout, ngTableParams, User,
                                       Device_Manage, Display_Manage, AuthMyTagging, AuthTagging, $http, $filter, Session, AdminList, Admin, ManagerList,
                                       Manager, MemberList, Member, CHECK_USER, CHECK_GROUP, DeviceGroup, AuthSite, AuthUserDeviceGroup,
                                       SILENT_INSTALL, OTP, Access, UserGroup, SharedService, CNN_UserGroup, $window, Servers, MediaPlayer_Models, Display_Models, COM, FaultPredict, AlarmTest){
        $scope.isAuthenticated = AuthService.isAuthenticated();
        $scope.$parent.updateInformation();

        $scope.setting = COM.setting;

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
            if ($scope.user){
                $scope.user_date_format = $scope.user.date_format;
                $scope.user_time_zone = $scope.user.time_zone;
            }else{
                $scope.user_date_format = 1;
                $scope.user_time_zone = 17;
            }
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });

        //--------------------------------------------------------------------------------------------------------------
        // User Classify
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        $scope.admin_nav_uri = "views/admin_nav.html";

        /* getQueryString */
        var queryString = $location.search();

        //--------------------------------------------------------------------------------------------------------------
        // Nav Area
        //--------------------------------------------------------------------------------------------------------------

        $scope.onActiveMenu = function(current_uri, link_tag){
            if(!current_uri) return false;

            if(current_uri.indexOf(link_tag) != -1){
                return true;
            }else{
                return false;
            }
        };

        $scope.currentURI = function () {
            var fullUrl = $location.absUrl();

            var link_tag = '';

            if (fullUrl.indexOf("admin/install") != -1){
                link_tag = "admin/install";
                return link_tag;
            }else if(fullUrl.indexOf("admin/access") != -1){
                link_tag = "admin/access";
                return link_tag;
            }else if(fullUrl.indexOf("admin/administrator") != -1){
                link_tag = "admin/administrator";
                return link_tag;
            }else if(fullUrl.indexOf("admin/device_mp") != -1){
              link_tag = "admin/device_mp";
              return link_tag;
            }else if(fullUrl.indexOf("admin/device_dp") != -1){
              link_tag = "admin/device_dp";
              return link_tag;
            }else if(fullUrl.indexOf("admin/server") != -1){
              link_tag = "admin/server";
              return link_tag;
            }else if (fullUrl.indexOf("admin/manager") != -1){
                link_tag = "admin/manager";
                return link_tag;
            }else if(fullUrl.indexOf("admin/user-group") != -1){
                link_tag = "admin/user-group";
                return link_tag;
            }else if(fullUrl.indexOf("admin/user-manage") != -1){
                link_tag = "admin/user-manage";
                return link_tag;
            }else if(fullUrl.indexOf("admin/group") != -1){
                link_tag = "admin/group";
                return link_tag;
            }else if(fullUrl.indexOf("admin/device") != -1){
                link_tag = "admin/device";
                return link_tag;
            }else if(fullUrl.indexOf("admin") != -1){
                link_tag = "my_info";
                return link_tag;
            }
        };

        $scope.super_nav_menu = [
            {
                title: 'Administrator List',
                icon:'users',
                link_tag: 'admin/administrator',
                a_href: './#/admin/administrator/list'
            },
            {
              title: 'MediaPlayer_Models',
              icon:'desktop',
              //link_tag: 'admin/administrator/device_MP_list',
              //a_href: './#/admin/administrator/device_MP_list'
              link_tag: 'admin/device_mp',
              a_href: './#/admin/device_mp/list'
            },
            {
              title: 'Display_Models',
              icon: 'desktop',
              //link_tag: 'admin/administrator/device_DP_list',
              //a_href: './#/admin/administrator/device_DP_list'
              link_tag: 'admin/device_dp',
              a_href: './#/admin/device_dp/list'
            },
            {
              title: 'ADMIN SERVER',
              icon: 'desktop',
              //link_tag: 'admin/administrator/server_list',
              //a_href: './#/admin/administrator/server_list'
              link_tag: 'admin/server',
              a_href: './#/admin/server/list'
            }
        ];

        $scope.admin_nav_menu = [
            {
                title: 'Account Manager',
                icon:'user',
                link_tag: 'admin/manager',
                a_href: './#/admin/manager/list'
            },
            {
                title: 'Device Management',
                icon: 'folder-open',
                link_tag: 'admin/device',
                a_href: './#/admin/device/list'
            },
            {
                title: 'Access History',
                icon: 'history',
                link_tag: 'admin/access',
                a_href: './#/admin/access/list'
            }
        ];

        $scope.manager_nav_menu = [
            {
                title: 'Silent Install',
                icon:'cloud-download',
                link_tag: 'admin/install',
                a_href: './#/admin/install/list'
            },
            {
                title: 'Device Management',
                icon: 'folder-open',
                link_tag: 'admin/device',
                a_href: './#/admin/device/list'
            },
            {
                title: 'Device Group',
                icon: 'folder',
                link_tag: 'admin/group',
                a_href: './#/admin/group/list'
            },
            {
                title: 'User Management',
                icon: 'user',
                link_tag: 'admin/user-manage',
                a_href: './#/admin/user-manage/list'
            },
            {
                title: 'User Group',
                icon: 'users',
                link_tag: 'admin/user-group',
                a_href: './#/admin/user-group/list'
            },
            {
                title: 'Access History',
                icon: 'history',
                link_tag: 'admin/access',
                a_href: './#/admin/access/list'
            }
        ];

        $scope.member_nav_menu = [
            {
                title: 'My Information',
                icon:'info-circle',
                link_tag: 'my_info',
                a_href: './#/admin'
            }
        ];

        /**
         * admin page nav menu event
         * - by KSM
         */

        //--------------------------------------------------------------------------------------------------------------
        // ACCESS HISTORY
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        $scope.access_history_table_headers = [
            {name: 'No.', class_name: 'no', sortable: false},
            {name: 'ID', class_name: 'id', sortable: false},
            {name: 'IP Address', class_name: 'ip_addr', sortable: false},
            {name: 'O/S', class_name: 'os_ver', sortable: false},
            {name: 'Browser', class_name: 'browser_ver', sortable: false},
            {name: 'Time', class_name: 'update_time', sortable: true, field: 'update_time'},
            {name: 'Type', class_name: 'type', sortable: true, field: 'type'}
        ];

        $scope.showLoginType = function(type){
            if(type)
                return 'LOGIN';
            return 'LOGOUT';
        };

        var access_params = $location.search();
        $scope.access_order = access_params['order'] || '-';
        $scope.access_order_by = access_params['order_by'] || 'update_time'; //default order by

        var access_parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var access_order_by = $scope.access_order_by;
        var access_str_order = '';
        if($scope.access_order == '-'){
            access_str_order = 'desc';
        }else{
            access_str_order = 'asc';
        }

        access_parameters['sorting'][access_order_by] = access_str_order;
        if($scope.accessHistoryTableParams == undefined){
            $scope.accessHistoryTableParams = new ngTableParams(access_parameters, {
                counts: [],
                total: 0,// length of data
                getData: function ($defer, params) {
                    var api_params = {};
                    api_params['limit'] = params.count();
                    api_params['offset'] = (params.page() - 1) * api_params['limit'];

                    var sorting = params.sorting();
                    for (var key in sorting) {
                        var value = sorting[key];
                        if (value == 'desc') {
                            api_params['order'] = '-';
                        }

                        api_params['order_by'] = key;
                    }
                    Access.get(api_params, function (data) {
                        $timeout(function () {
                            //console.log(data);
                            params.total(data.meta.total_count);
                            $defer.resolve(data.objects);
                        }, 500);
                    });
                }
            });
        }else{
            $scope.accessHistoryTableParams.reload();
        }


        //--------------------------------------------------------------------------------------------------------------
        // MY INFORMATION
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        $scope.show_flag = false;

        $scope.showSettingKey = function (statuses, val) {
            var selected = $filter('filter')(statuses, {value: val});
            return (val && selected.length) ? selected[0].text : 'Not set';
        };

        $scope.showAuth = function (val) {
            var selected = [];
            var val = val;

            if(val != undefined){
                angular.forEach($scope.setting.auth.statuses, function(s) {
                    if (val.indexOf(s.value) > -1) {
                        selected.push(s.text);
                    }
                });
            }

            return selected.length ? selected.join(', ') : 'Not set';
        };

        $scope.password = {
            type1:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            },
            type2:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            }
        };

        $scope.checkPassword = function(pass1, pass2){
            if(pass1){
                $scope.password.type1.txt = pass1;
            }else if(pass1==''){
                $scope.password.type1.txt = '';
            }else{
                $scope.password.type1.txt = $scope.password.type1.txt;
            }

            if(pass2){
                $scope.password.type2.txt = pass2;
            }else if(pass2==''){
                $scope.password.type2.txt = '';
            }else{
                $scope.password.type2.txt = $scope.password.type2.txt;
            }

            /* check password */
            if($scope.password.type1.txt == '' && $scope.password.type2.txt == ''){
                $scope.password.type1.status = 3;
                $scope.password.type1.check_flag = '';
                $scope.password.type1.check_icon = '';
                $scope.password.type1.check_msg = '';
                $scope.password.type2.status = 3;
                $scope.password.type2.check_flag = '';
                $scope.password.type2.check_icon = '';
                $scope.password.type2.check_msg = '';
                return 'normal';
            }else if($scope.password.type1.txt == '' && $scope.password.type2.txt != ''){
                $scope.password.type1.status = 1;
                $scope.password.type1.check_flag = 'warning';
                $scope.password.type1.check_icon = 'warning';
                $scope.password.type1.check_msg = 'Please enter a user Password.';
                return 'warning';
            }else if($scope.password.type1.txt != '' && $scope.password.type2.txt == ''){
                $scope.password.type1.status = 0;
                $scope.password.type1.check_flag = 'success';
                $scope.password.type1.check_icon = 'check-circle';
                $scope.password.type1.check_msg = 'User Password is available.';
                $scope.password.type2.status = 1;
                $scope.password.type2.check_flag = 'warning';
                $scope.password.type2.check_icon = 'warning';
                $scope.password.type2.check_msg = 'Please enter a user Retype Password.';
                return '1 depth OK';
            }else if($scope.password.type1.txt != '' && $scope.password.type2.txt != ''){
                $scope.password.type1.status = 0;
                $scope.password.type1.check_flag = 'success';
                $scope.password.type1.check_icon = 'check-circle';
                $scope.password.type1.check_msg = 'User Password is available.';
                if($scope.password.type1.txt == $scope.password.type2.txt){
                    $scope.password.type2.status = 0;
                    $scope.password.type2.check_flag = 'success';
                    $scope.password.type2.check_icon = 'check-circle';
                    $scope.password.type2.check_msg = 'User Retype Password is available.';
                    return '2 depth OK';
                }else{
                    $scope.password.type2.status = 2;
                    $scope.password.type2.check_flag = 'danger';
                    $scope.password.type2.check_icon = 'ban';
                    $scope.password.type2.check_msg = 'Please check the Retype Password.';
                }
            }
        };

        $scope.editPassword = function(){
            $scope.show_flag = true;
        };

        $scope.savePassword = function(){
            var argument = {};
            argument['userPW'] = $scope.password.type1.txt;

            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                Admin.update({}, argument);
            }else if($scope.userType == 'MANAGER'){
                Manager.update({}, argument);
            }else if($scope.userType == 'MEMBER'){
                Member.update({}, argument);
            }else{
                alert('guest');
            }

            $scope.show_flag = false;
        };

        $scope.cancelEditPassword = function(){
            $scope.password.type1.status = '';
            $scope.password.type1.status = 3;//default normal
            $scope.password.type2.status = '';
            $scope.password.type2.status = 3;//default normal
            $scope.show_flag = false;
        };

        $scope.updateUserByDB = function(field, data){
            var argument = {};
            argument[field] = data;

            if(field == 'date_format'){
                $scope.user_date_format = data;
            }else if(field == 'time_zone'){
                $scope.user_time_zone = data;
            }

            if(field == 'userOTP') {
                if (data == '0') {
                    argument[field] = false;
                } else if (data == '1') {
                    argument[field] = true;
                }
            }

            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                Admin.update({}, argument);
            }else if($scope.userType == 'MANAGER'){
                Manager.update({}, argument);
            }else if($scope.userType == 'MEMBER'){
                Member.update({}, argument);
            }else{
                alert('guest');
            }
        };

        $scope.tagging_headers = [
            {name: 'No.', class_name: 'no'},
            {name: 'Tagging', class_name: 'tag'},
            {name: 'Name', class_name: 'name'},
            {name: 'Account', class_name: 'site'},
            {name: 'Location', class_name: 'location'},
            {name: 'Shop', class_name: 'shop'}
        ];

        $scope.tag_selection = [];

        $scope.getIndexOf = function(arr, key, val){
            var l = arr.length;
            var k = 0;
            for (k = 0; k < l; k = k + 1) {
                if (arr[k][key] === val) {
                    return k;
                }
            }
            return -1;
        };

        $scope.tag_toggleSelection = function tag_toggleSelection(key, alias) {
            var tag_argument = {};
            tag_argument['key'] = key;
            tag_argument['alias'] = alias;
            tag_argument['tag'] = false;

            var idx = $scope.getIndexOf($scope.tag_selection, 'key', key);

            // is currently selected
            if (idx > -1) {
                $scope.tag_selection.splice(idx, 1);
            }
            // is newly selected
            else {
                if($scope.tag_selection.length >= 10){
                    alert("FULL");
                }else if($scope.tag_selection.length < 10){
                    $scope.tag_selection.push(tag_argument);
                }
            }
        };

        var tag_params = $location.search();
        $scope.tag_order = tag_params['order'] || '-';
        $scope.tag_order_by = tag_params['order_by'] || 'alias'; //default order by

        var tag_parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var tag_order_by = $scope.tag_order_by;
        var tag_str_order = '';
        if($scope.tag_order == '-'){
            tag_str_order = 'desc';
        }else{
            tag_str_order = 'asc';
        }

        tag_parameters['sorting'][tag_order_by] = tag_str_order;

        if($scope.tagTableParams == undefined){
            $scope.tagTableParams = new ngTableParams(tag_parameters, {
                counts: [],
                total: 0,// length of data
                getData: function ($defer, params) {
                    var api_params = {};
                    api_params['limit'] = params.count();
                    api_params['offset'] = (params.page() - 1) * api_params['limit'];
                    api_params['search_mode'] = "pagination";

                    var sorting_order = 'desc';//default order
                    var sorting_by = 'id';//default by

                    var sorting = params.sorting();
                    for (var k in sorting) {
                        var v = sorting[k];

                        sorting_order = v;
                        sorting_by = k;
                    }

                    api_params['order'] = sorting_order;
                    api_params['by'] = sorting_by;

                    var api_params_all = {};
                    api_params_all['search_mode'] = "normal";
                    AuthTagging.get(api_params_all, function(data){
                        $scope.tags = data.objects;
                        //$scope.tag_selection = [];
                        angular.forEach(data.objects, function(value, key){
                            if(value.tag && ($scope.getIndexOf($scope.tag_selection, 'key', value.key)==-1)){
                                var tag_argument = {};
                                tag_argument['key'] = value.key;
                                tag_argument['alias'] = value.alias;
                                tag_argument['tag'] = true;
                                $scope.tag_selection.push(tag_argument);
                            }
                        });
                    });

                    AuthTagging.get(api_params, function(data){
                        $timeout(function () {
                            //console.log(data);
                            params.total(data.meta.total_count);

                            $defer.resolve(data.objects);
                        }, 500);
                    });
                }
            });
        }else{
            $scope.tagTableParams.reload();
        }

        $scope.updateTagging = function(){
            var argument = {};
            $scope.update_tags = [];
            angular.forEach($scope.tag_selection, function(value, key){
                $scope.update_tags.push(value.key);
            });

            argument['check_list'] = $scope.update_tags;

            AuthTagging.update({}, argument);

            $route.reload();
        };

        $scope.otp_reset = function(type, user_key){
            var argument = {};
            argument['put_type'] = type;
            if(type != 'SELF')
                argument['key'] = user_key;
            argument['createOTP'] = false;
            OTP.update(argument, function(data){
                var alert_msg = "OTP has been reset ";

                if(data.status == 200){
                    alert_msg += "successfully.";
                }else{
                    alert_msg += "fail.";
                }

                alert(alert_msg);
            });
        };

        //--------------------------------------------------------------------------------------------------------------
        // SILENT INSTALL
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        /* Silent Install - List */
        $scope.install_info = {
            rkey: {name: 'Key', txt: '', field: 'rkey'},
            key: {name: 'Key', txt: '', field: 'key'},
            site: {name: 'Site', txt: '', field: 'site'},
            location: {name: 'Location', txt: '', field: 'location'},
            shop: {name: 'Shop', txt: '', field: 'shop'},
            mp_model: {name: 'MP Model', txt: '', field: 'mp_model'},
            mp_sn: {name: 'MP Serial', txt: '', field: 'mp_sn'},
            mp_fw_ver: {name: 'MP Firmware Ver.', txt: '', field: 'mp_fw_ver'},
            mp_alias: {name: 'MP Alias', txt: '', field: 'mp_alias'},
            mp_act_date: {name: 'MP Activate Date', txt: '', field: 'mp_activate_date'},
            mp_exp_date: {name: 'MP Expire Date', txt: '', field: 'mp_expire_date'},
            dp_alias: {name: 'DP Alias', txt: '', field: 'dp_alias'},
            dp_act_date: {name: 'DP Activate Date', txt: '', field: 'dp_activate_date'},
            dp_exp_date: {name: 'DP Expire Date', txt: '', field: 'dp_expire_date'},
            dp_method: {name: 'DP Method', txt: '', field: 'dp_method'},
            dp_conn: {name: 'DP Connect to', txt: '', field: 'dp_connect_to'},
            dp_set_id: {name: 'DP Set ID', txt: '', field: 'dp_set_id'}
        };

        $scope.install_table_headers = [
            {name: 'No.', class_name:'no'},
            {name: 'Key', class_name:'key'},
            {name: 'Account', class_name:'site'},
            {name: 'Location', class_name:'location'},
            {name: 'Shop', class_name:'shop'},
            {name: 'MP alias', class_name:'mp_alias'},
            {name: 'DP alias', class_name:'dp_alias'},
            {name: 'Delete', class_name:'delete'}
        ];

        var install_params = $location.search();
        $scope.install_order = install_params['order'] || '-';
        $scope.install_order_by = install_params['order_by'] || 'key'; //default order by

        var install_parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var install_order_by = $scope.install_order_by;
        var install_str_order = '';
        if($scope.install_order == '-'){
            install_str_order = 'desc';
        }else{
            install_str_order = 'asc';
        }

        install_parameters['sorting'][install_order_by] = install_str_order;

        if($scope.installTableParams == undefined){
            $scope.installTableParams = new ngTableParams(install_parameters, {
                counts: [],
                total: 0,// length of data
                getData: function ($defer, params) {
                    var api_params = {};
                    api_params['limit'] = params.count();
                    api_params['offset'] = (params.page() - 1) * params.count();
                    api_params['manager_key'] = queryString['manager'];

                    var sorting = params.sorting();
                    for (var key in sorting) {
                        var value = sorting[key];
                        if (value == 'desc') {
                            api_params['order'] = '-';
                        }

                        api_params['order_by'] = key;
                    }

                    SILENT_INSTALL.get(api_params, function(data){
                        $timeout(function () {
                            //console.log(data);
                            params.total(data.meta.total_count);
                            $defer.resolve(data.objects);
                        }, 500);
                    });
                }
            });
        }else{
            $scope.installTableParams.reload();
        }

        /* Silent Install - Create */
        $scope.createInstall = function(){
            var argument = {};

            argument['key'] = $scope.install_info.key.txt;
            argument['site'] = $scope.install_info.site.txt;
            argument['location'] = $scope.install_info.location.txt;
            argument['shop'] = $scope.install_info.shop.txt;
            argument['mp_model'] = $scope.install_info.mp_model.txt;
            argument['mp_sn'] = $scope.install_info.mp_sn.txt;
            argument['mp_fw_ver'] = $scope.install_info.mp_fw_ver.txt;
            argument['mp_alias'] = $scope.install_info.mp_alias.txt;
            argument['mp_activate_date'] = $scope.install_info.mp_act_date.txt;
            argument['mp_expire_date'] = $scope.install_info.mp_exp_date.txt;
            argument['dp_alias'] = $scope.install_info.dp_alias.txt;
            argument['dp_activate_date'] = $scope.install_info.dp_act_date.txt;
            argument['dp_expire_date'] = $scope.install_info.dp_exp_date.txt;
            argument['dp_method'] = $scope.install_info.dp_method.txt;
            argument['dp_connect_to'] = $scope.install_info.dp_conn.txt;
            argument['dp_set_id'] = $scope.install_info.dp_set_id.txt;

            SILENT_INSTALL.save(argument, function(data){
                if(data.status==200){
                    var change_url = '/admin/install/list';
                    $location.path(change_url, true);/* true: reload || false: reload X */
                }else if(data.status==400){
                    alert(data.notice);
                }
            });
        };

        /* Silent Install - Info */
        if(queryString['install']!=undefined){
            SILENT_INSTALL.get({install_id: queryString['install']}, function(data){
                if(data.status==200){
                    $rootScope.install_change_info = data.objects[0];
                }else{
                    alert("Information Error");
                }
            });
        };

        $scope.updateInstallData = function(field, data, id){
            var argument = {};

            argument[field] = data;

            SILENT_INSTALL.update({install_id: id}, argument);
        };

        //--------------------------------------------------------------------------------------------------------------
        // User Group
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        /* SUPER ADMIN - Info */
        if(queryString['user-group']!=undefined){
            UserGroup.get({key: queryString['user-group']}, function(data){
                if(data.status==200){
                    $rootScope.user_group_info_byManager = data.objects[0];
                }else{
                    alert("Information Error");
                }
            });
        }

        $scope.updateUserGroupByDB = function(field, data, key){
            var argument = {};

            if(field=='auth'){
                var auth_data = ''+data;
                var auth_data_list = auth_data.split(",");
                var parse_data = 0;
                angular.forEach(auth_data_list, function (s) {
                    parse_data += Number(s);
                });

                data = parse_data;
            }

            argument[field] = data;

            UserGroup.update({key: key}, argument);
        };

        $scope.user_group_table_headers = [
            {name:'Group Name', class_name: 'group'},
            {name:'Delete', class_name: 'delete'}
        ];

        UserGroup.get(function(data){
            if($scope.userType == 'MANAGER') {
                if(data.status==200)
                    $scope.user_group_info = data.objects;
            }
        });

        /* User Group - Create */
        $scope.user_group_Name = {
            name:{text: '', status: 3, check_flag: '', check_icon: '', check_msg: '', type: 'User_Group'}
        };

        $scope.checkUserGroupName = function(){
            if($scope.user_group_Name.name.text==''){
                $scope.user_group_Name.name.status = 1;
                $scope.user_group_Name.name.check_flag = 'warning';
                $scope.user_group_Name.name.check_icon = 'warning';
                $scope.user_group_Name.name.check_msg = 'user ID is required. You can not be empty.';
            }else if($scope.user_group_Name.name.text!=''){
                var argument = {};

                argument['user_id'] = $scope.user_group_Name.name.text;
                argument['type'] = $scope.user_group_Name.name.type;
                CHECK_GROUP.get(argument, function(data){
                    if(data.status==200){
                        $scope.user_group_Name.name.status = 0;
                        $scope.user_group_Name.name.check_flag = 'success';
                        $scope.user_group_Name.name.check_icon = 'check-circle';
                        $scope.user_group_Name.name.check_msg = 'User ID is available.';
                    }else if(data.status==400){
                        $scope.user_group_Name.name.status = 2;
                        $scope.user_group_Name.name.check_flag = 'danger';
                        $scope.user_group_Name.name.check_icon = 'ban';
                        $scope.user_group_Name.name.check_msg = 'The duplicate ID.';
                    }
                });
            }
        };

        $scope.user_group_auth = {
            status: [],
            statuses: [
                {value: 1, text: 'Modify Information'},    //Device Information
                {value: 2, text: 'MediaPlayer Control'},   //MediaPlayer
                {value: 4, text: 'Display Control'},        //Display
                {value: 8, text: 'View Report'}             //Report
            ]
        };

        $scope.setGroupAuth = function(){
            var selected = [];

            angular.forEach($scope.user_group_auth.statuses, function(s) {
                if ($scope.user_group_auth.status.indexOf(s.value) > -1) {
                    selected.push(s.text);
                }
            });

            return selected.length ? selected.join(', ') : 'Not set';
        };

        $scope.createUserGroup = function(){
            var argument = {};

            argument['name'] = $scope.user_group_info.name.txt;

            var auth_data = 0;
            var auth_array = ''+$scope.user_group_auth.status;
            var auth_list = auth_array.split(',');

            angular.forEach(auth_list, function(s){
                auth_data += Number(s);
            });

            argument['auth'] = auth_data;

            UserGroup.save(argument, function(data){
                if(data.status == 200){
                    var change_url = '/admin/user-group/list';
                    $location.path(change_url, true);/* true: reload || false: reload X */
                }
            });
        };

        //--------------------------------------------------------------------------------------------------------------
        // ACCOUNT SUPER
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        /* SUPER ADMIN - List */
        $scope.admin_table_headers = [
            {name: 'ID'},
            {name: 'Name'},
            {name: 'Phone'},
            {name: 'E-Mail'},
            {name: 'Delete'}
        ];

        AdminList.get(function (data) {
            $scope.admin_bySuper = data.objects;
        });

        $scope.managerSelection = [];

        $scope.toggleManagerSelection = function(key) {
            var idx = $scope.managerSelection.indexOf(key);

            // is currently selected
            if (idx > -1) {
                $scope.managerSelection.splice(idx, 1);
            }
            // is newly selected
            else {
                $scope.managerSelection.push(key);
            }
        };

        if(queryString['admin']!=undefined){
            ManagerList.get({key: queryString['admin']}, function(data){
                if(data.status==200){
                    $scope.manager_bySuper = data.objects;
                    angular.forEach($scope.manager_bySuper, function(value, key){
                        if(value.checked){
                            $scope.managerSelection.push(value.key);
                        }
                    });
                }else{
                    alert("Information Error");
                }
            });
        }

        $scope.updateManagerMapping = function(key){
            var argument = {};
            argument['key'] = key;
            argument['manager_list'] = $scope.managerSelection;

            ManagerList.update(argument, function(data){
                if(data.status == 200){
                    alert("The manager information was updated successfully.");
                    $route.reload();
                }else{
                    alert("The manager information was updated failed.");
                }
            });
        };

        /* SUPER ADMIN - Create */
        $scope.UserInfoBySuper = {
            userID : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userPW : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userRetypePW : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userName : {text: ''},
            phone : {text: ''},
            email : {text: ''},
            comp_key: {text: ''},
            date_format : {value: 1},
            time_zone : {value: 17},
            refresh_time : {text: '3000'}
        };

        $scope.checkAdminID = function(){
            if($scope.UserInfoBySuper.userID.text==''){
                $scope.UserInfoBySuper.userID.status = 1;
                $scope.UserInfoBySuper.userID.check_flag = 'warning';
                $scope.UserInfoBySuper.userID.check_icon = 'warning';
                $scope.UserInfoBySuper.userID.check_msg = 'user ID is required. You can not be empty.';
            }else if($scope.UserInfoBySuper.userID.text!=''){
                CHECK_USER.get({user_id: $scope.UserInfoBySuper.userID.text}, function(data){
                    if(data.status==200){
                        $scope.UserInfoBySuper.userID.status = 0;
                        $scope.UserInfoBySuper.userID.check_flag = 'success';
                        $scope.UserInfoBySuper.userID.check_icon = 'check-circle';
                        $scope.UserInfoBySuper.userID.check_msg = 'User ID is available.';
                    }else if(data.status==400){
                        $scope.UserInfoBySuper.userID.status = 2;
                        $scope.UserInfoBySuper.userID.check_flag = 'danger';
                        $scope.UserInfoBySuper.userID.check_icon = 'ban';
                        $scope.UserInfoBySuper.userID.check_msg = 'The duplicate ID.';
                    }
                });
            }
        };

        $scope.checkAdminPW = function(pass1, pass2){
            if(pass1){
                $scope.UserInfoBySuper.userPW.txt = pass1;
            }else if(pass1==''){
                $scope.UserInfoBySuper.userPW.txt = '';
            }else{
                $scope.UserInfoBySuper.userPW.txt = $scope.UserInfoBySuper.userPW.txt;
            }

            if(pass2){
                $scope.UserInfoBySuper.userRetypePW.txt = pass2;
            }else if(pass2==''){
                $scope.UserInfoBySuper.userRetypePW.txt = '';
            }else{
                $scope.UserInfoBySuper.userRetypePW.txt = $scope.UserInfoBySuper.userRetypePW.txt;
            }

            /* check password */
            if($scope.UserInfoBySuper.userPW.txt == '' && $scope.UserInfoBySuper.userRetypePW.txt == ''){
                $scope.UserInfoBySuper.userPW.status = 3;
                $scope.UserInfoBySuper.userPW.check_flag = '';
                $scope.UserInfoBySuper.userPW.check_icon = '';
                $scope.UserInfoBySuper.userPW.check_msg = '';
                $scope.UserInfoBySuper.userRetypePW.status = 3;
                $scope.UserInfoBySuper.userRetypePW.check_flag = '';
                $scope.UserInfoBySuper.userRetypePW.check_icon = '';
                $scope.UserInfoBySuper.userRetypePW.check_msg = '';
                return 'normal';
            }else if($scope.UserInfoBySuper.userPW.txt == '' && $scope.UserInfoBySuper.userRetypePW.txt != ''){
                $scope.UserInfoBySuper.userPW.status = 1;
                $scope.UserInfoBySuper.userPW.check_flag = 'warning';
                $scope.UserInfoBySuper.userPW.check_icon = 'warning';
                $scope.UserInfoBySuper.userPW.check_msg = 'Please enter a user Password.';
                return 'warning';
            }else if($scope.UserInfoBySuper.userPW.txt != '' && $scope.UserInfoBySuper.userRetypePW.txt == ''){
                $scope.UserInfoBySuper.userPW.status = 0;
                $scope.UserInfoBySuper.userPW.check_flag = 'success';
                $scope.UserInfoBySuper.userPW.check_icon = 'check-circle';
                $scope.UserInfoBySuper.userPW.check_msg = 'User Password is available.';
                $scope.UserInfoBySuper.userRetypePW.status = 1;
                $scope.UserInfoBySuper.userRetypePW.check_flag = 'warning';
                $scope.UserInfoBySuper.userRetypePW.check_icon = 'warning';
                $scope.UserInfoBySuper.userRetypePW.check_msg = 'Please enter a user Retype Password.';
                return '1 depth OK';
            }else if($scope.UserInfoBySuper.userPW.txt != '' && $scope.UserInfoBySuper.userRetypePW.txt != ''){
                $scope.UserInfoBySuper.userPW.status = 0;
                $scope.UserInfoBySuper.userPW.check_flag = 'success';
                $scope.UserInfoBySuper.userPW.check_icon = 'check-circle';
                $scope.UserInfoBySuper.userPW.check_msg = 'User Password is available.';
                if($scope.UserInfoBySuper.userPW.txt == $scope.UserInfoBySuper.userRetypePW.txt){
                    $scope.UserInfoBySuper.userRetypePW.status = 0;
                    $scope.UserInfoBySuper.userRetypePW.check_flag = 'success';
                    $scope.UserInfoBySuper.userRetypePW.check_icon = 'check-circle';
                    $scope.UserInfoBySuper.userRetypePW.check_msg = 'User Retype Password is available.';
                    return '2 depth OK';
                }else{
                    $scope.UserInfoBySuper.userRetypePW.status = 2;
                    $scope.UserInfoBySuper.userRetypePW.check_flag = 'danger';
                    $scope.UserInfoBySuper.userRetypePW.check_icon = 'ban';
                    $scope.UserInfoBySuper.userRetypePW.check_msg = 'Please check the Retype Password.';
                }
            }
        };

        $scope.disabledCreateAdmin = function(){
            if($scope.UserInfoBySuper.userID.status == 0 && $scope.UserInfoBySuper.userPW.status == 0 && $scope.UserInfoBySuper.userRetypePW.status == 0){
                return false;
            }else{
                return true;
            }
        };

        $scope.createAdmin = function(){
            if($scope.userType == 'SUPERADMIN'){
                var argument = {};
                argument['userID'] = $scope.UserInfoBySuper.userID.text;
                argument['userPW'] = $scope.UserInfoBySuper.userPW.text;
                argument['userName'] = $scope.UserInfoBySuper.userName.text;
                argument['phone'] = $scope.UserInfoBySuper.phone.text;
                argument['email'] = $scope.UserInfoBySuper.email.text;
                argument['date_format'] = $scope.UserInfoBySuper.date_format.value;
                argument['time_zone'] = $scope.UserInfoBySuper.time_zone.value;
                argument['refresh_time'] = $scope.UserInfoBySuper.refresh_time.text;
                argument['comp_key'] = $scope.UserInfoBySuper.comp_key.text;

                Admin.save(argument, function(data){
                    if(data.status==200){
                        var change_url = '/admin/administrator/list';
                        $location.path(change_url, true);/* true: reload || false: reload X */
                    }
                });
            }else{
                alert("ERROR");
            }
        };

        /* SUPER ADMIN - Info */
        if(queryString['admin']!=undefined){
            Admin.get({key: queryString['admin']}, function(data){
                if(data.status==200){
                    $rootScope.admin_info_bySuper = data.objects[0];
                }else{
                    alert("Information Error");
                }
            });
        }

        $scope.show_flag_toAdmin = false;
        $scope.password_toAdmin = {
            type1:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            },
            type2:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            }
        };

        $scope.checkPassword_toAdmin = function(pass1, pass2){
            if(pass1){
                $scope.password_toAdmin.type1.txt = pass1;
            }else if(pass1==''){
                $scope.password_toAdmin.type1.txt = '';
            }else{
                $scope.password_toAdmin.type1.txt = $scope.password_toAdmin.type1.txt;
            }

            if(pass2){
                $scope.password_toAdmin.type2.txt = pass2;
            }else if(pass2==''){
                $scope.password_toAdmin.type2.txt = '';
            }else{
                $scope.password_toAdmin.type2.txt = $scope.password_toAdmin.type2.txt;
            }

            /* check password */
            if($scope.password_toAdmin.type1.txt == '' && $scope.password_toAdmin.type2.txt == ''){
                $scope.password_toAdmin.type1.status = 3;
                $scope.password_toAdmin.type1.check_flag = '';
                $scope.password_toAdmin.type1.check_icon = '';
                $scope.password_toAdmin.type1.check_msg = '';
                $scope.password_toAdmin.type2.status = 3;
                $scope.password_toAdmin.type2.check_flag = '';
                $scope.password_toAdmin.type2.check_icon = '';
                $scope.password_toAdmin.type2.check_msg = '';
                return 'normal';
            }else if($scope.password_toAdmin.type1.txt == '' && $scope.password_toAdmin.type2.txt != ''){
                $scope.password_toAdmin.type1.status = 1;
                $scope.password_toAdmin.type1.check_flag = 'warning';
                $scope.password_toAdmin.type1.check_icon = 'warning';
                $scope.password_toAdmin.type1.check_msg = 'Please enter a user Password.';
                return 'warning';
            }else if($scope.password_toAdmin.type1.txt != '' && $scope.password_toAdmin.type2.txt == ''){
                $scope.password_toAdmin.type1.status = 0;
                $scope.password_toAdmin.type1.check_flag = 'success';
                $scope.password_toAdmin.type1.check_icon = 'check-circle';
                $scope.password_toAdmin.type1.check_msg = 'User Password is available.';
                $scope.password_toAdmin.type2.status = 1;
                $scope.password_toAdmin.type2.check_flag = 'warning';
                $scope.password_toAdmin.type2.check_icon = 'warning';
                $scope.password_toAdmin.type2.check_msg = 'Please enter a user Retype Password.';
                return '1 depth OK';
            }else if($scope.password_toAdmin.type1.txt != '' && $scope.password_toAdmin.type2.txt != ''){
                $scope.password_toAdmin.type1.status = 0;
                $scope.password_toAdmin.type1.check_flag = 'success';
                $scope.password_toAdmin.type1.check_icon = 'check-circle';
                $scope.password_toAdmin.type1.check_msg = 'User Password is available.';
                if($scope.password_toAdmin.type1.txt == $scope.password_toAdmin.type2.txt){
                    $scope.password_toAdmin.type2.status = 0;
                    $scope.password_toAdmin.type2.check_flag = 'success';
                    $scope.password_toAdmin.type2.check_icon = 'check-circle';
                    $scope.password_toAdmin.type2.check_msg = 'User Retype Password is available.';
                    return '2 depth OK';
                }else{
                    $scope.password_toAdmin.type2.status = 2;
                    $scope.password_toAdmin.type2.check_flag = 'danger';
                    $scope.password_toAdmin.type2.check_icon = 'ban';
                    $scope.password_toAdmin.type2.check_msg = 'Please check the Retype Password.';
                }
            }
        };

        $scope.editPassword_toAdmin = function(){
            $scope.show_flag_toAdmin = true;
        };

        $scope.savePassword_toAdmin = function(){
            var argument = {};
            argument['userPW'] = $scope.password_toAdmin.type1.txt;

            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                Admin.update({}, argument);
            }else{
                alert('guest');
            }

            $scope.show_flag_toAdmin = false;
        };

        $scope.cancelEditPassword_toAdmin = function(){
            $scope.password_toAdmin.type1.status = '';
            $scope.password_toAdmin.type1.status = 3;//default normal
            $scope.password_toAdmin.type2.status = '';
            $scope.password_toAdmin.type2.status = 3;//default normal
            $scope.show_flag_toAdmin = false;
        };

        //--------------------------------------------------------------------------------------------------------------
        // ACCOUNT MANAGER
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        /* ACCOUNT MANAGER - List */
        $scope.search_items = [
            {item: 'User ID', field: 'userID'},
            {item: 'User Name', field: 'userName'}
        ];

        $scope.search = $scope.search_items[0];
        $scope.search_field = $scope.search_items[0].field;

        var params={};
        params['item'] = "";
        params['data'] = "";

        $scope.setSearchItem = function(search_item, search_field){
            $scope.search_field = search_field;
            params['item'] = search_item;
            params['data'] = search_field;
        };

        ManagerList.get(params, function (data) {
            $scope.manager_byAdmin = data.objects;
        });

        $scope.manager_table_headers = [
            {name: 'ID'},
            {name: 'Name'},
            {name: 'Phone'},
            {name: 'E-Mail'},
            {name: 'Delete'}
        ];

        /* ACCOUNT MANAGER - Create */
        $scope.UserInfoByAdmin = {
            userID : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userPW : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userRetypePW : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userName : {text: ''},
            phone : {text: ''},
            email : {text: ''},
            comp_key: {text: ''},
            date_format : {value: 1},
            time_zone : {value: 17},
            refresh_time : {text: '3000'}
        };

        $scope.checkManagerID = function(){
            if($scope.UserInfoByAdmin.userID.text==''){
                $scope.UserInfoByAdmin.userID.status = 1;
                $scope.UserInfoByAdmin.userID.check_flag = 'warning';
                $scope.UserInfoByAdmin.userID.check_icon = 'warning';
                $scope.UserInfoByAdmin.userID.check_msg = 'user ID is required. You can not be empty.';
            }else if($scope.UserInfoByAdmin.userID.text!=''){
                CHECK_USER.get({user_id: $scope.UserInfoByAdmin.userID.text}, function(data){
                    if(data.status==200){
                        $scope.UserInfoByAdmin.userID.status = 0;
                        $scope.UserInfoByAdmin.userID.check_flag = 'success';
                        $scope.UserInfoByAdmin.userID.check_icon = 'check-circle';
                        $scope.UserInfoByAdmin.userID.check_msg = 'User ID is available.';
                    }else if(data.status==400){
                        $scope.UserInfoByAdmin.userID.status = 2;
                        $scope.UserInfoByAdmin.userID.check_flag = 'danger';
                        $scope.UserInfoByAdmin.userID.check_icon = 'ban';
                        $scope.UserInfoByAdmin.userID.check_msg = 'The duplicate ID.';
                    }
                });
            }
        };

        $scope.checkManagerPW = function(pass1, pass2){
            if(pass1){
                $scope.UserInfoByAdmin.userPW.txt = pass1;
            }else if(pass1==''){
                $scope.UserInfoByAdmin.userPW.txt = '';
            }else{
                $scope.UserInfoByAdmin.userPW.txt = $scope.UserInfoByAdmin.userPW.txt;
            }

            if(pass2){
                $scope.UserInfoByAdmin.userRetypePW.txt = pass2;
            }else if(pass2==''){
                $scope.UserInfoByAdmin.userRetypePW.txt = '';
            }else{
                $scope.UserInfoByAdmin.userRetypePW.txt = $scope.UserInfoByAdmin.userRetypePW.txt;
            }

            /* check password */
            if($scope.UserInfoByAdmin.userPW.txt == '' && $scope.UserInfoByAdmin.userRetypePW.txt == ''){
                $scope.UserInfoByAdmin.userPW.status = 3;
                $scope.UserInfoByAdmin.userPW.check_flag = '';
                $scope.UserInfoByAdmin.userPW.check_icon = '';
                $scope.UserInfoByAdmin.userPW.check_msg = '';
                $scope.UserInfoByAdmin.userRetypePW.status = 3;
                $scope.UserInfoByAdmin.userRetypePW.check_flag = '';
                $scope.UserInfoByAdmin.userRetypePW.check_icon = '';
                $scope.UserInfoByAdmin.userRetypePW.check_msg = '';
                return 'normal';
            }else if($scope.UserInfoByAdmin.userPW.txt == '' && $scope.UserInfoByAdmin.userRetypePW.txt != ''){
                $scope.UserInfoByAdmin.userPW.status = 1;
                $scope.UserInfoByAdmin.userPW.check_flag = 'warning';
                $scope.UserInfoByAdmin.userPW.check_icon = 'warning';
                $scope.UserInfoByAdmin.userPW.check_msg = 'Please enter a user Password.';
                return 'warning';
            }else if($scope.UserInfoByAdmin.userPW.txt != '' && $scope.UserInfoByAdmin.userRetypePW.txt == ''){
                $scope.UserInfoByAdmin.userPW.status = 0;
                $scope.UserInfoByAdmin.userPW.check_flag = 'success';
                $scope.UserInfoByAdmin.userPW.check_icon = 'check-circle';
                $scope.UserInfoByAdmin.userPW.check_msg = 'User Password is available.';
                $scope.UserInfoByAdmin.userRetypePW.status = 1;
                $scope.UserInfoByAdmin.userRetypePW.check_flag = 'warning';
                $scope.UserInfoByAdmin.userRetypePW.check_icon = 'warning';
                $scope.UserInfoByAdmin.userRetypePW.check_msg = 'Please enter a user Retype Password.';
                return '1 depth OK';
            }else if($scope.UserInfoByAdmin.userPW.txt != '' && $scope.UserInfoByAdmin.userRetypePW.txt != ''){
                $scope.UserInfoByAdmin.userPW.status = 0;
                $scope.UserInfoByAdmin.userPW.check_flag = 'success';
                $scope.UserInfoByAdmin.userPW.check_icon = 'check-circle';
                $scope.UserInfoByAdmin.userPW.check_msg = 'User Password is available.';
                if($scope.UserInfoByAdmin.userPW.txt == $scope.UserInfoByAdmin.userRetypePW.txt){
                    $scope.UserInfoByAdmin.userRetypePW.status = 0;
                    $scope.UserInfoByAdmin.userRetypePW.check_flag = 'success';
                    $scope.UserInfoByAdmin.userRetypePW.check_icon = 'check-circle';
                    $scope.UserInfoByAdmin.userRetypePW.check_msg = 'User Retype Password is available.';
                    return '2 depth OK';
                }else{
                    $scope.UserInfoByAdmin.userRetypePW.status = 2;
                    $scope.UserInfoByAdmin.userRetypePW.check_flag = 'danger';
                    $scope.UserInfoByAdmin.userRetypePW.check_icon = 'ban';
                    $scope.UserInfoByAdmin.userRetypePW.check_msg = 'Please check the Retype Password.';
                }
            }
        };

        $scope.disabledCreateManager = function(){
            if($scope.UserInfoByAdmin.userID.status == 0 && $scope.UserInfoByAdmin.userPW.status == 0 && $scope.UserInfoByAdmin.userRetypePW.status == 0){
                return false;
            }else{
                return true;
            }
        };

        $scope.createManager = function(){
            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                var argument = {};
                argument['userID'] = $scope.UserInfoByAdmin.userID.text;
                argument['userPW'] = $scope.UserInfoByAdmin.userPW.text;
                argument['userName'] = $scope.UserInfoByAdmin.userName.text;
                argument['phone'] = $scope.UserInfoByAdmin.phone.text;
                argument['email'] = $scope.UserInfoByAdmin.email.text;
                argument['date_format'] = $scope.UserInfoByAdmin.date_format.value;
                argument['time_zone'] = $scope.UserInfoByAdmin.time_zone.value;
                argument['refresh_time'] = $scope.UserInfoByAdmin.refresh_time.text;
                argument['comp_key'] = $scope.UserInfoByAdmin.comp_key.text;

                Admin.save(argument, function(data){
                    if(data.status==200){
                        var change_url = '/admin/manager/list';
                        $location.path(change_url, true);/* true: reload || false: reload X */
                    }
                });
            }else{
                alert("ERROR");
            }
        };

        /* ACCOUNT MANAGER - Info */
        if(queryString['manager']!=undefined){
            Manager.get({key: queryString['manager']}, function(data){
                if(data.status==200){
                    $rootScope.manager_info_byAdmin = data.objects[0];
                }else{
                    alert("Information Error");
                }
            });
        };

        AuthSite.get(function(data){
            if(data.status==200){
                $scope.site_list = data.objects;
            }else if(data.status==404){
                $scope.site_list = [];
                $scope.site_list.push({'name': 'Empty'});
            }else{
                alert("Site Get Error [ " + data.status + " ]");
            }
        });

        $scope.select_site = '';
        $scope.onClickSite = function(menu){
            if ($scope.selectedSite && $scope.selectedSite != menu) $scope.selectedSite.clicked = false;
            menu.clicked = !menu.clicked;
            $scope.selectedSite = menu;

            $scope.select_site = $scope.selectedSite;
        };

        $scope.select_mysite = '';
        $scope.onClickMySite = function(menu){
            if ($scope.selectedMySite && $scope.selectedMySite != menu) $scope.selectedMySite.clicked = false;
            menu.clicked = !menu.clicked;
            $scope.selectedMySite = menu;

            $scope.select_mysite = $scope.selectedMySite;
        };

        $scope.show_flag_toManager = false;
        $scope.password_toManager = {
            type1:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            },
            type2:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            }
        };

        $scope.checkPassword_toManager = function(pass1, pass2){
            if(pass1){
                $scope.password_toManager.type1.txt = pass1;
            }else if(pass1==''){
                $scope.password_toManager.type1.txt = '';
            }else{
                $scope.password_toManager.type1.txt = $scope.password_toManager.type1.txt;
            }

            if(pass2){
                $scope.password_toManager.type2.txt = pass2;
            }else if(pass2==''){
                $scope.password_toManager.type2.txt = '';
            }else{
                $scope.password_toManager.type2.txt = $scope.password_toManager.type2.txt;
            }

            /* check password */
            if($scope.password_toManager.type1.txt == '' && $scope.password_toManager.type2.txt == ''){
                $scope.password_toManager.type1.status = 3;
                $scope.password_toManager.type1.check_flag = '';
                $scope.password_toManager.type1.check_icon = '';
                $scope.password_toManager.type1.check_msg = '';
                $scope.password_toManager.type2.status = 3;
                $scope.password_toManager.type2.check_flag = '';
                $scope.password_toManager.type2.check_icon = '';
                $scope.password_toManager.type2.check_msg = '';
                return 'normal';
            }else if($scope.password_toManager.type1.txt == '' && $scope.password_toManager.type2.txt != ''){
                $scope.password_toManager.type1.status = 1;
                $scope.password_toManager.type1.check_flag = 'warning';
                $scope.password_toManager.type1.check_icon = 'warning';
                $scope.password_toManager.type1.check_msg = 'Please enter a user Password.';
                return 'warning';
            }else if($scope.password_toManager.type1.txt != '' && $scope.password_toManager.type2.txt == ''){
                $scope.password_toManager.type1.status = 0;
                $scope.password_toManager.type1.check_flag = 'success';
                $scope.password_toManager.type1.check_icon = 'check-circle';
                $scope.password_toManager.type1.check_msg = 'User Password is available.';
                $scope.password_toManager.type2.status = 1;
                $scope.password_toManager.type2.check_flag = 'warning';
                $scope.password_toManager.type2.check_icon = 'warning';
                $scope.password_toManager.type2.check_msg = 'Please enter a user Retype Password.';
                return '1 depth OK';
            }else if($scope.password_toManager.type1.txt != '' && $scope.password_toManager.type2.txt != ''){
                $scope.password_toManager.type1.status = 0;
                $scope.password_toManager.type1.check_flag = 'success';
                $scope.password_toManager.type1.check_icon = 'check-circle';
                $scope.password_toManager.type1.check_msg = 'User Password is available.';
                if($scope.password_toManager.type1.txt == $scope.password_toManager.type2.txt){
                    $scope.password_toManager.type2.status = 0;
                    $scope.password_toManager.type2.check_flag = 'success';
                    $scope.password_toManager.type2.check_icon = 'check-circle';
                    $scope.password_toManager.type2.check_msg = 'User Retype Password is available.';
                    return '2 depth OK';
                }else{
                    $scope.password_toManager.type2.status = 2;
                    $scope.password_toManager.type2.check_flag = 'danger';
                    $scope.password_toManager.type2.check_icon = 'ban';
                    $scope.password_toManager.type2.check_msg = 'Please check the Retype Password.';
                }
            }
        };

        $scope.editPassword_toManager = function(){
            $scope.show_flag_toManager = true;
        };

        $scope.savePassword_toManager = function(){
            var argument = {};
            argument['userPW'] = $scope.password_toManager.type1.txt;

            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                Admin.update({}, argument);
            }else{
                alert('guest');
            }

            $scope.show_flag_toManager = false;
        };

        $scope.cancelEditPassword_toManager = function(){
            $scope.password_toManager.type1.status = '';
            $scope.password_toManager.type1.status = 3;//default normal
            $scope.password_toManager.type2.status = '';
            $scope.password_toManager.type2.status = 3;//default normal
            $scope.show_flag_toManager = false;
        };


        $scope.updateUserByDB_toManager = function(field, data, key){
            var argument = {};
            argument['change_manager_id'] = key;
            argument[field] = data;

            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                Admin.update({}, argument);
            }else{
                alert('guest');
            }

            $scope.show_flag_toManager = false;
            $scope.show_flag_toAdmin = false;
        };

        //--------------------------------------------------------------------------------------------------------------
        // USER MANAGEMENT
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        /* USER MANAGEMENT - List */
        $scope.member_search_items = [
            {item: 'User ID', field: 'userID'},
            {item: 'User Name', field: 'userName'}
        ];

        $scope.member_search = $scope.member_search_items[0];
        $scope.member_search_field = $scope.member_search_items[0].field;

        var params={};
        params['item'] = "";
        params['data'] = "";

        $scope.setMemberSearchItem = function(search_item, search_field){
            $scope.member_search_field = search_field;
            params['item'] = search_item;
            params['data'] = search_field;
        };

        MemberList.get(params, function (data) {
            $scope.member_byManager = data.objects;
        });

        $scope.updateUserGroupByManager = function(group_key, target){
            var argument = {};
            argument['group_key'] = group_key;
            argument['user_key'] = target;

            CNN_UserGroup.update(argument);

            $window.location.reload();
        };

        $scope.member_table_headers = [
            {name: 'Group'},
            {name: 'ID'},
            {name: 'Name'},
            {name: 'Phone'},
            {name: 'E-Mail'},
            {name: 'Delete'}
        ];

        /* USER MANAGEMENT - Create */
        $scope.UserInfoByManager = {
            userID : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userPW : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userRetypePW : {text: '', status: 3, check_flag: '', check_icon: '', check_msg: ''},
            userName :{text: ''},
            phone : {text: ''},
            email : {text: ''},
            date_format : {value: 1},
            time_zone : {value: 17},
            refresh_time : {text: '3000'}
        };

        $scope.checkUserID = function(){
            if($scope.UserInfoByManager.userID.text==''){
                $scope.UserInfoByManager.userID.status = 1;
                $scope.UserInfoByManager.userID.check_flag = 'warning';
                $scope.UserInfoByManager.userID.check_icon = 'warning';
                $scope.UserInfoByManager.userID.check_msg = 'user ID is required. You can not be empty.';
            }else if($scope.UserInfoByManager.userID.text!=''){
                CHECK_USER.get({user_id: $scope.UserInfoByManager.userID.text}, function(data){
                    if(data.status==200){
                        $scope.UserInfoByManager.userID.status = 0;
                        $scope.UserInfoByManager.userID.check_flag = 'success';
                        $scope.UserInfoByManager.userID.check_icon = 'check-circle';
                        $scope.UserInfoByManager.userID.check_msg = 'User ID is available.';
                    }else if(data.status==400){
                        $scope.UserInfoByManager.userID.status = 2;
                        $scope.UserInfoByManager.userID.check_flag = 'danger';
                        $scope.UserInfoByManager.userID.check_icon = 'ban';
                        $scope.UserInfoByManager.userID.check_msg = 'The duplicate ID.';
                    }
                });
            }
        };

        $scope.checkUserPW = function(pass1, pass2){
            if(pass1){
                $scope.UserInfoByManager.userPW.txt = pass1;
            }else if(pass1==''){
                $scope.UserInfoByManager.userPW.txt = '';
            }else{
                $scope.UserInfoByManager.userPW.txt = $scope.UserInfoByManager.userPW.txt;
            }

            if(pass2){
                $scope.UserInfoByManager.userRetypePW.txt = pass2;
            }else if(pass2==''){
                $scope.UserInfoByManager.userRetypePW.txt = '';
            }else{
                $scope.UserInfoByManager.userRetypePW.txt = $scope.UserInfoByManager.userRetypePW.txt;
            }

            /* check password */
            if($scope.UserInfoByManager.userPW.txt == '' && $scope.UserInfoByManager.userRetypePW.txt == ''){
                $scope.UserInfoByManager.userPW.status = 3;
                $scope.UserInfoByManager.userPW.check_flag = '';
                $scope.UserInfoByManager.userPW.check_icon = '';
                $scope.UserInfoByManager.userPW.check_msg = '';
                $scope.UserInfoByManager.userRetypePW.status = 3;
                $scope.UserInfoByManager.userRetypePW.check_flag = '';
                $scope.UserInfoByManager.userRetypePW.check_icon = '';
                $scope.UserInfoByManager.userRetypePW.check_msg = '';
                return 'normal';
            }else if($scope.UserInfoByManager.userPW.txt == '' && $scope.UserInfoByManager.userRetypePW.txt != ''){
                $scope.UserInfoByManager.userPW.status = 1;
                $scope.UserInfoByManager.userPW.check_flag = 'warning';
                $scope.UserInfoByManager.userPW.check_icon = 'warning';
                $scope.UserInfoByManager.userPW.check_msg = 'Please enter a user Password.';
                return 'warning';
            }else if($scope.UserInfoByManager.userPW.txt != '' && $scope.UserInfoByManager.userRetypePW.txt == ''){
                $scope.UserInfoByManager.userPW.status = 0;
                $scope.UserInfoByManager.userPW.check_flag = 'success';
                $scope.UserInfoByManager.userPW.check_icon = 'check-circle';
                $scope.UserInfoByManager.userPW.check_msg = 'User Password is available.';
                $scope.UserInfoByManager.userRetypePW.status = 1;
                $scope.UserInfoByManager.userRetypePW.check_flag = 'warning';
                $scope.UserInfoByManager.userRetypePW.check_icon = 'warning';
                $scope.UserInfoByManager.userRetypePW.check_msg = 'Please enter a user Retype Password.';
                return '1 depth OK';
            }else if($scope.UserInfoByManager.userPW.txt != '' && $scope.UserInfoByManager.userRetypePW.txt != ''){
                $scope.UserInfoByManager.userPW.status = 0;
                $scope.UserInfoByManager.userPW.check_flag = 'success';
                $scope.UserInfoByManager.userPW.check_icon = 'check-circle';
                $scope.UserInfoByManager.userPW.check_msg = 'User Password is available.';
                if($scope.UserInfoByManager.userPW.txt == $scope.UserInfoByManager.userRetypePW.txt){
                    $scope.UserInfoByManager.userRetypePW.status = 0;
                    $scope.UserInfoByManager.userRetypePW.check_flag = 'success';
                    $scope.UserInfoByManager.userRetypePW.check_icon = 'check-circle';
                    $scope.UserInfoByManager.userRetypePW.check_msg = 'User Retype Password is available.';
                    return '2 depth OK';
                }else{
                    $scope.UserInfoByManager.userRetypePW.status = 2;
                    $scope.UserInfoByManager.userRetypePW.check_flag = 'danger';
                    $scope.UserInfoByManager.userRetypePW.check_icon = 'ban';
                    $scope.UserInfoByManager.userRetypePW.check_msg = 'Please check the Retype Password.';
                }
            }
        };

        $scope.disabledCreateUser = function(){
            if($scope.UserInfoByManager.userID.status == 0 && $scope.UserInfoByManager.userPW.status == 0 && $scope.UserInfoByManager.userRetypePW.status == 0){
                return false;
            }else{
                return true;
            }
        };

        $scope.createUser = function(){
            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN' || $scope.userType == 'MANAGER'){
                var argument = {};
                argument['userID'] = $scope.UserInfoByManager.userID.text;
                argument['userPW'] = $scope.UserInfoByManager.userPW.text;
                argument['userName'] = $scope.UserInfoByManager.userName.text;
                argument['phone'] = $scope.UserInfoByManager.phone.text;
                argument['email'] = $scope.UserInfoByManager.email.text;
                argument['date_format'] = $scope.UserInfoByManager.date_format.value;
                argument['time_zone'] = $scope.UserInfoByManager.time_zone.value;
                argument['refresh_time'] = $scope.UserInfoByManager.refresh_time.text;

                var auth_data = 0;
                var auth_array = ''+$scope.user_auth.status;
                var auth_list = auth_array.split(',');

                angular.forEach(auth_list, function(s){
                    auth_data += Number(s);
                });

                argument['auth'] = auth_data;

                Manager.save(argument, function(data){
                    if(data.status==200){
                        var change_url = '/admin/user-manage/list';
                        $location.path(change_url, true);/* true: reload || false: reload X */
                    }
                });
            }else{
                alert("ERROR");
            }
        };

        /* USER MANAGEMENT - Info */
        $scope.member_info_byManager_date_format = 1;
        $scope.member_info_byManager_time_zone = 17;
        if(queryString['member']!=undefined){
            Member.get({key: queryString['member']}, function(data){
                if(data.status==200){
                    $rootScope.member_info_byManager = data.objects[0];
                    $scope.member_info_byManager_date_format = $scope.member_info_byManager.date_format;
                    $scope.member_info_byManager_time_zone = $scope.member_info_byManager_time_zone;
                }else{
                    alert("Information Error");
                }
            });

            $scope.selection = [];

            $scope.toggleSelection = function toggleSelection(key) {
                var idx = $scope.selection.indexOf(key);

                // is currently selected
                if (idx > -1) {
                    $scope.selection.splice(idx, 1);
                }
                // is newly selected
                else {
                    $scope.selection.push(key);
                }
            };

            AuthUserDeviceGroup.get({key: queryString['member']}, function(data){
                if(data.status==200){
                    $rootScope.manager_device_group_list = data.objects;
                    angular.forEach($rootScope.manager_device_group_list, function(value, key){
                        if(value.checked){
                            $scope.selection.push(value.key);
                        }
                    });
                }else if(data.status==404){
                    $rootScope.manager_device_group_list = [];
                }else{
                    alert("Information Error");
                }
            });
        };

        $scope.updateDG = function(key){
            var argument = {};
            argument['dg_list'] = $scope.selection;

            AuthUserDeviceGroup.update({key: key}, argument);

            alert("The user information was updated successfully.");

            $route.reload();
        };

        $scope.show_flag_toMember = false;
        $scope.password_toMember = {
            type1:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            },
            type2:{
                txt:'',
                status: 3,
                check_flag: '',
                check_icon: '',
                check_msg: ''
            }
        };

        $scope.checkPassword_toMember = function(pass1, pass2){
            if(pass1){
                $scope.password_toMember.type1.txt = pass1;
            }else if(pass1==''){
                $scope.password_toMember.type1.txt = '';
            }else{
                $scope.password_toMember.type1.txt = $scope.password_toMember.type1.txt;
            }

            if(pass2){
                $scope.password_toMember.type2.txt = pass2;
            }else if(pass2==''){
                $scope.password_toMember.type2.txt = '';
            }else{
                $scope.password_toMember.type2.txt = $scope.password_toMember.type2.txt;
            }

            /* check password */
            if($scope.password_toMember.type1.txt == '' && $scope.password_toMember.type2.txt == ''){
                $scope.password_toMember.type1.status = 3;
                $scope.password_toMember.type1.check_flag = '';
                $scope.password_toMember.type1.check_icon = '';
                $scope.password_toMember.type1.check_msg = '';
                $scope.password_toMember.type2.status = 3;
                $scope.password_toMember.type2.check_flag = '';
                $scope.password_toMember.type2.check_icon = '';
                $scope.password_toMember.type2.check_msg = '';
                return 'normal';
            }else if($scope.password_toMember.type1.txt == '' && $scope.password_toMember.type2.txt != ''){
                $scope.password_toMember.type1.status = 1;
                $scope.password_toMember.type1.check_flag = 'warning';
                $scope.password_toMember.type1.check_icon = 'warning';
                $scope.password_toMember.type1.check_msg = 'Please enter a user Password.';
                return 'warning';
            }else if($scope.password_toMember.type1.txt != '' && $scope.password_toMember.type2.txt == ''){
                $scope.password_toMember.type1.status = 0;
                $scope.password_toMember.type1.check_flag = 'success';
                $scope.password_toMember.type1.check_icon = 'check-circle';
                $scope.password_toMember.type1.check_msg = 'User Password is available.';
                $scope.password_toMember.type2.status = 1;
                $scope.password_toMember.type2.check_flag = 'warning';
                $scope.password_toMember.type2.check_icon = 'warning';
                $scope.password_toMember.type2.check_msg = 'Please enter a user Retype Password.';
                return '1 depth OK';
            }else if($scope.password_toMember.type1.txt != '' && $scope.password_toMember.type2.txt != ''){
                $scope.password_toMember.type1.status = 0;
                $scope.password_toMember.type1.check_flag = 'success';
                $scope.password_toMember.type1.check_icon = 'check-circle';
                $scope.password_toMember.type1.check_msg = 'User Password is available.';
                if($scope.password_toMember.type1.txt == $scope.password_toMember.type2.txt){
                    $scope.password_toMember.type2.status = 0;
                    $scope.password_toMember.type2.check_flag = 'success';
                    $scope.password_toMember.type2.check_icon = 'check-circle';
                    $scope.password_toMember.type2.check_msg = 'User Retype Password is available.';
                    return '2 depth OK';
                }else{
                    $scope.password_toMember.type2.status = 2;
                    $scope.password_toMember.type2.check_flag = 'danger';
                    $scope.password_toMember.type2.check_icon = 'ban';
                    $scope.password_toMember.type2.check_msg = 'Please check the Retype Password.';
                }
            }
        };

        $scope.editPassword_toMember = function(){
            $scope.show_flag_toMember = true;
        };

        $scope.savePassword_toMember = function(){
            var argument = {};
            argument['userPW'] = $scope.password_toMember.type1.txt;

            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN' || $scope.userType == 'MANAGER'){
                Manager.update({}, argument);
            }else{
                alert('guest');
            }

            $scope.show_flag_toMember = false;
        };

        $scope.cancelEditPassword_toMember = function(){
            $scope.password_toMember.type1.status = '';
            $scope.password_toMember.type1.status = 3;//default normal
            $scope.password_toMember.type2.status = '';
            $scope.password_toMember.type2.status = 3;//default normal
            $scope.show_flag_toMember = false;
        };


        $scope.updateUserByDB_toMember = function(field, data, key){
            var argument = {};
            argument['change_member_id'] = key;

            if(field=='auth'){
                var auth_data = ''+data;
                var auth_data_list = auth_data.split(",");
                var parse_data = 0;
                angular.forEach(auth_data_list, function (s) {
                    parse_data += Number(s);
                });

                data = parse_data;
            }

            argument[field] = '' + data;

            if(field=='time_zone'){
                $scope.member_info_byManager_time_zone = data;
            }else if(field=='date_format'){
                $scope.member_info_byManager_date_format = data;
            }

            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN' || $scope.userType == 'MANAGER'){
                Manager.update({}, argument);
            }else{
                alert('guest');
            }

            if(field=='userPW'){
                $scope.show_flag_toMember = false;
            }
        };

        //--------------------------------------------------------------------------------------------------------------
        // DEVICE GROUP
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        /* Device Group - List */
        $scope.group_table_headers = [
            {name:'Group Name', class_name: 'group'},
            {name:'Delete', class_name: 'delete'}
        ];

        DeviceGroup.get({}, function(data){
            if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN' || $scope.userType == 'MANAGER') {
                $scope.device_group_info = data.objects;
            }
        });

        $scope.group_info_table_headers = [
            {name: 'Name', class_name: 'name'},
            {name: 'E-Mail', class_name: 'email'},
            {name: 'Phone', class_name: 'phone'},
            {name: 'Polling Time', class_name: 'polling_time'}
        ];

        $scope.predict_table_headers = [
            {name: 'Category', class_name: 'category'},
            {name: 'Code', class_name: 'code'},
            {name: 'Severity', class_name: 'level'},
            {name: 'Ruleset', class_name: 'ruleset'},
            {name: 'Adjustment', class_name: 'adjust'},
            {name: 'Activation', class_name: 'act'},
            {name: 'E-mail', class_name: 'mail_act'},
            {name: 'SMS', class_name: 'sms_act'},
            {name: "Delete", class_name: "del"}
        ];

        $scope.incident_table_headers = [
            {name: 'Category', class_name: 'category'},
            {name: 'Code', class_name: 'code'},
            {name: 'Severity', class_name: 'level'},
            {name: 'Threshold', class_name: 'threshold'},
            {name: 'Limit', class_name: 'limit'},
            {name: 'Timeout', class_name: 'timeout'},
            {name: 'Activation', class_name: 'act'},
            {name: 'E-mail', class_name: 'mail_act'},
            {name: 'SMS', class_name: 'sms_act'}
        ];

        $scope.incident_table_frames = {
            // Code : RED
            ORDER1_R01 : {
                line: 'RED',
                severity: 'HIGH',
                category: {class_name: 'category', name: 'Proof of Play'},
                code: {class_name: 'code', name: 'R01'},
                th: {class_name: 'threshold', name: 'No response from Media Player'},
                limit: {class_name: 'limit', field: 'ic_R01_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_R01_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_R01_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_R01_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_R01_sms_enable'}
            },
            ORDER1_R02 : {
                line: 'RED',
                severity: 'HIGH',
                category: {class_name: 'category', name: 'Proof of Play'},
                code: {class_name: 'code', name: 'R02'},
                th: {class_name: 'threshold', name: 'No response from Display'},
                limit: {class_name: 'limit', field: 'ic_R02_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_R02_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_R02_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_R02_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_R02_sms_enable'}
            },
            ORDER1_R11 : {
                line: 'RED',
                severity: 'HIGH',
                category: {class_name: 'category', name: 'Proof of Play'},
                code: {class_name: 'code', name: 'R11'},
                th: {class_name: 'threshold', name: 'Core App.1 is stopped'},
                limit: {class_name: 'limit', field: 'ic_R11_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_R11_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_R11_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_R11_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_R11_sms_enable'}
            },
            ORDER1_R12 : {
                line: 'RED',
                severity: 'HIGH',
                category: {class_name: 'category', name: 'Proof of Play'},
                code: {class_name: 'code', name: 'R12'},
                th: {class_name: 'threshold', name: 'Core App.2 is stopped'},
                limit: {class_name: 'limit', field: 'ic_R12_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_R12_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_R12_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_R12_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_R12_sms_enable'}
            },
            ORDER1_R13 : {
                line: 'RED',
                severity: 'HIGH',
                category: {class_name: 'category', name: 'Proof of Play'},
                code: {class_name: 'code', name: 'R13'},
                th: {class_name: 'threshold', name: 'Core App.3 is stopped'},
                limit: {class_name: 'limit', field: 'ic_R13_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_R13_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_R13_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_R13_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_R13_sms_enable'}
            },
            ORDER1_R14 : {
                line: 'RED',
                severity: 'HIGH',
                category: {class_name: 'category', name: 'Proof of Play'},
                code: {class_name: 'code', name: 'R14'},
                th: {class_name: 'threshold', name: 'Core App.4 is stopped'},
                limit: {class_name: 'limit', field: 'ic_R14_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_R14_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_R14_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_R14_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_R14_sms_enable'}
            },
            // Code : ORANGE
            ORDER2_O01 : {
                line: 'ORANGE',
                severity: 'MEDIUM',
                category: {class_name: 'category', name: 'Cable'},
                code: {class_name: 'code', name: 'O01'},
                th: {class_name: 'threshold', name: 'Media player is on-line & Display is on-line & No video signal to Display'},
                limit: {class_name: 'limit', field: 'ic_O01_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_O01_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_O01_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_O01_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_O01_sms_enable'}
            },
            ORDER2_O02 : {
                line: 'ORANGE',
                severity: 'MEDIUM',
                category: {class_name: 'category', name: 'Temperature'},
                code: {class_name: 'code', name: 'O02'},
                th: {class_name: 'threshold', name: 'higher than Error value of CPU Temperature'},
                limit: {class_name: 'limit', field: 'ic_O02_limit', unit: 'times'},
                timeout: {class_name: 'timeout', field: 'ic_O02_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_O02_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_O02_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_O02_sms_enable'}
            },
            ORDER2_O03 : {
                line: 'ORANGE',
                severity: 'MEDIUM',
                category: {class_name: 'category', name: 'Temperature'},
                code: {class_name: 'code', name: 'O03'},
                th: {class_name: 'threshold', name: 'higher than Error value of Display Temperature'},
                limit: {class_name: 'limit', field: 'ic_O03_limit', unit: 'times'},
                timeout: {class_name: 'timeout', field: 'ic_O03_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_O03_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_O03_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_O03_sms_enable'}
            },
            /*
            ORDER2_O09 : {
                line: 'ORANGE',
                category: {class_name: 'category', name: 'Prediction'},
                code: {class_name: 'code', name: 'O09'},
                th: {class_name: 'threshold', name: 'The probability that Display temperature on will be too high within 24 hours'},
                limit: {class_name: 'limit', field: 'ic_O09_limit', unit: 'times'},
                timeout: {class_name: 'timeout', field: 'ic_O09_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_O09_enable'}
            },*/
            // Code : Yellow
            ORDER3_Y01 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Heavy load'},
                code: {class_name: 'code', name: 'Y01'},
                th: {class_name: 'threshold', name: 'CPU usage is over Error value'},
                limit: {class_name: 'limit', field: 'ic_Y01_limit', unit: 'times'},
                timeout: {class_name: 'timeout', field: 'ic_Y01_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y01_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y01_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y01_sms_enable'}
            },
            ORDER3_Y02 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Heavy load'},
                code: {class_name: 'code', name: 'Y02'},
                th: {class_name: 'threshold', name: 'Memory usage is over Error value'},
                limit: {class_name: 'limit', field: 'ic_Y02_limit', unit: 'times'},
                timeout: {class_name: 'timeout', field: 'ic_Y02_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y02_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y02_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y02_sms_enable'}
            },
            ORDER3_Y03 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Heavy load'},
                code: {class_name: 'code', name: 'Y03'},
                th: {class_name: 'threshold', name: 'HDD usage is over Error value'},
                limit: {class_name: 'limit', field: 'ic_Y03_limit', unit: 'times'},
                timeout: {class_name: 'timeout', field: 'ic_Y03_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y03_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y03_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y03_sms_enable'}
            },
            ORDER3_Y04 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Heavy load'},
                code: {class_name: 'code', name: 'Y04'},
                th: {class_name: 'threshold', name: 'Response time usage is over Error value'},
                limit: {class_name: 'limit', field: 'ic_Y04_limit', unit: 'times'},
                timeout: {class_name: 'timeout', field: 'ic_Y04_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y04_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y04_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y04_sms_enable'}
            },
            ORDER3_Y05 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Misconfiguration'},
                code: {class_name: 'code', name: 'Y05'},
                th: {class_name: 'threshold', name: "Display's Energy saving is NOT Disabled"},
                limit: {class_name: 'limit', field: 'ic_Y05_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_Y05_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y05_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y05_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y05_sms_enable'}
            },
            ORDER3_Y06 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Misconfiguration'},
                code: {class_name: 'code', name: 'Y06'},
                th: {class_name: 'threshold', name: "Display's Sleep Time is NOT OFF"},
                limit: {class_name: 'limit', field: 'ic_Y06_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_Y06_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y06_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y06_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y06_sms_enable'}
            },
            ORDER3_Y07 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Misconfiguration'},
                code: {class_name: 'code', name: 'Y07'},
                th: {class_name: 'threshold', name: "Display's Auto Off is NOT OFF"},
                limit: {class_name: 'limit', field: 'ic_Y07_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_Y07_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y07_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y07_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y07_sms_enable'}
            },
            ORDER3_Y08 : {
                line: 'YELLOW',
                severity: 'LOW',
                category: {class_name: 'category', name: 'Misconfiguration'},
                code: {class_name: 'code', name: 'Y08'},
                th: {class_name: 'threshold', name: "Display's 4 Hours OFF is NOT OFF"},
                limit: {class_name: 'limit', field: 'ic_Y08_limit', unit: 'consecutive'},
                timeout: {class_name: 'timeout', field: 'ic_Y08_timeout', unit: 'min'},
                act: {class_name: 'act', field: 'ic_Y08_enable'},
                mail_act: {class_name: 'mail_act', field: 'ic_Y08_email_enable'},
                sms_act: {class_name: 'sms_act', field: 'ic_Y08_sms_enable'}
            }
        };

        /* Device Group - Info */
        if(queryString['device-group']!=undefined){
            DeviceGroup.get({key: queryString['device-group']}, function (data) {
                if (data.status == 200) {
                    $rootScope.device_group_byManager = data.objects[0];
                } else {
                    alert("Information Error");
                }
            });

            var fault_predict_arg = {};
            fault_predict_arg["FK_device_groups_id"] = queryString['device-group'];

            FaultPredict.get(fault_predict_arg, function (data) {
                if (data.status == 200) {
                    $scope.predict_info = data.objects;
                }else{
                    alert("Predict Information Error");
                }
            });
        }

        $scope.phone_guide = [
            "[Country code][area code or network code (without first 0) and mobile number, no space, brackets or dashes]",
            "Example: [1][4155551212](for USA) or [27][1234567894](for South Africa)"
        ];

        $scope.updatePredictByDB = function(field, data, key){
            var update_predict_arg = {};
            update_predict_arg[field] = data;

            FaultPredict.update({key: key}, update_predict_arg, function(data){
                if (data.status == 200) {
                }else{
                    alert("Predict Information Modified Error");
                }
            });
        };

        $scope.statuses = [
            {value: 1, text: 'ON'},
            {value: 0, text: 'OFF'}
        ];

        $scope.showStatus = function(day) {
            var selected = $filter('filter')($scope.statuses, {value: day});
            return (selected.length) ? selected[0].text : 'Not Set';
        };

        $scope.showCheckBox = function(check){
            if(check){
                return 'Checked';
            }else if(!check){
                return 'Not Checked';
            }else{
                return 'Not Set';
            }
        };

        $scope.showRadioBox = function(check){
            if(check){
                return 'ON';
            }else if(!check){
                return 'OFF';
            }else{
                return 'Not Set';
            }
        };

        /* Device Group - Modify & Delete */

        $scope.updateDeviceGroupThresholdByDB = function(field, warning, error, key){
            var argument = {};
            //----------------------------------------------------------------------------------------------------------
            // [ Validate ]
            //----------------------------------------------------------------------------------------------------------
            var str_field = ''+field;

            if(str_field.indexOf('error') > -1){
                if(warning == undefined){
                    argument[field] = error;
                }else if(Number(error) <= Number(warning)){
                    return 'Error';
                }else{
                    argument[field] = error;
                }
            }

            if(str_field.indexOf('warning') > -1){
                if(error == undefined){
                    argument[field] = warning;
                }else if(Number(warning) >= Number(error)){
                    return 'Error';
                }else{
                    argument[field] = warning;
                }
            }

            //----------------------------------------------------------------------------------------------------------
            // [ Update ]
            //----------------------------------------------------------------------------------------------------------
            DeviceGroup.update({key: key}, argument);
        };

        $scope.updateDeviceGroupByDB_specificTV_control = function(field, data, key){
            var argument = {};
            if(data == '0'){
                argument[field] = false;
            }else if(data == '1'){
                argument[field] = true;
            }

            DeviceGroup.update({key: key}, argument);
        };

        $scope.updateDeviceGroupByDB = function(field, data, key){

            var field_name = '' + field;
            if(field == 'timer_begin_sat' || field == 'timer_end_sat'
              || field == 'timer_begin_sun' || field == 'timer_end_sun'
              || field == 'timer_begin_mon' || field == 'timer_end_mon'
              || field == 'timer_begin_tue' || field == 'timer_end_tue'
              || field == 'timer_begin_wed' || field == 'timer_end_wed'
              || field == 'timer_begin_thu' || field == 'timer_end_thu'
              || field == 'timer_begin_fri' || field == 'timer_end_fri'){
              var val = ""+data;
              //var regExp = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
              var regExp = /^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]:[0][0]$/;
              var var_match = val.match(regExp) != null;
              var regExp2 = /^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]$/;
              var var_match2 = val.match(regExp2) != null;

              if(var_match || var_match2){

              }else{
                return "Date format is not correct.";
              }
            }


          //----------------------------------------------------------------------------------------------------------
            // [ Validate ]
            //----------------------------------------------------------------------------------------------------------
            if(field=='polling_time'){
                if(data != 0 && data < 30) return 'Please input more than 30 seconds!';
            }

            var argument = {};
            argument[field] = data;
            //----------------------------------------------------------------------------------------------------------
            // [ Parsing ]
            //----------------------------------------------------------------------------------------------------------
            if(field_name.indexOf('enable') > -1){
                if(data == '0'){
                    argument[field] = false;
                }else if(data == '1'){
                    argument[field] = true;
                }
            }

            DeviceGroup.update({key: key}, argument);
        };

        $scope.alarm_test = function(field, group_key){
            var alarm_arg = {};
            alarm_arg["type"] = field;
            alarm_arg["group_key"] = group_key;

            AlarmTest.save(alarm_arg, function(data){
                var alert_msg = "";

                if(alarm_arg["type"] == "test_email"){
                    alert_msg = "E-mail has been sent ";
                }else if(alarm_arg["type"] == "test_phone"){
                    alert_msg = "SMS has been sent ";
                }

                if(data.status == 200){
                    alert_msg += "successfully.";
                }else{
                    alert_msg += "fail.";
                }

                alert(alert_msg);
            });
        };

        $scope.updateDeviceGroupByDB_specificTV_control = function(field, data, key){
            var argument = {};
            if(data == '0'){
                argument[field] = false;
            }else if(data == '1'){
                argument[field] = true;
            }

            DeviceGroup.update({key: key}, argument);
        };

        $scope.showThreshold = function(th){
            if(th==0){
                return '0'
            }else if(th!=0 && th){
                return th;
            }else{
                return 'Not Set';
            }
        };

        /* Device Group - Create */
        $scope.group_info = {
            name:{txt:'', field:'', status: 3, check_flag: '', check_icon: '', check_msg: '', type:'Device_Group'},
            start:{time:''},
            end:{time:''},
            flag:{value: true}  //default True : Setting ON
        };

        $scope.checkDeviceGroupName = function(){
            if($scope.group_info.name.txt==''){
                $scope.group_info.name.status = 1;
                $scope.group_info.name.check_flag = 'warning';
                $scope.group_info.name.check_icon = 'warning';
                $scope.group_info.name.check_msg = 'user ID is required. You can not be empty.';
            }else if($scope.group_info.name.txt!=''){
                var argument = {};

                argument['user_id'] = $scope.group_info.name.txt;
                argument['type'] = $scope.group_info.name.type;
                CHECK_GROUP.get(argument, function(data){
                      if(data.status==200){
                          $scope.group_info.name.status = 0;
                          $scope.group_info.name.check_flag = 'success';
                          $scope.group_info.name.check_icon = 'check-circle';
                          $scope.group_info.name.check_msg = 'User ID is available.';
                      }else if(data.status==400){
                          $scope.group_info.name.status = 2;
                          $scope.group_info.name.check_flag = 'danger';
                          $scope.group_info.name.check_icon = 'ban';
                          $scope.group_info.name.check_msg = 'The duplicate ID.';
                      }
                  });
            }
        };

        $scope.createGroup = function(){
            var argument = {};

            argument['name'] = $scope.group_info.name.txt;
            argument['start_time'] = $scope.group_info.start.time;
            argument['end_time'] = $scope.group_info.end.time;
            argument['setting_flag'] = $scope.group_info.flag.value;

            DeviceGroup.save(argument, function(data){
                if(data.status == 200){
                    var change_url = '/admin/group/list';
                    $location.path(change_url, true);/* true: reload || false: reload X */
                }
            });
        };

        //--------------------------------------------------------------------------------------------------------------
        // DEVICE MANAGEMENT
        // by KSM
        //--------------------------------------------------------------------------------------------------------------
        $scope.device_info = {
            alias: {label:'Name', txt: '', field:''},
            site: {label:'Account', txt: '', field:''},
            location: {label:'Location', txt: '', field:''},
            shop: {label:'Shop', txt: '', field:''},
            serial: {label:'Serial Number', txt: '', field:''}
        };

        /**
         * Device Management - create
         * */
        $scope.createDevice = function(){
            var argument = {};
            argument['alias'] = $scope.device_info.alias.txt;
            argument['site'] = $scope.device_info.site.txt;
            argument['location'] = $scope.device_info.location.txt;
            argument['shop'] = $scope.device_info.shop.txt;
            argument['serial_number'] = $scope.device_info.serial.txt;

            Device_Manage.save(argument, function(data){
                if(data.status==200){
                    var change_url = '/admin/device/list';
                    $location.path(change_url, true);/* true: reload || false: reload X */
                }else if(data.status==400){
                    alert(data.notice);
                }
            });
        };

        /**
         * Device Management - list
         * */
        $scope.dg_table_headers_mediaplayer = [
            {name: 'No.', class_name:'no', sortable: false},
            {name: 'Device Group', suffix_name: 'Group', class_name:'dg_name', sortable: false},
            {name: 'Name', class_name:'name', sortable: true, field: 'alias'},
            {name: 'Account', class_name:'site', sortable: true, field: 'site'},
            {name: 'Location', class_name:'location', sortable: true, field: 'location'},
            {name: 'Shop', class_name:'shop', sortable: true, field: 'shop'},
            {name: 'Register Date', suffix_name:'Date', class_name:'regi_date', sortable: true, field: 'created_date'},
            {name: 'Manager E-Mail', suffix_name:'E-mail', class_name:'email', sortable: false, field: 'email'},
            {name: 'Manager Mobile', suffix_name:'Mobile', class_name:'phone', sortable: false, field: 'phone'},
            {name: 'Address', class_name:'addr', sortable: false},
            {name: 'Memo', class_name:'memo', sortable: false},
            {name: 'Delete', class_name:'del', sortable: false}
        ];

        $scope.dg_table_headers_mediaplayer_admin= [
            {name: 'No.', class_name:'no', sortable: false},
            {name: 'Manager Group', suffix_name: 'Group', class_name:'dg_name', sortable: false},
            {name: 'Name', class_name:'name', sortable: true, field: 'alias'},
            {name: 'Account', class_name:'site', sortable: true, field: 'site'},
            {name: 'Location', class_name:'location', sortable: true, field: 'location'},
            {name: 'Shop', class_name:'shop', sortable: true, field: 'shop'},
            {name: 'Register Date', suffix_name:'Date', class_name:'regi_date', sortable: true, field: 'created_date'},
            {name: 'Manager E-Mail', suffix_name:'E-mail', class_name:'email', sortable: false, field: 'email'},
            {name: 'Manager Mobile', suffix_name:'Mobile', class_name:'phone', sortable: false, field: 'phone'},
            {name: 'Address', class_name:'addr', sortable: false},
            {name: 'Memo', class_name:'memo', sortable: false},
            {name: 'Delete', class_name:'del', sortable: false}
        ];

        $scope.dg_table_headers_display = [
            {name: 'No.', class_name:'no', sortable: false},
            {name: 'Display Group', suffix_name: 'Group', class_name:'dg_name', sortable: false},
            {name: 'Set ID', class_name:'dg_name', sortable: true},
            {name: 'Name', class_name:'alias', sortable: true, field: 'alias'},
            {name: 'IP/Serial', class_name:'port_number', sortable: true, field: 'port_number'},
            {name: 'Model', class_name:'model', sortable: true, field: 'model'},
            {name: 'Serial Number', class_name:'serial_number', sortable: true, field: 'serial_number'},
            {name: 'Register Date', suffix_name:'Date', class_name:'create_date', sortable: true, field: 'created_date'},
            {name: 'Manager E-Mail', suffix_name:'E-mail', class_name:'email', sortable: false, field: 'email'},
            {name: 'Manager Mobile', suffix_name:'Mobile', class_name:'phone', sortable: false, field: 'phone'},
            {name: 'Delete', class_name:'del', sortable: false}
        ];

        var params = $location.search();
        $scope.order = params['order'] || '-';
        $scope.order_by = params['order_by'] || 'alias'; //default order by

        var parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var order_by = $scope.order_by;
        var str_order = '';
        if($scope.order == '-'){
            str_order = 'desc';
        }else{
            str_order = 'asc';
        }

        parameters['sorting'][order_by] = str_order;
        if($scope.deviceTableParams == undefined){
            $scope.deviceTableParams = new ngTableParams(parameters, {
                counts: [],
                total: 0,// length of data
                getData: function ($defer, params) {
                    var api_params = {};
                    api_params['limit'] = params.count();
                    api_params['offset'] = (params.page() - 1) * api_params['limit'];

                    var sorting = params.sorting();
                    for (var key in sorting) {
                        var value = sorting[key];
                        if (value == 'desc') {
                            api_params['order'] = '-';
                        }

                        api_params['order_by'] = key;
                    }

                    Device_Manage.get(api_params, function (data) {
                        $timeout(function () {
                            //console.log(data);
                            params.total(data.meta.total_count);
                            $defer.resolve(data.objects);
                        }, 500);
                    });
                }
            });
        }else{
            $scope.deviceTableParams.reload();
        }


        var dp_params = $location.search();
        $scope.order = dp_params['order'] || '-';
        $scope.order_by = dp_params['order_by'] || 'alias'; //default order by

        var dp_parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var dp_order_by = $scope.order_by;
        var dp_str_order = '';
        if($scope.order == '-'){
            dp_str_order = 'desc';
        }else{
            dp_str_order = 'asc';
        }

        dp_parameters['sorting'][dp_order_by] = dp_str_order;

        var link_params = {};
        if($scope.displayTableParams == undefined){
            $scope.displayTableParams = new ngTableParams(dp_parameters, {
                counts: [],
                total: 0,// length of data
                getData: function ($defer, params) {
                    var api_params = {};
                    api_params['limit'] = params.count();
                    api_params['offset'] = (params.page() - 1) * api_params['limit'];
                    api_params['key'] = link_params['mp_id'] || -1;

                    var sorting = params.sorting();
                    for (var key in sorting) {
                        var value = sorting[key];
                        if (value == 'desc') {
                            api_params['order'] = '-';
                        }

                        api_params['order_by'] = key;
                    }

                    Display_Manage.get(api_params, function (data) {
                        $timeout(function () {
                            params.total(data.meta.total_count);
                            $defer.resolve(data.objects);
                        }, 500);
                    });
                }
            });
        }else{
            $scope.displayTableParams.reload();
        }


        $scope.showDisplayTable = function(item){
            if($scope.selectedItem && $scope.selectedItem != item) $scope.selectedItem.clicked = false;
            item.clicked = !item.clicked;
            $scope.selectedItem = item;

            link_params['mp_id'] = item.key;
            $scope.displayTableParams.reload();
        };

        $scope.updateDeviceManage = function(field, data, key){
            var argument = {};
            argument[field] = data;

            Device_Manage.update({key: key}, argument);
        };

        $scope.updateDisplayManage = function(field, data, key){
            var argument = {};
            argument[field] = data;

            Display_Manage.update({key: key}, argument);
        };

        $scope.user_auth = {
            status: [],
            statuses: [
                {value: 1, text: 'Modify Information'},    //Device Information
                {value: 2, text: 'MediaPlayer Control'},   //MediaPlayer
                {value: 4, text: 'Display Control'},        //Display
                {value: 8, text: 'View Report'}             //Report
            ]
        };

        $scope.setAuth = function(){
            var selected = [];

            angular.forEach($scope.user_auth.statuses, function(s) {
                if ($scope.user_auth.status.indexOf(s.value) > -1) {
                    selected.push(s.text);
                }
            });

            return selected.length ? selected.join(', ') : 'Not set';
        };

        $scope.time_zones = [
            {value: 1, data: -((60*60*12)*1000), simple_text: 'UTC-12', text: 'UTC-12 (BIT)'},
            {value: 2, data: -((60*60*11)*1000), simple_text: 'UTC-11', text: 'UTC-11 (NUT, SST)'},
            {value: 3, data: -((60*60*10)*1000), simple_text: 'UTC-10',  text: 'UTC-10 (CKT, HAST, HST, TAHT)'},
            {value: 4, data: -(((60*60*9) + (60*30))*1000), simple_text: 'UTC-09:30',  text: 'UTC-09:30 (MART, MIT)'},
            {value: 5, data: -((60*60*9)*1000), simple_text: 'UTC-09',  text: 'UTC-09 (AKST, GAMT, GIT, HADT)'},
            {value: 6, data: -((60*60*8)*1000), simple_text: 'UTC-08',  text: 'UTC-08 (AKDT, CIST, PST)'},
            {value: 7, data: -((60*60*7)*1000), simple_text: 'UTC-07',  text: 'UTC-07 (MST, PDT)'},
            {value: 8, data: -((60*60*6)*1000), simple_text: 'UTC-06',  text: 'UTC-06 (CST, EAST, GALT, MDT)'},
            {value: 9, data: -((60*60*5)*1000), simple_text: 'UTC-05',  text: 'UTC-05 (CDT, COT, CST, EASST, ECT, EST, PET)'},
            {value: 10, data: -(((60*60*4) + (60*30))*1000), simple_text: 'UTC-04:30',  text: 'UTC-04:30 (VET)'},
            {value: 11, data: -((60*60*4)*1000), simple_text: 'UTC-04',  text: 'UTC-04 (AMT, AST, BOT, CDT, CLT, COST, ECT, EDT, FKT, GYT, PYT)'},
            {value: 12, data: -(((60*60*3) + (60*30))*1000), simple_text: 'UTC-03:30',  text: 'UTC-03:30 (NST, NT)'},
            {value: 13, data: -((60*60*3)*1000), simple_text: 'UTC-03',  text: 'UTC-03 (ADT, AMST, ART, BRT, CLST, FKST, GFT, PMST, PYST, ROTT, SRT, UYT)'},
            {value: 14, data: -(((60*60*2) + (60*30))*1000), simple_text: 'UTC-02:30',  text: 'UTC-02:30 (NDT)'},
            {value: 15, data: -((60*60*2)*1000), simple_text: 'UTC-02',  text: 'UTC-02 (FNT, GST, PMDT, UYST)'},
            {value: 16, data: -((60*60*1)*1000), simple_text: 'UTC-01',  text: 'UTC-01 (AZOST, CVT, EGT)'},
            {value: 17, data: ((60*60*0)*1000), simple_text: 'UTC',  text: 'UTC (GMT, UCT, WET, Z, EGST)'},
            {value: 18, data: ((60*60*1)*1000), simple_text: 'UTC+01',  text: 'UTC+01 (BST, CET, DFT, IST, MET, WAT, WEDT, WEST)'},
            {value: 19, data: ((60*60*2)*1000), simple_text: 'UTC+02',  text: 'UTC+02 (CAT, CEDT, CEST, EET, HAEC, IST, MEST, SAST, WAST)'},
            {value: 20, data: ((60*60*3)*1000), simple_text: 'UTC+03',  text: 'UTC+03 (AST, EAT, EEDT, EEST, FET, IDT, IOT, SYOT)'},
            {value: 21, data: ((60*60*3) + (60*30)*1000), simple_text: 'UTC+03:30',  text: 'UTC+03:30 (IRST)'},
            {value: 22, data: ((60*60*4)*1000), simple_text: 'UTC+04',  text: 'UTC+04 (AMT, AZT, GET, GST, MSK, MUT, RET, SAMT, SCT, VOLT)'},
            {value: 23, data: ((60*60*4) + (60*30)*1000), simple_text: 'UTC+04:30',  text: 'UTC+04:30 (AFT, IRDT)'},
            {value: 24, data: ((60*60*5)*1000), simple_text: 'UTC+05',  text: 'UTC+05 (AMST, HMT, MAWT, MVT, ORAT, PKT, TFT, TJT, TMT, UZT)'},
            {value: 25, data: ((60*60*5) + (60*30)*1000), simple_text: 'UTC+05:30',  text: 'UTC+05:30 (IST, SLST)'},
            {value: 26, data: ((60*60*5) + (60*45)*1000), simple_text: 'UTC+05:45',  text: 'UTC+05:45 (NPT)'},
            {value: 27, data: ((60*60*6)*1000), simple_text: 'UTC+06:00',  text: 'UTC+06:00 (BIOT, BST, BTT, KGT, VOST, YEKT)'},
            {value: 28, data: ((60*60*6) + (60*30)*1000), simple_text: 'UTC+06:30',  text: 'UTC+06:30 (CCT, MMT, MST)'},
            {value: 29, data: ((60*60*7)*1000), simple_text: 'UTC+07',  text: 'UTC+07 (CXT, DAVT, HOVT, ICT, KRAT, OMST, THA, WIT)'},
            {value: 30, data: ((60*60*8)*1000), simple_text: 'UTC+08',  text: 'UTC+08 (ACT, AWST, BDT, CHOT, CIT, CST, CT, HKT, MST, MYT, PST, SGT, SST, ULAT, WST)'},
            {value: 31, data: ((60*60*8) + (60*45)*1000), simple_text: 'UTC+08:45',  text: 'UTC+08:45 (CWST)'},
            {value: 32, data: ((60*60*9)*1000), simple_text: 'UTC+09',  text: 'UTC+09 (AWDT, EIT, IRKT, JST, KST, TLT)'},
            {value: 33, data: ((60*60*9) + (60*30)*1000), simple_text: 'UTC+09:30',  text: 'UTC+09:30 (ACST, CST)'},
            {value: 34, data: ((60*60*10)*1000), simple_text: 'UTC+10',  text: 'UTC+10 (AEST, CHST, CHUT, DDUT, EST, PGT, VLAT, YAKT)'},
            {value: 35, data: ((60*60*10) + (60*30)*1000), simple_text: 'UTC+10:30',  text: 'UTC+10:30 (ACDT, CST, LHST)'},
            {value: 36, data: ((60*60*11)*1000), simple_text: 'UTC+11',  text: 'UTC+11 (AEDT, KOST, LHST, MIST, NCT, PONT, SAKT, SBT)'},
            {value: 37, data: ((60*60*11) + (60*30)*1000), simple_text: 'UTC+11:30',  text: 'UTC+11:30 (NFT)'},
            {value: 38, data: ((60*60*12)*1000), simple_text: 'UTC+12',  text: 'UTC+12 (FJT, GILT, MAGT, MHT, NZST, PETT, TVT, WAKT)'},
            {value: 39, data: ((60*60*12) + (60*45)*1000), simple_text: 'UTC+12:45',  text: 'UTC+12:45 (CHAST)'},
            {value: 40, data: ((60*60*13)*1000), simple_text: 'UTC+13',  text: 'UTC+13 (NZDT, PHOT, TKT, TOT)'},
            {value: 41, data: ((60*60*13) + (60*45)*1000), simple_text: 'UTC+13:45',  text: 'UTC+13:45 (CHADT)'},
            {value: 42, data: ((60*60*14)*1000), simple_text: 'UTC+14',  text: 'UTC+14 (LINT)'}
        ];

        $scope.date_formats = [
            {value: 1, text: 'YYYY/MM/DD'},
            {value: 2, text: 'MM/DD/YYYY'},
            {value: 3, text: 'DD/MM/YYYY'}
        ];

        // [ DEFAULT ] set
        $scope.select_zone = $scope.time_zones[16];

        $scope.setZone = function(value){
            $scope.UserInfoByAdmin.time_zone.value = value;
            $scope.UserInfoByManager.time_zone.value = value;
        };

        // [ DEFAULT ] set
        $scope.select_format = $scope.date_formats[0];

        $scope.setFormat = function(value){
            $scope.UserInfoByAdmin.date_format.value = value;
            $scope.UserInfoByManager.date_format.value = value;
        };

        //--------------------------------------------------------------------------------------------------------------
        // [ Common ] Delete Confirm
        //--------------------------------------------------------------------------------------------------------------
        $scope.openDeleteDialog = function (del_type, key) {
            Modal.open(
                'views/delete_modal.html',
                'DelCtrl',
                'lg',
                {
                    del_type: function(){
                        return del_type;
                    },
                    key: function(){
                        return key;
                    }
                }
            );
        };
        //--------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------
        $scope.editServerInfo = function(){
          $scope.show_flag = true;
        };
        $scope.admin_server_table = [
          {name: 'ID', field: "id"},
          {name: 'server_address', field: "server_address"},
          {name: 'server_key', field: "server_key"},
          {name: 'server_label', field: "server_label"},
          {name: 'rc_address', field: "rc_address"},
          {name: 'rc_port', field: "rc_port"}
        ];

        $scope.admin_mpdp_table = [
          {name: 'ID', field: "id", sortable: true},
          {name: 'Name', field: "name", sortable: false},
          {name: 'Img_location', field: "img_location", sortable: false},
          {name: 'Specification', field: "specification", sortable: false}
        ];

        $scope.display_header = [
          {name: 'ID', field: "id"},
          {name: 'Name', field: "name"},
          {name: 'PowerStatus', field: "tag_PowerStatus"},
          {name: 'InputSelect', field: "tag_InputSelect"},
          {name: 'AspectRatio', field: "tag_AspectRatio"},
          {name: 'PictureMode', field: "tag_PictureMode"},
          {name: 'Temp', field: "tag_Temp"},
          {name: 'Fan', field: "tag_Fan"},
          {name: 'Signal', field: "tag_Signal"},
          {name: 'Lamp', field: "tag_Lamp"},
          {name: 'FirmwareVer', field: "tag_FirmwareVer"},
          {name: 'SerialNo', field: "tag_SerialNo"},
          {name: 'EnergySaving', field: "tag_EnergySaving"},
          {name: 'BackLight', field: "tag_Backlight"},
          {name: 'Constrast', field: "tag_Constrast"},
          {name: 'Brightness', field: "tag_Brightness"},
          {name: 'Sharpness', field: "tag_Sharpness"},
          {name: 'Color', field: "tag_Color"},
          {name: 'Tint', field: "tag_Tint"},
          {name: 'ColorTemp', field: "tag_ColorTemp"},
          {name: 'MonitorStatus', field: "tag_MonitorStatus"},
          {name: 'RemoteControl', field: "tag_RemoteControl"},
          {name: 'Speaker', field: "tag_Speaker"},
          {name: 'SoundMode', field: "tag_SoundMode"},
          {name: 'Balance', field: "tag_Balance"},
          {name: 'VolumeControl', field: "tag_VolumControl"},
          {name: 'VolumeMute', field: "tag_VolumMute"},
          {name: 'Treble', field: "tag_Treble"},
          {name: 'Bass', field: "tag_Bass"},
          {name: 'AutoVolume', field: "tag_AutoVolume"},
          {name: 'Date', field: "tag_Date"},
          {name: 'Time', field: "tag_Time"},
          {name: 'SleepTime', field: "tag_SleepTime"},
          {name: 'AutoStandBy', field: "tag_AutoStandby"},
          {name: 'AutoOff', field: "tag_AutoOff"},
          {name: 'ISM Mode', field: "tag_ismMode"},
          {name: 'DPM Select', field: "tag_dpmSelect"},
          {name: 'OSD Select', field: "tag_osdSelect"},
          {name: 'Language', field: "tag_Lang"},
          {name: 'Remote', field: "tag_Remote"},
          {name: 'PowerOnDelay', field: "tag_PowerOnDelay"},
          {name: 'TileMode', field: "tag_TileMode"},
          {name: 'TileModeCheck', field: "tag_TileModeCheck"},
          {name: 'TileID', field: "tag_TileID"},
          {name: 'TileNaturalMode', field: "tag_TileNaturalMode"},
          {name: 'SizeH', field: "tag_SizeH"},
          {name: 'SizeV', field: "tag_SizeV"},
          {name: 'Position H', field: "tag_PositionH"},
          {name: 'Position V', field: "tag_PositionV"},
          {name: 'RedGain', field: "tag_RedGain"},
          {name: 'RedOffset', field: "tag_RedOffset"},
          {name: 'GreenGain', field: "tag_GreenGain"},
          {name: 'GreenOffset', field: "tag_GreenOffset"},
          {name: 'BlueGain', field: "tag_BlueGain"},
          {name: 'BlueOffset', field: "tag_BlueOffset"},
          {name: 'OnTimer', field: "tag_OnTimer"},
          {name: 'OffTimer', field: "tag_OffTimer"},
          {name: 'OnTimerInput', field: "tag_OnTimerInput"},
          {name: 'FirmwareUpdate', field: "tag_FirmwareUpdate"},
          {name: 'Image Location', field: "img_location"},
          {name: 'Specification', field: "specification"}
        ];

        $scope.UrlParsing = queryString["dp_num"];
        //alert(queryString["dp_num"]);

        Servers.get(function(data){//list - get
          $scope.servers = data.objects;
        });

        Servers.get({pk: 2}, function(data){//only - get
          $scope.server = data.object;
        });
        // [ Server_List Update ]
        $scope.updateServerInfo = function(key, field, data){
            var argument = {};
            argument[field] = data;

            Servers.update({pk: key}, argument);
        };

        var media_params = $location.search();
        $scope.media_order = media_params['order'] || '-';
        $scope.media_order_by = media_params['order_by'] || 'id'; //default order by

        var media_parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var media_order_by = $scope.media_order_by;
        var media_str_order = '';
        if($scope.media_order == '-'){
            media_str_order = 'asc';
        }else{
            media_str_order = 'desc';
        }
        media_parameters['sorting'][media_order_by] = media_str_order;
        if($scope.mediaModelTableParams == undefined){
            $scope.mediaModelTableParams = new ngTableParams(media_parameters, {
                counts: [],
                total: 0,// length of data
                getData: function ($defer, params) {
                    var api_params = {};
                    api_params['limit'] = params.count();
                    api_params['offset'] = (params.page() - 1) * api_params['limit'];

                    var sorting = params.sorting();
                    for (var key in sorting) {
                        var value = sorting[key];
                        if (value == 'desc') {
                            api_params['order'] = '-';
                        }

                        api_params['order_by'] = key;
                    }

                    MediaPlayer_Models.get(api_params, function (data) {
                        $timeout(function () {
                            params.total(data.meta.total_count);
                            $defer.resolve(data.objects);
                        }, 500);
                    });
                }
            });
        }else{
            $scope.mediaModelTableParams.reload();
        }

        $scope.updateMediaPlayerInfo = function(key, field, data){
            var argument = {};
            argument[field] = data;
            MediaPlayer_Models.update({pk: key}, argument);
        };


        //Display_Models.get(function(data){//list - get
        //  $scope.display_models = data.objects;
        //});
        var display_params = $location.search();
        $scope.display_order = display_params['order'] || '-';
        $scope.display_order_by = display_params['order_by'] || 'id'; //default order by

        var display_parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var display_order_by = $scope.display_order_by;
        var display_str_order = '';
        if($scope.display_order == '-'){
            display_str_order = 'asc';
        }else{
            display_str_order = 'desc';
        }
        display_parameters['sorting'][display_order_by] = display_str_order;
        if($scope.displayModelTableParams == undefined){
            $scope.displayModelTableParams = new ngTableParams(display_parameters, {
                counts: [],
                total: 0,// length of data
                getData: function ($defer, params) {
                    var api_params = {};
                    api_params['limit'] = params.count();
                    api_params['offset'] = (params.page() - 1) * api_params['limit'];

                    var sorting = params.sorting();
                    for (var key in sorting) {
                        var value = sorting[key];
                        if (value == 'desc') {
                            api_params['order'] = '-';
                        }

                        api_params['order_by'] = key;
                    }

                    Display_Models.get(api_params, function (data) {
                        $timeout(function () {
                            params.total(data.meta.total_count);
                            $defer.resolve(data.objects);
                        }, 500);
                    });
                }
            });
        }else{
            $scope.displayModelTableParams.reload();
        }


        if(queryString['dp_num']!=undefined) {
            Display_Models.get({pk: $scope.UrlParsing}, function (data) {//only - get
                $scope.display_model = data.object;
                //console.log(JSON.stringify($scope.display_model));
            });
        }

        // [ update ]
        $scope.updateMediaPlayerInfo = function(key, field, data){
            var argument = {};
            argument[field] = data;

            MediaPlayer_Models.update({pk: key}, argument);
        };

        $scope.updateDisplay = function(key, value){
            var argument = {};
            argument["name"] = value;

            Display_Models.update({pk: key}, argument);
        };

        $scope.updateDisplayInfo = function(key, field, data){
            var argument = {};
            argument[field] = data;

            Display_Models.update({pk: key}, argument);
        };

        // modal -------------------------------------------------------------------------------------------------------
        $scope.create_predict_open = function(device_groups_id){
            Modal.open(
                'views/admin_group_info_create_predict_modal.html',
                'PredictCreateCtrl',
                'lg',
                {
                    device_groups_id: function(){
                        return device_groups_id;
                    }
                },
                function(res_msg){
                    if(res_msg == "OK"){
                        $route.reload();
                    }
                }
            );
        };

        $scope.change_predict_open = function(predict_id, code_num){
            Modal.open(
                'views/admin_group_info_change_predict_modal.html',
                'PredictChangeCtrl',
                'lg',
                {
                    predict_id: function(){
                        return predict_id;
                    },
                    code_num: function(){
                        return code_num;
                    }
                }
            );
        };
    });
