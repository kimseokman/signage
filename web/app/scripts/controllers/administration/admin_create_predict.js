/**
 * Created by ksm on 2015-04-28.
 */
'use strict';

angular.module('signageApp')
    .controller('PredictCreateCtrl', function ($scope, $modalInstance, COM, device_groups_id, FaultPredict) {
        $scope.setting = COM.setting;

        $scope.create_predict_headers = [
            {name: "Category", class_name: "category"},
            {name: "Code", class_name: "code"},
            {name: "Ruleset", class_name: "ruleset"},
            {name: "Activation", class_name: "act"}
        ];

        $scope.create_predict_th1_headers = [
            {name: "Th Num.", class_name:"th_num"},
            {name: "Device", class_name:"device"},
            {name: "Compare", class_name:"compare"},
            {name: "Value", class_name:"value"},
            {name: "Limit", class_name:"limit"},
            {name: "Timeout", class_name:"timeout"},
            {name: "Co", class_name:"co"}
        ];

        $scope.create_predict_th2_headers = [
            {name: "Th Num.", class_name:"th_num"},
            {name: "Device", class_name:"device"},
            {name: "Compare", class_name:"compare"},
            {name: "Value", class_name:"value"},
            {name: "Limit", class_name:"limit"},
            {name: "Timeout", class_name:"timeout"},
            {name: "Co", class_name:"co"}
        ];

        $scope.create_predict_th3_headers = [
            {name: "Th Num.", class_name:"th_num"},
            {name: "Device", class_name:"device"},
            {name: "Compare", class_name:"compare"},
            {name: "Value", class_name:"value"},
            {name: "Limit", class_name:"limit"},
            {name: "Timeout", class_name:"timeout"},
            {name: "Co", class_name:"co"}
        ];

        $scope.create_predict_th4_headers = [
            {name: "Th Num.", class_name:"th_num"},
            {name: "Device", class_name:"device"},
            {name: "Compare", class_name:"compare"},
            {name: "Value", class_name:"value"},
            {name: "Limit", class_name:"limit"},
            {name: "Timeout", class_name:"timeout"},
            {name: "", class_name:"co"}
        ];

        $scope.create_predict = {
            category: $scope.setting.predict.category.statuses[0],
            ruleset: {value: null},
            active: $scope.setting.predict.active.statuses[0],
            th1_device: $scope.setting.predict.th_device.statuses[0],
            th1_compare: $scope.setting.predict.th_compare.statuses[0],
            th1_num: {value: 0},
            th1_limit: {value: 0},
            th1_timeout: {value: 0},
            co1_string: $scope.setting.predict.co_string.statuses[0],
            th2_device: $scope.setting.predict.th_device.statuses[0],
            th2_compare: $scope.setting.predict.th_compare.statuses[0],
            th2_num: {value: 0},
            th2_limit: {value: 0},
            th2_timeout: {value: 0},
            co2_string: $scope.setting.predict.co_string.statuses[0],
            th3_device: $scope.setting.predict.th_device.statuses[0],
            th3_compare: $scope.setting.predict.th_compare.statuses[0],
            th3_num: {value: 0},
            th3_limit: {value: 0},
            th3_timeout: {value: 0},
            co3_string: $scope.setting.predict.co_string.statuses[0],
            th4_device: $scope.setting.predict.th_device.statuses[0],
            th4_compare: $scope.setting.predict.th_compare.statuses[0],
            th4_num: {value: 0},
            th4_limit: {value: 0},
            th4_timeout: {value: 0}
        };

        $scope.createPredict = function(){
            var add_predict_arg = {};
            add_predict_arg["FK_device_groups_id"] = device_groups_id;
            add_predict_arg["category_id"] = $scope.create_predict.category.value;
            add_predict_arg["ruleset"] = $scope.create_predict.ruleset.value;
            add_predict_arg["active"] = $scope.create_predict.active.value;

            add_predict_arg["th1_device_id"] = $scope.create_predict.th1_device.value;
            add_predict_arg["th1_compare_id"] = $scope.create_predict.th1_compare.value;
            add_predict_arg["th1_num"] = $scope.create_predict.th1_num.value;
            add_predict_arg["th1_limit"] = $scope.create_predict.th1_limit.value;
            add_predict_arg["th1_timeout"] = $scope.create_predict.th1_timeout.value;
            add_predict_arg["th1_co_id"] = $scope.create_predict.co1_string.value;

            add_predict_arg["th2_device_id"] = $scope.create_predict.th2_device.value;
            add_predict_arg["th2_compare_id"] = $scope.create_predict.th2_compare.value;
            add_predict_arg["th2_num"] = $scope.create_predict.th2_num.value;
            add_predict_arg["th2_limit"] = $scope.create_predict.th2_limit.value;
            add_predict_arg["th2_timeout"] = $scope.create_predict.th2_timeout.value;
            add_predict_arg["th2_co_id"] = $scope.create_predict.co2_string.value;

            add_predict_arg["th3_device_id"] = $scope.create_predict.th3_device.value;
            add_predict_arg["th3_compare_id"] = $scope.create_predict.th3_compare.value;
            add_predict_arg["th3_num"] = $scope.create_predict.th3_num.value;
            add_predict_arg["th3_limit"] = $scope.create_predict.th3_limit.value;
            add_predict_arg["th3_timeout"] = $scope.create_predict.th3_timeout.value;
            add_predict_arg["th3_co_id"] = $scope.create_predict.co3_string.value;

            add_predict_arg["th4_device_id"] = $scope.create_predict.th4_device.value;
            add_predict_arg["th4_compare_id"] = $scope.create_predict.th4_compare.value;
            add_predict_arg["th4_num"] = $scope.create_predict.th4_num.value;
            add_predict_arg["th4_limit"] = $scope.create_predict.th4_limit.value;
            add_predict_arg["th4_timeout"] = $scope.create_predict.th4_timeout.value;

            FaultPredict.save(add_predict_arg, function(data){
                if(data.status==200){
                    $modalInstance.close("OK");
                }else if(data.status==400){
                    alert(data.notice);
                }
            });
        };

        $scope.showCreatePredictTable = function(data){
            var show_flag = false;

            if(data.value != 0){
                show_flag = true;
            }

            return show_flag;
        };

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
    });