/**
 * Created by ksm on 2014-12-11.
 */
'use strict';

angular.module('signageApp')
    .controller('DelCtrl', function ($scope, $modalInstance, $location, del_type, $route, DeviceGroup, UserGroup, Device_Manage, Display_Manage, Manager, SILENT_INSTALL, Admin, Modal, key, FaultPredict) {

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function(){
            // [ Case by ] delete Admin
            if(del_type == 'ADMIN'){
                var argument = {};
                argument['change_manager_id'] = key;
                Admin.remove(argument, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                });
            }

            // [ Case by ] delete Manager
            if(del_type == 'MANAGER'){
                var argument = {};
                argument['change_manager_id'] = key;
                Admin.remove(argument, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                });
            }

            // [ Case by ] delete Member
            if(del_type == 'MEMBER'){
                var argument = {};
                argument['change_member_id'] = key;
                Manager.remove(argument, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                });
            }

            // [ Case by ] delete Device Group
            if(del_type == 'DG'){
                DeviceGroup.remove({key: key}, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                });
            }

            // [ Case by ] delete Device Group
            if(del_type == 'UG'){
                UserGroup.remove({key: key}, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                });
            }

            // [ Case by ] delete Device
            if(del_type == 'Device'){
                Device_Manage.remove({key: key}, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                });
            }

            // [ Case by ] delete Display
            if(del_type == 'Display'){
                Display_Manage.remove({key: key}, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                });
            }

            if(del_type == 'Install'){
                SILENT_INSTALL.remove({install_id: key}, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                })
            }

            if(del_type == 'FaultPredict'){
                FaultPredict.remove({key: key}, function(data){
                    if(data.status==200){
                        $modalInstance.dismiss('cancel');
                        $route.reload();
                    }else{
                        alert(data.notice);
                    }
                })
            }
        };
    });