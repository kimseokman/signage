/**
 * Created by ksm on 2015-05-11.
 */
'use strict';

angular.module('signageApp')
    .controller('LoginImageCtrl', function($scope){
        var login_image_root = "/img/login/";
        //var login_image_root = "./images/login/";
        $scope.myInterval = 5000;
        var slides = $scope.slides = [];
        $scope.addSlide = function (imgsrc) {
            slides.push({
                image: imgsrc,
                text: ''
            });
        };

        $scope.addSlide(login_image_root + 'main_v1.png');
        $scope.addSlide(login_image_root + 'main_v2.png');
        $scope.addSlide(login_image_root + 'main_v3.png');
        $scope.addSlide(login_image_root + 'main_v4.png');
        $scope.addSlide(login_image_root + 'main_v5.png');
    });
