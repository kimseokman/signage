/**
 * Created by ksm on 2014-09-22.
 */

'use strict';

angular.module('signageApp')
    .controller('ReportsCtrl', function ($scope, $location, AuthService, $filter, User, Admin, SharedService) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        $scope.isAuthenticated = AuthService.isAuthenticated();
        $scope.$parent.updateInformation();

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });

        //--------------------------------------------------------------------------------------------------------------
        // Report Nav
        //--------------------------------------------------------------------------------------------------------------
        $scope.reports_nav_menu = [
            {
                title: 'Top 10',
                icon: 'fa-list-alt',
                link_tag: 'reports/top10',
                link_type: 'static',
                a_href: './#/reports/top10'
            },
            {
                title: 'Incident List',
                icon: 'fa-ticket',
                link_tag: 'reports/incident',
                link_type: 'static',
                a_href: './#/reports/incident'
            },
            {
                title: 'Statistics',
                icon: 'fa-pie-chart',
                link_tag: 'reports/stats',
                link_type: 'static',
                a_href: './#/reports/stats'
            },
            {
              title: 'Summary',
              icon: 'fa-table',
              link_tag: 'reports/summary',
              link_type: 'static',
              a_href: './#/reports/summary'
            }
        ];

        $scope.onActiveMenu = function(current_uri, link_tag, click_flag){
            if(!current_uri) return false;

            if(current_uri.indexOf(link_tag) != -1 || click_flag){
                return true;
            }else{
                return false;
            }
        };

        $scope.currentURI = function () {
            var fullUrl = $location.absUrl();

            var link_tag = '';

            if (fullUrl.indexOf("reports/history") != -1){
                link_tag = "reports/history";
                return link_tag;
            }else if (fullUrl.indexOf("reports/top10") != -1){
                link_tag = "reports/top10";
                return link_tag;
            }else if(fullUrl.indexOf("reports/incident") != -1){
                link_tag = "reports/incident";
                return link_tag;
            }else if(fullUrl.indexOf("reports/stats") != -1){
                link_tag = "reports/stats";
                return link_tag;
            }else if(fullUrl.indexOf("reports/summary") != -1){
                link_tag = "reports/summary";
                return link_tag;
            }
        };
        $scope.onClickMenu = function(menu){
            if ($scope.selectedMenu && $scope.selectedMenu != menu) $scope.selectedMenu.clicked = false;
            menu.clicked = !menu.clicked;
            $scope.selectedMenu = menu;
        };

        //--------------------------------------------------------------------------------------------------------------
        // mobile reports_nav
        //--------------------------------------------------------------------------------------------------------------
        $scope.mobile_reports_nav_menu = [
            {
              title: 'Top 10',
              icon: 'fa-list-alt',
              link_tag: 'm.reports/top10',
              link_type: 'static',
              a_href: './#/m.reports/top10'
            }/*,
            {
              title: 'Incident List',
              icon: 'fa-ticket',
              link_tag: 'm.reports/incident',
              link_type: 'static',
              a_href: './#/m.reports/incident'
            }*/
        ];

        $scope.mobile_currentURI = function () {
            var fullUrl = $location.absUrl();

            var link_tag = '';

            if (fullUrl.indexOf("m.reports/top10") != -1){
              link_tag = "m.reports/top10";
              return link_tag;
            }else if(fullUrl.indexOf("m.reports/incident") != -1){
              link_tag = "m.reports/incident";
              return link_tag;
            }
        };
    });
