/**
 * Created by ksm on 2014-12-29.
 */

'use strict';

angular.module('signageApp')
    .controller('ReportsTop10Ctrl', function ($scope, $rootScope, $location, SharedService, TOP10) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });

        $scope.usage_headers = [
            {name: 'Name', class_name: 'th_name'},
            {name: 'Usage', class_name: 'th_usage'},
            {name: 'Action', class_name: 'th_action'}
        ];

        $scope.temp_headers = [
            {name: 'Name', class_name: 'th_name'},
            {name: 'Temp.', class_name: 'th_temp'},
            {name: 'Action', class_name: 'th_action'}
        ];

        $scope.top10_setting = {
            search_time_range: 3,
            statuses : [
                {value: 3, text: '3 hours'},
                {value: 6, text: '6 hours'},
                {value: 12, text: '12 hours'},
                {value: 24, text: '24 hours'}
            ]
        };

        //--------------------------------------------------------------------------------------------------------------
        // Data
        //--------------------------------------------------------------------------------------------------------------
        $scope.$watch('top10_setting.search_time_range', function(){
            var argument = {};
            argument['type'] = 'mp_cpu_usage';
            argument['time_range'] = $scope.top10_setting.search_time_range;

            TOP10.get(argument, function(data){
                $scope.cpu_use = data.objects;
            });

            var argument = {};
            argument['type'] = 'mp_mem_usage';
            argument['time_range'] = $scope.top10_setting.search_time_range;

            TOP10.get(argument, function(data){
                $scope.memory_use = data.objects;
            });

            var argument = {};
            argument['type'] = 'mp_hdd_usage';
            argument['time_range'] = $scope.top10_setting.search_time_range;

            TOP10.get(argument, function(data){
                $scope.disk_use = data.objects;
            });

            var argument = {};
            argument['type'] = 'mp_cpu_temp';
            argument['time_range'] = $scope.top10_setting.search_time_range;

            TOP10.get(argument, function(data){
                $scope.cpu_state = data.objects;
            });

            var argument = {};
            argument['type'] = 'dp_temp';
            argument['time_range'] = $scope.top10_setting.search_time_range;

            TOP10.get(argument, function(data){
                $scope.device_state = data.objects;
            });
        });

        //--------------------------------------------------------------------------------------------------------------
        // Func
        //--------------------------------------------------------------------------------------------------------------

        $scope.tempConverter = function(to_temp_flag, temp){
            var F = 0.0;
            var C = 0.0;
            if(to_temp_flag == 'F'){//to fahrenheit ℉
                F = temp * 9.0 / 5.0 + 32;
                return F;
            }else if(to_temp_flag == 'C'){//to celsius ℃
                C = (temp - 32) * 5.0 / 9.0;
                return C;
            }
        };

        //--------------------------------------------------------------------------------------------------------------
        // Exception Handling
        //--------------------------------------------------------------------------------------------------------------
        $scope.status_type_NA = function(input_value){
            if(input_value == undefined || input_value == '' || input_value == 'None'){
                return true;
            }
            return false;
        };

        $scope.status_type_normal = function(input_value){
            if(input_value != undefined && input_value != '' && input_value != 'None'){
                return true;
            }
            return false;
        };

        $scope.status_type_zero_NA = function(input_value){
            if(input_value == undefined || input_value == '' || input_value == 'None' || input_value == 0){
                return true;
            }
            return false;
        };

        $scope.status_type_zero = function(input_value){
            if(input_value != undefined && input_value != '' && input_value != 'None' && input_value != 0){
                return true;
            }
            return false;
        };

        $scope.moveToLiveFormTop10 = function(mp_serial_number, dp_serial_number){
            if(!dp_serial_number){
                var change_uri = '/live/media-players/'+mp_serial_number;
            }else{
                var change_uri = '/live/media-players/'+mp_serial_number+'/displays/'+dp_serial_number;
            }

            $location.path(change_uri, true);
        };

        $scope.mobile_moveToLiveFormTop10 = function(mp_serial_number, dp_serial_number){
            if(!dp_serial_number){
                var change_uri = '/m.live_status/media-players/'+mp_serial_number;
            }else{
                var change_uri = '/m.live_status/media-players/'+mp_serial_number+'/displays/'+dp_serial_number;
            }

            $location.path(change_uri, true);
        };
    });
