/**
 * Created by ksm on 2015-04-09.
 */
'use strict';

angular.module('signageApp')
    .controller('StatsDPModalCtrl', function ($scope, ENV, $filter, $modalInstance, $location, dp_list, StatsDPList, EXPORT_EXCEL, $window, ngTableParams, $timeout){
        $scope.malfunc_dp_list = dp_list;

        $scope.malfuncDPTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "S/N", className: "sn", sortable: true, field: "serial_number"},
            {name: "Alias", className: "alias", sortable: false},
            {name: "Device Group", className: "dg", sortable: false},
            {name: "Account", className: "account", sortable: false},
            {name: "Location", className: "location", sortable: false},
            {name: "Shop", className: "shop", sortable: false},
            {name: "Live Status", className: "live", sortable: false},
            {name: "Incidents", className: "incident", sortable: false}
        ];

        $scope.malfuncDPTableCol = $scope.malfuncDPTableHeader.length;//device table head col count
        $scope.malfuncDPTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.malfuncDPTableLimit = $scope.malfuncDPTableLimits[0];//set default limit line
        $scope.malfunc_dp_list_array = [];
        angular.forEach($scope.malfunc_dp_list, function(v, k){
          $scope.malfunc_dp_list_array.push(v.dp_id);
        });
        if($scope.malfuncDPTable == undefined){
            $scope.malfuncDPTable = new ngTableParams({//get device
                page: 1,//show first page
                count: $scope.malfuncDPTableLimit.value,//count per page
                sorting: {}//initial sorting
            }, {
                total: 0,// length of data
                getData: function ($defer, params) {
                    var malfunc_arg = {};
                    malfunc_arg['limit'] = params.count();
                    malfunc_arg['offset'] = (params.page() - 1) * malfunc_arg['limit'];


                    malfunc_arg["dp_list"] = $scope.malfunc_dp_list_array;
                    var sorting_order = 'desc';//default order
                    var sorting_by = 'alias';//default by

                    var sorting = params.sorting();
                    for (var k in sorting) {
                        var v = sorting[k];

                        sorting_order = v;
                        sorting_by = k;
                    }

                    malfunc_arg['order'] = sorting_order;
                    malfunc_arg['by'] = sorting_by;
                    malfunc_arg['pagination'] = 'pagination';
                    malfunc_arg['excel_create'] = $scope.IsIE8nIE9();

                    StatsDPList.get(malfunc_arg, function(data){
                        $timeout(function () {
                            $scope.malfuncDP_set = data.objects;
                            params.total(data.meta.total_count);
                            $defer.resolve($scope.malfuncDP_set);
                        });
                    });
                }
            });
        }else{
            $scope.malfuncDPTable.reload();
        }

        $scope.IsIE8nIE9 = function() {
            if (navigator.appVersion.indexOf("MSIE 9") != -1) {
                return true;
            } else if (navigator.appVersion.indexOf("MSIE 8") != -1) {
                return true;
            } else if (navigator.userAgent.indexOf("MSIE 7") > 0 && navigator.appVersion.indexOf("Trident") != -1) {
                return true;
            } else {
                return false;
            }
        };

        /* DPtableList Excel */
        var malfunc_arg = {};

        malfunc_arg['limit'] = 0;
        malfunc_arg['offset'] = 0;
        malfunc_arg["dp_list"] = $scope.malfunc_dp_list_array;
        var sorting_order = 'desc';//default order
        var sorting_by = 'alias';//default by

        malfunc_arg['limit'] = 0;
        malfunc_arg['offset'] = 0;
        malfunc_arg['order'] = sorting_order;
        malfunc_arg['by'] = sorting_by;
        malfunc_arg['pagination'] = 'normal';
        malfunc_arg['excel_create'] = $scope.IsIE8nIE9();

        StatsDPList.get(malfunc_arg, function(data){
            $scope.malfuncDP_set_excel = data.objects;
            var value = data.objects;

            //$scope.excel_host = "https://lgedev.adamssuite.com/";
            $scope.excel_host = ENV.host;
            $scope.excel_url = "/download/excel/" + value[0].userID;
            $scope.excel_DP_file = "/Display_Fault_Detail-" + value[0].create_date + ".xlsx";
            $scope.excel_DP_down_url = $scope.excel_host + $scope.excel_url + $scope.excel_DP_file;

            $scope.ie8_DP_excel_create = true;
            /*
            var isIE = false;
            if(navigator.appVersion.indexOf("MSIE 9") != -1){
              isIE = true;
            }else if(navigator.appVersion.indexOf("MSIE 8") != -1){
              isIE = true;
            } else if(navigator.userAgent.indexOf("MSIE 7") > 0 && navigator.appVersion.indexOf("Trident") != -1){
              isIE = true;
            } else {
              isIE = false;
            }
            //creat excel
            if(isIE){
              $timeout(function() {
                angular.element('#excel_ie8_DP').triggerHandler('click');
              }, 500);
            }
            */
        });

        $scope.moveLivePage = function(mp_serial_number, dp_serial_number){
            var change_uri = 'live/media-players/' + mp_serial_number + '/displays/' + dp_serial_number;

            $location.path(change_uri, true);

            $modalInstance.dismiss('cancel');
        };

        $scope.moveIncidentPage = function(mp_serial_number, dp_serial_number){
            var change_uri = 'reports/incident';

            $location.search({mp_serial_number: null, dp_serial_number: null});
            $location.path(change_uri, true).search({dp_serial_number: dp_serial_number});

            $modalInstance.dismiss('cancel');
        };

        $scope.cancelStatsDPModal = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.exportToExcel_DP = function(){
            var blob = new Blob([document.getElementById('export_excel_dp_fault').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            var today = new Date();
            var date = $filter('date')(today, 'yyyy-MM-dd');

            saveAs(blob, "Display_Fault_Detail-" + date + ".xls");
        };
        /*
        $scope.createExcelSheet_DP = function(objArray){
            $scope.parse_objArray = [];
            $scope.arr_index = 1;
            angular.forEach(objArray, function(v, k){
                $scope.parse_objArray.push(
                    {
                        "No": $scope.arr_index,
                        "S/N": v.serial_number,
                        "Alias": v.alias,
                        "Account": v.account,
                        "Location": v.location,
                        "Shop": v.shop,
                        "Live Status": "View",
                        "Incidents": "View",
                        "Search_Type": "DP_Detail"
                    }
                );
                $scope.arr_index++;
            });

            EXPORT_EXCEL.save($scope.parse_objArray, function(data){
                if(data.status == 200) {
                    var value = data.objects;
                    //$scope.excel_host = "https://lgedev.adamssuite.com/";
                    $scope.excel_host = ENV.host;
                    $scope.excel_url = "/download/excel/" + value.userID;
                    $scope.excel_DP_file = "/Display_Fault_Detail-" + value.create_date + ".xlsx";
                    $scope.excel_DP_down_url = $scope.excel_host + $scope.excel_url + $scope.excel_DP_file;

                    $scope.ie8_DP_excel_create = true;
                }
            });
        };
        */

        $scope.JSONToCSVConvertor = function(JSONData, fileName, ShowLabel){
          var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
          var CSV = '';
          if (ShowLabel) {
            var row = "";
            for (var index in arrData[0]) {
              row += index + ',';
            }
            row = row.slice(0, -1);
            CSV += row + '\r\n';
          }

          for (var i = 0; i < arrData.length; i++) {
            var row = "";
            for (var index in arrData[i]) {
              var arrValue = arrData[i][index] == null ? "" : '="' + arrData[i][index] + '"';
              row += arrValue + ',';
            }
            row.slice(0, row.length - 1);
            CSV += row + '\r\n';
          }

          if (CSV == '') {
            alert("Invalid data");
            return;
          }

          var fileName = "Display Fault Detail";

          var IEwindow = $window.open();

          IEwindow.document.write('sep=,\r\n' + CSV);
          IEwindow.document.close();
          IEwindow.document.execCommand('SaveAs', true, fileName + ".csv");
          IEwindow.close();
        };
    });
