/**
 * Created by ksm on 2014-12-29.
 */

'use strict';

angular.module('signageApp')
    .controller('ReportsIncidentCtrl', function ($scope, Incident, User, $location, SharedService, ngTableParams, $timeout, StatsSetting) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        User.get().$promise.then(function (data) {
            if(data.status==200) {
                $scope.userType = data.objects[0].userType;
                $scope.userID = data.objects[0].userID;
                $scope.userAuth = data.objects[0].auth;
            }
        });

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });

        $scope.incident_bases = [
            {name: "Account", class_name:"account", id: "account", value: "account"},
            {name: "Device Group", class_name:"dg", id: "dg", value: "dg"}
        ];

        $scope.incident_base = {value: "account"};

        $scope.incident_select = {
            manager: {name: "Manager", id:"select_manager", active: false},
            account: {name: "Account", id:"select_account", active: false},
            region: {name: "Location", id:"select_region", active: false},
            shop: {name: "Shop", id:"select_shop", active: false},
            mp: {name: "MediaPlayer", id:"select_mp", active: false},
            dg: {name: "Device Group", id:"select_dg", active: false},
            dg_mp: {name: "MediaPlayer", id:"select_dg_mp", active: false}
        };

        /* incident */
        $scope.incident_headers = [
            {name: 'No.', class_name: 'no', sortable: false},
            {name: 'Generated', class_name: 'create', sortable: true, field: 'created'},
            {name: 'Category', class_name: 'category', sortable: true, field: 'error_code'},
            {name: 'Incident Message', class_name: 'msg', sortable: false},
            {name: 'Severity', class_name: 'level', sortable: true, field: 'level'},
            {name: 'Action', class_name: 'act', sortable: false}
        ];

        $scope.$watch('userType', function(newValue, oldValue){
            $scope.userType = newValue;

            if($scope.userType == "ADMIN"){
                var search_manager_arg = {};
                search_manager_arg["target"] = "manager";
                StatsSetting.get(search_manager_arg, function(data){
                    if(data.status == 200){
                        $scope.search_managers = data.objects;
                        $scope.search_manager = $scope.search_managers[0];
                        $scope.incident_select.manager.active = true;

                        $scope.search_manager_update($scope.search_manager);
                    }
                });
            }
        });

        $scope.$watch('incident_base.value', function(newValue, oldValue){
            $scope.incident_base.value = newValue;

            if($scope.incident_base.value == "account"){
                $scope.incident_select.dg.active = false;
                $scope.incident_select.dg_mp.active = false;
                $scope.incident_select.account.active = true;
                $scope.incident_select.region.active = true;
                $scope.incident_select.shop.active = true;
                $scope.incident_select.mp.active = true;
            }else if($scope.incident_base.value == "dg"){
                $scope.incident_select.dg.active = true;
                $scope.incident_select.dg_mp.active = true;
                $scope.incident_select.account.active = false;
                $scope.incident_select.region.active = false;
                $scope.incident_select.shop.active = false;
                $scope.incident_select.mp.active = false;
            }

            $scope.search_manager_update($scope.search_manager);
        });

        $scope.search_manager_update = function(manager_info){
            $scope.search_manager = manager_info;

            var search_account_or_dg_arg = {};
            search_account_or_dg_arg["target"] = $scope.incident_base.value;

            if($scope.search_manager != undefined && $scope.userType == "ADMIN"){
                search_account_or_dg_arg["manager_id"] = $scope.search_manager.id;
            }else{
                search_account_or_dg_arg["manager_id"] = "All";
            }

            StatsSetting.get(search_account_or_dg_arg, function(data){
                if(data.status == 200){
                    if($scope.incident_base.value == "account"){
                        $scope.search_accounts = data.objects;
                        $scope.search_account = $scope.search_accounts[0];
                        $scope.incident_select.account.active = true;

                        $scope.search_account_update($scope.search_account);
                    } else if($scope.incident_base.value == "dg"){
                        $scope.search_dgs = data.objects;
                        $scope.search_dg = $scope.search_dgs[0];
                        $scope.incident_select.dg.active = true;

                        $scope.search_dg_update($scope.search_dg);
                    }
                }
            });
        };

        $scope.search_account_update = function(account_info){
            $scope.search_account = account_info;

            var search_region_arg = {};
            search_region_arg["target"] = "region";
            search_region_arg["site_name"] = $scope.search_account.name;
            StatsSetting.get(search_region_arg, function(data){
                if(data.status == 200){
                    $scope.search_regions = data.objects;
                    $scope.search_region = $scope.search_regions[0];
                    $scope.incident_select.region.active = true;

                    $scope.search_region_update($scope.search_region);
                }
            });
        };

        $scope.search_region_update = function(region_info){
            $scope.search_region = region_info;

            var search_shop_arg = {};
            search_shop_arg["target"] = "shop";
            search_shop_arg["region_name"] = $scope.search_region.name;
            StatsSetting.get(search_shop_arg, function(data){
                if(data.status == 200){
                    $scope.search_shops = data.objects;
                    $scope.search_shop = $scope.search_shops[0];
                    $scope.incident_select.shop.active = true;

                    $scope.search_shop_update($scope.search_shop);
                }
            });
        };

        $scope.search_shop_update = function(shop_info){
            $scope.search_shop = shop_info;

            var search_mp_arg = {};
            search_mp_arg["target"] = "mp";
            search_mp_arg["shop_name"] = $scope.search_shop.name;
            StatsSetting.get(search_mp_arg, function(data){
                if(data.status == 200){
                    $scope.search_mps = data.objects;
                    $scope.search_mp = $scope.search_mps[0];
                    $scope.incident_select.mp.active = true;

                    $scope.search_mp_update($scope.search_mp);
                }
            });
        };

        $scope.search_mp_update = function(mp_info){
            $scope.search_mp = mp_info
        };

        $scope.search_dg_update = function(dg_info){
            $scope.search_dg = dg_info;

            var search_mp_arg = {};
            search_mp_arg["target"] = "dg_mp";
            search_mp_arg["dg_name"] = $scope.search_dg.name;

            StatsSetting.get(search_mp_arg, function(data){
                if(data.status == 200){
                    $scope.search_dg_mps = data.objects;
                    $scope.search_dg_mp = $scope.search_dg_mps[0];
                    $scope.incident_select.dg_mp.active = true;

                    $scope.search_dg_mp_update($scope.search_dg_mp);
                }
            });
        };

        $scope.search_dg_mp_update = function(mp_info){
            $scope.search_dg_mp = mp_info;
        };

        var params = $location.search();
        $scope.order = params['order'] || '-';
        $scope.order_by = params['order_by'] || 'created'; //default order by

        var parameters = {
            page: 1,
            count: 20,
            sorting: {}
        };

        var order_by = $scope.order_by;
        var str_order = '';
        if($scope.order == '-'){
            str_order = 'desc';
        }else{
            str_order = 'asc';
        }

        parameters['sorting'][order_by] = str_order;

        var query_params = {};
        if(params['mp_serial_number']){
            query_params['mp_serial_number'] = (params['mp_serial_number']);
        }

        if(params['dp_serial_number']){
            query_params['dp_serial_number'] = (params['dp_serial_number']);
        }

        $scope.incidentTableParams = new ngTableParams(parameters, {
            counts: [],
            total: 0,// length of data
            getData: function ($defer, params) {
                var api_params = {};
                api_params['limit'] = params.count();
                api_params['offset'] = (params.page() - 1) * api_params['limit'];

                var sorting = params.sorting();
                for (var key in sorting) {
                    var value = sorting[key];
                    if (value == 'desc') {
                        api_params['order'] = '-';
                    }

                    api_params['order_by'] = key;
                }

                api_params["base"] = $scope.incident_base.value;
                if($scope.search_manager){
                    if($scope.userType == "ADMIN"){
                        api_params['manager_id'] = $scope.search_manager.id;
                    }else{
                        api_params['manager_id'] = "All";
                    }
                }

                if(api_params["base"] == "account"){
                    if($scope.search_account){
                        api_params["account"] = $scope.search_account.name;
                    }
                    if($scope.search_region){
                        api_params["region"] = $scope.search_region.name;
                    }
                    if($scope.search_shop){
                        api_params["shop"] = $scope.search_shop.name;
                    }
                    if($scope.search_mp){
                        api_params["mp_sn"] = $scope.search_mp.serial_number;
                    }
                }else if(api_params["base"] == "dg"){
                    if($scope.search_dg){
                        api_params["dg_id"] = $scope.search_dg.id;
                    }
                    if($scope.search_dg_mp){
                        api_params["dg_mp_sn"] = $scope.search_dg_mp.serial_number;
                    }
                }

                if(query_params['mp_serial_number']){
                    api_params["mp_serial_number"] = query_params['mp_serial_number'];
                }
                if(query_params['dp_serial_number']){
                    api_params["dp_serial_number"] = query_params['dp_serial_number'];
                }

                Incident.get(api_params, function(data){
                    $timeout(function () {
                        $scope.dataSet = data.objects;
                        params.total(data.meta.total_count);
                        $defer.resolve($scope.dataSet);
                    }, 500);
                });
            }
        });

        $scope.searchDetail = function(){
            $location.search('mp_serial_number', null);
            $location.search('dp_serial_number', null);

            if($scope.incidentTableParams == undefined){
                var params = $location.search();
                $scope.order = params['order'] || '-';
                $scope.order_by = params['order_by'] || 'created'; //default order by

                var parameters = {
                    page: 1,
                    count: 20,
                    sorting: {}
                };

                var order_by = $scope.order_by;
                var str_order = '';
                if($scope.order == '-'){
                    str_order = 'desc';
                }else{
                    str_order = 'asc';
                }

                parameters['sorting'][order_by] = str_order;

                $scope.incidentTableParams = new ngTableParams(parameters, {
                    counts: [],
                    total: 0,// length of data
                    getData: function ($defer, params) {
                        var api_params = {};
                        api_params['limit'] = params.count();
                        api_params['offset'] = (params.page() - 1) * api_params['limit'];

                        var sorting = params.sorting();
                        for (var key in sorting) {
                            var value = sorting[key];
                            if (value == 'desc') {
                                api_params['order'] = '-';
                            }

                            api_params['order_by'] = key;
                        }

                        api_params["base"] = $scope.incident_base.value;
                        if($scope.search_manager){
                            if($scope.userType == "ADMIN"){
                                api_params['manager_id'] = $scope.search_manager.id;
                            }else{
                                api_params['manager_id'] = "All";
                            }
                        }

                        if(api_params["base"] == "account"){
                            if($scope.search_account){
                                api_params["account"] = $scope.search_account.name;
                            }
                            if($scope.search_region){
                                api_params["region"] = $scope.search_region.name;
                            }
                            if($scope.search_shop){
                                api_params["shop"] = $scope.search_shop.name;
                            }
                            if($scope.search_mp){
                                api_params["mp_sn"] = $scope.search_mp.serial_number;
                            }
                        }else if(api_params["base"] == "dg"){
                            if($scope.search_dg){
                                api_params["dg_id"] = $scope.search_dg.id;
                            }
                            if($scope.search_dg_mp){
                                api_params["dg_mp_sn"] = $scope.search_dg_mp.serial_number;
                            }
                        }

                        Incident.get(api_params, function(data){
                            $timeout(function () {
                                $scope.dataSet = data.objects;
                                params.total(data.meta.total_count);
                                $defer.resolve($scope.dataSet);
                            }, 500);
                        });
                    }
                });
            }else{
                $scope.incidentTableParams.reload();
            }
        };

        $scope.moveToLiveFormIncident = function(incident){
            var change_uri = '/live';
            if(incident.type == 0){//case mediaplayer
                change_uri += '/media-players/'+ incident.mp_serial_number;
            }else{//case display
                change_uri += '/media-players/'+ incident.mp_serial_number + "/displays/" + incident.dp_serial_number;
            }

            $location.path(change_uri, true);
        };

        $scope.codeToCategory = function(code){
            var message = "";
            switch(code) {
                case 'R01':
                case 'R02':
                case 'R11':
                case 'R12':
                case 'R13':
                case 'R14':
                case 'O01':
                  message = "Proof of Play";
                  break;
                // [ Case by ] Orange
                case 'O02':
                case 'O03':
                  message = "Temperature";
                  break;
                case 'O04':
                case 'O05':
                case 'O06':
                case 'O07':
                case 'O08':
                case 'O09':
                  message = "Prediction";
                  break;
                // [ Case by ] Yellow
                case 'Y01':
                case 'Y02':
                case 'Y03':
                case 'Y04':
                  message = "Heavy load";
                  break;
                case 'Y05':
                case 'Y06':
                case 'Y07':
                case 'Y08':
                  message = "Misconfiguration";
                  break;
            }

            return message;
        };

        $scope.showMessage = function(incident){
            var message = 'Empty';

            switch(incident.error_code){
                // [ Case by ] Red
                case 'R01':
                    message = "Media Player '" + incident.alias + "' is Disconnected";
                    break;
                case 'R02':
                    message = "Display '" + incident.alias + "' is Disconnected";
                    break;
                case 'R11':
                    message = "Core Application 1 on '" + incident.alias + "' is Stopped";
                    break;
                case 'R12':
                    message = "Core Application 2 on '" + incident.alias + "' is Stopped";
                    break;
                case 'R13':
                    message = "Core Application 3 on '" + incident.alias + "' is Stopped";
                    break;
                case 'R14':
                    message = "Core Application 4 on '" + incident.alias + "' is Stopped";
                    break;
                // [ Case by ] Orange
                case 'O01':
                    message = "Check required the video cable of '" + incident.alias + "'";
                    break;
                case 'O02':
                    message = "CPU temperature on '" + incident.alias + "' is too high";
                    break;
                case 'O03':
                    message = "Display temperature on '" + incident.alias + "' is too high";
                    break;
                case 'O04':
                    message = "The probability that Media Player '" + incident.alias + "' will be down within a week";
                    break;
                case 'O05':
                    message = "The probability that Media Player '" + incident.alias + "' will be down within 24 hours";
                    break;
                case 'O06':
                    message = "The probability that Display '" + incident.alias + "' will be down within a week";
                    break;
                case 'O07':
                    message = "The probability that Display '" + incident.alias + "' will be down within 24 hours";
                    break;
                case 'O08':
                    message = "The probability that CPU temperature on '" + incident.alias + "' will be too high within 24 hours";
                    break;
                case 'O09':
                    message = "The probability that Display temperature on '" + incident.alias + "' will be too high within 24 hours";
                    break;
                // [ Case by ] Yellow
                case 'Y01':
                    message = "CPU usage of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y02':
                    message = "Memory usage of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y03':
                    message = "Disk usage of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y04':
                    message = "Network response time of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y05':
                    message = "'Display Energy Saving' value of '" + incident.alias + "' is enabled";
                    break;
                case 'Y06':
                    message = "'Sleep Time' value of '" + incident.alias + "' is enabled";
                    break;
                case 'Y07':
                    message = "'Auto Off' value of '" + incident.alias + "' is enabled";
                    break;
                case 'Y08':
                    message = "'4 Hours Off' value of '" + incident.alias + "' is enabled";
                    break;
            }

            return message;
        };

        $scope.showSeverity = function(severity){
            if(severity == 0){
                return "LOW";
            }else if(severity == 1){
                return "MEDIUM";
            }else if(severity == 2){
                return "HIGH";
            }else{
                return severity;
            }
        };

        $scope.if_select = function(select_id, select_info){
            var select_flag = true;

            if(select_id == "select_manager"){
                if(select_info == undefined){
                    if($scope.userType != "ADMIN"){
                        select_flag = false;
                    }else{
                        select_flag = true;
                    }
                }else{
                    if(select_info.id != "All"){
                        select_flag = false;
                    }
                }
            }else{
                if(select_info == undefined)    return select_flag;

                if(select_info.id != "All"){
                    select_flag = false;
                }else{
                    select_flag = true;
                }
            }

            return select_flag;
        };
    });
