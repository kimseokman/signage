/**
 * Created by ksm on 2014-12-29.
 */

'use strict';

angular.module('signageApp')
    .controller('ReportsStatsCtrl', function ($scope, ENV, $filter, $http, $window, $sce, User, SharedService, EXPORT_EXCEL, Display, $location, $timeout, StatsSetting, StatsDP, Session, StatsMP, StatsDetail, Modal, StatsRatio) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        User.get().$promise.then(function (data) {
            if(data.status==200) {
                $scope.userType = data.objects[0].userType;
                $scope.userID = data.objects[0].userID;
                $scope.userAuth = data.objects[0].auth;
            }
        });

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });

        $scope.isIE8_ko = function(){
            var active_flag = true;

            if(navigator.appVersion.indexOf("MSIE 8") != -1){
                if(navigator.browserLanguage.indexOf("ko") != -1){
                    active_flag = true;
                }else{
                    active_flag = false;
                }
            } else if(navigator.userAgent.indexOf("MSIE 7") > 0 && navigator.appVersion.indexOf("Trident") != -1){
                if(navigator.browserLanguage.indexOf("ko") != -1){
                    active_flag = true;
                }else{
                    active_flag = false;
                }
            }
            return active_flag;
        };

        $scope.report_search_flag = false;
        $scope.report_search_prevent = true;
        $scope.report_stats_mp_active = false;
        $scope.report_stats_dp_active = false;
        $scope.stats_total_ratio_flag = false;
        $scope.ie8_excel_create = false;

        $scope.title = {
            start_date: "",
            end_date: "",
            userID: "",
            account: "",
            dg: ""
        };

        $scope.stats_periods = [
            {name: "Daily", class_name: "daily", id: "daily", value: "daily"},
            {name: "Weekly", class_name: "weekly", id: "weekly", value: "weekly"},
            {name: "Monthly", class_name: "monthly", id: "monthly", value: "monthly"}
        ];

        var today = new Date();
        //var yesterday = new Date(today.setDate(today_d.getDate() - 1));

        $scope.max_start_date = $filter('date')(today, 'yyyy-MM-dd');
        $scope.start_date = $filter('date')(today, 'yyyy-MM-dd');
        $scope.watch_start_date = $filter('date')(today, 'yyyy-MM-dd');
        $scope.end_date = $filter('date')(today, 'yyyy-MM-dd');

        $scope.stats_period = {name:"Daily", value: "daily"};

        $scope.$watch("stats_period.value", function(newVal, oldVal){
            if(newVal == oldVal){
                return;
            }

            $scope.stats_period.value = newVal;

            var today = new Date();

            var start_day = $filter('date')(today, 'yyyy-MM-dd');

            var today = new Date();
            var today_yyyy = $filter('date')(today, 'yyyy');
            var today_mm = $filter('date')(today, 'MM');
            var today_dd = $filter('date')(today, 'dd');

            var start_date = null;
            var end_date = null;
            if($scope.stats_period.value == "daily"){
                start_date = $filter('date')(start_day, 'yyyy-MM-dd');
                end_date = $filter('date')(start_date, 'yyyy-MM-dd');

            }else if($scope.stats_period.value == "weekly"){
                if(start_day.length != 10){
                    return;
                }

                start_date = $filter('date')($scope.getWeekFirstDay(start_day), 'yyyy-MM-dd');

                var start_yyyy = $filter('date')($scope.getWeekFirstDay(start_day), 'yyyy');
                var start_mm = $filter('date')($scope.getWeekFirstDay(start_day), 'MM');
                var start_dd = $filter('date')($scope.getWeekFirstDay(start_day), 'dd');
                var end_dd = $filter('date')($scope.getWeekEndDay(start_day), 'dd');

                if(today_yyyy == start_yyyy){
                    if(today_mm == start_mm){
                        if(today_dd >= start_dd && today_dd <= end_dd){
                            end_date = $filter('date')(today, 'yyyy-MM-dd');
                        } else {
                            end_date = $filter('date')($scope.getWeekEndDay(start_day), 'yyyy-MM-dd');
                        }
                    }else{
                        end_date = $filter('date')($scope.getWeekEndDay(start_day), 'yyyy-MM-dd');
                    }
                }else{
                    end_date = $filter('date')($scope.getWeekEndDay(start_day), 'yyyy-MM-dd');
                }
            }else if($scope.stats_period.value == "monthly"){
                if(start_day.length != 10){
                    return;
                }
                start_date = $filter('date')($scope.getFirstDay(start_day), 'yyyy-MM-dd');
                var start_yyyy = $filter('date')($scope.getFirstDay(start_day), 'yyyy');
                var start_mm = $filter('date')($scope.getFirstDay(start_day), 'MM');

                if(today_yyyy == start_yyyy){
                    if(today_mm == start_mm){
                        end_date = $filter('date')(today, 'yyyy-MM-dd');
                    }else{
                        end_date = $filter('date')($scope.getMonthly(start_date), 'yyyy-MM-dd');
                    }
                }else{
                    end_date = $filter('date')($scope.getMonthly(start_date), 'yyyy-MM-dd');
                }
            }

            $scope.start_date = start_date;
            $scope.end_date = end_date;
        });

        $scope.modifyDate = function(start_date){

            var start_day = start_date;

            var today = new Date();
            var today_yyyy = $filter('date')(today, 'yyyy');
            var today_mm = $filter('date')(today, 'MM');
            var today_dd = $filter('date')(today, 'dd');

            var start_date = null;
            var end_date = null;
            if($scope.stats_period.value == "daily"){
                start_date = $filter('date')(start_day, 'yyyy-MM-dd');
                end_date = $filter('date')(start_date, 'yyyy-MM-dd');

            }else if($scope.stats_period.value == "weekly"){
                if(start_day.length != 10){
                    return;
                }

                start_date = $filter('date')($scope.getWeekFirstDay(start_day), 'yyyy-MM-dd');

                var start_yyyy = $filter('date')($scope.getWeekFirstDay(start_day), 'yyyy');
                var start_mm = $filter('date')($scope.getWeekFirstDay(start_day), 'MM');
                var start_dd = $filter('date')($scope.getWeekFirstDay(start_day), 'dd');
                var end_dd = $filter('date')($scope.getWeekEndDay(start_day), 'dd');

                if(today_yyyy == start_yyyy){
                    if(today_mm == start_mm){
                        if(today_dd >= start_dd && today_dd <= end_dd){
                            end_date = $filter('date')(today, 'yyyy-MM-dd');
                        } else {
                            end_date = $filter('date')($scope.getWeekEndDay(start_day), 'yyyy-MM-dd');
                        }
                    }else{
                        end_date = $filter('date')($scope.getWeekEndDay(start_day), 'yyyy-MM-dd');
                    }
                }else{
                    end_date = $filter('date')($scope.getWeekEndDay(start_day), 'yyyy-MM-dd');
                }
            }else if($scope.stats_period.value == "monthly"){
                if(start_day.length != 10){
                    return;
                }
                start_date = $filter('date')($scope.getFirstDay(start_day), 'yyyy-MM-dd');

                var start_yyyy = $filter('date')($scope.getFirstDay(start_day), 'yyyy');
                var start_mm = $filter('date')($scope.getFirstDay(start_day), 'MM');

                if(today_yyyy == start_yyyy){

                    if(today_mm == start_mm){
                        end_date = $filter('date')(today, 'yyyy-MM-dd');
                    }else{
                        end_date = $filter('date')($scope.getMonthly(start_date, true), 'yyyy-MM-dd');
                    }
                }else{
                    end_date = $filter('date')($scope.getMonthly(start_date), 'yyyy-MM-dd');
                }
            }

            $scope.start_date = start_date;
            $scope.end_date = end_date;
        };

        $scope.$watch("start_date", function(newVal, oldVal){});

        $scope.stats_bases = [
            {name: "Account", class_name:"account", id: "account", value: "account"},
            {name: "Device Group", class_name:"dg", id: "dg", value: "dg"}
        ];

        $scope.stats_base = {value: "account"};

        $scope.stats_select = {
            manager: {name: "Manager", id:"select_manager", active: false},
            account: {name: "Account", id:"select_account", active: false},
            region: {name: "Location", id:"select_region", active: false},
            shop: {name: "Shop", id:"select_shop", active: false},
            dg: {name: "Device Group", id:"select_dg", active: false}
        };
        //$scope.search_manager = null;
        //$scope.search_account = null;
        //$scope.search_region = null;
        //$scope.search_shop = null;

        $scope.stats_device = {
            mp: {name: "Media Player", id: "mp", value: true},
            dp: {name: "Display", id: "dp", value: true}
        };

        $scope.detailTableHeader = [
            {name: "", className: "day", sortable: false, unit: null},
            {name: "Type", className: "type", sortable: false, unit: null},
            {name: "Total", className: "total", sortable: false, unit: "EA"},
            {name: "Operation Ratio", className: "operation", sortable: false, unit: "%"},
            {name: "Uptime Ratio", className: "uptime", sortable: false, unit: "%"},
            {name: "Malfunction Devices", className: "device", sortable: false, unit: "EA"},
            {name: "Downtime (min)", className: "downtime", sortable: false, unit: "min."}
        ];

        $scope.detailTableHeader_excel = [
            {name: "Day", className: "day", sortable: false, unit: null},
            {name: "Manager", className: "manager", sortable: false, unit: null},
            {name: "Account", className: "account", sortable: false, unit: null},
            {name: "Location", className: "location", sortable: false, unit: null},
            {name: "Shop", className: "shop", sortable: false, unit: null},
            {name: "Type", className: "type", sortable: false, unit: null},
            {name: "Total", className: "total", sortable: false, unit: "EA"},
            {name: "Operation Ratio", className: "operation", sortable: false, unit: "%"},
            {name: "Uptime Ratio", className: "uptime", sortable: false, unit: "%"},
            {name: "Malfunction Devices", className: "device", sortable: false, unit: "EA"},
            {name: "Downtime (min)", className: "downtime", sortable: false, unit: "min."}
        ];

        $scope.detailTableCol = $scope.detailTableHeader.length;//device table head col count
        $scope.detailTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.detailTableLimit = $scope.detailTableLimits[0];//set default limit line

        $scope.$watch('userType', function(newValue, oldValue){
            if($scope.userType == "ADMIN"){
                var search_manager_arg = {};
                search_manager_arg["target"] = "manager";
                StatsSetting.get(search_manager_arg, function(data){
                    if(data.status == 200){
                        $scope.search_managers = data.objects;
                        $scope.search_manager = $scope.search_managers[0];
                        $scope.stats_select.manager.active = true;

                        $scope.search_manager_update($scope.search_manager);
                    }
                });
            }
        });

        $scope.$watch('stats_base.value', function(newValue, oldValue){
            $scope.stats_base.value = newValue;

            if($scope.stats_base.value == "account"){
                $scope.stats_select.dg.active = false;
                $scope.stats_select.account.active = true;
                $scope.stats_select.region.active = true;
                $scope.stats_select.shop.active = true;
            }else if($scope.stats_base.value == "dg"){
                $scope.stats_select.dg.active = true;
                $scope.stats_select.account.active = false;
                $scope.stats_select.region.active = false;
                $scope.stats_select.shop.active = false;
            }

            $scope.search_manager_update($scope.search_manager);
        });

        $scope.search_manager_update = function(manager_info){
            $scope.search_manager = manager_info;

            var search_account_or_dg_arg = {};
            search_account_or_dg_arg["target"] = $scope.stats_base.value;

            if($scope.search_manager != undefined && $scope.userType == "ADMIN"){
                search_account_or_dg_arg["manager_id"] = $scope.search_manager.id;
            }else{
                search_account_or_dg_arg["manager_id"] = "All";
            }

            StatsSetting.get(search_account_or_dg_arg, function(data){
                if(data.status == 200){
                    if($scope.stats_base.value == "account"){
                        $scope.search_accounts = data.objects;
                        $scope.search_account = $scope.search_accounts[0];
                        $scope.stats_select.account.active = true;

                        $scope.search_account_update($scope.search_account);
                    } else if($scope.stats_base.value == "dg"){
                        $scope.search_dgs = data.objects;
                        $scope.search_dg = $scope.search_dgs[0];
                        $scope.stats_select.dg.active = true;

                        $scope.search_dg_update($scope.search_dg);
                    }
                }
            });
        };

        $scope.search_account_update = function(account_info){
            $scope.search_account = account_info;

            var search_region_arg = {};
            search_region_arg["target"] = "region";
            search_region_arg["site_name"] = $scope.search_account.name;
            StatsSetting.get(search_region_arg, function(data){
                if(data.status == 200){
                    $scope.search_regions = data.objects;
                    $scope.search_region = $scope.search_regions[0];
                    $scope.stats_select.region.active = true;

                    $scope.search_region_update($scope.search_region);
                }
            });
        };

        $scope.search_region_update = function(region_info){
            $scope.search_region = region_info;

            var search_shop_arg = {};
            search_shop_arg["target"] = "shop";
            search_shop_arg["region_name"] = $scope.search_region.name;
            StatsSetting.get(search_shop_arg, function(data){
                if(data.status == 200){
                    $scope.search_shops = data.objects;
                    $scope.search_shop = $scope.search_shops[0];
                    $scope.stats_select.shop.active = true;

                    $scope.search_shop_update($scope.search_shop);
                }
            });
        };

        $scope.search_shop_update = function(shop_info){
            $scope.search_shop = shop_info;
            $scope.report_search_prevent = false;
        };

        $scope.search_dg_update = function(dg_info){
            $scope.search_dg = dg_info;
            $scope.report_search_prevent = false;
        };

        $scope.searchDetail = function(){
            $scope.ie8_excel_create = false;
            $scope.stats_total_ratio_flag = false;
            $scope.report_search_flag = true;
            $scope.report_stats_mp_active = false;
            $scope.report_stats_dp_active = false;

            $scope.operationMPChart.loading = true;
            $scope.downtimeMPChart.loading = true;
            $scope.operationDPChart.loading = true;
            $scope.downtimeDPChart.loading = true;
            $scope.totalRatioChart.loading = true;

            var search_arg = {};
            search_arg["device_mp"] = $scope.stats_device.mp.value;
            search_arg["device_dp"] = $scope.stats_device.dp.value;
            search_arg["period"] = $scope.stats_period.value;
            search_arg["start_date"] = $scope.start_date;
            $scope.title.start_date = $scope.start_date;
            search_arg["end_date"] = $scope.end_date;
            $scope.title.end_date = $scope.end_date;
            search_arg["base"] = $scope.stats_base.value;



            var today = new Date();
            search_arg["end_date_month"] = $filter('date')(today, 'yyyy-MM-dd');

            if($scope.userType == "ADMIN"){
                search_arg["manager_id"] = $scope.search_manager.id;
            }else{
                search_arg["manager_id"] = "All";
            }


            if(search_arg["base"] == "account"){
                search_arg["account"] = $scope.search_account.name;
                $scope.title.account = $scope.search_account.name;
                search_arg["region"] = $scope.search_region.name;
                $scope.title.region = $scope.search_region.name;
                search_arg["shop"] = $scope.search_shop.name;
                $scope.title.shop = $scope.search_shop.name;
            }else if(search_arg["base"] == "dg"){
                search_arg["dg_id"] = $scope.search_dg.id;
                $scope.title.dg = $scope.search_dg.name;
            }

            if(search_arg["device_mp"]){
                StatsMP.get(search_arg, function(data){
                    if(data.status == 200){
                        if(data.objects.length != 0){
                            $scope.report_stats_mp = data.objects[0];

                            $scope.operationMPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.operationMPChart.series[0].data = [
                                ["malfunction", $scope.report_stats_mp.malfunction_ratio],
                                ["operation", $scope.report_stats_mp.operation_ratio]
                            ];

                            $scope.downtimeMPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.downtimeMPChart.series[0].data = [
                                ["downtime", $scope.report_stats_mp.downtime_ratio],
                                ["uptime", $scope.report_stats_mp.uptime_ratio]
                            ];

                            $scope.report_stats_mp_active = true;
                            $scope.operationMPChart.loading = false;
                            $scope.downtimeMPChart.loading = false;
                        }else{
                            $scope.report_stats_mp_active = false;
                        }
                    }else{
                        $scope.report_stats_mp_active = false;
                    }
                });
            }else{
                $scope.report_stats_mp_active = false;
            }

            if(search_arg["device_dp"]){
                StatsDP.get(search_arg, function(data){
                    if(data.status == 200){
                        if(data.objects.length != 0){
                            $scope.report_stats_dp = data.objects[0];

                            $scope.operationDPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.operationDPChart.series[0].data = [
                                ["malfunction", $scope.report_stats_dp.malfunction_ratio],
                                ["operation", $scope.report_stats_dp.operation_ratio]
                            ];

                            $scope.downtimeDPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.downtimeDPChart.series[0].data = [
                                ["downtime", $scope.report_stats_dp.downtime_ratio],
                                ["uptime", $scope.report_stats_dp.uptime_ratio]
                            ];

                            $scope.report_stats_dp_active = true;
                            $scope.operationDPChart.loading = false;
                            $scope.downtimeDPChart.loading = false;
                        }else{
                            $scope.report_stats_dp_active = false;
                        }
                    }else{
                        $scope.report_stats_dp_active = false;
                    }
                });
            }else{
                $scope.report_stats_dp_active = false;
            }

            search_arg['excel_create'] = $scope.IsIE8nIE9();
            StatsDetail.get(search_arg, function(data){
                if(data.status == 200){
                    $scope.report_stats_detail = data.objects;
                    $scope.report_stats_detail_all = data.objects;

                    angular.forEach($scope.report_stats_detail, function(v, k){

                        $scope.report_stats_detail[k].active = false;

                        if(k == ($scope.report_stats_detail.length-1)){
                            //$scope.report_stats_detail[k].active = true;
                            if(search_arg["period"] == "weekly"){
                                var week = "Weekly";
                                /*
                                switch(k){
                                    case 0: week = (k+1) + "st Week"; break;
                                    case 1: week = (k+1) + "nd Week"; break;
                                    case 2: week = (k+1) + "rd Week"; break;
                                    case 3: week = (k+1) + "th Week"; break;
                                    case 4: week = (k+1) + "th Week"; break;
                                    case 5: week = (k+1) + "th Week"; break;
                                    case 6: week = (k+1) + "th Week"; break;
                                }*/
                                $scope.totalRatioChart.title.text = week + " Ratio";
                            }else if(search_arg["period"] == "monthly"){
                                var month = "Monthly";
                                /*
                                switch(k){
                                    case 0: month = "Jan."; break;
                                    case 1: month = "Feb."; break;
                                    case 2: month = "Mar."; break;
                                    case 3: month = "Apr."; break;
                                    case 4: month = "May"; break;
                                    case 5: month = "Jun."; break;
                                    case 6: month = "Jul."; break;
                                    case 7: month = "Aug."; break;
                                    case 8: month = "Sep."; break;
                                    case 9: month = "Oct."; break;
                                    case 10: month = "Nov."; break;
                                    case 11: month = "Dec."; break;
                                }
                                */
                                $scope.totalRatioChart.title.text = month + " Ratio";
                            }
                        }
                    });

                    var value = data.objects;
                    //$scope.excel_host = "https://lgedev.adamssuite.com/";
                    $scope.excel_host = ENV.host;
                    $scope.excel_url = "/download/excel/" + value[0].userID;
                    $scope.excel_file = "/Statistics-" + value[0].create_date + ".xlsx";
                    $scope.excel_down_url = $scope.excel_host + $scope.excel_url + $scope.excel_file;

                    $scope.ie8_excel_create = true;
                    /*
                    var isIE = false;

                    if(navigator.appVersion.indexOf("MSIE 9") != -1){
                      isIE = true;
                    }else if(navigator.appVersion.indexOf("MSIE 8") != -1){
                      isIE = true;
                    } else if(navigator.userAgent.indexOf("MSIE 7") > 0 && navigator.appVersion.indexOf("Trident") != -1){
                      isIE = true;
                    } else {
                      isIE = false;
                    }
                    //creat excel
                    if(isIE){
                      $timeout(function() {
                        angular.element('#excel_ie8').triggerHandler('click');
                      }, 500);
                    }
                    */
                }
            });

            StatsRatio.get(search_arg, function(data){
                if(data.status == 200){
                    $scope.report_stats_total_ratio = data.objects;
                    $scope.totalRatioChart.xAxis.categories = [];

                    $scope.totalRatioChart.series[0].data = [];//mp operation
                    $scope.totalRatioChart.series[1].data = [];//mp uptime
                    $scope.totalRatioChart.series[2].data = [];//dp operation
                    $scope.totalRatioChart.series[3].data = [];//dp uptime

                    angular.forEach($scope.report_stats_total_ratio, function(v, k){
                        if(search_arg["period"] == "weekly"){
                            var weekday = "";
                            switch(v.week_day){
                                case 0: weekday = v.week_date + " " + "Mon."; break;
                                case 1: weekday = v.week_date + " " + "Tue."; break;
                                case 2: weekday = v.week_date + " " + "Wed."; break;
                                case 3: weekday = v.week_date + " " + "Thu."; break;
                                case 4: weekday = v.week_date + " " + "Fri."; break;
                                case 5: weekday = v.week_date + " " + "Sat."; break;
                                case 6: weekday = v.week_date + " " + "Sun."; break;
                            }
                            $scope.totalRatioChart.xAxis.categories.push(weekday);

                        }else if(search_arg["period"] == "monthly"){
                            $scope.totalRatioChart.xAxis.categories.push(v.start_date + " ~ " + v.end_date);
                        }
                        $scope.totalRatioChart.series[0].data.push(v.mp_operation_ratio);
                        $scope.totalRatioChart.series[1].data.push(v.mp_uptime_ratio);
                        $scope.totalRatioChart.series[2].data.push(v.dp_operation_ratio);
                        $scope.totalRatioChart.series[3].data.push(v.dp_uptime_ratio);
                    });

                    $scope.totalRatioChart.loading = false;
                }
            });

            if($scope.stats_period.value == "daily"){
                $scope.stats_period.name = "Daily";
                $scope.stats_total_ratio_flag = false;
                $scope.detailTableHeader[0].name = "Day";
            }else if($scope.stats_period.value == "weekly"){
                $scope.stats_period.name = "Weekly";
                $scope.stats_total_ratio_flag = true;
                $scope.detailTableHeader[0].name = "Week";
            }else if($scope.stats_period.value == "monthly"){
                $scope.stats_period.name = "Monthly";
                $scope.stats_total_ratio_flag = true;
                $scope.detailTableHeader[0].name = "Month";
            }
        };

        $scope.reSearchDetail = function(start_date, end_date, row_index){
            angular.forEach($scope.report_stats_detail, function(v, k) {
                $scope.report_stats_detail[k].active = false;

                if (k == row_index) {
                    $scope.report_stats_detail[k].active = true;
                }
            });

            $scope.start_date = start_date;
            $scope.end_date = end_date;

            $scope.stats_total_ratio_flag = false;
            $scope.report_search_flag = true;
            $scope.report_stats_mp_active = false;
            $scope.report_stats_dp_active = false;

            $scope.operationMPChart.loading = true;
            $scope.downtimeMPChart.loading = true;
            $scope.operationDPChart.loading = true;
            $scope.downtimeDPChart.loading = true;
            $scope.totalRatioChart.loading = true;
            $scope.activeClick = false;

            var search_arg = {};
            search_arg["device_mp"] = $scope.stats_device.mp.value;
            search_arg["device_dp"] = $scope.stats_device.dp.value;
            search_arg["period"] = $scope.stats_period.value;
            search_arg["start_date"] = $scope.start_date;
            $scope.title.start_date = $scope.start_date;
            search_arg["end_date"] = $scope.end_date;
            $scope.title.end_date = $scope.end_date;

            search_arg["base"] = $scope.stats_base.value;
            var today = new Date();
            search_arg["end_date_month"] = $filter('date')(today, 'yyyy-MM-dd');

            if($scope.userType == "ADMIN"){
                search_arg["manager_id"] = $scope.search_manager.id;
            }else{
                search_arg["manager_id"] = "All";
            }


            if(search_arg["base"] == "account"){
                search_arg["account"] = $scope.search_account.name;
                $scope.title.account = $scope.search_account.name;
                search_arg["region"] = $scope.search_region.name;
                $scope.title.region = $scope.search_region.name;
                search_arg["shop"] = $scope.search_shop.name;
                $scope.title.shop = $scope.search_shop.name;
            }else if(search_arg["base"] == "dg"){
                search_arg["dg_id"] = $scope.search_dg.id;
                $scope.title.dg = $scope.search_dg.name;
            }

            if(search_arg["device_mp"]){
                StatsMP.get(search_arg, function(data){
                    if(data.status == 200){
                        if(data.objects.length != 0){
                            $scope.report_stats_mp = data.objects[0];

                            $scope.operationMPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.operationMPChart.series[0].data = [
                                ["malfunction", $scope.report_stats_mp.malfunction_ratio],
                                ["operation", $scope.report_stats_mp.operation_ratio]
                            ];

                            $scope.downtimeMPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.downtimeMPChart.series[0].data = [
                                ["downtime", $scope.report_stats_mp.downtime_ratio],
                                ["uptime", $scope.report_stats_mp.uptime_ratio]
                            ];

                            $scope.report_stats_mp_active = true;
                            $scope.operationMPChart.loading = false;
                            $scope.downtimeMPChart.loading = false;
                        }else{
                            $scope.report_stats_mp_active = false;
                        }
                    }else{
                        $scope.report_stats_mp_active = false;
                    }
                });
            }else{
                $scope.report_stats_mp_active = false;
            }

            if(search_arg["device_dp"]){
                StatsDP.get(search_arg, function(data){
                    if(data.status == 200){
                        if(data.objects.length != 0){
                            $scope.report_stats_dp = data.objects[0];

                            $scope.operationDPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.operationDPChart.series[0].data = [
                                ["malfunction", $scope.report_stats_dp.malfunction_ratio],
                                ["operation", $scope.report_stats_dp.operation_ratio]
                            ];

                            $scope.downtimeDPChart.options.colors = ['#f05050', '#428bca'];
                            $scope.downtimeDPChart.series[0].data = [
                                ["downtime", $scope.report_stats_dp.downtime_ratio],
                                ["uptime", $scope.report_stats_dp.uptime_ratio]
                            ];

                            $scope.report_stats_dp_active = true;
                            $scope.operationDPChart.loading = false;
                            $scope.downtimeDPChart.loading = false;
                        }else{
                            $scope.report_stats_dp_active = false;
                        }
                    }else{
                        $scope.report_stats_dp_active = false;
                    }
                });
            }else{
                $scope.report_stats_dp_active = false;
            }

            StatsDetail.get(search_arg, function(data){
                if(data.status == 200){
                    $scope.report_stats_detail = $scope.report_stats_detail_all;

                    angular.forEach($scope.report_stats_detail, function(v, k){
                        $scope.report_stats_detail[k].active = false;

                        if(k == row_index){
                            $scope.report_stats_detail[k].active = true;
                            if(search_arg["period"] == "weekly"){
                                var week = "";
                                switch(k){
                                    case 0: week = (k+1) + "st Week"; break;
                                    case 1: week = (k+1) + "nd Week"; break;
                                    case 2: week = (k+1) + "rd Week"; break;
                                    case 3: week = (k+1) + "th Week"; break;
                                    case 4: week = (k+1) + "th Week"; break;
                                    case 5: week = (k+1) + "th Week"; break;
                                    case 6: week = (k+1) + "th Week"; break;
                                }
                                $scope.totalRatioChart.title.text = week + " Ratio";
                            }else if(search_arg["period"] == "monthly"){
                                var month = "";
                                switch(k){
                                    case 0: month = "Jan."; break;
                                    case 1: month = "Feb."; break;
                                    case 2: month = "Mar."; break;
                                    case 3: month = "Apr."; break;
                                    case 4: month = "May"; break;
                                    case 5: month = "Jun."; break;
                                    case 6: month = "Jul."; break;
                                    case 7: month = "Aug."; break;
                                    case 8: month = "Sep."; break;
                                    case 9: month = "Oct."; break;
                                    case 10: month = "Nov."; break;
                                    case 11: month = "Dec."; break;
                                }
                                $scope.totalRatioChart.title.text = month + " Ratio";
                            }
                        }
                    });
                }
            });

            StatsRatio.get(search_arg, function(data){
                if(data.status == 200){
                    $scope.report_stats_total_ratio = data.objects;
                    $scope.totalRatioChart.xAxis.categories = [];

                    $scope.totalRatioChart.series[0].data = [];//mp operation
                    $scope.totalRatioChart.series[1].data = [];//mp uptime
                    $scope.totalRatioChart.series[2].data = [];//dp operation
                    $scope.totalRatioChart.series[3].data = [];//dp uptime

                    angular.forEach($scope.report_stats_total_ratio, function(v, k){
                        if(search_arg["period"] == "weekly"){
                            var weekday = "";
                            switch(v.week_day){
                                case 0: weekday = v.week_date + " " + "Mon."; break;
                                case 1: weekday = v.week_date + " " + "Tue."; break;
                                case 2: weekday = v.week_date + " " + "Wed."; break;
                                case 3: weekday = v.week_date + " " + "Thu."; break;
                                case 4: weekday = v.week_date + " " + "Fri."; break;
                                case 5: weekday = v.week_date + " " + "Sat."; break;
                                case 6: weekday = v.week_date + " " + "Sun."; break;
                            }
                            $scope.totalRatioChart.xAxis.categories.push(weekday);
                        }else if(search_arg["period"] == "monthly"){
                            $scope.totalRatioChart.xAxis.categories.push(v.start_date + " ~ " + v.end_date);
                        }
                        $scope.totalRatioChart.series[0].data.push(v.mp_operation_ratio);
                        $scope.totalRatioChart.series[1].data.push(v.mp_uptime_ratio);
                        $scope.totalRatioChart.series[2].data.push(v.dp_operation_ratio);
                        $scope.totalRatioChart.series[3].data.push(v.dp_uptime_ratio);
                    });

                    $scope.totalRatioChart.loading = false;
                }
            });

            if($scope.stats_period.value == "daily"){
                $scope.stats_period.name = "Daily";
                $scope.stats_total_ratio_flag = false;
                $scope.detailTableHeader[0].name = "Day";
            }else if($scope.stats_period.value == "weekly"){
                $scope.stats_period.name = "Weekly";
                $scope.stats_total_ratio_flag = true;
                $scope.detailTableHeader[0].name = "Week";
            }else if($scope.stats_period.value == "monthly"){
                $scope.stats_period.name = "Monthly";
                $scope.stats_total_ratio_flag = true;
                $scope.detailTableHeader[0].name = "Month";
            }
        };

        $scope.$watch('report_stats_mp_active', function(newVal, oldVal){
            $scope.report_stats_mp_active = newVal;
        });

        $scope.$watch('report_stats_dp_active', function(newVal, oldVal){
            $scope.report_stats_dp_active = newVal;
        });

        $scope.exportToExcel = function(){
            var blob = new Blob([document.getElementById('excel_export_stats_detail').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            var today = new Date();
            var date = $filter('date')(today, 'yyyy-MM-dd');

            saveAs(blob, "Statistics-" + date + ".xls");
        };
        /*
        $scope.createExcelSheet = function(search_tag, objArray, manager, site, location, shop) {
            $scope.parse_objArray = [];

            angular.forEach(objArray, function (v, k) {
                var mp_operation_ratio = $filter("number")(v.mp_operation_ratio, 2);
                var mp_uptime_ratio = $filter("number")(v.mp_uptime_ratio, 2);
                var dp_operation_ratio = $filter("number")(v.dp_operation_ratio, 2);
                var dp_uptime_ratio = $filter("number")(v.dp_uptime_ratio, 2);

                if (search_tag == "daily") {
                    $scope.parse_objArray.push(
                        {
                          "Day": v.start_date,
                          "Manager": manager,
                          "Account": site,
                          "Location": location,
                          "Shop": shop,
                          "Type": v.mp_type,
                          "Total": v.mp_total_cnt,
                          "Operation Ratio": mp_operation_ratio,
                          "Uptime Ratio": mp_uptime_ratio,
                          "Malfunction Devices": v.mp_malfunction_cnt,
                          "Downtime (min)": v.mp_downtime,
                          "Search_Type": "stats"
                        },
                        {
                          "Day": v.start_date,
                          "Manager": manager,
                          "Account": site,
                          "Location": location,
                          "Shop": shop,
                          "Type": v.dp_type,
                          "Total": v.dp_total_cnt,
                          "Operation Ratio": dp_operation_ratio,
                          "Uptime Ratio": dp_uptime_ratio,
                          "Malfunction Devices": v.dp_malfunction_cnt,
                          "Downtime (min)": v.dp_downtime,
                          "Search_Type": "stats"
                        }
                    );
                } else if (search_tag == "weekly") {
                    $scope.parse_objArray.push(
                        {
                          "Weekly": v.start_date + "~" + v.end_date,
                          "Manager": manager,
                          "Account": site,
                          "Location": location,
                          "Shop": shop,
                          "Type": v.mp_type,
                          "Total": v.mp_total_cnt,
                          "Operation Ratio": v.mp_operation_ratio,
                          "Uptime Ratio": v.mp_uptime_ratio,
                          "Malfunction Devices": v.mp_malfunction_cnt,
                          "Downtime (min)": v.mp_downtime,
                          "Search_Type": "stats"
                        },
                        {
                          "Weekly": v.start_date + "~" + v.end_date,
                          "Manager": manager,
                          "Account": site,
                          "Location": location,
                          "Shop": shop,
                          "Type": v.dp_type,
                          "Total": v.dp_total_cnt,
                          "Operation Ratio": v.dp_operation_ratio,
                          "Uptime Ratio": v.dp_uptime_ratio,
                          "Malfunction Devices": v.dp_malfunction_cnt,
                          "Downtime (min)": v.dp_downtime,
                          "Search_Type": "stats"
                        }
                    );
                } else if (search_tag == "monthly") {
                    $scope.parse_objArray.push(
                        {
                          "Monthly": v.start_date,
                          "Manager": manager,
                          "Account": site,
                          "Location": location,
                          "Shop": shop,
                          "Type": v.mp_type,
                          "Total": v.mp_total_cnt,
                          "Operation Ratio": v.mp_operation_ratio,
                          "Uptime Ratio": v.mp_uptime_ratio,
                          "Malfunction Devices": v.mp_malfunction_cnt,
                          "Downtime (min)": v.mp_downtime,
                          "Search_Type": "stats"
                        },
                        {
                          "Monthly": v.start_date,
                          "Manager": manager,
                          "Account": site,
                          "Location": location,
                          "Shop": shop,
                          "Type": v.dp_type,
                          "Total": v.dp_total_cnt,
                          "Operation Ratio": v.dp_operation_ratio,
                          "Uptime Ratio": v.dp_uptime_ratio,
                          "Malfunction Devices": v.dp_malfunction_cnt,
                          "Downtime (min)": v.dp_downtime,
                          "Search_Type": "stats"
                        }
                    );
                }
            });
            console.log(ENV.host);
            console.log(JSON.stringify($scope.parse_objArray));
            EXPORT_EXCEL.save($scope.parse_objArray, function(data){
                console.log(data.status);
                if(data.status == 200) {
                    var value = data.objects;
                    //$scope.excel_host = "https://lgedev.adamssuite.com/";
                    console.log(ENV.host);
                    $scope.excel_host = ENV.host;
                    $scope.excel_url = "/download/excel/" + value.userID;
                    $scope.excel_file = "/Statistics-" + value.create_date + ".xlsx";
                    $scope.excel_down_url = $scope.excel_host + $scope.excel_url + $scope.excel_file;

                    $scope.ie8_excel_create = true;
                }
            });
        };
        */

        $scope.ieExcelBtnActive = function(search_flag, create_flag){
            var disabled = true;
            if(search_flag && create_flag){
                disabled = false;
            }
            return disabled;
        };

        $scope.JSONToCSVConvertor = function(JSONData, fileName, ShowLabel){
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
            var CSV = '';
            if (ShowLabel) {
                var row = "";
                for (var index in arrData[0]) {
                    row += index + ',';
                }
                row = row.slice(0, -1);
                CSV += row + '\r\n';
            }

            for (var i = 0; i < arrData.length; i++) {
                var row = "";
                for (var index in arrData[i]) {
                    var arrValue = arrData[i][index] == null ? "" : '="' + arrData[i][index] + '"';
                    row += arrValue + ',';
                }
                row.slice(0, row.length - 1);
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            var fileName = "Result";

            var IEwindow = $window.open();

            IEwindow.document.write('sep=,\r\n' + CSV);
            IEwindow.document.close();
            IEwindow.document.execCommand('SaveAs', true, fileName + ".csv");
            IEwindow.close();
        };

        // if ----------------------------------------------------------------------------------------------------------
        $scope.if_select = function(select_id, select_info){
            var select_flag = true;

            if(select_id == "select_manager"){
                if(select_info == undefined){
                    if($scope.userType != "ADMIN"){
                        select_flag = false;
                    }else{
                        select_flag = true;
                    }
                }else{
                    if(select_info.id != "All"){
                        select_flag = false;
                    }
                }
            }else{
                if(select_info == undefined)    return select_flag;

                if(select_info.id != "All"){
                    select_flag = false;
                }else{
                    select_flag = true;
                }
            }

            return select_flag;
        };

        $scope.showReportStatsDetail = function(flag){
            var show_flag = false;

            if(flag != undefined){
                show_flag = flag;
            }

            return show_flag;
        };

        $scope.openMalfuncMPInfo = function(mp_list){
            Modal.open(
                'views/reports_stats_mp_modal.html',
                'StatsMPModalCtrl',
                'lg',
                {
                    mp_list: function(){
                        return mp_list;
                    }
                }
            );
        };

        $scope.openMalfuncDPInfo = function(dp_list){
            Modal.open(
                'views/reports_stats_dp_modal.html',
                'StatsDPModalCtrl',
                'lg',
                {
                    dp_list: function(){
                        return dp_list;
                    }
                }
            );
        };

        // chart set ---------------------------------------------------------------------------------------------------
        $scope.operationMPChart = {
            options: {
                chart: {
                    type: 'pie'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            },
                            format: '{point.percentage:.2f} %'
                        },
                        showInLegend: true
                    }
                }
            },
            title: {
                text: 'Operation Ratio',
                align: 'center',
                verticalAlign: 'top',
                y: 0,
                style: {
                    fontSize: '8',
                    fontWeight: 'bold',
                    color: 'black'
                }
            },
            series: [{
                name: 'Ratio',
                innerSize: '50%',
                data: []
            }],
            loading: false
        };

        $scope.downtimeMPChart = {
            options: {
                chart: {
                    type: 'pie'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            },
                            format: '{point.percentage:.2f} %'
                        },
                        showInLegend: true
                    }
                }
            },
            title: {
                text: 'Downtime Ratio',
                align: 'center',
                verticalAlign: 'top',
                y: 0,
                style: {
                    fontSize: '8',
                    fontWeight: 'bold',
                    color: 'black'
                }
            },
            series: [{
                name: 'Ratio',
                innerSize: '50%',
                data: []
            }],
            loading: false
        };

        $scope.operationDPChart = {
            options: {
                chart: {
                    type: 'pie'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            },
                            format: '{point.percentage:.2f} %'
                        },
                        showInLegend: true
                    }
                }
            },
            title: {
                text: 'Operation Ratio',
                align: 'center',
                verticalAlign: 'top',
                y: 0,
                style: {
                    fontSize: '8',
                    fontWeight: 'bold',
                    color: 'black'
                }
            },
            series: [{
                name: 'Ratio',
                innerSize: '50%',
                data: []
            }],
            loading: false
        };

        $scope.downtimeDPChart = {
            options: {
                chart: {
                    type: 'pie'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            },
                            format: '{point.percentage:.2f} %'
                        },
                        showInLegend: true
                    }
                }
            },
            title: {
                text: 'Donwtime Ratio',
                align: 'center',
                verticalAlign: 'top',
                y: 0,
                style: {
                    fontSize: '8',
                    fontWeight: 'bold',
                    color: 'black'
                }
            },
            series: [{
                name: 'Ratio',
                innerSize: '50%',
                data: []
            }],
            loading: false
        };

        $scope.totalRatioChart = {
            options: {
                chart: {
                    type: 'column'
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                }
            },
            title: {
                text: "Total Ratio"
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '8px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
                categories: []
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'Ratio (%)'
                }
            },
            series: [
                {
                    name: 'MP Operation',
                    data: []
                },
                {
                    name: 'MP Uptime',
                    data: []
                },
                {
                    name: 'DP Operation',
                    data: []
                },
                {
                    name: 'DP Uptime',
                    data: []
                }
            ],
            loading: false
        };

        $scope.getFirstDay = function (d){
            var d = new Date(d);
            var firstDay = new Date(d.getUTCFullYear(), d.getUTCMonth(), 1);
            return firstDay;
        };

        $scope.getWeekFirstDay = function (d) {
            var d = new Date(d);
            var current_d = new Date(d);
            var last_d = new Date(d);

            var day = d.getUTCDay();

            var diff = d.getUTCDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
            var resDate = new Date(d.setUTCDate(diff));

            if(current_d.getUTCMonth() != resDate.getUTCMonth()){
                var firstDate = new Date(last_d.getUTCFullYear(), last_d.getUTCMonth(), 1);
                return firstDate;
            }

            return resDate;
        };

        $scope.getWeekEndDay = function (d){
            var d = new Date(d);
            var current_d = new Date(d);
            var last_d = new Date(d);

            var day = d.getUTCDay();

            var day_interval = 0;

            for(var i=day; i<7; i++){
                day_interval = day_interval + 1;
            }

            var diff = d.getUTCDate() + day_interval; // adjust when day is sunday
            var resDate = new Date(d.setUTCDate(diff));

            if(current_d.getUTCMonth() != resDate.getUTCMonth()){
                var lastDate = new Date(last_d.getUTCFullYear(), last_d.getUTCMonth() + 1, 0);
                return lastDate;
            }

            return resDate;
        };

        $scope.getMonthly = function (d, debug){
            var d = new Date(d);

            var lastDate = new Date(d.getUTCFullYear(), d.getUTCMonth() + 1, 0);

            return lastDate;
        };

        $scope.convertDateToUTC = function(date) {
            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        };
    });

