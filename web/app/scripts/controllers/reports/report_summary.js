'use strict';

angular.module('signageApp')
  .controller('ReportsSummaryCtrl', function ($scope, SharedService, $sce) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });

        $scope.trustSrc = function(src, id, isAdmin) {
          return $sce.trustAsResourceUrl(src+id);
        };

        $scope.StatURL = "https://anymondev.adamssuite.com/report/report_summary.asp?key=LGEKR&id=";
  });

