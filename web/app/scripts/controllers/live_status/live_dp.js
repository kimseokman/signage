/**
 * Created by ksm on 2014-12-29.
 */

'use strict';

angular.module('signageApp')
    .controller('DisplayCtrl', function ($route, $rootScope, $scope, SharedService, $http, MediaPlayer, User, Admin, Manager, Member, DP_By_MP, $location, $routeParams, Session, Display, Utils) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
            $scope.mediaPlayerSerialNumber = $scope.media_player_serial_number;
        });
        //only live status
        $scope.$on("DP_SERIAL_NUMBER", function(){
            $scope.display_serial_number = SharedService.display_serial_number;
            $scope.displaySerialNumber = $scope.display_serial_number;
        });

        //--------------------------------------------------------------------------------------------------------------
        // Data
        //--------------------------------------------------------------------------------------------------------------
        $scope.slides = [];

        MediaPlayer.get({serial_number: $scope.mediaPlayerSerialNumber}, function(data){
            $scope.media_player = data.objects[0];
        });

        $scope.$watch('displaySerialNumber', function(newVal, oldVal){
          $scope.displaySerialNumber = newVal;

          Display.get({serial_number: $scope.displaySerialNumber}, function(data){
            if(data.status == 200){
              $scope.display = data.objects[0];

              $scope.dp = data.objects[0];

              var dp_file_name = '' + $scope.dp.model.substr(0,6);

              $scope.addSlide($scope.dp_model_img_uri + dp_file_name + '/' + $scope.img[0].name, $scope.img[0].name, true);
              $scope.addSlide($scope.dp_model_img_uri + dp_file_name + '/' + $scope.img[1].name, $scope.img[1].name, false);
              $scope.addSlide($scope.dp_model_img_uri + dp_file_name + '/' + $scope.img[2].name, $scope.img[2].name, false);
              $scope.addSlide($scope.dp_model_img_uri + dp_file_name + '/' + $scope.img[3].name, $scope.img[3].name, false);
              $scope.addSlide($scope.dp_model_img_uri + dp_file_name + '/' + $scope.img[4].name, $scope.img[4].name, false);

              $scope.static_dp_img_uri = $scope.dp_model_img_uri + $scope.dp.model + '/' + $scope.img[0].name;
            }
          });
        });


        $scope.addSlide = function (imgsrc, seq, active_flag) {
            var img_name = seq;
            var img_seq = img_name.split(".")[0];
            $scope.slides = [];
            Utils.isImage(imgsrc).then(function (result) {
                if (result) {
                    $scope.slides.push({
                        image: imgsrc,
                        seq: Number(img_seq),
                        active: active_flag
                    });
                }
            });
        };

        $scope.$watch('slides', function(newVal){
            $scope.slides = newVal.sort(function(a, b){
                return (a.seq < b.seq) ? -1 : ((a.seq > b.seq) ? 1 : 0);
            });
        }, true);

        //--------------------------------------------------------------------------------------------------------------
        // JSON Set
        //--------------------------------------------------------------------------------------------------------------
        $scope.MANAGEMENT_INFO = [
            {field: 'si_company', name: 'Management Company', type: 'text'},
            {field: 'se_name', name: 'Field Engineer Name', type: 'text'},
            {field: 'se_email', name: 'Field Engineer E-Mail', type: 'text'},
            {field: 'se_mobile', name: 'Field Engineer Mobile', type: 'text'}
        ];

        $scope.DP_INFO = [
            {field: 'model', name: 'Model', type: 'text'},
            {field: 'serial_number', name: 'Serial Number', type: 'readOnly'},
            {field: 'firmware_version', name: 'F/W Version', type: 'readOnly'},
            {field: 'alias', name: 'Alias', type: 'text'},
            {field: 'activation_date', name: 'Start Date', type: 'date', unit_type:'date_format', editable_type: 'warrent'},
            {field: 'expiration_date', name: 'End Date', type: 'date', related: 'activation_date', unit_type:'date_format', editable_type: 'warrent'},
            {field: 'warrent_start_date', name: 'Extended Warrenty Start Date', type: 'date', editable_type: 'warrent'},
            {field: 'warrent_end_date', name: 'Extended Warrenty End Date', type: 'date', related: 'warrnet_start_date', editable_type: 'warrent'},
            {field: 'service_offer', name: 'Service Offering', type: 'text', editable_type: 'warrent'}
        ];

        /* TV Table */
        $scope.tv_table_headers = [
            {name: 'Day', class_name: 'day'},
            {name: 'Start Time', class_name: 's_time'},
            {name: 'End Time', class_name: 'e_time'},
            {name: 'Power Setting', class_name: 'tv'}
        ];

        $scope.tv_check = {
            dp:{label: 'Use Power Control', field_key:'dp_timer_flag', field:'display_timer_flag'}
        };

        $scope.tv_table_bodys = [
            {
                day: {txt:'Saturday', class_name:'day'},
                s_time: {class_name:'s_time', field:'timer_begin_sat'},
                e_time: {class_name:'e_time', field:'timer_end_sat'},
                tv: {class_name:'tv', field:'timer_onoff_sat'}
            },
            {
                day: {txt:'Sunday', class_name:'day'},
                s_time: {class_name:'s_time', field:'timer_begin_sun'},
                e_time: {class_name:'e_time', field:'timer_end_sun'},
                tv: {class_name:'tv', field:'timer_onoff_sun'}
            },
            {
                day: {txt:'Monday', class_name:'day'},
                s_time: {class_name:'s_time', field:'timer_begin_mon'},
                e_time: {class_name:'e_time', field:'timer_end_mon'},
                tv: {class_name:'tv', field:'timer_onoff_mon'}
            },
            {
                day: {txt:'Tuesday', class_name:'day'},
                s_time: {class_name:'s_time', field:'timer_begin_tue'},
                e_time: {class_name:'e_time', field:'timer_end_tue'},
                tv: {class_name:'tv', field:'timer_onoff_tue'}
            },
            {
                day: {txt:'Wednesday', class_name:'day'},
                s_time: {class_name:'s_time', field:'timer_begin_wed'},
                e_time: {class_name:'e_time', field:'timer_end_wed'},
                tv: {class_name:'tv', field:'timer_onoff_wed'}
            },
            {
                day: {txt:'Thursday', class_name:'day'},
                s_time: {class_name:'s_time', field:'timer_begin_thu'},
                e_time: {class_name:'e_time', field:'timer_end_thu'},
                tv: {class_name:'tv', field:'timer_onoff_thu'}
            },
            {
                day: {txt:'Friday', class_name:'day'},
                s_time: {class_name:'s_time', field:'timer_begin_fri'},
                e_time: {class_name:'e_time', field:'timer_end_fri'},
                tv: {class_name:'tv', field:'timer_onoff_fri'}
            }
        ];

        $scope.DISPLAY_TAB = [
            {
                title: 'Information',
                mobile: '',
                icon: 'info-circle',
                layout_type: 'normal',
                data: [
                    {
                        field: 'val_PowerStatus', command: 'tag_PowerStatus', name: 'Power Status', type: 'RO_select',
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: 'ON'}
                        ]
                    },
                    {
                        field: 'val_MonitorStatus', command: 'tag_MonitorStatus', name: 'Screen Status', type: 'RO_select',
                        statuses: [
                            {value: '00', text: 'ON'},
                            {value: '01', text: 'OFF'}
                        ]
                    },
                    //화씨 표현을 위해 unit_type 과 unit2 추가
                    {field: 'val_Temp', command: 'tag_Temp', name: 'Temperature', type: 'temp', unit: '℃', unit_type: 'temp', unit2: '℉', check_status: 'status_temp'},    //제품 현재 온도

                    {
                        field: 'val_AspectRatio', command: 'tag_AspectRatio', name: 'Aspect Ratio', type: 'select',
                        statuses: [
                            {value: '01', text: '4:3'},
                            {value: '02', text: '16:9'},
                            {value: '04', text: 'Zoom'},
                            {value: '09', text: 'Source'},
                            {value: '10', text: 'Cinema Zoom'}
                        ]
                    },

                    {
                        field: 'val_Fan', command: 'tag_Fan', name: 'Fan Fault', type: 'RO_select',   //팬 고장 체크
                        statuses: [
                            {value: '00', text: 'Breakdown'},
                            {value: '01', text: 'Working'},
                            {value: '02', text: 'No FAN'}
                        ]
                    },
                    {
                        field: 'val_InputSelect', command: 'tag_InputSelect', name: 'Input Select', type: 'select',
                        statuses: [
                            {value: '20', text: 'AV'},
                            {value: '40', text: 'Component'},
                            {value: '60', text: 'RGB'},
                            {value: '70', text: 'DVI-D(PC)'},
                            {value: '80', text: 'DVI-D(DTV)'},
                            {value: '90', text: 'HDMI(HDMI1)(DTV)'},
                            {value: 'a0', text: 'HDMI(HDMI1)(PC)'},
                            {value: 'C0', text: 'Display Port(DTV)'},
                            {value: 'D0', text: 'Display Port(PC)'},
                            {value: '91', text: 'HDMI2/SDI(DTV)'},
                            {value: 'A1', text: 'HDMI2/SDI(PC)'},
                            {value: 'B0', text: 'SuperSign'}
                        ]
                    },

                    {field: 'val_Lamp', command: 'tag_Lamp', name: 'Lamp', type: 'RO_check'},

                    {
                        field: 'val_PictureMode', command: 'tag_PictureMode', name: 'Picture Mode', type: 'select',
                        statuses: [
                            {value: '00', text: 'Vivid'},
                            {value: '01', text: 'Standard'},
                            {value: '02', text: 'Cinema'},
                            {value: '03', text: 'Sport'},
                            {value: '04', text: 'Game'}
                        ]
                    },

                    {field: 'port_number', name: 'Serial Port / IP Address', type: 'RO'},

                    {field: 'val_Signal', command: 'tag_Signal', name: 'Video Signal', type: 'RO_check'}
                ]
            },
            {
                title: 'Picture',
                mobile: '',
                icon: 'picture-o',
                layout_type: 'normal',
                data: [
                    {
                        field: 'val_AspectRatio', command: 'tag_AspectRatio', name: 'Aspect Ratio', type: 'select',
                        statuses: [
                            {value: '01', text: '4:3'},
                            {value: '02', text: '16:9'},
                            {value: '04', text: 'Zoom'},
                            {value: '09', text: 'Source'},
                            {value: '10', text: 'Cinema Zoom'}
                        ]
                    },
                    {field: 'val_Constrast', command: 'tag_Constrast', name: 'Contrast', type: 'text'},     //명암 type: 'range'
                    {
                        field: 'val_PictureMode', command: 'tag_PictureMode', name: 'Picture Mode', type: 'select',  //영상 모드
                        statuses: [
                            {value: '00', text: 'Vivid'},
                            {value: '01', text: 'Standard'},
                            {value: '02', text: 'Cinema'},
                            {value: '03', text: 'Sport'},
                            {value: '04', text: 'Game'}
                        ]
                    },
                    {field: 'val_Brightness', command: 'tag_Brightness', name: 'Brightness', type: 'text'},   //밝기 type: 'range'
                    {
                        field: 'val_EnergySaving', command: 'tag_EnergySaving', name: 'Energy Saving', type: 'select', //절전 모드
                        statuses: [
                            {value: '00', text: 'Disabled'},
                            {value: '01', text: 'Minimum'},
                            {value: '02', text: 'Medium'},
                            {value: '03', text: 'Maximum'},
                            {value: '04', text: 'Auto'},
                            {value: '05', text: 'Screen Off'}
                        ]
                    },
                    {field: 'val_Color', command: 'tag_Color', name: 'Color', type: 'text', memo: 'AV/Component Only'},        //색농도 type: 'range'
                    {field: 'val_Backlight', command: 'tag_Backlight', name: 'Backlight', type: 'text'},    //백라이트 밝기 type: 'range'
                    {field: 'val_ColorTemp', command: 'tag_ColorTemp', name: 'Color Temperature', type: 'text'}, //색온도 type: 'range'
                    {field: 'val_Sharpness', command: 'tag_Sharpness', name: 'Sharpness', type: 'text', memo: 'AV/Component Only'},    //선명도 type: 'range'
                    {field: 'val_Tint', command: 'tag_Tint', name: 'Tint', type: 'text', memo: 'AV/Component Only'}         //색상 type: 'range'

                ]
            },
            {
                title: 'Option',
                mobile: '',
                icon: 'cog',
                layout_type: 'normal',
                data: [
                    {
                        field: 'val_ismMode', command: 'tag_ismMode', name: 'ISM Mode', type: 'select',  //화면 잔상 방지
                        statuses: [
                            {value: '01', text: 'Inversion'},
                            {value: '02', text: 'Orbiter'},
                            {value: '04', text: 'White Wash'},
                            {value: '08', text: 'Normal'}
                        ]
                    },
                    {
                        field: 'val_dpmSelect', command: 'tag_dpmSelect', name: 'DPM Select', type: 'select',    //절전 모드 설정
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: '5 (sec)'},
                            {value: '02', text: '10 (sec)'},
                            {value: '03', text: '15 (sec)'},
                            {value: '04', text: '1 (min)'},
                            {value: '05', text: '3 (min)'},
                            {value: '06', text: '5 (min)'},
                            {value: '07', text: '10 (min)'}
                        ]
                    },
                    {
                        field: 'val_osdSelect', command: 'tag_osdSelect', name: 'OSD Select', type: 'select',    //OSD 메뉴 선택
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: 'ON'}
                        ]
                    },
                    {
                        field: 'val_Lang', command: 'tag_Lang', name: 'Language', type: 'select',
                        statuses: [
                            {value: '00', text: 'Czech'},    //체코
                            {value: '01', text: 'Danish'},   //덴마크
                            {value: '02', text: 'German'},   //독일
                            {value: '03', text: 'English'},  //영어
                            {value: '04', text: 'Spanish(European)'},    //스페인(유럽)
                            {value: '05', text: 'Greek'},    //그리스
                            {value: '06', text: 'French'},   //프랑스
                            {value: '07', text: 'Italian'},  //이탈리아
                            {value: '08', text: 'Dutch'},    //네덜란드
                            {value: '09', text: 'Norwegian'},    //노르웨이
                            {value: '0A', text: 'Portuguese'},  //포르투갈
                            {value: '0B', text: 'Portuguese(Brazil)'},  //포르투갈(브라질)
                            {value: '0C', text: 'Russian'}, //러시아
                            {value: '0D', text: 'Finnish'}, //핀란드
                            {value: '0E', text: 'Swedish'}, //스위스
                            {value: '0F', text: 'Korean'},  //한국
                            {value: '10', text: 'Chinese(Cantonese)'},  //중국(광동)
                            {value: '11', text: 'Japanese'},    //일본
                            {value: '12', text: 'Chinese(Mandarin)'}    //중국(북경)
                        ]
                    },
                    {
                        field: 'val_Remote', command: 'tag_Remote', name: 'Remote / Key Lock', type: 'select', //리모컨 로컨 키 잠금
                        statuses: [
                            {value: '00', text: 'Off'},
                            {value: '01', text: 'On'}
                        ]
                    },
                    {field: 'val_PowerOnDelay', command: 'tag_PowerOnDelay', name: 'Power On Delay', type: 'text', memo: 'sec.'}, //전원 켜짐 지연 type: 'range'
                    {field: '', name: 'Initial Setting', type: 'button'}    //init set
                ]
            },
            {
                title: 'Support',
                mobile: '',
                icon: 'arrow-circle-up',
                layout_type: 'free',
                data: [
                    {
                        name: 'support_content',
                        command: 'tag_FirmwareUpdate'
                    }
                ]
            },
            {
                title: 'Timetab',
                mobile: '',
                icon: 'clock-o',
                layout_type: 'normal',
                data: [
                    {field: 'val_Date', command: 'tag_Date', name: 'Date', type: 'date', memo: 'MM/DD/YYYY'},  //현재시작  년원일
                    {field: 'val_Time', command: 'tag_Time', name: 'Time', type: 'time', memo: 'hh:mm'},  //현재시작  시분초
                    {
                        field: 'val_SleepTime', command: 'tag_SleepTime', name: 'Sleep Time', type: 'select',
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: '10 Minutes'},
                            {value: '02', text: '20 Minutes'},
                            {value: '03', text: '30 Minutes'},
                            {value: '04', text: '60 Minutes'},
                            {value: '05', text: '90 Minutes'},
                            {value: '06', text: '120 Minutes'},
                            {value: '07', text: '180 Minutes'},
                            {value: '08', text: '240 Minutes'}
                        ]
                    },
                    {
                        field: 'val_AutoStandby', command: 'tag_AutoStandby', name: '4 Hours OFF', type: 'select',   //자동 대기 설정
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: 'OFF After 4 Hours'}
                        ]
                    },
                    {
                        field: 'val_AutoOff', command: 'tag_AutoOff', name: 'Auto OFF', type: 'select',
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: 'OFF After 15 Minutes'}
                        ]
                    }
                ]
            },
            {
                title: 'Schedule',
                mobile: '',
                icon: 'calendar',
                layout_type: 'free',
                data: [
                    {
                        name: 'schedule_content'
                    }
                ]
            },
            {
                title: 'Audio',
                mobile: '',
                icon: 'volume-up',
                layout_type: 'normal',
                data: [
                    {
                        field: 'val_Speaker', command: 'tag_Speaker', name: 'Speaker', type: 'select',   //스피커
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: 'ON'}
                        ]
                    },
                    {
                        field: 'val_VolumMute', command: 'tag_VolumMute', name: 'Volume Mute', type: 'select',   //음소거
                        statuses: [
                            {value: '00', text: 'ON'},
                            {value: '01', text: 'OFF'}
                        ]
                    },
                    {
                        field: 'val_SoundMode', command: 'tag_SoundMode', name: 'Sound Mode', type: 'select',    //음향모드
                        statuses: [
                            {value: '00', text: 'Clear'},
                            {value: '01', text: 'Standard'},
                            {value: '02', text: 'Music'},
                            {value: '03', text: 'Cinema'},
                            {value: '04', text: 'Sport'}
                        ]
                    },
                    {
                        field: 'val_AutoVolume', command: 'tag_AutoVolume', name: 'Auto Volume', type: 'select',
                        statuses: [
                            {value: '00', text: 'OFF'},
                            {value: '01', text: 'ON'}
                        ]
                    },
                    {field: 'val_Balance', command: 'tag_Balance', name: 'Balance', type: 'text'},  //음 평균 type: 'range'
                    {field: 'val_VolumControl', command: 'tag_VolumControl', name: 'Volume', type: 'text'},   //음량 조정 type: 'range'
                    {field: 'val_Bass', command: 'tag_Bass', name: 'Bass', type: 'text'},     //저음 type: 'range'
                    {field: 'val_Treble', command: 'tag_Treble', name: 'Treble', type: 'text'}   //고음 type: 'range'
                ]
            },
            {
                title: 'Tile',
                mobile: '',
                icon: 'th-large',
                layout_type: 'free',
                data: [
                    {
                        name: 'tile_content',
                        input_mode: { //input mode
                            field: 'val_InputSelect', command: 'tag_InputSelect', name: 'Input', type: 'select',
                            statuses: [
                                {value: '20', text: 'AV'},
                                {value: '40', text: 'Component'},
                                {value: '60', text: 'RGB'},
                                {value: '70', text: 'DVI-D(PC)'},
                                {value: '80', text: 'DVI-D(DTV)'},
                                {value: '90', text: 'HDMI(HDMI1)(DTV)'},
                                {value: 'A0', text: 'HDMI(HDMI1)(PC)'},
                                {value: 'B0', text: 'SuperSign'},
                                {value: 'C0', text: 'Display Port(DTV)'},
                                {value: 'D0', text: 'Display Port(PC)'}
                            ]
                        },
                        tile_mode: { //타일모드 확인
                            field: 'val_TileModeCheck', command: 'tag_TileMode', name: 'Row X Column', type: 'select',
                            statuses: [
                                {value: '00', text: 'OFF'},
                                //1 X 2
                                {value: '0201', text: '1 X 2'},{value: '0301', text: '1 X 3'},
                                {value: '0401', text: '1 X 4'},{value: '0501', text: '1 X 5'},{value: '0601', text: '1 X 6'},
                                {value: '0701', text: '1 X 7'},{value: '0801', text: '1 X 8'},{value: '0901', text: '1 X 9'},
                                {value: '0A01', text: '1 X 10'},{value: '0B01', text: '1 X 11'},{value: '0C01', text: '1 X 12'},
                                {value: '0D01', text: '1 X 13'},{value: '0E01', text: '1 X 14'},{value: '0F01', text: '1 X 15'},
                                //2 X 1
                                {value: '0102', text: '2 X 1'},{value: '0202', text: '2 X 2'},{value: '0302', text: '2 X 3'},
                                {value: '0402', text: '2 X 4'},{value: '0502', text: '2 X 5'},{value: '0602', text: '2 X 6'},
                                {value: '0702', text: '2 X 7'},{value: '0802', text: '2 X 8'},{value: '0902', text: '2 X 9'},
                                {value: '0A02', text: '2 X 10'},{value: '0B02', text: '2 X 11'},{value: '0C02', text: '2 X 12'},
                                {value: '0D02', text: '2 X 13'},{value: '0E02', text: '2 X 14'},{value: '0F02', text: '2 X 15'},
                                //3 X 1
                                {value: '0103', text: '3 X 1'},{value: '0203', text: '3 X 2'},{value: '0303', text: '3 X 3'},
                                {value: '0403', text: '3 X 4'},{value: '0503', text: '3 X 5'},{value: '0603', text: '3 X 6'},
                                {value: '0703', text: '3 X 7'},{value: '0803', text: '3 X 8'},{value: '0903', text: '3 X 9'},
                                {value: '0A03', text: '3 X 10'},{value: '0B03', text: '3 X 11'},{value: '0C03', text: '3 X 12'},
                                {value: '0D03', text: '3 X 13'},{value: '0E03', text: '3 X 14'},{value: '0F03', text: '3 X 15'},
                                //4 X 1
                                {value: '0104', text: '4 X 1'},{value: '0204', text: '4 X 2'},{value: '0304', text: '4 X 3'},
                                {value: '0404', text: '4 X 4'},{value: '0504', text: '4 X 5'},{value: '0604', text: '4 X 6'},
                                {value: '0704', text: '4 X 7'},{value: '0804', text: '4 X 8'},{value: '0904', text: '4 X 9'},
                                {value: '0A04', text: '4 X 10'},{value: '0B04', text: '4 X 11'},{value: '0C04', text: '4 X 12'},
                                {value: '0D04', text: '4 X 13'},{value: '0E04', text: '4 X 14'},{value: '0F04', text: '4 X 15'},
                                //5 X 1
                                {value: '0105', text: '5 X 1'},{value: '0205', text: '5 X 2'},{value: '0305', text: '5 X 3'},
                                {value: '0405', text: '5 X 4'},{value: '0505', text: '5 X 5'},{value: '0605', text: '5 X 6'},
                                {value: '0705', text: '5 X 7'},{value: '0805', text: '5 X 8'},{value: '0905', text: '5 X 9'},
                                {value: '0A05', text: '5 X 10'},{value: '0B05', text: '5 X 11'},{value: '0C05', text: '5 X 12'},
                                {value: '0D05', text: '5 X 13'},{value: '0E05', text: '5 X 14'},{value: '0F05', text: '5 X 15'},
                                //6 X 1
                                {value: '0106', text: '6 X 1'},{value: '0206', text: '6 X 2'},{value: '0306', text: '6 X 3'},
                                {value: '0406', text: '6 X 4'},{value: '0506', text: '6 X 5'},{value: '0606', text: '6 X 6'},
                                {value: '0706', text: '6 X 7'},{value: '0806', text: '6 X 8'},{value: '0906', text: '6 X 9'},
                                {value: '0A06', text: '6 X 10'},{value: '0B06', text: '6 X 11'},{value: '0C06', text: '6 X 12'},
                                {value: '0D06', text: '6 X 13'},{value: '0E06', text: '6 X 14'},{value: '0F06', text: '6 X 15'},
                                //7 X 1
                                {value: '0107', text: '7 X 1'},{value: '0207', text: '7 X 2'},{value: '0307', text: '7 X 3'},
                                {value: '0407', text: '7 X 4'},{value: '0507', text: '7 X 5'},{value: '0607', text: '7 X 6'},
                                {value: '0707', text: '7 X 7'},{value: '0807', text: '7 X 8'},{value: '0907', text: '7 X 9'},
                                {value: '0A07', text: '7 X 10'},{value: '0B07', text: '7 X 11'},{value: '0C07', text: '7 X 12'},
                                {value: '0D07', text: '7 X 13'},{value: '0E07', text: '7 X 14'},{value: '0F07', text: '7 X 15'},
                                //8 X 1
                                {value: '0108', text: '8 X 1'},{value: '0208', text: '8 X 2'},{value: '0308', text: '8 X 3'},
                                {value: '0408', text: '8 X 4'},{value: '0508', text: '8 X 5'},{value: '0608', text: '8 X 6'},
                                {value: '0708', text: '8 X 7'},{value: '0808', text: '8 X 8'},{value: '0908', text: '8 X 9'},
                                {value: '0A08', text: '8 X 10'},{value: '0B08', text: '8 X 11'},{value: '0C08', text: '8 X 12'},
                                {value: '0D08', text: '8 X 13'},{value: '0E08', text: '8 X 14'},{value: '0F08', text: '8 X 15'},
                                //9 X 1
                                {value: '0109', text: '9 X 1'},{value: '0209', text: '9 X 2'},{value: '0309', text: '9 X 3'},
                                {value: '0409', text: '9 X 4'},{value: '0509', text: '9 X 5'},{value: '0609', text: '9 X 6'},
                                {value: '0709', text: '9 X 7'},{value: '0809', text: '9 X 8'},{value: '0909', text: '9 X 9'},
                                {value: '0A09', text: '9 X 10'},{value: '0B09', text: '9 X 11'},{value: '0C09', text: '9 X 12'},
                                {value: '0D09', text: '9 X 13'},{value: '0E09', text: '9 X 14'},{value: '0F09', text: '9 X 15'},
                                //10 X 1
                                {value: '010A', text: '10 X 1'},{value: '020A', text: '10 X 2'},{value: '030A', text: '10 X 3'},
                                {value: '040A', text: '10 X 4'},{value: '050A', text: '10 X 5'},{value: '060A', text: '10 X 6'},
                                {value: '070A', text: '10 X 7'},{value: '080A', text: '10 X 8'},{value: '090A', text: '10 X 9'},
                                {value: '0A0A', text: '10 X 10'},{value: '0B0A', text: '10 X 11'},{value: '0C0A', text: '10 X 12'},
                                {value: '0D0A', text: '10 X 13'},{value: '0E0A', text: '10 X 14'},{value: '0F0A', text: '10 X 15'},
                                //11 X 1
                                {value: '010B', text: '11 X 1'},{value: '020B', text: '11 X 2'},{value: '030B', text: '11 X 3'},
                                {value: '040B', text: '11 X 4'},{value: '050B', text: '11 X 5'},{value: '060B', text: '11 X 6'},
                                {value: '070B', text: '11 X 7'},{value: '080B', text: '11 X 8'},{value: '090B', text: '11 X 9'},
                                {value: '0A0B', text: '11 X 10'},{value: '0B0B', text: '11 X 11'},{value: '0C0B', text: '11 X 12'},
                                {value: '0D0B', text: '11 X 13'},{value: '0E0B', text: '11 X 14'},{value: '0F0B', text: '11 X 15'},
                                //12 X 1
                                {value: '010C', text: '12 X 1'},{value: '020C', text: '12 X 2'},{value: '030C', text: '12 X 3'},
                                {value: '040C', text: '12 X 4'},{value: '050C', text: '12 X 5'},{value: '060C', text: '12 X 6'},
                                {value: '070C', text: '12 X 7'},{value: '080C', text: '12 X 8'},{value: '090C', text: '12 X 9'},
                                {value: '0A0C', text: '12 X 10'},{value: '0B0C', text: '12 X 11'},{value: '0C0C', text: '12 X 12'},
                                {value: '0D0C', text: '12 X 13'},{value: '0E0C', text: '12 X 14'},{value: '0F0C', text: '12 X 15'},
                                //13 X 1
                                {value: '010D', text: '13 X 1'},{value: '020D', text: '13 X 2'},{value: '030D', text: '13 X 3'},
                                {value: '040D', text: '13 X 4'},{value: '050D', text: '13 X 5'},{value: '060D', text: '13 X 6'},
                                {value: '070D', text: '13 X 7'},{value: '080D', text: '13 X 8'},{value: '090D', text: '13 X 9'},
                                {value: '0A0D', text: '13 X 10'},{value: '0B0D', text: '13 X 11'},{value: '0C0D', text: '13 X 12'},
                                {value: '0D0D', text: '13 X 13'},{value: '0E0D', text: '13 X 14'},{value: '0F0D', text: '13 X 15'},
                                //14 X 1
                                {value: '010E', text: '14 X 1'},{value: '020E', text: '14 X 2'},{value: '030E', text: '14 X 3'},
                                {value: '040E', text: '14 X 4'},{value: '050E', text: '14 X 5'},{value: '060E', text: '14 X 6'},
                                {value: '070E', text: '14 X 7'},{value: '080E', text: '14 X 8'},{value: '090E', text: '14 X 9'},
                                {value: '0A0E', text: '14 X 10'},{value: '0B0E', text: '14 X 11'},{value: '0C0E', text: '14 X 12'},
                                {value: '0D0E', text: '14 X 13'},{value: '0E0E', text: '14 X 14'},{value: '0F0E', text: '14 X 15'},
                                //15 X 1
                                {value: '010F', text: '15 X 1'},{value: '020F', text: '15 X 2'},{value: '030F', text: '15 X 3'},
                                {value: '040F', text: '15 X 4'},{value: '050F', text: '15 X 5'},{value: '060F', text: '15 X 6'},
                                {value: '070F', text: '15 X 7'},{value: '080F', text: '15 X 8'},{value: '090F', text: '15 X 9'},
                                {value: '0A0F', text: '15 X 10'},{value: '0B0F', text: '15 X 11'},{value: '0C0F', text: '15 X 12'},
                                {value: '0D0F', text: '15 X 13'},{value: '0E0F', text: '15 X 14'},{value: '0F0F', text: '15 X 15'}
                            ]
                        },
                        tile_id: { //타일 ID
                            field: 'val_TileID', command: 'tag_TileID', name: 'Tile ID', type: 'select',
                            statuses: [
                                //1 ~ 16
                                {value: '01', text: '1'},{value: '02', text: '2'},{value: '03', text: '3'},{value: '04', text: '4'},{value: '05', text: '5'},
                                {value: '06', text: '6'},{value: '07', text: '7'},{value: '08', text: '8'},{value: '09', text: '9'},{value: '0A', text: '10'},
                                {value: '0B', text: '11'},{value: '0C', text: '12'},{value: '0D', text: '13'},{value: '0E', text: '14'},{value: '0F', text: '15'},
                                {value: '10', text: '16'},
                                //17 ~ 32
                                {value: '11', text: '17'},{value: '12', text: '18'},{value: '13', text: '19'},{value: '14', text: '20'},{value: '15', text: '21'},
                                {value: '16', text: '22'},{value: '17', text: '23'},{value: '18', text: '24'},{value: '19', text: '25'},{value: '1A', text: '26'},
                                {value: '1B', text: '27'},{value: '1C', text: '28'},{value: '1D', text: '29'},{value: '1E', text: '30'},{value: '1F', text: '31'},
                                {value: '20', text: '32'},
                                //33 ~ 48
                                {value: '21', text: '33'},{value: '22', text: '34'},{value: '23', text: '35'},{value: '24', text: '36'},{value: '25', text: '37'},
                                {value: '26', text: '38'},{value: '27', text: '39'},{value: '28', text: '40'},{value: '29', text: '41'},{value: '2A', text: '42'},
                                {value: '2B', text: '43'},{value: '2C', text: '44'},{value: '2D', text: '45'},{value: '2E', text: '46'},{value: '2F', text: '47'},
                                {value: '30', text: '48'},
                                //49 ~ 64
                                {value: '31', text: '49'},{value: '32', text: '50'},{value: '33', text: '51'},{value: '34', text: '52'},{value: '35', text: '53'},
                                {value: '36', text: '54'},{value: '37', text: '55'},{value: '38', text: '56'},{value: '39', text: '57'},{value: '3A', text: '58'},
                                {value: '3B', text: '59'},{value: '3C', text: '60'},{value: '3D', text: '61'},{value: '3E', text: '62'},{value: '3F', text: '63'},
                                {value: '40', text: '64'},
                                //65 ~ 80
                                {value: '41', text: '65'},{value: '42', text: '66'},{value: '43', text: '67'},{value: '44', text: '68'},{value: '45', text: '69'},
                                {value: '46', text: '70'},{value: '47', text: '71'},{value: '48', text: '72'},{value: '49', text: '73'},{value: '4A', text: '74'},
                                {value: '4B', text: '75'},{value: '4C', text: '76'},{value: '4D', text: '77'},{value: '4E', text: '78'},{value: '4F', text: '79'},
                                {value: '50', text: '80'},
                                //81 ~ 96
                                {value: '51', text: '81'},{value: '52', text: '82'},{value: '53', text: '83'},{value: '54', text: '84'},{value: '55', text: '85'},
                                {value: '56', text: '86'},{value: '57', text: '87'},{value: '58', text: '88'},{value: '59', text: '89'},{value: '5A', text: '90'},
                                {value: '5B', text: '91'},{value: '5C', text: '92'},{value: '5D', text: '93'},{value: '5E', text: '94'},{value: '5F', text: '95'},
                                {value: '60', text: '96'},
                                //97 ~ 112
                                {value: '61', text: '97'},{value: '62', text: '98'},{value: '63', text: '99'},{value: '64', text: '100'},{value: '65', text: '101'},
                                {value: '66', text: '102'},{value: '67', text: '103'},{value: '68', text: '104'},{value: '69', text: '105'},{value: '6A', text: '106'},
                                {value: '6B', text: '107'},{value: '6C', text: '108'},{value: '6D', text: '109'},{value: '6E', text: '110'},{value: '6F', text: '111'},
                                {value: '70', text: '112'},
                                //113 ~ 128
                                {value: '71', text: '113'},{value: '72', text: '114'},{value: '73', text: '115'},{value: '74', text: '116'},{value: '75', text: '117'},
                                {value: '76', text: '118'},{value: '77', text: '119'},{value: '78', text: '120'},{value: '79', text: '121'},{value: '7A', text: '122'},
                                {value: '7B', text: '123'},{value: '7C', text: '124'},{value: '7D', text: '125'},{value: '7E', text: '126'},{value: '7F', text: '127'},
                                {value: '80', text: '128'},
                                //129 ~ 144
                                {value: '81', text: '129'},{value: '82', text: '130'},{value: '83', text: '131'},{value: '84', text: '132'},{value: '85', text: '133'},
                                {value: '86', text: '134'},{value: '87', text: '135'},{value: '88', text: '136'},{value: '89', text: '137'},{value: '8A', text: '138'},
                                {value: '8B', text: '139'},{value: '8C', text: '140'},{value: '8D', text: '141'},{value: '8E', text: '142'},{value: '8F', text: '143'},
                                {value: '90', text: '144'},
                                //145 ~ 160
                                {value: '91', text: '145'},{value: '92', text: '146'},{value: '93', text: '147'},{value: '94', text: '148'},{value: '95', text: '149'},
                                {value: '96', text: '150'},{value: '97', text: '151'},{value: '98', text: '152'},{value: '99', text: '153'},{value: '9A', text: '154'},
                                {value: '9B', text: '155'},{value: '9C', text: '156'},{value: '9D', text: '157'},{value: '9E', text: '158'},{value: '9F', text: '159'},
                                {value: 'A0', text: '160'},
                                //161 ~ 176
                                {value: 'A1', text: '161'},{value: 'A2', text: '162'},{value: 'A3', text: '163'},{value: 'A4', text: '164'},{value: 'A5', text: '165'},
                                {value: 'A6', text: '166'},{value: 'A7', text: '167'},{value: 'A8', text: '168'},{value: 'A9', text: '169'},{value: 'AA', text: '170'},
                                {value: 'AB', text: '171'},{value: 'AC', text: '172'},{value: 'AD', text: '173'},{value: 'AE', text: '174'},{value: 'AF', text: '175'},
                                {value: 'B0', text: '176'},
                                //177 ~ 192
                                {value: 'B1', text: '177'},{value: 'B2', text: '178'},{value: 'B3', text: '179'},{value: 'B4', text: '180'},{value: 'B5', text: '181'},
                                {value: 'B6', text: '182'},{value: 'B7', text: '183'},{value: 'B8', text: '184'},{value: 'B9', text: '185'},{value: 'BA', text: '186'},
                                {value: 'BB', text: '187'},{value: 'BC', text: '188'},{value: 'BD', text: '189'},{value: 'BE', text: '190'},{value: 'BF', text: '191'},
                                {value: 'C0', text: '192'},
                                //193 ~ 208
                                {value: 'C1', text: '193'},{value: 'C2', text: '194'},{value: 'C3', text: '195'},{value: 'C4', text: '196'},{value: 'C5', text: '197'},
                                {value: 'C6', text: '198'},{value: 'C7', text: '199'},{value: 'C8', text: '200'},{value: 'C9', text: '201'},{value: 'CA', text: '202'},
                                {value: 'CB', text: '203'},{value: 'CC', text: '204'},{value: 'CD', text: '205'},{value: 'CE', text: '206'},{value: 'CF', text: '207'},
                                {value: 'D0', text: '208'},
                                //209 ~ 224
                                {value: 'D1', text: '209'},{value: 'D2', text: '210'},{value: 'D3', text: '211'},{value: 'D4', text: '212'},{value: 'D5', text: '213'},
                                {value: 'D6', text: '214'},{value: 'D7', text: '215'},{value: 'D8', text: '216'},{value: 'D9', text: '217'},{value: 'DA', text: '218'},
                                {value: 'DB', text: '219'},{value: 'DC', text: '220'},{value: 'DD', text: '221'},{value: 'DE', text: '222'},{value: 'DF', text: '223'},
                                {value: 'E0', text: '224'},
                                // 225
                                {value: 'E1', text: '225'}
                            ]
                        },
                        tile_N_mode: { //자연스러운 모드
                            field: 'val_TileNaturalMode', command: 'tag_TileNaturalMode', name: 'Tile Natural Mode', type: 'select',
                            statuses: [
                                {value: '00', text: 'OFF'},
                                {value: '01', text: 'ON'}
                            ]
                        },
                        tile_size_H: {//타일 수평 크기
                            field: 'val_SizeH', command: 'tag_SizeH', name: 'H', type: 'text'
                        },
                        tile_size_V: {//타일 수직 크기
                            field: 'val_SizeV', command: 'tag_SizeV', name: 'V', type: 'text'
                        },
                        tile_posi_H: {//타일 수평
                            field: 'val_PositionH', command: 'tag_PositionH', name: 'H', type: 'text'
                        },
                        tile_posi_V: {//타일 수직
                            field: 'val_PositionV', command: 'tag_PositionV', name: 'V', type: 'text'
                        }
                    }
                ]
            },
            {
                title: 'White Balance',
                mobile: '',
                icon: 'adjust',
                layout_type: 'normal',
                data: [
                    {field: 'val_RedGain', command: 'tag_RedGain', name: 'Red - Gain', type: 'text'}, //type: 'range'
                    {field: 'val_RedOffset', command: 'tag_RedOffset', name: 'Red - Offset', type: 'text'},//type: 'range'
                    {field: 'val_GreenGain', command: 'tag_GreenGain', name: 'Green - Gain', type: 'text'},//type: 'range'
                    {field: 'val_GreenOffset', command: 'tag_GreenOffset', name: 'Green - Offset', type: 'text'},//type: 'range'
                    {field: 'val_BlueGain', command: 'tag_BlueGain', name: 'Blue - Gain', type: 'text'},//type: 'range'
                    {field: 'val_BlueOffset', command: 'tag_BlueOffset', name: 'Blue - Offset', type: 'text'}//type: 'range'
                ]
            },
            {
                title: 'Remote Controller',
                mobile: '',
                icon: 'rss',
                layout_type: 'free',
                data: [
                    {
                        name: 'remote_content',
                        power: {field: 'val_PowerStatus', command: 'tag_PowerStatus'},
                        monitor: {field: 'val_MonitorStatus', command: 'tag_MonitorStatus'},
                        remote: {field: 'val_RemoteControl', command: 'tag_RemoteControl'}
                    }
                ]
            }
        ];
  });

