'use strict';

angular.module('signageApp')
    .controller('LiveCtrl', function ($rootScope, $window, AuthService, $route, Session, $http, $scope, ENV, DP_By_MP, AuthMyTagging, $upload, ngTableParams,
                                             $timeout, User, Admin, Manager, Member, $routeParams, $location, $cookieStore,
                                             $filter, $q, MediaPlayer, MP_DB, MP_DM, Display, DP_DB, DP_DM, History, Organization,
                                             Site, Location, Shop, Tree, Modal, SharedService, Utils) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        $scope.isAuthenticated = AuthService.isAuthenticated();
        $scope.$parent.updateInformation();

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });
        //only live status
        $scope.$on("DP_SERIAL_NUMBER", function(){
            $scope.display_serial_number = SharedService.display_serial_number;
        });

        //--------------------------------------------------------------------------------------------------------------
        // Static
        //--------------------------------------------------------------------------------------------------------------
        $scope.mp_spec_pdf_uri = '/download/doc/spec/mediaplayer/';         // -> /download/doc/spec/mediaplayer/<string:model_name>
        $scope.dp_spec_pdf_uri = '/download/doc/spec/display/';
        $scope.mp_model_img_uri = './images/model/mp/'; // -> /images/model/mp/<string:model_name>/<string:img_name>
        $scope.dp_model_img_uri = './images/model/dp/'; // -> /images/model/dp/<string:model_name>/<string:img_name>
        $scope.img = [
            {name: '1.jpg'},    //front
            {name: '2.jpg'},    //back
            {name: '3.jpg'},    //top
            {name: '4.jpg'},    //left
            {name: '5.jpg'}     //right
        ];

        //--------------------------------------------------------------------------------------------------------------
        // Logic
        //--------------------------------------------------------------------------------------------------------------
        var META = {
            'media_player': {
                resource: MediaPlayer
            },
            'displays': {
                resource: Display
            },
            'history' : {
                resource: History
            }
        };

        //--------------------------------------------------------------------------------------------------------------
        // Tree
        //--------------------------------------------------------------------------------------------------------------
        $scope.tree_menu = 'by_site';

        $scope.live_status = {
            Information: {data: null, flag: false},
            MediaPlayer: {flag: false},
            Display: {flag: false}
        };

        if($route.current.params.mediaPlayerSerialNumber == undefined){
            $scope.mediaPlayerSerialNumber = $scope.media_player_serial_number;
        }else{
            $scope.mediaPlayerSerialNumber = $route.current.params.mediaPlayerSerialNumber;
        }

        if($route.current.params.displaySerialNumber != undefined){
            $scope.displaySerialNumber = $route.current.params.displaySerialNumber;
        }

        $scope.$watch(function(){//live status define
            return $location.path();
        }, function(value){
            var current_uri = value;
            var uri_tag = current_uri.split("/");

            var live_status_mediaplayer_flag = false;
            var live_status_display_flag = false;

            if(uri_tag[2] == "media-players"){
                $scope.mediaPlayerSerialNumber = uri_tag[3];
                SharedService.prepForMediaPlayerSerialNumberBroadcast($scope.mediaPlayerSerialNumber);
                $scope.currentSN = $scope.mediaPlayerSerialNumber;
                live_status_mediaplayer_flag = true;
            }

            if(uri_tag[4] == "displays"){
                $scope.displaySerialNumber = uri_tag[5];
                SharedService.prepForDisplaySerialNumberBroadcast($scope.displaySerialNumber);
                $scope.currentSN = $scope.displaySerialNumber;
                live_status_display_flag = true;
                live_status_mediaplayer_flag = false;
            }else{
                $scope.displaySerialNumber = undefined;
                SharedService.prepForDisplaySerialNumberBroadcast($scope.displaySerialNumber);
                live_status_display_flag = false;
            }

            $scope.live_status.MediaPlayer.flag = live_status_mediaplayer_flag;
            $scope.live_status.Display.flag = live_status_display_flag;
        });

        //Tagging List Remove
        //AuthMyTagging.get(function(data){
        //    $scope.tag_list = data.objects;
        //});

        $scope.historyTableParams = new ngTableParams({
            page: 1,            // show first page
            count: 20           // count per page
        }, {
            counts: [],
            total: 0,// length of data
            getData: function ($defer, params) {
                var api_params = {};
                api_params['limit'] = params.count();
                api_params['offset'] = (params.page() - 1) * api_params['limit'];
                api_params['mp_serial_number'] = $scope.mediaPlayerSerialNumber;

                History.get(api_params, function (data) {
                    $timeout(function () {
                        $scope.dataSet = data.objects;
                        params.total(data.meta.total_count);
                        $defer.resolve($scope.dataSet);
                    }, 500);
                });
            }
        });

        $scope.active_tree = function(tree_tag){
            $scope.tree_menu = tree_tag;
        };

        $scope.$watch('tree_menu', function(newVal, oldVal){
            $scope.tree_menu = newVal;

            var tree_arg = {};
            tree_arg["type"] = $scope.tree_menu;

            Tree.get(tree_arg, function (data) {
                //Tree View
                $scope.site_manager = data.objects;

                //Tree Open
                $scope.$watch('mediaPlayerSerialNumber', function (newVal, oldVal) {
                    $scope.mediaPlayerSerialNumber = newVal;
                    SharedService.prepForMediaPlayerSerialNumberBroadcast($scope.mediaPlayerSerialNumber);

                    var isInitializing = newVal == oldVal;

                    MediaPlayer.get({serial_number: $scope.mediaPlayerSerialNumber}, function (data) {
                        if(data.status == 200){
                            //reload history table
                            $timeout(function(){
                                $scope.historyTableParams.reload();
                            }, 500);

                            $scope.mp = data.objects[0];

                            if($scope.mp.group_status > 0 && $scope.mp.group_status < 3){//open pop
                                $timeout(function() {
                                    angular.element('.diag_btn').triggerHandler('click');
                                }, 0);
                            }

                            if (isInitializing) {//tree auto open
                                $scope.media_player = $scope.mp;
                                var direct_open_flag = false;

                                var siteCount = $scope.site_manager.length;
                                for (var i = 0; i < siteCount; i++) {
                                    var site_manager = $scope.site_manager[i];
                                    if($scope.media_player.FK_managers_id == site_manager.id){
                                        $scope.onClickManager(site_manager);

                                        if(tree_arg["type"] == 'by_site'){

                                            var siteCount = site_manager.site.length;
                                            for(var j = 0; j < siteCount; j++){
                                                var site = site_manager.site[j];
                                                if ($scope.media_player.site == site.text) {
                                                    $scope.onClickSite(site);

                                                    var locationCount = site.location.length;
                                                    for (var k = 0; k < locationCount; k++) {
                                                        var location = site.location[k];
                                                        if ($scope.media_player.location == location.text && site.sub_count > 1) {
                                                            $scope.onClickLocation(location);

                                                            var shopCount = location.shop.length;
                                                            for (var l = 0; l < shopCount; l++) {
                                                                var shop = location.shop[l];
                                                                if ($scope.media_player.shop == shop.text && site.sub_count > 1 && location.sub_count > 1) {
                                                                    $scope.onClickShop(shop);
                                                                    break;
                                                                }
                                                            }
                                                            break;
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                            break;
                                        }else if(tree_arg["type"] == 'by_dg'){
                                            for (var k = 0, shopCount = site_manager.dg.length; k < shopCount; ++k) {
                                                var dg = site_manager.dg[k];

                                                if ($scope.media_player.FK_device_groups_id == dg.id) {
                                                    $scope.onClickDeviceGroup(dg);
                                                }
                                            }
                                            break;
                                        }
                                        break;
                                    }
                                }
                            }

                            $scope.live_status.Information.data = null;
                            $scope.live_status.Information.flag = false;

                            $scope.currentSN = $scope.mediaPlayerSerialNumber;

                            var argument = {};
                            argument['mp_serial_number'] = $scope.mediaPlayerSerialNumber;

                            if($scope.displaySerialNumber != undefined){//case display serial number
                                $scope.currentSN = $scope.displaySerialNumber;

                                angular.forEach($scope.mp.dp_group_objs, function(v, k){
                                    angular.forEach(v.search_dps_by_dpg_obj, function(sub_v, sub_k){
                                        if(sub_v.serial_number == $scope.displaySerialNumber){
                                            argument['dp_group_name'] = v.name;
                                        }
                                    });
                                });

                                $scope.live_status.MediaPlayer.flag = false;
                                $scope.live_status.Display.flag = true;
                            }else{
                                $scope.live_status.MediaPlayer.flag = true;
                                $scope.live_status.Display.flag = false;
                            }

                            DP_By_MP.get(argument, function(data){
                                $scope.link_list = data.objects;
                                $scope.tab_length = $scope.link_list.length;
                            });
                        }else{
                            $scope.live_status.Information.data = null;
                            $scope.live_status.Information.flag = false;
                            $scope.live_status.MediaPlayer.flag = false;
                            $scope.live_status.Display.flag = false;
                        }
                    });
                });
            });
        });

        $scope.$watch('displaySerialNumber', function (newVal, oldVal){
            $scope.displaySerialNumber = newVal;
            SharedService.prepForDisplaySerialNumberBroadcast($scope.displaySerialNumber);

            $scope.live_status.Information.data = null;
            $scope.live_status.Information.flag = false;
            if($scope.displaySerialNumber != undefined){
                Display.get({serial_number: $scope.displaySerialNumber}, function(data){
                    if(data.status == 200) {
                        $scope.display = data.objects[0];
                    }
                });
            }
        });

        $scope.$watch('currentSN', function(newVal, oldVal){
            $scope.currentSN = newVal;
        });

        $scope.$watch('live_status.Information.flag', function(newVal, oldVal){
            $scope.live_status.Information.flag = newVal;
        });

        $scope.$watch('live_status.MediaPlayer.flag', function(newVal, oldVal){
            $scope.live_status.MediaPlayer.flag = newVal;
        });

        $scope.$watch('live_status.Display.flag', function(newVal, oldVal){
            $scope.live_status.Display.flag = newVal;
        });

        //--------------------------------------------------------------------------------------------------------------
        // Filter
        //--------------------------------------------------------------------------------------------------------------

        $scope.get = function (dic, field) {
            if (!dic || !field) {
                return '';
            }
            if (!dic[field]) {
                return 'N/A';
            }

            return dic[field];
        };

        $scope.showStatus = function (statuses, val) {
            var selected = $filter('filter')(statuses, {value: val});
            return (val && selected.length) ? selected[0].text : 'Not set';
        };

        //--------------------------------------------------------------------------------------------------------------
        // Func
        //--------------------------------------------------------------------------------------------------------------

        $scope.openPDF = function(pdf_location, model_name){
            $scope.pdf_uri = pdf_location + model_name + '.pdf';
            $http.get(ENV.host + $scope.pdf_uri)
                .success(function(data, status, headers, config) {
                    if(status == 200){
                        $scope.down_pdf_uri = $scope.pdf_uri;

                        if(navigator.appVersion.indexOf("MSIE 8") != -1)
                            window.location = $scope.down_pdf_uri;
                        else
                            window.open($scope.down_pdf_uri);
                    }
                }).error(function(data, status, headers, config) {
                    if(status == 404){
                        $scope.down_pdf_uri = '';
                        alert("There is no item on this model.");
                    }
                });
        };

        $scope.getOS = function (osString) {
            var windowString = 'windows';
            var linuxString = 'linux';

            if (osString == undefined)
                return windowString;

            var find_os = osString.toLowerCase().indexOf(windowString);    //없는 경우 -1 리턴

            if (find_os != -1) {
                return windowString;
            } else {
                return linuxString;
            }

            return 'ERROR';
        };

        //--------------------------------------------------------------------------------------------------------------
        // Modified
        //--------------------------------------------------------------------------------------------------------------
        $scope.updateMediaPlayerByDB = function(field, data){
            //----------------------------------------------------------------------------------------------------------
            // [ Validate ]
            //----------------------------------------------------------------------------------------------------------
            if(field=='polling_time'){
                if(data != 0 && data < 30) return 'Please input more than 30 seconds!';
            }

            //----------------------------------------------------------------------------------------------------------
            // [ Update ]
            //----------------------------------------------------------------------------------------------------------
            var argument = {};

            argument[field] = data;

            MP_DB.update({serial_number: $scope.mediaPlayerSerialNumber}, argument);
        };

        $scope.updateMediaPlayerByDM = function (type, command, data) {
            var argument = {};

            argument['type'] = type;
            argument['command'] = command;
            argument['data'] = data;
            argument['log'] = command + " " + data;

            MP_DM.update({serial_number: $scope.mediaPlayerSerialNumber}, argument);
        };

        $scope.updateDisplayByDB = function(field, data, serial_number){

            if(field == 'timer_begin_sat' || field == 'timer_end_sat'
              || field == 'timer_begin_sun' || field == 'timer_end_sun'
              || field == 'timer_begin_mon' || field == 'timer_end_mon'
              || field == 'timer_begin_tue' || field == 'timer_end_tue'
              || field == 'timer_begin_wed' || field == 'timer_end_wed'
              || field == 'timer_begin_thu' || field == 'timer_end_thu'
              || field == 'timer_begin_fri' || field == 'timer_end_fri'){
                var val = ""+data;
                //var regExp = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
                var regExp = /^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]:[0][0]$/;
                var var_match = val.match(regExp) != null;
                var regExp2 = /^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]$/;
                var var_match2 = val.match(regExp2) != null;

                if(var_match || var_match2){

                }else{
                  return "Date format is not correct.";
                }
            }

            var argument = {};

            argument[field] = data;

            DP_DB.update({serial_number: serial_number}, argument);
        };

        $scope.updateDisplayTimerSettingByDB = function(field, data, serial_number){
            var argument = {};

            if(data == '0'){
                argument[field] = false;
            }else if(data == '1'){
                argument[field] = true;
            }

            DP_DB.update({serial_number: serial_number}, argument);
        };

        // Calendar
        $scope.opened = {};
        $scope.openCalendar = function ($event, target, info, index) {
            $event.preventDefault();
            $event.stopPropagation();

            if (!$scope.opened[target]) $scope.opened[target] = {};
            if (index != undefined) {
                if (!$scope.opened[target][index]) $scope.opened[target][index] = {};
                $scope.opened[target][index][info.field] = true;
            } else {
                $scope.opened[target][info.field] = true;
            }
        };

        $scope.changeDate = function (target, data, info, index, dp_serial_number) {
            var model = index != undefined ? $scope[target][index] : $scope[target];
            var argument = {};

            argument[info.field] = $filter('date')(data, 'yyyy-MM-dd');
            if(target == 'media_player'){
                MP_DB.update({serial_number: $scope.mediaPlayerSerialNumber}, argument);
            }else if(target == 'link_list'){
                DP_DB.update({serial_number: dp_serial_number}, argument);
            }else{
                alert('target error');
            }
        };

        $scope.updateDisplayByDM = function(type, command, data, serial_number, pre_data){
            var argument = {};

            var command_mean = command.split('_');

            argument['type'] = type;
            argument['command'] = command;
            argument['data'] = data;

            if(pre_data == undefined){
                argument['log'] = command_mean[1] + " : " + data;
            }else{
                argument['log'] = command_mean[1] + " : "+ pre_data + " -> " + data;
            }

            var log_data = '';
            var log_pre_data = '';

            //----------------------------------------------------------------------------------------------------------
            // [ Log ] Picture
            //----------------------------------------------------------------------------------------------------------
            if(command_mean[1] == "AspectRatio"){
                switch(pre_data){
                    case '01':    log_pre_data = '4:3';   break;
                    case '02':    log_pre_data = '16:9';   break;
                    case '04':    log_pre_data = 'Zoom';   break;
                    case '09':    log_pre_data = 'Source';   break;
                    case '10':    log_pre_data = 'Cinema Zoom';   break;
                    default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '01':    log_data = '4:3';   break;
                    case '02':    log_data = '16:9';   break;
                    case '04':    log_data = 'Zoom';   break;
                    case '09':    log_data = 'Source';   break;
                    case '10':    log_data = 'Cinema Zoom';   break;
                    default:    log_data = 'Unknown';
                }

                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "PictureMode"){
                switch(pre_data){
                  case '00':    log_pre_data = 'Vivid';   break;
                  case '01':    log_pre_data = 'Standard';   break;
                  case '02':    log_pre_data = 'Cinema';   break;
                  case '03':    log_pre_data = 'Sport';   break;
                  case '04':    log_pre_data = 'Game';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'Vivid';   break;
                    case '01':    log_data = 'Standard';   break;
                    case '02':    log_data = 'Cinema';   break;
                    case '03':    log_data = 'Sport';   break;
                    case '04':    log_data = 'Game';   break;
                    default:    log_data = 'Unknown';
                }
              argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;

            }else if(command_mean[1] == "EnergySaving"){
                switch(pre_data){
                  case '00':    log_pre_data = 'Disabled';   break;
                  case '01':    log_pre_data = 'Minimum';   break;
                  case '02':    log_pre_data = 'Medium';   break;
                  case '03':    log_pre_data = 'Maximum';   break;
                  case '04':    log_pre_data = 'Auto';   break;
                  case '05':    log_pre_data = 'Screen Off';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'Disabled';   break;
                    case '01':    log_data = 'Minimum';   break;
                    case '02':    log_data = 'Medium';   break;
                    case '03':    log_data = 'Maximum';   break;
                    case '04':    log_data = 'Auto';   break;
                    case '05':    log_data = 'Screen Off';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }
            //----------------------------------------------------------------------------------------------------------
            // [ Log ] Option
            //----------------------------------------------------------------------------------------------------------
            if(command_mean[1] == "ismMode"){
                switch(pre_data){
                  case '01':    log_pre_data = 'Inversion';   break;
                  case '02':    log_pre_data = 'Orbiter';   break;
                  case '04':    log_pre_data = 'White Wash';   break;
                  case '08':    log_pre_data = 'Normal';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '01':    log_data = 'Inversion';   break;
                    case '02':    log_data = 'Orbiter';   break;
                    case '04':    log_data = 'White Wash';   break;
                    case '08':    log_data = 'Normal';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "dpmSelect"){
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = '5 (sec)';   break;
                  case '02':    log_pre_data = '10 (sec)';   break;
                  case '03':    log_pre_data = '15 (sec)';   break;
                  case '04':    log_pre_data = '1 (min)';   break;
                  case '05':    log_pre_data = '3 (min)';   break;
                  case '06':    log_pre_data = '5 (min)';   break;
                  case '07':    log_pre_data = '10 (min)';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = '5 (sec)';   break;
                    case '02':    log_data = '10 (sec)';   break;
                    case '03':    log_data = '15 (sec)';   break;
                    case '04':    log_data = '1 (min)';   break;
                    case '05':    log_data = '3 (min)';   break;
                    case '06':    log_data = '5 (min)';   break;
                    case '07':    log_data = '10 (min)';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "osdSelect"){
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = 'ON';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'ON';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "Lang"){
                switch(pre_data){
                  case '00':    log_pre_data = 'Czech';   break;
                  case '01':    log_pre_data = 'Danish';   break;
                  case '02':    log_pre_data = 'German';   break;
                  case '03':    log_pre_data = 'English';   break;
                  case '04':    log_pre_data = 'Spanish(European)';   break;
                  case '05':    log_pre_data = 'Greek';   break;
                  case '06':    log_pre_data = 'French';   break;
                  case '07':    log_pre_data = 'Italian';   break;
                  case '08':    log_pre_data = 'Dutch';   break;
                  case '09':    log_pre_data = 'Norwegian';   break;
                  case '0A':    log_pre_data = 'Portuguese';   break;
                  case '0B':    log_pre_data = 'Portuguese(Brazil)';   break;
                  case '0C':    log_pre_data = 'Russian';   break;
                  case '0D':    log_pre_data = 'Finnish';   break;
                  case '0E':    log_pre_data = 'Swedish';   break;
                  case '0F':    log_pre_data = 'Korean';   break;
                  case '10':    log_pre_data = 'Chinese(Cantonese)';   break;
                  case '11':    log_pre_data = 'Japanese';   break;
                  case '12':    log_pre_data = 'Chinese(Mandarin)';   break;
                  default:    log_pre_data = 'Unknown';
                }

                switch(data){
                    case '00':    log_data = 'Czech';   break;
                    case '01':    log_data = 'Danish';   break;
                    case '02':    log_data = 'German';   break;
                    case '03':    log_data = 'English';   break;
                    case '04':    log_data = 'Spanish(European)';   break;
                    case '05':    log_data = 'Greek';   break;
                    case '06':    log_data = 'French';   break;
                    case '07':    log_data = 'Italian';   break;
                    case '08':    log_data = 'Dutch';   break;
                    case '09':    log_data = 'Norwegian';   break;
                    case '0A':    log_data = 'Portuguese';   break;
                    case '0B':    log_data = 'Portuguese(Brazil)';   break;
                    case '0C':    log_data = 'Russian';   break;
                    case '0D':    log_data = 'Finnish';   break;
                    case '0E':    log_data = 'Swedish';   break;
                    case '0F':    log_data = 'Korean';   break;
                    case '10':    log_data = 'Chinese(Cantonese)';   break;
                    case '11':    log_data = 'Japanese';   break;
                    case '12':    log_data = 'Chinese(Mandarin)';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "Remote"){
                switch(pre_data){
                  case '00':    log_data = 'OFF';   break;
                  case '01':    log_data = 'ON';   break;
                  default:    log_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'ON';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }
            //----------------------------------------------------------------------------------------------------------
            // [ Log ] Time
            //----------------------------------------------------------------------------------------------------------
            if(command_mean[1] == "SleepTime"){
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = '10 Minutes';   break;
                  case '02':    log_pre_data = '20 Minutes';   break;
                  case '03':    log_pre_data = '30 Minutes';   break;
                  case '04':    log_pre_data = '60 Minutes';   break;
                  case '05':    log_pre_data = '90 Minutes';   break;
                  case '06':    log_pre_data = '120 Minutes';   break;
                  case '07':    log_pre_data = '180 Minutes';   break;
                  case '08':    log_pre_data = '240 Minutes';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = '10 Minutes';   break;
                    case '02':    log_data = '20 Minutes';   break;
                    case '03':    log_data = '30 Minutes';   break;
                    case '04':    log_data = '60 Minutes';   break;
                    case '05':    log_data = '90 Minutes';   break;
                    case '06':    log_data = '120 Minutes';   break;
                    case '07':    log_data = '180 Minutes';   break;
                    case '08':    log_data = '240 Minutes';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "AutoOff"){
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = 'OFF After 4 Hours';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'OFF After 4 Hours';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "AutoStandby"){
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = 'OFF After 15 Minutes';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'OFF After 15 Minutes';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }
            //----------------------------------------------------------------------------------------------------------
            // [ Log ] Audio
            //----------------------------------------------------------------------------------------------------------
            if(command_mean[1] == "Speaker"){
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = 'ON';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'ON';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "SoundMode"){
                switch(pre_data){
                  case '00':    log_pre_data = 'Clear';   break;
                  case '01':    log_pre_data = 'Standard';   break;
                  case '02':    log_pre_data = 'Music';   break;
                  case '03':    log_pre_data = 'Cinema';   break;
                  case '04':    log_pre_data = 'Sport';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'Clear';   break;
                    case '01':    log_data = 'Standard';   break;
                    case '02':    log_data = 'Music';   break;
                    case '03':    log_data = 'Cinema';   break;
                    case '04':    log_data = 'Sport';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "VolumMute"){
                switch(pre_data){
                  case '00':    log_pre_data = 'ON';   break;
                  case '01':    log_pre_data = 'OFF';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'ON';   break;
                    case '01':    log_data = 'OFF';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "AutoVolume"){
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = 'ON';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'ON';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }
            //----------------------------------------------------------------------------------------------------------
            // [ Log ] Tile
            //----------------------------------------------------------------------------------------------------------
            if(command_mean[1] == "InputSelect"){
                argument['log'] = command_mean[1] + " : " + showInput(pre_data) + " -> " + showInput(data);
            }else if(command_mean[1] == "TileID"){
                argument['log'] = command_mean[1] + " : " + todecimal(pre_data) + " -> " + todecimal(data);
            }else if(command_mean[1] == "TileNaturalMode"){
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'ON';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : " + log_data;
            }

            //----------------------------------------------------------------------------------------------------------
            // [ Log ] Remote Control
            //----------------------------------------------------------------------------------------------------------
            if(command_mean[1] == "PowerStatus")
            {
                var n = $scope.display.port_number.search("COM");
                if(n == -1) {
                    alert("It only works on serial communication.");
                    return;
                }
                switch(pre_data){
                  case '00':    log_pre_data = 'OFF';   break;
                  case '01':    log_pre_data = 'ON';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'OFF';   break;
                    case '01':    log_data = 'ON';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "MonitorStatus"){
                switch(pre_data){
                  case '00':    log_pre_data = 'ON';   break;
                  case '01':    log_pre_data = 'OFF';   break;
                  default:    log_pre_data = 'Unknown';
                }
                switch(data){
                    case '00':    log_data = 'ON';   break;
                    case '01':    log_data = 'OFF';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_pre_data + " -> " + log_data;
            }else if(command_mean[1] == "RemoteControl"){
                switch(data){
                    case '0B':    log_data = 'Input';   break;
                    case '10':    log_data = '0';   break;
                    case '11':    log_data = '1';   break;
                    case '12':    log_data = '2';   break;
                    case '13':    log_data = '3';   break;
                    case '14':    log_data = '4';   break;
                    case '15':    log_data = '5';   break;
                    case '16':    log_data = '6';   break;
                    case '17':    log_data = '7';   break;
                    case '18':    log_data = '8';   break;
                    case '19':    log_data = '9';   break;
                    case '43':    log_data = 'Menu';   break;
                    case '28':    log_data = 'Back';   break;
                    case '7B':    log_data = 'Tile';   break;
                    case '5B':    log_data = 'Exit';   break;
                    case 'E0':    log_data = 'brightness ( up )';   break;
                    case 'E1':    log_data = 'brightness ( down )';   break;
                    case '09':    log_data = 'Mute';   break;
                    case '02':    log_data = 'volume ( up )';   break;
                    case '03':    log_data = 'volume ( down )';   break;
                    case '07':    log_data = 'move ( left )';   break;
                    case '40':    log_data = 'move ( up )';   break;
                    case '44':    log_data = 'OK';   break;
                    case '41':    log_data = 'move ( down )';   break;
                    case '06':    log_data = 'move ( right )';   break;
                    default:    log_data = 'Unknown';
                }
                argument['log'] = command_mean[1] + " : "+ log_data;
            }

            DP_DM.update({serial_number: serial_number}, argument);
        };

        $scope.updateDisplayDateByDM = function (type, command, data, serial_number, target, info, index) {
            var argument = {};
            var command_mean = command.split('_');
            //var model = index != undefined ? $scope[target][index] : $scope[target];

            var param_data = data;
            var pre_data = ''+$filter('date')(target[info.field], 'yyyy-MM-dd');
            var pre_data_split = pre_data.split("-");
            var parse_pre_data = pre_data_split[0] + "-" + pre_data_split[1] + "-" + pre_data_split[2];

            var data = ''+$filter('date')(param_data, 'yyyy-MM-dd');
            var data_split = data.split("-");
            var parse_data = toHex(Number(data_split[0]) - 2010) + " " + toHex(data_split[1]) + " " + toHex(data_split[2]);

            argument['type'] = type;
            argument['command'] = command;
            argument['data'] = parse_data;

            if(pre_data == undefined){
                argument['log'] = command_mean[1] + " : " + data;
            }else{
                argument['log'] = command_mean[1] + " : "+ parse_pre_data + " -> " + parse_data;
            }

            DP_DM.update({serial_number: serial_number}, argument);
        };

        $scope.updateDisplayTimeByDM = function(type, command, data, serial_number, pre_data){
            var val = ""+data;
            //var regExp = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
            var regExp = /^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]$/;
            var var_match = val.match(regExp) != null;

            if(var_match){

            }else{
              return "Date format is not correct.";
            }

            var argument = {};

            var command_mean = command.split('_');

            argument['type'] = type;
            argument['command'] = command;
            var str_data = ''+data;
            var time = str_data.split(":");
            var parse_time = toHex(time[0]) + " " + toHex(time[1]) +  " " + toHex('00');
            argument['data'] = parse_time;

            if(pre_data == undefined){
                argument['log'] = command_mean[1] + " : " + data;
            }else{
                argument['log'] = command_mean[1] + " : "+ pre_data + " -> " + data;
            }

            DP_DM.update({serial_number: serial_number}, argument);
        };

        $scope.updateText = function(type, command, data, serial_number, pre_data){
            var argument = {};

            var command_mean = command.split('_');

            argument['type'] = type;
            argument['command'] = command;
            argument['data'] = toHex(data);

            //default
            argument['log'] = command_mean[1] + " : "+ pre_data + " -> " + data;

            if(command_mean[1] == "SizeV"){
                argument['log'] = command_mean[1] + " Set : " + data;
            }else if(command_mean[1] == "PositionV"){
                argument['log'] = command_mean[1] + " Set : " + data;
            }else if(command_mean[1] == "SizeH"){
                argument['log'] = command_mean[1] + " Set : " + data;
            }

            DP_DM.update({serial_number: serial_number}, argument);
        };

        $scope.updateRowColumn = function(type, command, data, serial_number, pre_data){
            var argument = {};
            var parse_data = '';
            var command_mean = command.split('_');

            argument['type'] = type;
            argument['command'] = command;
            if(data != '00'){
                parse_data= '' + data.substr(1,1) + data.substr(3,3)
            }else{
                parse_data = data;
            }
            argument['data'] = parse_data;
            argument['log'] = command_mean[1] + " Set : " + $scope.showRowColumn(parse_data);

            DP_DM.update({serial_number: serial_number}, argument);
        };

        $scope.showRowColumn = function(data){
            var prefix = data.substr(1,1);
            var suffix = data.substr(0,1);
            var result_data = '';

            switch(prefix){
                case 'A': prefix = '10'; break;
                case 'B': prefix = '11'; break;
                case 'C': prefix = '12'; break;
                case 'D': prefix = '13'; break;
                case 'E': prefix = '14'; break;
                case 'F': prefix = '15'; break;
                default:
                    prefix = prefix;
            }

            switch(suffix){
                case 'A': suffix = '10'; break;
                case 'B': suffix = '11'; break;
                case 'C': suffix = '12'; break;
                case 'D': suffix = '13'; break;
                case 'E': suffix = '14'; break;
                case 'F': suffix = '14'; break;
                default:
                    suffix = suffix;
            }

            if(prefix == '0' && suffix == '0'){
                result_data = 'OFF';
            }else{
                result_data = prefix + ' X ' + suffix;
            }

            return result_data;
        };

        $scope.updatePositionH = function (type, command, data, serial_number, pre_data) {
            var argument = {};

            var command_mean = command.split('_');

            argument['type'] = type;
            argument['command'] = command;
            argument['data'] = toHex(data+50);
            argument['log'] = command_mean[1] + " Set : " + (data+50);

            DP_DM.update({serial_number: serial_number}, argument);
        };

        //--------------------------------------------------------------------------------------------------------------
        // Click Event
        //--------------------------------------------------------------------------------------------------------------
        $scope.pathShowFlag_manager = false;
        $scope.pathShowFlag_site = false;
        $scope.pathShowFlag_location = false;
        $scope.pathShowFlag_shop = false;

        $scope.onClickManagerPath = function (manager) {
            $scope.selectedManager.clicked = false;    //먼저 닫고
            manager.clicked = !manager.clicked;               //무조건 열어주기
            $scope.selectedManager = manager;
            $scope.site_by_manager = manager.site;

            $scope.live_status.Information.data = manager;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = false;
            $scope.pathShowFlag_location = false;
            $scope.pathShowFlag_shop = false;
        };

        $scope.onClickSitePath = function (site) {
            $scope.selectedSite.clicked = false;    //먼저 닫고
            site.clicked = !site.clicked;               //무조건 열어주기
            $scope.selectedSite = site;
            $scope.locations_by_site = site.location;

            $scope.live_status.Information.data = site;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = false;
            $scope.pathShowFlag_shop = false;
        };

        $scope.onClickLocationPath = function (location) {
            $scope.selectedLocation.clicked = false;
            location.clicked = !location.clicked;
            $scope.selectedLocation = location;
            $scope.shops_by_location = location.shop;

            $scope.live_status.Information.data = location;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = true;
            $scope.pathShowFlag_shop = false;
        };

        $scope.onClickShopPath = function (shop) {
            $scope.selectedShop.clicked = false;
            shop.clicked = !shop.clicked;
            $scope.selectedShop = shop;

            $scope.media_players_by_shop = [];

            var api_params = {};
            api_params['shop'] = shop.text;
            api_params['location'] = shop.location;
            api_params['site'] = shop.site;

            $scope.live_status.Information.data = shop;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            Tree.get(api_params, function (data) {
              $scope.media_players_by_shop = data.objects;
            });

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = true;
            $scope.pathShowFlag_shop = true;
        };

        $scope.onClickManager = function (manager) {
            if ($scope.selectedManager && $scope.selectedManager != manager) $scope.selectedManager.clicked = false;
            manager.clicked = !manager.clicked;
            $scope.selectedManager = manager;

            $scope.live_status.Information.data = manager;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = false;
            $scope.pathShowFlag_shop = false;

            if(manager.site){
                $scope.site_by_manager = manager.site;
                if (manager.sub_count == 1 && !manager.clicked) {
                    $scope.onClickSite(manager.site[0]);
                }
            }

            if(manager.dg){
                $scope.dg_by_manager = manager.dg;
                if (manager.sub_count == 1 && !manager.clicked) {
                    $scope.onClickDeviceGroup(manager.dg[0]);
                }
            }
        };

        $scope.onClickDeviceGroup = function(device_group){
            if ($scope.selectedDeviceGroup && $scope.selectedDeviceGroup != device_group) $scope.selectedDeviceGroup.clicked = false;
            device_group.clicked = !device_group.clicked;
            $scope.selectedDeviceGroup = device_group;

            var api_params = {};
            api_params['shop'] = "UNKOWN";
            api_params['device_group_id'] = device_group.id;

            $scope.live_status.Information.data = device_group;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            Tree.get(api_params, function (data) {
                $scope.media_players_by_dg = data.objects;
            });

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = true;
            $scope.pathShowFlag_shop = true;
        };

        $scope.onClickSite = function (site) {
            if ($scope.selectedSite && $scope.selectedSite != site) $scope.selectedSite.clicked = false;
            site.clicked = !site.clicked;
            $scope.selectedSite = site;
            $scope.locations_by_site = site.location;

            $scope.live_status.Information.data = site;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = true;
            $scope.pathShowFlag_shop = false;

            if(site.sub_count == 1) {
                $scope.onClickLocation(site.location[0]);
            }
        };

        $scope.onClickLocation = function (location) {
            if ($scope.selectedLocation && $scope.selectedLocation != location) $scope.selectedLocation.clicked = false;
            location.clicked = !location.clicked;
            $scope.selectedLocation = location;
            $scope.shops_by_location = location.shop;

            $scope.live_status.Information.data = location;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = true;
            $scope.pathShowFlag_shop = false;

            if(location.sub_count == 1) {
                $scope.onClickShop(location.shop[0]);
            }
        };

        $scope.onClickShop = function (shop) {
            if ($scope.selectedShop && $scope.selectedShop != shop) $scope.selectedShop.clicked = false;
            shop.clicked = !shop.clicked;
            $scope.selectedShop = shop;

            $scope.media_players_by_shop = shop.mediaplayer;

            $scope.live_status.Information.data = shop;
            $scope.live_status.Information.flag = true;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = false;

            $scope.pathShowFlag_manager = true;
            $scope.pathShowFlag_site = true;
            $scope.pathShowFlag_location = true;
            $scope.pathShowFlag_shop = true;
        };

        $scope.onClickMediaPlayer = function (media_player) {
            if ($scope.selectedMediaPlayer && $scope.selectedMediaPlayer != media_player) $scope.selectedMediaPlayer.clicked = false;
            media_player.clicked = !media_player.clicked;
            $scope.selectedMediaPlayer = media_player;

            $scope.displays = null;
            $scope.disPlaySerialNumber = undefined;

            var argument = {};
            argument['mp_serial_number'] = media_player.serial_number;

            DP_By_MP.get(argument, function (data) {
                $scope.link_list = data.objects;
                $scope.tab_length = $scope.link_list.length;
            });

            $scope.mediaPlayerSerialNumber = media_player.serial_number;
            $scope.currentSN = $scope.mediaPlayerSerialNumber;
            $scope.live_status.Information.data = null;
            $scope.live_status.Information.flag = false;
            $scope.live_status.MediaPlayer.flag = true;
            $scope.live_status.Display.flag = false;
            $scope.lg_depth = media_player;

            var change_uri = '/live/media-players/'+$scope.mediaPlayerSerialNumber;
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if(current_uri != change_uri)
                $location.path(change_uri, false);

            $scope.img_url = '/live_view/' + $scope.mediaPlayerSerialNumber + '.jpg?cache=' + (new Date()).getTime();   //이미지도 새로 고침
        };

        /* image cache solve */
        $scope.img_url = '/live_view/' + $scope.mediaPlayerSerialNumber + '.jpg?cache=' + (new Date()).getTime();

        $scope.onClickDG = function(dp_group, media_player){
            if ($scope.selectedDG && $scope.selectedDG != dp_group) $scope.selectedDG.clicked = false;
            dp_group.clicked = true;
            $scope.selectedDG = dp_group;

            $scope.live_status.Information.data = null;
            $scope.live_status.Information.flag = false;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = true;

            var argument = {};
            argument['mp_serial_number'] = media_player.serial_number;
            argument['dp_group_name'] = dp_group.name;

            DP_By_MP.get(argument, function (data) {
                $scope.link_list = data.objects;
                $scope.tab_length = $scope.link_list.length;
            });

            $scope.displaySerialNumber = dp_group.dp_serial_number;
            $scope.currentSN = $scope.displaySerialNumber;

            $scope.img_url = '/live_view/' + media_player.serial_number + '.jpg?cache=' + (new Date()).getTime();

            var change_uri = '/live/media-players/'+media_player.serial_number+'/displays/'+dp_group.dp_serial_number;
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if(current_uri != change_uri)
                $location.path(change_uri, false);
        };

        $scope.onClickTab = function(device){
            // [ Case by ] MP
            if(device.type=='MP'){
                $scope.live_status.Information.data = null;
                $scope.live_status.Information.flag = false;
                $scope.live_status.MediaPlayer.flag = true;
                $scope.live_status.Display.flag = false;
                $scope.mediaPlayerSerialNumber = device.serial_number;
                $scope.currentSN = $scope.mediaPlayerSerialNumber;
                $scope.img_url = '/live_view/' + device.serial_number + '.jpg?cache=' + (new Date()).getTime();
                var change_uri = '/live/media-players/'+device.serial_number;
                var full_uri = $location.absUrl();
                var uri_split = full_uri.split('#');
                var current_uri = uri_split[1];

                if(current_uri != change_uri)
                    $location.path(change_uri, false);

                // [ Case by ] DP
            }else if(device.type=='DP'){
                $scope.live_status.Information.data = null;
                $scope.live_status.Information.flag = false;
                $scope.live_status.MediaPlayer.flag = false;
                $scope.live_status.Display.flag = true;
                $scope.mediaPlayerSerialNumber = device.mp_serial_number;
                $scope.displaySerialNumber = device.serial_number;
                $scope.currentSN = $scope.displaySerialNumber;
                $scope.img_url = '/live_view/' + device.mp_serial_number + '.jpg?cache=' + (new Date()).getTime();

                var change_uri = '/live/media-players/'+device.mp_serial_number+'/displays/'+device.serial_number;
                var full_uri = $location.absUrl();
                var uri_split = full_uri.split('#');
                var current_uri = uri_split[1];

                if(current_uri != change_uri)
                    $location.path(change_uri, false);
            }
        };

        $scope.treeDisplayGroupNoneSelected = function(display_serial_list, currentSN){
            var selected_flag = false;
            angular.forEach(display_serial_list, function(v, k){
                if(v.serial_number == currentSN){
                    selected_flag = true;
                }
            });
            return selected_flag;
        };

        $scope.treeDisplayGroupSelected = function(display_serial_list, currentSN){
            var selected_flag = false;
            angular.forEach(display_serial_list, function(v, k){
                if(v.serial_number == currentSN){
                    selected_flag = true;
                }
            });
            return selected_flag;
        };

        //--------------------------------------------------------------------------------------------------------------
        // mobile click Event
        //--------------------------------------------------------------------------------------------------------------
        $scope.tree_hide = function (flag) {
            if (flag.MediaPlayer.flag == true){
                return 'live_status.MediaPlayer.flag'
            }else if (flag.Display.flag == true){
                return 'live_status.Display.flag'
            }else{
                if(flag.Information.flag == true){
                    $scope.live_status.Information.flag = false;
                    return 'live_status.Information.flag'
                }
            }
        };
        $scope.onClickMobileMediaPlayer = function (media_player) {
            if ($scope.selectedMediaPlayer && $scope.selectedMediaPlayer != media_player) $scope.selectedMediaPlayer.clicked = false;
            media_player.clicked = !media_player.clicked;
            $scope.selectedMediaPlayer = media_player;

            $scope.displays = null;
            $scope.disPlaySerialNumber = undefined;

            var argument = {};
            argument['mp_serial_number'] = media_player.serial_number;

            DP_By_MP.get(argument, function (data) {
              $scope.link_list = data.objects;
              $scope.tab_length = $scope.link_list.length;
            });


            $scope.mediaPlayerSerialNumber = media_player.serial_number;
            $scope.currentSN = $scope.mediaPlayerSerialNumber;
            $scope.live_status.Information.data = null;
            $scope.live_status.Information.flag = false;
            $scope.live_status.MediaPlayer.flag = true;
            $scope.live_status.Display.flag = false;
            $scope.lg_depth = media_player;
            $scope.onClickSubTitle('sec_tree');

            var change_uri = '/m.live_status/media-players/'+$scope.mediaPlayerSerialNumber;
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if(current_uri != change_uri)
                $location.path(change_uri, false);

            $scope.img_url = '/live_view/' + $scope.mediaPlayerSerialNumber + '.jpg?cache=' + (new Date()).getTime();   //이미지도 새로 고침
        };

        $scope.onClickMobileDG = function(dp_group, media_player){
            if ($scope.selectedDG && $scope.selectedDG != dp_group) $scope.selectedDG.clicked = false;
            dp_group.clicked = true;
            $scope.selectedDG = dp_group;

            $scope.live_status.Information.data = null;
            $scope.live_status.Information.flag = false;
            $scope.live_status.MediaPlayer.flag = false;
            $scope.live_status.Display.flag = true;

            var argument = {};
            argument['mp_serial_number'] = media_player.serial_number;
            argument['dp_group_name'] = dp_group.name;

            DP_By_MP.get(argument, function (data) {
                $scope.link_list = data.objects;
                $scope.tab_length = $scope.link_list.length;
            });

            $scope.displaySerialNumber = dp_group.dp_serial_number;
            $scope.currentSN = $scope.displaySerialNumber;

            $scope.img_url = '/live_view/' + media_player.serial_number + '.jpg?cache=' + (new Date()).getTime();

            var change_uri = '/m.live_status/media-players/'+media_player.serial_number+'/displays/'+dp_group.dp_serial_number;
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if(current_uri != change_uri)
                $location.path(change_uri, false);
        };

        $scope.onClickMobileTab = function(device){
            // [ Case by ] MP
            if(device.type=='MP'){
                $scope.live_status.Information.data = null;
                $scope.live_status.Information.flag = false;
                $scope.live_status.MediaPlayer.flag = true;
                $scope.live_status.Display.flag = false;
                $scope.mediaPlayerSerialNumber = device.serial_number;
                $scope.img_url = '/live_view/' + device.serial_number + '.jpg?cache=' + (new Date()).getTime();
                var change_uri = '/m.live_status/media-players/'+device.serial_number;
                var full_uri = $location.absUrl();
                var uri_split = full_uri.split('#');
                var current_uri = uri_split[1];

                if(current_uri != change_uri)
                  $location.path(change_uri, false);

                // [ Case by ] DP
            }else if(device.type=='DP'){
                $scope.live_status.Information.data = null;
                $scope.live_status.Information.flag = false;
                $scope.live_status.MediaPlayer.flag = false;
                $scope.live_status.Display.flag = true;
                $scope.mediaPlayerSerialNumber = device.mp_serial_number;
                $scope.displaySerialNumber = device.serial_number;
                $scope.img_url = '/live_view/' + device.mp_serial_number + '.jpg?cache=' + (new Date()).getTime();

                var change_uri = '/m.live_status/media-players/'+device.mp_serial_number+'/displays/'+device.serial_number;
                var full_uri = $location.absUrl();
                var uri_split = full_uri.split('#');
                var current_uri = uri_split[1];

                if(current_uri != change_uri)
                    $location.path(change_uri, false);
            }
        };


        //--------------------------------------------------------------------------------------------------------------
        // Func
        //--------------------------------------------------------------------------------------------------------------
        //14 08 28 접어보기 기능
        $scope.section_cookie_set = function () {
            //media player
            if(!$cookieStore.get("m_sec1")){          //없으면
                $cookieStore.put("m_sec1", false);
                $scope.m_sec1_cookie = false;
            }else{                                  //있으면
                $scope.m_sec1_cookie = $cookieStore.get("m_sec1");
            }
            if(!$cookieStore.get("m_sec2")){
                $cookieStore.put("m_sec2", false);
                $scope.m_sec2_cookie = false;
            }else{
                $scope.m_sec2_cookie = $cookieStore.get("m_sec2");
            }
            if(!$cookieStore.get("m_sec3")){
                $cookieStore.put("m_sec3", false);
                $scope.m_sec3_cookie = false;
            }else{
                $scope.m_sec3_cookie = $cookieStore.get("m_sec3");
            }
            if(!$cookieStore.get("m_sec4")){
                $cookieStore.put("m_sec4", false);
                $scope.m_sec4_cookie = false;
            }else{
                $scope.m_sec4_cookie = $cookieStore.get("m_sec4");
            }
            if(!$cookieStore.get("m_sec5")){
                $cookieStore.put("m_sec5", false);
                $scope.m_sec5_cookie = false;
            }else{
                $scope.m_sec5_cookie = $cookieStore.get("m_sec5");
            }
            if(!$cookieStore.get("sec_tree")){
                $cookieStore.put("sec_tree", false);
                $scope.m_sec_tree_cookie = false;
            }else{
                $scope.m_sec_tree_cookie = $cookieStore.get("sec_tree");
            }

            //display
            if(!$cookieStore.get("d_sec1")){
                $cookieStore.put("d_sec1", false);
                $scope.d_sec1_cookie = false;
            }else{
                $scope.d_sec1_cookie = $cookieStore.get("d_sec1");
            }
            if(!$cookieStore.get("d_sec2")){
                $cookieStore.put("d_sec2", false);
                $scope.d_sec2_cookie = false;
            }else{
                $scope.d_sec2_cookie = $cookieStore.get("d_sec2");
            }
            if(!$cookieStore.get("d_sec3")){
                $cookieStore.put("d_sec3", false);
                $scope.d_sec3_cookie = false;
            }else{
                $scope.d_sec3_cookie = $cookieStore.get("d_sec3");
            }
            if(!$cookieStore.get("d_sec4")){
                $cookieStore.put("d_sec4", false);
                $scope.d_sec4_cookie = false;
            }else{
                $scope.d_sec4_cookie = $cookieStore.get("d_sec4");
            }
        };

        $scope.onClickSubTitle = function (section_tag) {
            $scope.changeCookie = !$cookieStore.get(section_tag);
            $cookieStore.put(section_tag, $scope.changeCookie);

            $scope.section_cookie_set();
        };

        $scope.IsIE8 = function() {
            if(navigator.appVersion.indexOf("MSIE 8") != -1)
                return true;
            else
                return false;
        };

        //--------------------------------------------------------------------------------------------------------------
        // Html bind
        //--------------------------------------------------------------------------------------------------------------
        $scope.showEmptySpace = function(type, tab_length){
            var emptyHTML = "";

            if(type == 'mp'){
                if(tab_length <= 25){
                    emptyHTML = '';
                }else if(tab_length > 25 && tab_length <= 54){
                    emptyHTML = "<div class='clearfix visible-xs-block SPACE_H28'>&nbsp;</div>";
                }else if(tab_length > 55){
                    emptyHTML = "<div class='clearfix visible-xs-block SPACE_H57'>&nbsp;</div>";
                }
            }else if(type == 'dp'){
                if(tab_length <= 25){
                    emptyHTML = '';
                }else if(tab_length > 25 && tab_length <= 54){
                    emptyHTML = "<div class='clearfix visible-xs-block SPACE_H28'>&nbsp;</div>";
                }else if(tab_length > 55){
                    emptyHTML = "<div class='clearfix visible-xs-block SPACE_H57'>&nbsp;</div>";
                }
            }

            return emptyHTML;
        };

        $scope.showDisconnected = function(type, mp_alive_flag, dp_alive_flag){
            var disconnHTML = "";

            if(type == 'mp'){
                if(mp_alive_flag == false){
                    disconnHTML = "<div class='disconnected_section mp_disconnected_section'>" +
                                    "<p class='comment'>" +
                                         "<span>" +
                                            "Disconnected" +
                                        "</span>" +
                                    "</p>" +
                                  "</div>";
                }
            }else if(type == 'dp'){
                if(mp_alive_flag == false || dp_alive_flag == false){
                    disconnHTML = "<div class='disconnected_section dp_disconnected_section'>" +
                                    "<p class='comment'>" +
                                        "<span>" +
                                            "Disconnected" +
                                        "</span>" +
                                    "</p>" +
                                  "</div>";
                }
            }

            return disconnHTML;
        };

        $scope.IsIE8 = function() {
            if(navigator.appVersion.indexOf("MSIE 8") != -1){
                return true;
            }else{
                return false;
            }
        };

        //--------------------------------------------------------------------------------------------------------------
        // Modal
        //--------------------------------------------------------------------------------------------------------------
        $scope.openRequestDM = function (type, command, data, serial_number) {
            Modal.open(
                'views/requestDM.html',
                'RequestCtrl',
                'lg',
                {
                    type: function(){
                        return type;
                    },
                    command: function(){
                        return command;
                    },
                    data: function(){
                        return data;
                    },
                    serial_number: function(){
                        return serial_number;
                    }
                }
            );
        };

        $scope.openDiagnosticDialog = function (mp_serial_number) {
            if($rootScope.isModalOpened == true)
                return;
            $rootScope.isModalOpened = true;

            Modal.open(
                'views/diagnostic.html',
                'DiagnosticCtrl',
                'lg',
                {
                    getMP_SN: function(){
                        return mp_serial_number || $scope.mediaPlayerSerialNumber;
                    }
                }
            );
        };

        $scope.openStaticGalleryDialog = function (img_uri) {
            Utils.isImage(img_uri).then(function(result) {
                if(result){
                    Modal.open(
                        'views/static_gallery.html',
                        'SGalleryCtrl',
                        'sm',
                        {
                            img_uri: function () {
                                return img_uri;
                            }
                        }
                    );
                }
            });
        };

        $scope.openGalleryDialog = function (isSlide, slides, img_src) {
            Utils.isImage(img_src).then(function(result) {
                if(result){
                    Modal.open(
                        'views/gallery.html',
                        'GalleryCtrl',
                        'lg',
                        {
                            isSlide: function () {
                                return isSlide;
                            },
                            slides: function () {
                                return slides;
                            }
                        }
                    );
                }
            });
        };

        $scope.openInitialSettingDialog = function (mp_serial_number) {
            Modal.open(
                'views/initialsetting.html',
                'ReSetControlCtrl',
                'lg',
                {
                    getMP_SN: function(){
                        return mp_serial_number;
                    }
                }
            );
        };

        $scope.openTerminalDialog = function (mp_serial_number) {
            Modal.open(
                'views/terminal.html',
                'TerminalCtrl',
                'lg',
                {
                    getMP_SN: function(){
                        return mp_serial_number;
                    }
                }
            );
        };

        $scope.openVProDialog = function (mp_serial_number) {
          Modal.open(
            'views/vpro.html',
            'VProCtrl',
            'lg',
            {
              getMP_SN: function(){
                return mp_serial_number;
              }
            }
          );
        };

        $scope.openRemoteControlDialog = function (mp_serial_number) {
            Modal.open(
                'views/remotecontrol.html',
                'RemoteControlCtrl',
                'lg',
                {
                    getMP_SN: function(){
                        return mp_serial_number;
                    }
                }
            );
        };

        //--------------------------------------------------------------------------------------------------------------
        // mobile modal
        //--------------------------------------------------------------------------------------------------------------
        $scope.openMobileRequestDM = function (type, command, data, serial_number) {
            Modal.open(
                'views/mobile_requestDM.html',
                'RequestCtrl',
                'lg',
                {
                    type: function(){
                        return type;
                    },
                    command: function(){
                        return command;
                    },
                    data: function(){
                        return data;
                    },
                    serial_number: function(){
                        return serial_number;
                    }
                }
            );
        };

        $scope.openMobileDiagnosticDialog = function (mp_serial_number) {
            if($rootScope.isModalOpened == true)
                return;
            $rootScope.isModalOpened = true;

            Modal.open(
                'views/mobile_diagnostic.html',
                'DiagnosticCtrl',
                'lg',
                {
                  getMP_SN: function(){
                      return mp_serial_number || $scope.mediaPlayerSerialNumber;
                  }
                }
            );
        };

        /* *
         * File Upload - KSM
         * */
        $scope.segments = [
            {size: '128KB', data: '128'},
            {size: '256KB', data: '256'},
            {size: '512KB', data: '512'},
            {size: '1MB', data: '1024'}
        ];

        $scope.segment = $scope.segments[0];
        $scope.segment_size = $scope.segments[0].data;  //default segment size

        $scope.setSegmentSize = function(size_data){
            $scope.segment_size = size_data;
        };

        $scope.firmwareUpgrade = function(type, command, serial_number){
            var argument = {};

            var command_mean = command.split('_');

            argument['type'] = type;
            argument['command'] = command;
            argument['data'] = $scope.segment_size + ' ' + ENV.host+'/uploads/'+$scope.file_name;
            argument['log'] = command_mean[1] + " : " + $scope.file_name;

            DP_DM.update({serial_number: serial_number}, argument);
        };

        $scope.progress_bar = 0;
        $scope.progress_status = "Ready";
        $scope.progress_ment = '';
        $scope.file_path = 'file path auto input';
        $scope.file_name = '';
        $scope.onFileSelect = function($files) {
            $scope.progress_status = "Ready";
            $scope.progress_ment = 'Uplaod Ready!';
            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];
                $scope.upload = $upload.upload({
                    url: ENV.host + '/api/v1/file_upload',
                    method: 'POST',
                    file: file
                }).success(function(data, status, headers, config) {
                    if(status == 200){
                        $scope.progress_status = "Complete";
                        $scope.file_name = $files[0].name;
                        $scope.progress_ment = '[ ' + $scope.file_name + ' ] Uplaod Complete!';
                    }else{
                        $scope.progress_status = "Fail";
                        $scope.progress_ment = '[ ' + $scope.file_name + ' ] Uplaod Fail!';
                    }
                });
            }
        };

        //schedule
        $scope.schedule_days = [
            {data: '01' , value: 'ONCE'},
            {data: '02' , value: 'EVERYDAY'},
            {data: '03' , value: 'MON - FRI'},
            {data: '04' , value: 'MON - SAT'},
            {data: '05' , value: 'SAT - SUN'},
            {data: '06' , value: 'SUN'},
            {data: '07' , value: 'MON'},
            {data: '08' , value: 'TUE'},
            {data: '09' , value: 'WED'},
            {data: '0A' , value: 'THU'},
            {data: '0B' , value: 'FRI'},
            {data: '0C' , value: 'SAT'}
        ];

        $scope.schedule_inputs = [
            {data: '20' , value: 'AV'},
            {data: '40' , value: 'COMPONENT'},
            {data: '60' , value: 'RGB'},
            {data: '70' , value: 'DVI-D'},
            {data: '90' , value: 'HDMI'},
            {data: 'C0' , value: 'DISPLAY PORT'},
            {data: '91' , value: 'HDMI2/SDI'},
            {data: 'B0' , value: 'SUPERSIGN'}
        ];

        var showDays = function(day){
            day = day.toUpperCase();
            var result_day = '';
            switch(day){
                case '01':  result_day='ONCE';  break;
                case '02':  result_day='EVERYDAY';  break;
                case '03':  result_day='MON - FRI'; break;
                case '04':  result_day='MON - SAT'; break;
                case '05':  result_day='SAT - SUN'; break;
                case '06':  result_day='SUN'; break;
                case '07':  result_day='MON'; break;
                case '08':  result_day='TUE'; break;
                case '09':  result_day='WED'; break;
                case '0A':  result_day='THU'; break;
                case '0B':  result_day='FRI'; break;
                case '0C':  result_day='SAT'; break;
            }
            return result_day;
        };

        var addZero = function(this_num){
            var result_num = '';
            if(this_num.length==1)
                result_num = '0'+this_num;
            else
                result_num = this_num;
            return result_num;
        };

        var showInput = function(input){
            input = input.toUpperCase();
            var result_input = '';
            switch(input){
                case '20':  result_input='AV';  break;
                case '40':  result_input='COMPONENT';  break;
                case '60':  result_input='RGB';  break;
                case '70':  result_input='DVI-D(PC)';  break;
                case '80':  result_input='DVI-D(DTV)';  break;
                case '90':  result_input='HDMI(HDMI1)(DTV)';  break;
                case 'A0':  result_input='HDMI(HDMI1)(PC)';  break;
                case 'C0':  result_input='Display Port(DTV)';  break;
                case 'D0':  result_input='Display Port(PC)';  break;
                case '91':  result_input='HDMI2/SDI(DTV)';  break;
                case 'A1':  result_input='HDMI2/SDI(PC)';  break;
                case 'B0':  result_input='SuperSign';  break;
            }
            return result_input;
        };

        var parseInput = function(input_value){
            var result_input = '';
            input_value = input_value.toUpperCase();
            switch(input_value){
                case 'AV':  result_input='20';  break;
                case 'COMPONENT':  result_input='40';  break;
                case 'RGB':  result_input='60';  break;
                case 'DVI-D':  result_input='70';  break;
                case 'HDMI':  result_input='90';  break;
                case 'DISPLAY PORT':  result_input='C0';  break;
                case 'HDMI2':  result_input='91';  break;
                case 'SUPERSIGN':  result_input='B0';  break;
            }
            return result_input;
        };

        var onTimerPush = function(time_list, input_list){
            angular.forEach(time_list, function(value, key){
                if(value != 'NG'){
                    var day = showDays(value.substr(0,2));
                    var time = addZero(''+parseInt(value.substr(2,2), 16)) + ":" + addZero(''+parseInt(value.substr(4,2), 16));
                    if(input_list[key] != null)
                        var input = showInput(input_list[key].substr(0,2));
                    var no = key.substr(1,2);

                    $scope.OnTimerList.push({no: no, field: key, day : day, time : time, input :input});
                }
            });
        };

        var offTimerPush = function(time_list){
            angular.forEach(time_list, function(value, key){
                if(value != 'NG'){
                    var day = showDays(value.substr(0,2));
                    var time = addZero(''+parseInt(value.substr(2,2), 16)) + ":" + addZero(''+parseInt(value.substr(4,2), 16));
                    var no = key.substr(1,2);
                    $scope.OffTimerList.push({no: no, field: key, day : day, time : time});
                }
            });
        };


        $scope.OnTimerList = [];    //limit 7 file Queue
        $scope.OffTimerList = [];   //limit 7 file Queue
        $scope.setDisplaySN = function(displaySN){
            Display.get({serial_number: $scope.displaySerialNumber}, function (data) {
                if(data.objects) {
                    $scope.display_schedule = data.objects[0];
                    $scope.OffTimerList = [];   //limit 7 file Queue init

                    onTimerPush($scope.display_schedule.val_OnTimer, $scope.display_schedule.val_OnTimerInput);
                    offTimerPush($scope.display_schedule.val_OffTimer);
                }
            });
        };

        //default setting
        $scope.schedule_btn_power = 'on';
        $scope.schedule_input = $scope.schedule_inputs[4]; //default set HDMI
        $scope.schedule_day = $scope.schedule_days[0];
        //$scope.current_time = $filter('date')(new Date(), 'HH:mm:ss');
        $scope.schedule_time = '';//$scope.current_time;

        function toHex(d) {
            return  ("0"+(Number(d).toString(16))).slice(-2).toUpperCase();
        };

        function todecimal(hex){
            return parseInt(hex, 16);
        };

        $scope.TimerSave = function(type, serial_number){
            if(this.schedule_time){
                $scope.temp_list = [];

                if(this.schedule_btn_power == 'on'){        //case by 'power on'
                    //temp
                    $scope.temp_list = $scope.OnTimerList;
                    //clean
                    $scope.OnTimerList = [];


                    //temp copy
                    angular.forEach($scope.temp_list, function(value, key){
                        var num = Number(value.no)+1;
                         $scope.OnTimerList.push({
                            no: num,
                            field: 'f'+num,
                            day : value.day,
                            time : value.time,
                            input : value.input
                        });
                    });

                    //new
                    $scope.OnTimerList.push({
                        no: 1,
                        field: 'f1',
                        day : ''+this.schedule_day.value,
                        time : ''+this.schedule_time,
                        input : ''+this.schedule_input.value
                    });

                    if($scope.OnTimerList[0]['no'] == 8) {
                        $scope.OnTimerList.splice(0, 1);
                    }

                    var on_time = this.schedule_time.split(':');

                    var argument = {};

                    argument['type'] = type;
                    var command = 'tag_OnTimer';
                    var data = '' + this.schedule_day.data +' '+ toHex(on_time[0]) +' '+ toHex(on_time[1]);
                    var second_command = 'tag_OnTimerInput';
                    var second_data = ''+ parseInput(this.schedule_input.value);
                    argument['command'] = command;
                    argument['data'] = data+'.'+second_command+ "|" +second_data;
                    argument['log'] = 'Register On Timer :' + this.schedule_day.value + ", " + this.schedule_time + ", " + this.schedule_input.value;

                    DP_DM.update({serial_number: serial_number}, argument);

                }else if(this.schedule_btn_power == 'off'){//case by 'power off'
                    //temp
                    $scope.temp_list = $scope.OffTimerList;
                    //clean
                    $scope.OffTimerList = [];

                    //temp copy
                    angular.forEach($scope.temp_list, function(value, key){
                        var num = Number(value.no)+1;
                        $scope.OffTimerList.push({
                            no: num,
                            field: 'f'+num,
                            day : value.day,
                            time : value.time
                        });
                    });

                    //new
                    $scope.OffTimerList.push({
                        no: 1,
                        field: 'f1',
                        day : ''+this.schedule_day.value,
                        time : ''+this.schedule_time
                    });

                    if($scope.OffTimerList[0]['no'] == 8) {
                      $scope.OffTimerList.splice(0, 1);
                    }

                    var off_time = this.schedule_time.split(':');

                    var argument = {};
                    argument['type'] = type;
                    var command = 'tag_OffTimer';
                    var data = '' + this.schedule_day.data +' '+ toHex(off_time[0]) +' '+ toHex(off_time[1]);
                    argument['command'] = command;
                    argument['data'] = data;
                    argument['log'] = 'Register Off Timer :' + this.schedule_day.value + ", " + this.schedule_time;

                    DP_DM.update({serial_number: serial_number}, argument);
                }
            }
            else{  //시간이 설정되지 않으면 작동 X
                return '';
            }
        };

        $scope.getIndexOf = function(arr, key, val){
            var l = arr.length;
            var k = 0;
            for (k = 0; k < l; k = k + 1) {
                if (arr[k][key] === val) {
                    return k;
                }
            }
            return -1;
        };

        $scope.removeList = function (field, type, power_flag, no, list_index, serial_number){
            if(power_flag == 'on'){
                var idx = $scope.getIndexOf($scope.OnTimerList, 'field', field);
                //temp
                $scope.temp_list = $scope.OnTimerList;
                //clean
                $scope.OnTimerList = [];

                //temp copy
                angular.forEach($scope.temp_list, function(value, key){
                    var num = Number(value.no)-1;

                    if(value.field!=field){
                        if(key > idx){
                            $scope.OnTimerList.push({
                                no: num,
                                field: 'f'+num,
                                day : value.day,
                                time : value.time,
                                input : value.input
                            });
                        }else{
                            $scope.OnTimerList.push({
                                no: value.no,
                                field: value.field,
                                day : value.day,
                                time : value.time,
                                input : value.input
                            });
                        }
                    }
                });

                var argument = {};
                var command = 'tag_OnTimer';
                var f_index = list_index+1;

                var data = 'e' + f_index + ' FF FF';
                argument['type'] = type;
                argument['fd'] = parseInt('e'+data, 16) +'_'+ parseInt('ff', 16) +'_'+ parseInt('ff', 16);
                argument['command'] = command;
                argument['data'] = data;
                argument['log'] = 'Remove Schedule On Timer';

                DP_DM.update({serial_number: serial_number}, argument);
            }else if(power_flag == 'off'){
                var idx = $scope.getIndexOf($scope.OffTimerList, 'field', field);
                //temp
                $scope.temp_list = $scope.OffTimerList;
                //clean
                $scope.OffTimerList = [];

                //temp copy
                angular.forEach($scope.temp_list, function(value, key){
                    var num = Number(value.no)-1;

                    if(value.field!=field){

                        if(key > idx){
                            $scope.OffTimerList.push({
                                no: num,
                                field: 'f'+num,
                                day : value.day,
                                time : value.time
                            });
                        }else{
                            $scope.OffTimerList.push({
                                no: value.no,
                                field: value.field,
                                day : value.day,
                                time : value.time
                            });
                        }
                    }
                });

                var argument = {};
                var command = 'tag_OffTimer';
                var f_index = list_index+1;
                var data = 'e' + f_index + ' FF FF';
                argument['type'] = type;
                argument['fe'] = parseInt('e'+data, 16) +'_'+ parseInt('ff', 16) +'_'+ parseInt('ff', 16);
                argument['command'] = command;
                argument['data'] = data;
                argument['log'] = 'Remove Schedule Off Timer';

                DP_DM.update({serial_number: serial_number}, argument);
            }
        };

        $scope.showTileOneStep = function(tile_mode){
            if(tile_mode != 'OFF' && tile_mode != 'Not set' && tile_mode != '')
                return true;
            else
                return false;
        };

        $scope.showTileOneStep_NA = function(tile_mode){
            if(tile_mode == 'OFF' || tile_mode == 'Not set' || tile_mode == '')
                return true;
            else
                return false;
        };

        $scope.showTileTwoStep = function(tile_N_mode){
            if(tile_N_mode != 'ON' && tile_N_mode != 'Not set' && tile_N_mode != '')
                return true;
            else
                return false;
        };

        $scope.showTileTwoStep_NA = function(tile_N_mode){
            if(tile_N_mode == 'ON' || tile_N_mode == 'Not set' || tile_N_mode == '')
                return true;
            else
                return false;
        };

        /* 화씨 표현을 위해 tempConverter */
        $scope.tempConverter = function(to_temp_flag, temp){
            var F = 0.0;
            var C = 0.0;

            if(to_temp_flag == 'F'){//to fahrenheit ℉
                F = temp * 9.0 / 5.0 + 32;
                return F;
            }else if(to_temp_flag == 'C'){//to celsius ℃
                C = (temp - 32) * 5.0 / 9.0;
                return C;
            }
        };

        /* Support History Device Display */
        $scope.showDevice = function(device_number){
            if(device_number == 0){
                return 'MediaPlayer';
            }else if(device_number){
                return 'Display'+device_number;
            }
        };

        $scope.updateHistoryByDB = function(field, data, key){
            var argument = {};
            argument['memo_id'] = key;
            argument[field] = data;
            History.update(argument);
        };

        $scope.tv_setting_statuses = [
            {value: 1, text: 'ON'},
            {value: 0, text: 'OFF'}
        ];

        $scope.showTime = function(time){
            if(time == 'None'){
                return 'Not Set';
            }else{
                return time;
            }
        };

        $scope.showCheckBox = function(check){
            if(check){
                return 'Checked';
            }else if(!check){
                return 'Not Checked';
            }else{
                return 'Not Set';
            }
        };

        $scope.showTV_flag = function(flag) {
            //var selected = $filter('filter')($scope.tv_setting_statuses, {value: flag});
            //return (selected.length) ? selected[0].text : 'Not Set';
            if(flag){
                return 'ON';
            }else if(!flag){
                return 'OFF';
            }else{
                return 'Not Set';
            }
        };


        //--------------------------------------------------------------------------------------------------------------
        //  Exception Handling
        //--------------------------------------------------------------------------------------------------------------

        // Tab - Resource Usage & Status
        $scope.status_type_NA = function(input_value){
            if(input_value == undefined || input_value == '' || input_value == 'None'){
                return true;
            }
            return false;
        };

        $scope.status_type_normal = function(input_value){
            if(input_value != undefined && input_value != '' && input_value != 'None'){
                return true;
            }
            return false;
        };

        $scope.status_type_zero_NA = function(input_value){
            if(input_value == undefined || input_value == '' || input_value == 'None' || input_value == 0){
                return true;
            }
            return false;
        };

        $scope.status_type_zero = function(input_value){
            if(input_value != undefined && input_value != '' && input_value != 'None' && input_value != 0){
                return true;
            }
            return false;
        };

        // Tab - Running Status
        $scope.app_status_type_normal = function(input_value){
            if(input_value != undefined  && input_value != '' && input_value != 'None'){
                return true;
            }
            return false;
        };

        $scope.app_status_type_NA = function(input_value){
            if(input_value == undefined  || input_value == '' || input_value == 'None'){
                return true;
            }
            return false;
        };

        $scope.app_status_action_disable = function(input_value){
            if(input_value == undefined  || input_value == '' || input_value == 'None'){
                return true;
            }
            return false;
        };

        // Tab - MediaPlayer Information
        $scope.mp_info_type_normal = function(input_value){
            if(input_value != undefined  && input_value != '' && input_value != 'None'){
                return true;
            }
            return false;
        };
        $scope.mp_info_type_NA = function(input_value){
            if(input_value == undefined  || input_value == '' || input_value == 'None'){
                return true;
            }
            return false;
        };

        // Tab - Display Information
        $scope.dp_info_type_normal = function(input_value){
            if(input_value != undefined  && input_value != '' && input_value != 'None' && input_value != '00-00-00'){
                return true;
            }
            return false;
        };

        $scope.dp_info_type_NA = function(input_value){
            if(input_value == undefined  || input_value == '' || input_value == 'None' || input_value == '00-00-00'){
                return true;
            }
            return false;
        };

        $scope.network_NA = function(field_name, input_value){
            var value_str = ''+input_value;
            var return_flag = false;

            // is Value Logic
            if(input_value == undefined || input_value == '' || input_value == 'None')  return_flag = true;
            else                                                                          return_flag = false;

            if(field_name == 'response_time'){
                if(value_str.indexOf('-') > -1)    return_flag = true;
                else                                return_flag = false;
            }

            if(field_name == 'netmask' || field_name == 'ip'){
                if(value_str == '0.0.0.0')  return_flag = true;
                else                         return_flag = false;
            }

            return return_flag;
        };

        $scope.network_no_NA = function(field_name, input_value){
            var value_str = ''+input_value;
            var return_flag = false;

            // is Value Logic
            if(input_value != undefined && input_value != '' && input_value != 'None')  return_flag = true;
            else                                                                          return_flag = false;

            if(field_name == 'response_time'){
                if(value_str.indexOf('-') == -1)    return_flag = true;
                else                                return_flag = false;
            }

            if(field_name == 'netmask' || field_name == 'ip'){
                if(value_str != '0.0.0.0')  return_flag = true;
                else                         return_flag = false;
            }

            return return_flag;
        };

        $scope.openMPHistoricalView = function(){
            Modal.open(
                'views/live_mp_historical_view.html',
                'LiveMPHistoricalModalCtrl',
                'lg',
                {
                    serial_number: function(){
                        return $scope.mediaPlayerSerialNumber;
                    }
                }
            );
        };

        $scope.openDPHistoricalView = function(serial_number){
            Modal.open(
                'views/live_dp_historical_view.html',
                'LiveDPHistoricalModalCtrl',
                'lg',
                {
                    mp_serial_number: function(){
                        return $scope.mediaPlayerSerialNumber;
                    },
                    serial_number: function(){
                        return serial_number;
                    }
                }
            );
        };
    });

var sec = 600;
function setRefreshTimer() {
    if (sec == 0)
        location.reload(true);
    else {
        sec = sec - 1;
        var timer = document.getElementById('timer');
        if (timer)
            timer.innerHTML = "<font color=blue>Refresh after " + sec + " secs.</font>";
    }
    setTimeout("setRefreshTimer()", 1000);
}

