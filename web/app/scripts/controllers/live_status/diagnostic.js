'use strict';

angular.module('signageApp')
    .controller('DiagnosticCtrl', function ($scope, $rootScope, $http, Session, AuthService, SharedService, ngTableParams, $modalInstance, Modal, Manager, Member, Admin, User, Incidents, $filter, $location, MediaPlayer, $timeout, Display, DeviceGroup, getMP_SN, MP_DB, COM) {
        $scope.Modal = Modal;

        // INHERIT -----------------------------------------------------------------------------------------------------
        $scope.isAuthenticated = AuthService.isAuthenticated();

        if($scope.isAuthenticated){
            $scope.accessToken = Session.get('accessToken');
            $http.defaults.headers.get = {token: $scope.accessToken};
            $http.defaults.headers.get['If-Modified-Since'] = new Date().getTime();
            $http.defaults.headers.get['Cache-Control'] = 'no-cache';
            $http.defaults.headers.get['Pragma'] = 'no-cache';
            $http.defaults.headers.put["token"] = $scope.accessToken;
            $http.defaults.headers.post["token"] = $scope.accessToken;
            $http.defaults.headers.common["token"] = $scope.accessToken;

            MediaPlayer.get({meta_only: 1}).$promise.then(function (data) {
                if(data.status==200){
                    $scope.quick_dash = {
                        count: data.meta.total_count + " / " + data.meta.total_display_count,
                        checkRequiredCount: data.meta.check_required_count,
                        unresolvedCount: data.meta.unresolved_count,
                        deadCount: data.meta.disconnected_count,
                        latestResolvedCount: data.meta.recent_resolved_count,
                        latestAddedCount: data.meta.today_activation_count
                    };

                    SharedService.prepForQuickDashBroadcast($scope.quick_dash);
                }
            });

            User.get().$promise.then(function (data) {
                if(data.status==200) {
                    $scope.userType = data.objects[0].userType;
                    $scope.userID = data.objects[0].userID;
                    $scope.userAuth = data.objects[0].auth;

                    $scope.isDevice_Info = false;
                    $scope.isMP_Info = false;
                    $scope.isDP_Info = false;
                    $scope.isReport_Info = false;

                    angular.forEach($scope.userAuth, function(s){
                        if(s == 1) $scope.isDevice_Info = true;
                        if(s == 2) $scope.isMP_Info = true;
                        if(s == 4) $scope.isDP_Info = true;
                        if(s == 8) $scope.isReport_Info = true;
                    });

                    if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                        Admin.get({}, function(data){
                            if(data.status == 200){
                                $scope.user = data.objects[0];
                                SharedService.prepForUserBroadcast($scope.user);
                            }
                        });
                    }else if($scope.userType == 'MANAGER'){
                        Manager.get({}, function(data){
                            if(data.status == 200) {
                                $scope.user = data.objects[0];
                                SharedService.prepForUserBroadcast($scope.user);
                            }
                        });
                    }else if($scope.userType == 'MEMBER'){
                        Member.get({}, function(data){
                            if(data.status == 200) {
                                $scope.user = data.objects[0];
                                SharedService.prepForUserBroadcast($scope.user);
                            }
                        });
                    }
                }
            });
        }else{
            $scope.quick_dash = {
                count: 0,
                checkRequiredCount: 0,
                unresolvedCount: 0,
                deadCount: 0,
                latestResolvedCount: 0,
                latestAddedCount: 0
            };
            SharedService.prepForQuickDashBroadcast($scope.quick_dash);
            $scope.user = undefined;
            SharedService.prepForUserBroadcast($scope.user);

            var change_uri ="/logout";
            $location.path(change_uri, true);
        }

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
        });
        //only live status
        $scope.$on("DP_SERIAL_NUMBER", function(){
            $scope.display_serial_number = SharedService.display_serial_number;
        });

        MediaPlayer.get({serial_number: getMP_SN}, function (data) {
            $scope.modal_media_player = data.objects[0];
        });

        $scope.updateMediaPlayerByDB = function(){
            var argument = {};

            argument['status'] = -1;
            argument['group_status'] = -1;
            MP_DB.update({serial_number: getMP_SN}, argument);

            $scope.close();
            //$modalInstance.dismiss('cancel');
        };

        Display.get({mp_serial_number: getMP_SN, dp_group_name: "_ALL_DATA_"}, function (data) {
            if(data.status==200)
                $scope.modal_displays = data.objects;
        });

        DeviceGroup.get({mp_serial_number: getMP_SN}, function (data) {
            if(data.status==200)
                $scope.modal_device_group = data.objects[0];
        });

        $scope.Modal_MP_INFO_alive = [
            {name: 'Alive', field: 'alive', status: 'status_cpu_usage', type: 'alive'}
        ];

        $scope.Modal_MP_INFO = [
            {name: 'CPU Usage', error: 'th_cpu_usage_error', warning: 'th_cpu_usage_warning', field: 'cpu_usage', status: 'status_cpu_usage'},
            {name: 'Memory Usage', error: 'th_mem_usage_error', warning: 'th_mem_usage_warning', field: 'mem_usage', status: 'status_mem_usage'},
            {name: 'HDD Usage', error: 'th_hdd_usage_error', warning: 'th_hdd_usage_warning', field: 'hdd_usage', status: 'status_hdd_usage'},
            {name: 'CPU Temp.', error: 'th_cpu_temp_error', warning: 'th_cpu_temp_warning', field: 'cpu_temp', status: 'status_cpu_temp'},
            {name: 'HDD Temp.', error: 'th_hdd_temp_error', warning: 'th_hdd_temp_warning', field: 'hdd_temp', status: 'status_hdd_temp'},
            {name: 'Fan Speed', error: 'th_fan_speed_error', warning: 'th_fan_speed_warning', field: 'fan_speed', status: 'status_fan_speed'},
            {name: 'Response Time', error: 'th_res_time_error', warning: 'th_res_time_warning', field: 'response_time', status: 'status_res_time'}
        ];

        $scope.Modal_APP_INFO = [
            {index: 'Application 1', app: 'th_app1', name: 'app_path1', app_status: 'app_status1', status: 'status_app1'},
            {index: 'Application 2', app: 'th_app2', name: 'app_path2', app_status: 'app_status2', status: 'status_app2'},
            {index: 'Application 3', app: 'th_app3', name: 'app_path3', app_status: 'app_status3', status: 'status_app3'},
            {index: 'Application 4', app: 'th_app4', name: 'app_path4', app_status: 'app_status4', status: 'status_app4'}
        ];

        $scope.Modal_incident_headers = [
            {name: 'No.', class_name: 'no', sortable: false},
            {name: 'Generated', class_name: 'create', sortable: true, field: 'created'},
            {name: 'Incident Message', class_name: 'msg', sortable: false},
            {name: 'Severity', class_name: 'level', sortable: true, field: 'level'}
        ];

        var params = $location.search();
        $scope.order = params['order'] || '-';
        $scope.order_by = params['order_by'] || 'created'; //default order by

        var parameters = {
            page: 1,
            count: 5,
            sorting: {}
        };

        var order_by = $scope.order_by;
        var str_order = '';
        if($scope.order == '-'){
            str_order = 'desc';
        }else{
            str_order = 'asc';
        }

        parameters['sorting'][order_by] = str_order;

        $scope.incidentsTableParams = new ngTableParams(parameters, {
            counts: [],
            total: 0,// length of data
            getData: function ($defer, params) {
                var api_params = {};
                api_params['limit'] = params.count();
                api_params['offset'] = (params.page() - 1) * api_params['limit'];
                api_params['mp_serial_number'] = getMP_SN;

                var sorting = params.sorting();
                for (var key in sorting) {
                    var value = sorting[key];
                    if (value == 'desc') {
                        api_params['order'] = '-';
                    }

                    api_params['order_by'] = key;
                }

                Incidents.get(api_params, function(data){
                    if(data.status==200){
                        $timeout(function () {
                            $scope.dataSet = data.objects;
                            params.total(data.meta.total_count);
                            $defer.resolve($scope.dataSet);
                        }, 500);
                    }
                });
            }
        });

        $scope.showMessage = function(incident){
            var message = 'Empty';

            switch(incident.error_code){
                // [ Case by ] Red
                case 'R01':
                    message = "Media Player '" + incident.alias + "' is Disconnected";
                    break;
                case 'R02':
                    message = "Display '" + incident.alias + "' is Disconnected";
                    break;
                case 'R11':
                    message = "Core Application 1 on '" + incident.alias + "' is Stopped";
                    break;
                case 'R12':
                    message = "Core Application 2 on '" + incident.alias + "' is Stopped";
                    break;
                case 'R13':
                    message = "Core Application 3 on '" + incident.alias + "' is Stopped";
                    break;
                case 'R14':
                    message = "Core Application 4 on '" + incident.alias + "' is Stopped";
                    break;
                // [ Case by ] Orange
                case 'O01':
                    message = "Check required the video cable of '" + incident.alias + "'";
                    break;
                case 'O02':
                    message = "CPU temperature on '" + incident.alias + "' is too high";
                    break;
                case 'O03':
                    message = "Display temperature on '" + incident.alias + "' is too high";
                    break;
                case 'O04':
                    message = "The probability that Media Player '" + incident.alias + "' will be down within a week";
                    break;
                case 'O05':
                    message = "The probability that Media Player '" + incident.alias + "' will be down within 24 hours";
                    break;
                case 'O06':
                    message = "The probability that Display '" + incident.alias + "' will be down within a week";
                    break;
                case 'O07':
                    message = "The probability that Display '" + incident.alias + "' will be down within 24 hours";
                    break;
                case 'O08':
                    message = "The probability that CPU temperature on '" + incident.alias + "' will be too high within 24 hours";
                    break;
                case 'O09':
                    message = "The probability that Display temperature on '" + incident.alias + "' will be too high within 24 hours";
                    break;
                // [ Case by ] Yellow
                case 'Y01':
                    message = "CPU usage of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y02':
                    message = "Memory usage of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y03':
                    message = "Disk usage of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y04':
                    message = "Network response time of '" + incident.alias + "' is over Error Value";
                    break;
                case 'Y05':
                    message = "'Display Energy Saving' value of '" + incident.alias + "' is enabled";
                    break;
                case 'Y06':
                    message = "'Sleep Time' value of '" + incident.alias + "' is enabled";
                    break;
                case 'Y07':
                    message = "'Auto Off' value of '" + incident.alias + "' is enabled";
                    break;
                case 'Y08':
                    message = "'4 Hours Off' value of '" + incident.alias + "' is enabled";
                    break;
            }

            return message;
        };

        $scope.showSeverity = function(severity){
            if(severity == 0){
                return "LOW";
            }else if(severity == 1){
                return "MEDIUM";
            }else if(severity == 2){
                return "HIGH";
            }else{
                return severity;
            }
        };

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.show_flag = function(status){
            if(status == 1 || status == 2 || status == 'NG'){
                return true;
            }else{
                return false;
            }
        };

        $scope.showAliveStatus = function(alive){
            if(alive){
                return 'Running';
            }else{
                return 'Disconnected';
            }
        };

        $scope.show_status = function(status){
            if(status == 1){
                return 'Warning';
            }else if(status == 2){
                return 'Error';
            }else if(status == 0){
                return 'OK';
            }else if(status == null){
                return 'empty';
            }else if(status == 'NG'){
                return 'Error';
            }
        };

        $scope.setting = COM.setting;

        $scope.showTimeZoneStatus = function(value) {
            if(value == undefined){
                value = 17;//default
            }

            var selected = $filter('filter')($scope.setting.time_zone.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].simple_text;
            }
        };

        $scope.showTimeZoneData = function(value) {
            if(value == undefined){
                value = 17;//default
            }

            var selected = $filter('filter')($scope.setting.time_zone.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].data;
            }
        };

        $scope.showTimeFormatStatus = function(value) {
            if(value == undefined){
                value = 0;//default
            }

            var selected = $filter('filter')($scope.setting.time_fmt.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showDateFormatStatus = function(value) {
            if(value == undefined){
                value = 0;//default
            }

            var selected = $filter('filter')($scope.setting.date_fmt.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.$on('$destroy', function() {
            $rootScope.isModalOpened = false;
        });
    });
