/**
 * Created by ksm on 2015-04-10.
 */
'use strict';

angular.module('signageApp')
    .controller('LiveMPHistoricalModalCtrl', function ($scope, $modalInstance, serial_number, DeviceGroup, ReportMPHistory){
        $scope.mediaPlayerSerialNumber = serial_number;

        $scope.get_history_data = function(){
            DeviceGroup.get({mp_serial_number: $scope.mediaPlayerSerialNumber}, function (data) {
                $scope.modal_device_group = data.objects[0];
                $scope.hv_cpu_usage_config.yAxis.plotLines[0].value = $scope.modal_device_group.th_cpu_usage_error;
                $scope.hv_cpu_usage_config.yAxis.plotLines[1].value = $scope.modal_device_group.th_cpu_usage_warning;

                $scope.hv_cpu_temp_config.yAxis.plotLines[0].value = $scope.modal_device_group.th_cpu_temp_error;
                $scope.hv_cpu_temp_config.yAxis.plotLines[1].value = $scope.modal_device_group.th_cpu_temp_warning;

                $scope.hv_mem_usage_config.yAxis.plotLines[0].value = $scope.modal_device_group.th_mem_usage_error;
                $scope.hv_mem_usage_config.yAxis.plotLines[1].value = $scope.modal_device_group.th_mem_usage_warning;

                $scope.hv_hdd_temp_config.yAxis.plotLines[0].value = $scope.modal_device_group.th_hdd_temp_error;
                $scope.hv_hdd_temp_config.yAxis.plotLines[1].value = $scope.modal_device_group.th_hdd_temp_warning;

                $scope.hv_hdd_usage_config.yAxis.plotLines[0].value = $scope.modal_device_group.th_hdd_usage_error;
                $scope.hv_hdd_usage_config.yAxis.plotLines[1].value = $scope.modal_device_group.th_hdd_usage_warning;

                $scope.hv_fan_speed_config.yAxis.plotLines[0].value = $scope.modal_device_group.th_fan_speed_error;
                $scope.hv_fan_speed_config.yAxis.plotLines[1].value = $scope.modal_device_group.th_fan_speed_warning;
            });

            var history_arg = {};
            history_arg['serial_number'] = $scope.mediaPlayerSerialNumber;
            ReportMPHistory.get(history_arg, function(data){
                $scope.history_data = data.objects;

                if( $scope.history_data != 'None' && $scope.history_data != '' && $scope.history_data != undefined ) {
                    var count = $scope.history_data.length;
                    for (var i = 0; i < count; i++) {
                        var data = $scope.history_data[i];
                        var m_time = data.last_updated_date * 1000;
                        $scope.hv_cpu_usage_config.series[0].data.push({x: m_time, y: data.cpu_usage});
                        $scope.hv_cpu_temp_config.series[0].data.push({x: m_time, y: data.cpu_temp});
                        $scope.hv_mem_usage_config.series[0].data.push({x: m_time, y: data.mem_usage});
                        $scope.hv_hdd_temp_config.series[0].data.push({x: m_time, y: data.hdd_temp});
                        $scope.hv_hdd_usage_config.series[0].data.push({x: m_time, y: data.hdd_usage});
                        $scope.hv_fan_speed_config.series[0].data.push({x: m_time, y: data.fan_speed});
                    }
                }

                $scope.hv_cpu_usage_config.loading = false;
                $scope.hv_cpu_temp_config.loading = false;
                $scope.hv_mem_usage_config.loading = false;
                $scope.hv_hdd_temp_config.loading = false;
                $scope.hv_hdd_usage_config.loading = false;
                $scope.hv_fan_speed_config.loading = false;
            });
        };

        $scope.hv_cpu_usage_config = {
            options: {
                chart: {
                    zoomType: 'x',
                    height: '200',
                    width: '740',
                    backgroundColor: '#FFF'
                }
            },
            title: {
                text: 'CPU Usage',
                style: {
                    fontWeight: 'bold',
                    fontSize: '12'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 1 * 24 * 3600000 // one days
            },
            yAxis: {
                max: 100,
                min: 0,
                title: {
                    text: 'Usage (%)'
                },
                plotLines : [
                    {
                        value : 0,
                        color : '#d9534f',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    },
                    {
                        value : 0,
                        color : '#f0ad4e',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    }
                ]
            },
            plotOptions: {
                area: {
                    lineWidth: 1,
                    marker : {
                        enabled : false,
                        states : {
                            hover : {
                                enabled : true,
                                radius : 5
                            }
                        }
                    },
                    shadow : false,
                    states : {
                        hover : {
                            lineWidth : 1
                        }
                    }
                }
            },
            series: [{
                showInLegend: false,
                name: "Usage",
                type: 'area',
                fillColor : {
                    linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointInterval: 24 * 3600 * 1000,
                data: []
            }],
            loading: true
        };

        $scope.hv_cpu_temp_config = {
            options: {
                chart: {
                    type: 'line',
                    height: '200',
                    width: '740',
                    backgroundColor: '#F5F5F5'
                }
            },
            title: {
                text: 'CPU Temperature',
                style: {
                    fontWeight: 'bold',
                    fontSize: '12'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 1 * 24 * 3600000 // one days
            },
            yAxis: {
                max: 100,
                min: 0,
                title: {
                    text: 'Temperature (℃)'
                },
                plotLines : [
                    {
                        value : 0,
                        color : '#d9534f',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    },
                    {
                        value : 0,
                        color : '#f0ad4e',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    }
                ]
            },
            plotOptions: {
                area: {
                    lineWidth: 1,
                        marker : {
                        enabled : false,
                            states : {
                            hover : {
                                enabled : true,
                                    radius : 5
                            }
                        }
                    },
                    shadow : false,
                        states : {
                        hover : {
                            lineWidth : 1
                        }
                    }
                }
            },
            series: [{
                showInLegend: false,
                name: "Temperature",
                type: 'area',
                fillColor : {
                    linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointInterval: 24 * 3600 * 1000,
                data: []
            }],
            loading: true
        };

        $scope.hv_mem_usage_config = {
            options: {
                chart: {
                    type: 'line',
                    height: '200',
                    width: '740',
                    backgroundColor: '#FFF'
                }
            },
            title: {
                text: 'Memory Usage',
                style: {
                    fontWeight: 'bold',
                    fontSize: '12'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 1 * 24 * 3600000 // one days
            },
            yAxis: {
                max: 100,
                min: 0,
                title: {
                    text: 'Usage (%)'
                },
                plotLines : [
                    {
                        value : 0,
                        color : '#d9534f',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    },
                    {
                        value : 0,
                        color : '#f0ad4e',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    }
                ]
            },
            plotOptions: {
                area: {
                    lineWidth: 1,
                    marker : {
                        enabled : false,
                        states : {
                            hover : {
                                enabled : true,
                                radius : 5
                            }
                        }
                    },
                    shadow : false,
                    states : {
                        hover : {
                            lineWidth : 1
                        }
                    }
                }
            },
            series: [{
                showInLegend: false,
                name: "Usage",
                type: 'area',
                fillColor : {
                    linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointInterval: 24 * 3600 * 1000,
                data: []
            }],
            loading: true
        };

        $scope.hv_hdd_temp_config = {
            options: {
                chart: {
                    type: 'line',
                    height: '200',
                    width: '740',
                    backgroundColor: '#F5F5F5'
                }
            },
            title: {
                text: 'Disk Temperature',
                style: {
                    fontWeight: 'bold',
                    fontSize: '12'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 1 * 24 * 3600000 // one days
            },
            yAxis: {
                max: 100,
                min: 0,
                title: {
                    text: 'Temperature (℃)'
                },
                plotLines : [
                    {
                        value : 0,
                        color : '#d9534f',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    },
                    {
                        value : 0,
                        color : '#f0ad4e',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    }
                ]
            },
            plotOptions: {
                area: {
                    lineWidth: 1,
                    marker : {
                        enabled : false,
                        states : {
                            hover : {
                                enabled : true,
                                radius : 5
                            }
                        }
                    },
                    shadow : false,
                    states : {
                        hover : {
                            lineWidth : 1
                        }
                    }
                }
            },
            series: [{
                showInLegend: false,
                name: "Temperature",
                type: 'area',
                fillColor : {
                    linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointInterval: 24 * 3600 * 1000,
                data: []
            }],
            loading: true
        };

        $scope.hv_hdd_usage_config = {
            options: {
                chart: {
                    type: 'line',
                    height: '200',
                    width: '740',
                    backgroundColor: '#FFF'
                }
            },
            title: {
                text: 'Disk Usage',
                style: {
                    fontWeight: 'bold',
                    fontSize: '12'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 1 * 24 * 3600000 // one days
            },
            yAxis: {
                max: 100,
                min: 0,
                title: {
                    text: 'Usage (%)'
                },
                plotLines : [
                    {
                        value : 0,
                        color : '#d9534f',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    },
                    {
                        value : 0,
                        color : '#f0ad4e',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    }
                ]
            },
            plotOptions: {
                area: {
                    lineWidth: 1,
                    marker : {
                        enabled : false,
                        states : {
                            hover : {
                                enabled : true,
                                radius : 5
                            }
                        }
                    },
                    shadow : false,
                    states : {
                        hover : {
                            lineWidth : 1
                        }
                    }
                }
            },
            series: [{
                showInLegend: false,
                name: "Usage",
                type: 'area',
                fillColor : {
                    linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointInterval: 24 * 3600 * 1000,
                data: []
            }],
            loading: true
        };

        $scope.hv_fan_speed_config = {
            options: {
                chart: {
                    zoomType: 'x',
                    height: '200',
                    width: '740',
                    backgroundColor: '#F5F5F5'
                }
            },
            title: {
                text: 'Fan Speed',
                style: {
                    fontWeight: 'bold',
                    fontSize: '12'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 1 * 24 * 3600000 // one days
            },
            yAxis: {
                max: 5000,
                min: 0,
                title: {
                    text: 'RPM (rpm)'
                },
                plotLines : [
                    {
                        value : 0,
                        color : '#d9534f',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    },
                    {
                        value : 0,
                        color : '#f0ad4e',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    }
                ]
            },
            plotOptions: {
                area: {
                    lineWidth: 1,
                    marker : {
                        enabled : false,
                        states : {
                            hover : {
                                enabled : true,
                                radius : 5
                            }
                        }
                    },
                    shadow : false,
                    states : {
                        hover : {
                            lineWidth : 1
                        }
                    }
                }
            },
            series: [{
                showInLegend: false,
                name: "RPM",
                type: 'area',
                fillColor : {
                    linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointInterval: 24 * 3600 * 1000,
                data: []
            }],
            loading: true
        };

        //--------------------------------------------------------------------------------------------------------------

        $scope.get_history_data();

        $scope.hvTimer = null;

        $scope.run_hv = function() {
            $scope.hvTimer = setInterval(function () {
                $scope.get_history_data();
            }, 9000000);
        };

        $scope.run_hv();

        $scope.$on('$destroy', function() {
            clearInterval($scope.hvTimer);
        });

        //--------------------------------------------------------------------------------------------------------------

        $scope.close = function(){
            $modalInstance.dismiss('cancel');
        };
    });