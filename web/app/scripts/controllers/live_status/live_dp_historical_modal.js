/**
 * Created by ksm on 2015-04-10.
 */
'use strict';

angular.module('signageApp')
    .controller('LiveDPHistoricalModalCtrl', function ($scope, $modalInstance, mp_serial_number, serial_number, DeviceGroup, ReportDPHistory){
        $scope.mediaPlayerSerialNumber = mp_serial_number;
        $scope.displaySerialNumber = serial_number;

        $scope.get_history_data = function(){
            DeviceGroup.get({mp_serial_number: $scope.mediaPlayerSerialNumber}, function (data) {
                $scope.modal_device_group = data.objects[0];
                $scope.hv_dp_temp_config.yAxis.plotLines[0].value = $scope.modal_device_group.th_dp_temp_error;
                $scope.hv_dp_temp_config.yAxis.plotLines[1].value = $scope.modal_device_group.th_dp_temp_warning;
            });

            var history_arg = {};
            history_arg['serial_number'] = $scope.displaySerialNumber;
            ReportDPHistory.get(history_arg, function(data){
                $scope.history_data = data.objects;

                if( $scope.history_data != 'None' && $scope.history_data != '' && $scope.history_data != undefined ) {
                    for (var i = 0, count = $scope.history_data.length; i < count; ++i) {
                        var data = $scope.history_data[i];

                        var m_time = data.last_updated_date * 1000;

                        $scope.hv_dp_temp_config.series[0].data.push({x: m_time, y: parseInt(data.dp_temp)});
                    }
                }

                $scope.hv_dp_temp_config.loading = false;
            });
        };

        $scope.hv_dp_temp_config = {
            options: {
                chart: {
                    zoomType: 'x',
                    height: '200',
                    width: '740',
                    backgroundColor: '#FFF'
                }
            },
            title: {
                text: 'Display Temperature',
                style: {
                    fontWeight: 'bold',
                    fontSize: '12'
                }
            },
            xAxis: {
                type: 'datetime',
                minRange: 1 * 24 * 3600000 // one days
            },
            yAxis: {
                max: 100,
                min: 0,
                title: {
                    text: 'Temperature (℃)'
                },
                plotLines : [
                    {
                        value : 0,
                        color : '#d9534f',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    },
                    {
                        value : 0,
                        color : '#f0ad4e',
                        dashStyle : 'ShortDash',
                        zIndex: 3,
                        width : 2
                    }
                ]
            },
            plotOptions: {
                area: {
                    lineWidth: 1,
                    marker : {
                        enabled : false,
                        states : {
                            hover : {
                                enabled : true,
                                radius : 5
                            }
                        }
                    },
                    shadow : false,
                    states : {
                        hover : {
                            lineWidth : 1
                        }
                    }
                }
            },
            series: [{
                showInLegend: false,
                name: "Temperature",
                type: 'area',
                fillColor : {
                    linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                pointInterval: 24 * 3600 * 1000,
                data: []
            }],
            loading: true
        };

        //--------------------------------------------------------------------------------------------------------------

        $scope.get_history_data();

        $scope.hvTimer = null;

        $scope.run_hv = function() {
            $scope.hvTimer = setInterval(function () {
                $scope.get_history_data();
            }, 9000000);
        };

        $scope.run_hv();

        $scope.$on('$destroy', function() {
            clearInterval($scope.hvTimer);
        });

        //--------------------------------------------------------------------------------------------------------------

        $scope.close = function(){
            $modalInstance.dismiss('cancel');
        };
    });