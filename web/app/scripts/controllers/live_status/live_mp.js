/**
 * Created by ksm on 2014-12-29.
 */

'use strict';

angular.module('signageApp')
    .controller('MediaPlayerCtrl', function ($route, $rootScope, $routeParams, SharedService, $scope, $location, $cookieStore, $http, User, Admin, Manager, Member, $timeout, Session, Utils, DP_By_MP, Modal, MediaPlayer, History, ngTableParams) {
        // INHERIT -----------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("DASH", function(){
            $scope.quick_dash = SharedService.quick_dash;
        });
        $scope.$on("MP_SERIAL_NUMBER", function(){
            $scope.media_player_serial_number = SharedService.media_player_serial_number;
            $scope.mediaPlayerSerialNumber = $scope.media_player_serial_number;
        });
        $scope.$on("DP_SERIAL_NUMBER", function(){
            $scope.display_serial_number = SharedService.display_serial_number;
            $scope.displaySerialNumber = $scope.display_serial_number;
        });

        //--------------------------------------------------------------------------------------------------------------
        // JSON Set
        //--------------------------------------------------------------------------------------------------------------
        $scope.slides = [];

        $scope.RESOURCE_USAGE_AND_STATUS = [
            {field: 'cpu_usage', name: 'CPU Usage', type: 'text', unit: '%', status: 'status_cpu_usage'},
            {field: 'fan_speed', name: 'Fan Speed', type: 'text_fan', unit: 'RPM', status: 'status_fan_speed'},
            {field: 'hdd_usage', name: 'Disk Usage', type: 'text', unit: '%', status: 'status_hdd_usage'},
            {field: 'running_time', name: 'Running Time', type: 'text', unit: ''},
            {field: 'mem_usage', name: 'Memory Usage', type: 'text', unit: '%', status: 'status_mem_usage'},
            {field: 'system_time', name: 'System Time', type: 'text_date', unit: '', format:'date_format'},
            {field: 'cpu_temp', name: 'CPU Temp.', type: 'text_temp', unit: '℃', unit2: '℉', status: 'status_cpu_temp'},
            {field: 'system_time_zone', name: 'Timezone', type: 'text', unit: ''},
            {field: 'hdd_temp', name: 'Disk Temp.', type: 'text_temp', unit: '℃', unit2: '℉', status: 'status_hdd_temp'},
            {field: 'svr_conn', name: 'Session Status', type: 'text_conn', unit: ''}
        ];

        $scope.APP_INFO_Headers = [
            {name: 'Type', class_name: 'th_type'},
            {name: 'Name', class_name: 'th_name'},
            {name: 'CPU', class_name: 'th_cpu'},
            {name: 'Memory', class_name: 'th_memory'},
            {name: 'Status', class_name: 'th_status'},
            {name: 'Action', class_name: 'th_action'}
        ];

        $scope.mobile_APP_INFO_Headers = [
          {name: 'Type', class_name: 'th_type'},
          {name: 'Name', class_name: 'th_name'},
          {name: 'Status', class_name: 'th_status'},
          {name: 'Action', class_name: 'th_action'}
        ];

        $scope.APP_INFO = [
            {status: 'app_status1', path: 'app_path1', cpu: 'app_cpu1', memory: 'app_memory1', unit: '%', check_status: 'status_app1'},
            {status: 'app_status2', path: 'app_path2', cpu: 'app_cpu2', memory: 'app_memory2', unit: '%', check_status: 'status_app2'},
            {status: 'app_status3', path: 'app_path3', cpu: 'app_cpu3', memory: 'app_memory3', unit: '%', check_status: 'status_app3'},
            {status: 'app_status4', path: 'app_path4', cpu: 'app_cpu4', memory: 'app_memory4', unit: '%', check_status: 'status_app4'}
        ];

        $scope.MP_INFO_SPEC = [
            {name:'Processor', field:'processor'},
            {name:'BIOS Version/Date', field:'bios'},
            {name:'Physical Memory', field:'physical_memory', unit:'MB'},
            {name:'Physical Disk', field:'physical_hdd', unit:'MB'}
        ];

        $scope.MP_INFO = [
            {field: 'manufact', name: 'Manufacturer', type: 'readOnly'},
            {field: 'model', name: 'Model', type: 'text'},
            {field: 'manage_serial_number', name: 'Serial Number', type: 'text'},
            {field: 'version', name: 'Version', type: 'readOnly'},
            {field: 'alias', name: 'Alias', type: 'readOnly'},
            {field: 'activation_date', name: 'Start Date', type: 'date', unit_type:'date_format', editable_type: 'warrent'},
            {field: 'expiration_date', name: 'End Date', type: 'date', related: 'activation_date', unit_type:'date_format', editable_type: 'warrent'},
            {field: 'warrent_start_date', name: 'Extended Warrenty Start Date', type: 'date', editable_type: 'warrent'},
            {field: 'warrent_end_date', name: 'Extended Warrenty End Date', type: 'date', related: 'warrnet_start_date', editable_type: 'warrent'},
            {field: 'service_offer', name: 'Service Offering', type: 'text', editable_type: 'warrent'}
        ];

        $scope.NETWORK_INFO = [
            {field: 'mac', name: 'MAC Address', type: 'text'},
            {field: 'ip', name: 'IP Address', type: 'text'},
            {field: 'netmask', name: 'Netmask', type: 'text'},
            {field: 'gateway', name: 'Gateway', type: 'text'},
            {field: 'dns', name: 'DNS', type: 'text'},
            {field: 'response_time', name: 'Response Time', type: 'text', unit: 'ms', status: 'status_res_time'}
        ];

        //--------------------------------------------------------------------------------------------------------------
        // Data
        //--------------------------------------------------------------------------------------------------------------
        //mp data
        $scope.$watch('mediaPlayerSerialNumber', function(newVal, oldVal){
          $scope.mediaPlayerSerialNumber = newVal;

          MediaPlayer.get({serial_number: $scope.mediaPlayerSerialNumber}, function (data) {
            if(data.status == 200) {
              $scope.media_player = data.objects[0];

              //get img
              $scope.mp = data.objects[0];

              var mp_model_name = '' + $scope.mp.model;
              var mp_file_name = '';
              if (mp_model_name.indexOf("-") > -1) {
                var str_mp_model = mp_model_name.split("-");
                mp_file_name = str_mp_model[0].toUpperCase();
              } else {
                mp_file_name = mp_model_name.toUpperCase();
              }

              $scope.addSlide($scope.mp_model_img_uri + mp_file_name + '/' + $scope.img[0].name, $scope.img[0].name, true);
              $scope.addSlide($scope.mp_model_img_uri + mp_file_name + '/' + $scope.img[1].name, $scope.img[1].name, false);
              $scope.addSlide($scope.mp_model_img_uri + mp_file_name + '/' + $scope.img[2].name, $scope.img[2].name, false);
              $scope.addSlide($scope.mp_model_img_uri + mp_file_name + '/' + $scope.img[3].name, $scope.img[3].name, false);
              $scope.addSlide($scope.mp_model_img_uri + mp_file_name + '/' + $scope.img[4].name, $scope.img[4].name, false);

              $scope.static_mp_img_uri = $scope.mp_model_img_uri + $scope.mp.model + '/' + $scope.img[0].name;
            }
          });
        });


        $scope.addSlide = function (imgsrc, seq, active_flag) {
            var img_name = seq;
            var img_seq = img_name.split(".")[0];
            $scope.slides = [];
            Utils.isImage(imgsrc).then(function (result) {
                if (result) {
                    $scope.slides.push({
                        image: imgsrc,
                        seq: Number(img_seq)
                        //active: active_flag
                    });
                }
            });
        };

        $scope.$watch('slides', function(newVal){
            $scope.slides = newVal.sort(function(a, b){
                return (a.seq < b.seq) ? -1 : ((a.seq > b.seq) ? 1 : 0);
            });
        }, true);

        $scope.conn_change_char = function(conn_char){
            var show_string = "Fully enabled";
            if(conn_char == "Connected"){
                show_string = "Fully enabled";
            }else if(conn_char == "Disconnected"){
                show_string = "Monitoring only";
            }

            return show_string;
        };
    });
