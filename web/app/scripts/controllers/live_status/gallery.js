'use strict';

angular.module('signageApp')
    .controller('GalleryCtrl', function ($scope, isSlide, slides) {
        $scope.isSlide = isSlide;
        $scope.slides = slides;
    });
