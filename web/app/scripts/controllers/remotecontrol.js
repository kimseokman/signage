'use strict';

angular.module('signageApp')
    .controller('RemoteControlCtrl', function ($scope, MediaPlayer, $modalInstance, MP_DM, getMP_SN) {
        MediaPlayer.get({serial_number: getMP_SN}, function (data) {
            $scope.media_player = data.objects[0];
        });

        $scope.onClickConnect = function () {
            var now = new Date();
            var get_d = '' + now.getDate();
            get_d = (get_d.length == 1) ? (0 + get_d) : (get_d);

            var get_h = '' + now.getHours();
            get_h = (get_h.length == 1) ? (0 + get_h) : (get_h);

            var get_m = '' + now.getMinutes();
            get_m = (get_m.length == 1) ? (0 + get_m) : (get_m);

            var get_s = '' + now.getSeconds();
            get_s = (get_s.length == 1) ? (0 + get_s) : (get_s);

            var current_date = "" + get_d + get_h + get_m + get_s;
            document.title = 'DIGITAL_SIGNAGE_REMOTE_CONTROL:' + current_date;

            var argument = {};
            argument['type'] = 'remote_control';
            argument['command'] = current_date;
            MP_DM.update({serial_number: getMP_SN}, argument);
        };

        $scope.close = function(){
            document.title = 'Digital Signage Total Care Service';
            $modalInstance.dismiss('cancel');
        };
    });
