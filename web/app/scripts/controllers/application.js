'use strict';

angular.module('signageApp')
    .controller('ApplicationCtrl', function ($scope, $rootScope, $cookieStore, $http, $location, $filter, $translate, $window, $interval, $route, SharedService, Admin, Manager, Member, Session, AuthService, User, Modal, MediaPlayer, COM) {
        $scope.classMobile = function(){
            var mobile_class = "mobile";
            if(navigator.userAgent.match("Android") || navigator.userAgent.match("iPhone") || navigator.userAgent.match("iPad")){
                mobile_class = "mobile";
            }else{
                mobile_class = "desktop";
            }
            return mobile_class;
        };

        $scope.langs = {//language label scaffold
            en_US: "English",
            ko: "한국어"
        };

        var language = "";
        if($cookieStore.get("lang")){//check cookie
            language = $cookieStore.get("lang");
        }
        else {//check browser
            language = $translate.proposedLanguage();//default set
            if(navigator.appVersion.indexOf("MSIE 8") != -1 || navigator.appVersion.indexOf("MSIE 9") != -1 || navigator.appVersion.indexOf("MSIE 10") != -1){//exist browser lang
                language = (navigator.browserLanguage).toLowerCase();
                if(language == 'en-us'){
                    language = 'en_US';
                }
                else if(language == 'ko-kr'){
                    language = 'ko';
                }
            }
            else{
                language = (navigator.language).toLowerCase();
                if(language == 'en-us'){
                    language = 'en_US';
                }else if(language == 'ko-kr'){
                    language = language.substring(0,2);
                }
            }
        }

        $cookieStore.put("lang", language);//update cookie
        $translate.use(language);

        $scope.selectLang = $scope.langs[language] || "English";

        $scope.setLang = function(langKey) {//set language
            $scope.selectLang = $scope.langs[langKey];
            $cookieStore.put("lang", langKey);
            $translate.use(langKey);
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('/#/');
            var current_uri = uri_split[1];
            var uri_parse = current_uri.substr(0,4);
            if(uri_parse == 'dash'){

                $route.reload();
            }else{

            }
        };

        $scope.isMobile = function() {
            if(navigator.userAgent.match("Android") || navigator.userAgent.match("iPhone") || navigator.userAgent.match("iPad")){
                return true;
            } else if(navigator.userAgent.match(("window"))){
                return false;
            }
        };

        $rootScope.IE_reload = false;

        $scope.date_validate = function(date){
          var val = ""+date;
          var regExp = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
          //var regExp = /^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]:[0-5][0-9]$/;
          var var_match = val.match(regExp) != null;

          return var_match;
        };

        $scope.time_validate = function(time){
          var val = ""+time;
          //var regExp = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
          var regExp = /^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]:[0-5][0-9]$/;
          var var_match = val.match(regExp) != null;

          return var_match;
        };

        $scope.IsIE8 = function() {
            if(navigator.appVersion.indexOf("MSIE 8") != -1){
                $rootScope.IE_reload = true;
                return true;
            } else if(navigator.userAgent.indexOf("MSIE 7") > 0 && navigator.appVersion.indexOf("Trident") != -1){
                $rootScope.IE_reload = true;
                return true;
            } else {
                $rootScope.IE_reload = false;
                return false;
            }
        };

        $scope.IsIE8nIE9 = function(){
            if(navigator.appVersion.indexOf("MSIE 9") != -1){
                return true;
            }else if(navigator.appVersion.indexOf("MSIE 8") != -1){
                return true;
            } else if(navigator.userAgent.indexOf("MSIE 7") > 0 && navigator.appVersion.indexOf("Trident") != -1){
                return true;
            } else {
                return false;
            }
        };

        if(navigator.appVersion.indexOf("MSIE 8") != -1){
            $scope.IE_compatible = false;
        } else if(navigator.userAgent.indexOf("MSIE 7") > 0 && navigator.appVersion.indexOf("Trident") != -1){
            $scope.IE_compatible = true;
        } else {
            $scope.IE_compatible = false;
        }

        if($scope.IE_compatible){
            alert("Please disable compatibility view mode when do you use Internet Explorer 8.");
        }

        if(navigator.userAgent.match("Android") || navigator.userAgent.match("iPhone") || navigator.userAgent.match("iPad")) {
            $scope.access_device = "mobile";
        } else {
            $scope.access_device = "desktop";
        }

        $scope.show_zero_value = function(value){
            if(value != undefined){
                return value;
            }else{
                return 'empty';
            }
        };

        // COMMON ------------------------------------------------------------------------------------------------------
        $scope.tree_uri = "views/tree.html";
        $scope.tab_uri = "views/tab.html";
        $scope.path_uri = "views/path.html";
        $scope.info_uri = "views/live_information.html";
        $scope.support_history_uri = "views/support_history.html";
        $scope.reports_nav_uri = "views/reports_nav.html";
        $scope.mobile_reports_nav_uri = "views/mobile_report_nav.html";
        $scope.setting = COM.setting;

        // DEFAULT -----------------------------------------------------------------------------------------------------
        var d = new Date();
        d.setDate(d.getDate() - 1);
        $scope.yesterday = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
        $scope.spin_flag = false;

        $scope.isAuthenticated = AuthService.isAuthenticated();

        $scope.$watch(AuthService.isAuthenticated, function(newVal){//watch - isAuthenticated at services/api/access.js
            $scope.isAuthenticated = newVal;

            if($scope.isAuthenticated){
                $scope.accessToken = Session.get('accessToken');
                $http.defaults.headers.get = {token: $scope.accessToken};
                $http.defaults.headers.get['If-Modified-Since'] = new Date().getTime();
                $http.defaults.headers.get['Cache-Control'] = 'no-cache';
                $http.defaults.headers.get['Pragma'] = 'no-cache';
                $http.defaults.headers.put["token"] = $scope.accessToken;
                $http.defaults.headers.post["token"] = $scope.accessToken;
                $http.defaults.headers.common["token"] = $scope.accessToken;

                MediaPlayer.get({meta_only: 1}).$promise.then(function (data) {
                    if(data.status==200){
                        $scope.quick_dash = {
                            count: data.meta.total_count + " / " + data.meta.total_display_count,
                            totalCount: data.meta.total_count,
                            display_totalCount: data.meta.total_display_count,
                            checkRequiredCount: data.meta.check_required_count,
                            unresolvedCount: data.meta.unresolved_count,
                            deadCount: data.meta.disconnected_count,
                            latestResolvedCount: data.meta.recent_resolved_count,
                            latestAddedCount: data.meta.today_activation_count,
                            normalCount: data.meta.normal_count
                        };
                        SharedService.prepForQuickDashBroadcast($scope.quick_dash);
                    }
                });

                if($scope.media_player_serial_number == undefined){
                    var api_params = {};
                    api_params['enabled'] = 1;
                    api_params['limit'] = 1;
                    api_params['group_status'] = 'all';
                    api_params['item'] = 'group_status';
                    api_params['order'] = '-';

                    MediaPlayer.get(api_params, function (data) {
                        if(data.status==200){
                            if(!!data.objects[0]){ // 2014 10 08 liveView auto mapping by ksm
                                $scope.media_player_serial_number = data.objects[0]['serial_number'];
                            }
                        }
                        SharedService.prepForMediaPlayerSerialNumberBroadcast($scope.media_player_serial_number);
                    });
                }

                User.get().$promise.then(function (data) {
                    if(data.status==200) {
                        $scope.userType = data.objects[0].userType;
                        $scope.userID = data.objects[0].userID;
                        $scope.userAuth = data.objects[0].auth;

                        $scope.isDevice_Info = false;
                        $scope.isMP_Info = false;
                        $scope.isDP_Info = false;
                        $scope.isReport_Info = false;

                        angular.forEach($scope.userAuth, function(s){
                            if(s == 1) $scope.isDevice_Info = true;
                            if(s == 2) $scope.isMP_Info = true;
                            if(s == 4) $scope.isDP_Info = true;
                            if(s == 8) $scope.isReport_Info = true;
                        });

                        if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                            Admin.get({}, function(data){
                                if(data.status == 200){
                                    $scope.user = data.objects[0];
                                    SharedService.prepForUserBroadcast($scope.user);
                                }
                            });
                        }else if($scope.userType == 'MANAGER'){
                            Manager.get({}, function(data){
                                if(data.status == 200) {
                                    $scope.user = data.objects[0];
                                    SharedService.prepForUserBroadcast($scope.user);
                                }
                            });
                        }else if($scope.userType == 'MEMBER'){
                            Member.get({}, function(data){
                                if(data.status == 200) {
                                    $scope.user = data.objects[0];
                                    SharedService.prepForUserBroadcast($scope.user);
                                }
                            });
                        }
                    }
                });
            }else{
                $scope.quick_dash = {
                    count: 0,
                    totalCount: 0,
                    display_totalCount: 0,
                    checkRequiredCount: 0,
                    unresolvedCount: 0,
                    deadCount: 0,
                    latestResolvedCount: 0,
                    latestAddedCount: 0,
                    normalCount: 0
                };
                SharedService.prepForQuickDashBroadcast($scope.quick_dash);
                $scope.user = undefined;
                SharedService.prepForUserBroadcast($scope.user);

                var change_uri ="/logout";
                $location.path(change_uri, true);
            }
        });



        // SHOW & HIDE -------------------------------------------------------------------------------------------------
        $scope.showTimeZoneStatus = function(value) {
            var selected = $filter('filter')($scope.setting.time_zone.statuses, {value: value});

            if(value == undefined) {
                return 'Not Set';
            } else {
                return selected[0].simple_text;
            }
        };

        $scope.showTimeZoneData = function(value) {
            if(value == undefined){
                value = 17;//default
            }

            var selected = $filter('filter')($scope.setting.time_zone.statuses, {value: value});

            if(value == undefined) {
                return 'Not Set';
            } else {
                return selected[0].data;
            }
        };

        $scope.showTimeFormatStatus = function(value) {
            if(value == undefined){
                value = 0;//default
            }

            var selected = $filter('filter')($scope.setting.time_fmt.statuses, {value: value});

            if(value == undefined) {
                return 'Not Set';
            } else {
                return selected[0].text;
            }
        };

        $scope.showDateFormatStatus = function(value) {
            if(value == undefined) {
                value = 0;//default
            }

            var selected = $filter('filter')($scope.setting.date_fmt.statuses, {value: value});

            if(value == undefined) {
                return 'Not Set';
            } else {
                return selected[0].text;
            }
        };

        $scope.showPredictCategory = function(value){
            var selected = $filter('filter')($scope.setting.predict.category.statuses, {value: value});

            if(value == undefined) {
                return 'Not Set';
            } else {
                return selected[0].text;
            }
        };

        $scope.showIntMethodStatus = function(value){
            if(value == undefined){
                return 'Not set';
            }else{
                var selected = $filter('filter')($scope.setting.method_case.statuses, {value: value});
                return selected[0].text;
            }
        };

        $scope.showIntActiveStatus = function(value){
            if(value == undefined){
                return 'Not set';
            }else{
                var selected = $filter('filter')($scope.setting.int_active.statuses, {value: value});
                return selected[0].text;
            }
        };

        $scope.showActiveStatus = function (value) {
            if(value == undefined){
                return 'Not set';
            }else{
                var selected = $filter('filter')($scope.setting.active.statuses, {value: value});
                return selected[0].text;
            }
        };

        $scope.showPredictCode = function(value){
            var code = value;
            var show_code = "P";//prediction
            if(code < 10) {
                show_code += "0" + code;
            } else {
                show_code += code;
            }

            return show_code;
        };

        $scope.showPredictStatus = function(value){
            var selected = $filter('filter')($scope.setting.predict.active.statuses, {value: value});

            if(value == undefined) {
                return 'Not Set';
            } else {
                return selected[0].text;
            }
        };

        // FUNCTIONS ---------------------------------------------------------------------------------------------------
        $scope.searchDash_mobile = function(key, value){
          var base_uri = '/m.dash';
          var full_uri = $location.absUrl();
          var uri_split = full_uri.split('#');
          var current_uri = uri_split[1];

          //etc param remove
          //dash
          $location.search('index', null);
          $location.search('member', null);
          $location.search('group_status', null);
          $location.search('resolved_date', null);
          $location.search('activation_date', null);
          //admin
          $location.search('install', null);
          $location.search('device-group', null);
          $location.search('member', null);
          $location.search('manager', null);

          $location.search(key, value);
          var change_uri = base_uri + "?" + key + "=" + value;

          if(key == "group_status"){
            $location.search("index", 0);
            change_uri += "&" + "index" + "=" + 0;
          }else if(key == "activation_date"){
            $location.search("index", 7);
            change_uri += "&" + "index" + "=" + 7;
          }else if(key == "resolved_date"){
            $location.search("index", 8);
            change_uri += "&" + "index" + "=" + 8;
          }

          if(current_uri != change_uri){
            $location.path(base_uri, true);
          } else {
            $route.reload();
          }
        };

        $scope.searchDash = function(key, value){
            var base_uri = '/dash';
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            //etc param remove
            //dash
            $location.search('index', null);
            $location.search('member', null);
            $location.search('group_status', null);
            $location.search('resolved_date', null);
            $location.search('activation_date', null);
            //admin
            $location.search('install', null);
            $location.search('device-group', null);
            $location.search('member', null);
            $location.search('manager', null);

            $location.search(key, value);

            var change_uri = base_uri + "?" + key + "=" + value;
            if(key == "group_status"){
                $location.search("index", 0);
                change_uri += "&" + "index" + "=" + 0;
            }else if(key == "activation_date"){
                $location.search("index", 7);
                change_uri += "&" + "index" + "=" + 7;
            }else if(key == "resolved_date"){
                $location.search("index", 8);
                change_uri += "&" + "index" + "=" + 8;
            }

            if(current_uri != change_uri){
                $location.path(base_uri, true);
            } else {
                $route.reload();
            }
        };

        $scope.movePage = function(page){
            window.scrollTo(0,0);
            //etc param remove
            //dash
            $location.search('index', null);
            $location.search('member', null);
            $location.search('group_status', null);
            $location.search('resolved_date', null);
            $location.search('activation_date', null);
            //admin
            $location.search('install', null);
            $location.search('device-group', null);
            $location.search('member', null);
            $location.search('manager', null);

            var change_uri = '';
            if(page == 'dash'){
                change_uri = '/dash';
            } else if(page == 'live') {
                change_uri = '/live/media-players/' + $scope.media_player_serial_number;
            } else if(page == 'report') {
                change_uri = '/reports/top10';
            } else if(page == 'admin') {
                change_uri = '/admin';
            } else {
                change_uri = '/dash';
            }

            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if(current_uri != change_uri){
                $location.path(change_uri, true);
            } else {
                $route.reload();
            }
        };

        $scope.movePage_mobile = function(page){
            window.scrollTo(0,0);

          $location.search('index', null);
          $location.search('member', null);
          $location.search('group_status', null);
          $location.search('resolved_date', null);
          $location.search('activation_date', null);
          //admin
          $location.search('install', null);
          $location.search('device-group', null);
          $location.search('member', null);
          $location.search('manager', null);

            var change_uri = '';
            if(page == 'm.dash'){
                change_uri = '/m.dash';
            }
            else if(page == 'm.live_status'){
                change_uri = '/m.live_status/media-players/' + $scope.media_player_serial_number;
            }
            else if(page == 'm.report'){
                change_uri = '/m.reports/top10';
            }
            else{
                change_uri = '/m.dash';
            }

          var full_uri = $location.absUrl();
          var uri_split = full_uri.split('#');
          var current_uri = uri_split[1];

          if(current_uri != change_uri){
            $location.path(change_uri, true);
          } else {
            $route.reload();
          }
        };

        $scope.currentURI = function () {
            var fullUrl = $location.absUrl();

            if(fullUrl.indexOf("dash") != -1)
                return "device_search";
            else if (fullUrl.indexOf("live") != -1)
                return "live";
            else if(fullUrl.indexOf("admin") != -1)
                return "admin";
            else if(fullUrl.indexOf("reports") != -1)
                return "reports";
            else if(fullUrl.indexOf("help") != -1)
                return "help";
            else
                return "device_search";
        };

        $scope.refreshPage = function () {
            $route.reload();
        };

        // DYNAMIC -----------------------------------------------------------------------------------------------------
        $scope.$watch('user.refresh_time', function (newVal, oldVal) {
            if (newVal == undefined)
                return;

            $scope.refreshTime = $scope.user['refresh_time'];
            if (newVal != oldVal) {
                $interval.cancel($scope.stop);
                $scope.stop = $interval(function () {
                    $scope.refreshTime -= 1;

                    if($scope.refreshTime <= 2){ //refresh icon rotated
                        $scope.spin_flag = true;
                    }else{
                        $scope.spin_flag = false;
                    }

                    if ($scope.refreshTime <= 0) {
                        $scope.refreshTime = $scope.user['refresh_time'];

                        $route.reload();
                    }
                }, 1000);
            }
        });

        // MODAL -------------------------------------------------------------------------------------------------------
        $scope.openHelpMenu = function () {
            Modal.open(
                'views/help.html',
                'HelpCtrl',
                'lg',
                {
                }
            );
        };

        // UPDATE ------------------------------------------------------------------------------------------------------
        $scope.updateInformation = function(){
            $scope.isAuthenticated = AuthService.isAuthenticated();

            if($scope.isAuthenticated){
                $scope.accessToken = Session.get('accessToken');
                $http.defaults.headers.get = {token: $scope.accessToken};
                $http.defaults.headers.get['If-Modified-Since'] = new Date().getTime();
                $http.defaults.headers.get['Cache-Control'] = 'no-cache';
                $http.defaults.headers.get['Pragma'] = 'no-cache';
                $http.defaults.headers.put["token"] = $scope.accessToken;
                $http.defaults.headers.post["token"] = $scope.accessToken;
                $http.defaults.headers.common["token"] = $scope.accessToken;

                MediaPlayer.get({meta_only: 1}).$promise.then(function (data) {
                    if(data.status==200){
                        $scope.quick_dash = {
                            count: data.meta.total_count + " / " + data.meta.total_display_count,
                            totalCount: data.meta.total_count,
                            display_totalCount: data.meta.total_display_count,
                            checkRequiredCount: data.meta.check_required_count,
                            unresolvedCount: data.meta.unresolved_count,
                            deadCount: data.meta.disconnected_count,
                            latestResolvedCount: data.meta.recent_resolved_count,
                            latestAddedCount: data.meta.today_activation_count,
                            normalCount: data.meta.normal_count
                        };

                        SharedService.prepForQuickDashBroadcast($scope.quick_dash);
                    }
                });

                if($scope.media_player_serial_number == undefined){
                    var api_params = {};
                    api_params['enabled'] = 1;
                    api_params['limit'] = 1;
                    api_params['group_status'] = 'all';
                    api_params['item'] = 'group_status';
                    api_params['order'] = '-';

                    MediaPlayer.get(api_params, function (data) {
                        if(data.status==200){
                            if(!!data.objects[0]){ // 2014 10 08 liveView auto mapping by ksm
                                $scope.media_player_serial_number = data.objects[0]['serial_number'];
                            }
                            SharedService.prepForMediaPlayerSerialNumberBroadcast($scope.media_player_serial_number);
                        }
                    });
                }

                User.get().$promise.then(function (data) {
                    if(data.status==200) {
                        $scope.userType = data.objects[0].userType;
                        $scope.userID = data.objects[0].userID;
                        $scope.userAuth = data.objects[0].auth;

                        $scope.isDevice_Info = false;
                        $scope.isMP_Info = false;
                        $scope.isDP_Info = false;
                        $scope.isReport_Info = false;

                        angular.forEach($scope.userAuth, function(s){
                            if(s == 1) $scope.isDevice_Info = true;
                            if(s == 2) $scope.isMP_Info = true;
                            if(s == 4) $scope.isDP_Info = true;
                            if(s == 8) $scope.isReport_Info = true;
                        });

                        if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                            Admin.get({}, function(data){
                                if(data.status == 200){
                                    $scope.user = data.objects[0];
                                    SharedService.prepForUserBroadcast($scope.user);
                                }
                            });
                        }else if($scope.userType == 'MANAGER'){
                            Manager.get({}, function(data){
                                if(data.status == 200) {
                                    $scope.user = data.objects[0];
                                    SharedService.prepForUserBroadcast($scope.user);
                                }
                            });
                        }else if($scope.userType == 'MEMBER'){
                            Member.get({}, function(data){
                                if(data.status == 200) {
                                    $scope.user = data.objects[0];
                                    SharedService.prepForUserBroadcast($scope.user);
                                }
                            });
                        }
                    }
                });
            }else{
                $scope.quick_dash = {
                    count: 0,
                    totalCount: 0,
                    display_totalCount: 0,
                    checkRequiredCount: 0,
                    unresolvedCount: 0,
                    deadCount: 0,
                    latestResolvedCount: 0,
                    latestAddedCount: 0,
                    normalCount: 0
                };
                SharedService.prepForQuickDashBroadcast($scope.quick_dash);
                $scope.user = undefined;
                SharedService.prepForUserBroadcast($scope.user);

                var change_uri ="/logout";
                $location.path(change_uri, true);
            }
        };
    });

