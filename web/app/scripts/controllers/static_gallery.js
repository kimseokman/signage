/**
 * Created by ksm on 2014-12-22.
 */

angular.module('signageApp')
    .controller('SGalleryCtrl', function ($scope, img_uri) {
        $scope.img_uri = img_uri;
    });
