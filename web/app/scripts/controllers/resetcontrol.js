/**
 * Created by ksm on 2014-11-27.
 */

'use strict';

angular.module('signageApp')
    .controller('ReSetControlCtrl', function ($scope, $modalInstance, DP_DM, getMP_SN) {

        $scope.onClickReset= function () {
            var argument = {};

            argument['type'] = "display_control";
            argument['command'] = "tag_Reset";
            argument['data'] = "02";
            argument['log'] = "Factory Reset";

            DP_DM.update({serial_number: getMP_SN}, argument);
        };

        $scope.close = function(){
            $modalInstance.dismiss('cancel');
        };
    });