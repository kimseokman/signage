/**
 * Created by ksm on 2014-12-16.
 */

'use strict';

angular.module('signageApp')
    .controller('RequestCtrl', function($scope, $modalInstance, Modal, MP_DM, type, command, data, serial_number){

        $scope.close = function(){
            $modalInstance.dismiss('cancel');
        };

        var log_text = '';

        if(type=='agent_control'){
            $scope.comment = 'Do you want to restart agent?';
        }else if(type=='mediaplayer_control'){
            $scope.comment = 'Do you want to restart OS?';
        }else if(type=='check_control'){
            $scope.comment = 'Do you want to readjust displays?';
        }else{
            $scope.comment = 'ERROR [500] Unknown Type';
        }

        var argument = {};

        argument['type'] = type;
        argument['command'] = command;
        argument['data'] = data;

        if(type=='agent_control'){
            log_text = "Restart";
        }else if(type=='check_control'){
            log_text = "Check";
        }else{
            log_text = data + " ";
        }

        argument['log'] = log_text;

        $scope.ok = function(){
            MP_DM.update({serial_number: serial_number}, argument);

            $modalInstance.dismiss('cancel');
        };
    });
