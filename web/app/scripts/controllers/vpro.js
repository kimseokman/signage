'use strict';

angular.module('signageApp')
    .controller('VProCtrl', function ($scope, MediaPlayer, $modalInstance, ENV, getMP_SN) {

        MediaPlayer.get({serial_number: getMP_SN}, function (data) {
            $scope.media_player = data.objects[0];
        });

        $scope.onClickConnect = function (ip_address) {
            document.title = 'DIGITAL_SIGNAGE_VPRO:' + ip_address;
        };

        $scope.close = function(){
            document.title = 'Digital Signage Total Care Service';
            $modalInstance.dismiss('cancel');
        };
    });
