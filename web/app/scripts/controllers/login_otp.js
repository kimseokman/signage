/**
 * Created by ksm on 2015-01-13.
 */

'use strict';

angular.module('signageApp')
    .controller('OtpCtrl', function ($http, $scope, $route, $location, OTP, Session, User, Admin, Manager, Member, Access) {
        $scope.accessToken = Session.get('accessToken');
        //$scope.$parent.updateStatus();
        //$scope.$parent.updateUser();

        $http.defaults.headers.get = {token: $scope.accessToken};
        $http.defaults.headers.get['If-Modified-Since'] = new Date().getTime();
        $http.defaults.headers.get['Cache-Control'] = 'no-cache';
        $http.defaults.headers.get['Pragma'] = 'no-cache';
        $http.defaults.headers.put["token"] = $scope.accessToken;
        $http.defaults.headers.post["token"] = $scope.accessToken;
        $http.defaults.headers.common["token"] = $scope.accessToken;

        $scope.qr_code_uri = '/qr_code/';
        //$scope.qr_code_uri ='./images/test/test.png';
        $scope.verification_code = '';

        User.get().$promise.then(function (data) {
            if(data.status==200) {
                $scope.userType = data.objects[0].userType;
                $scope.userID = data.objects[0].userID;
                $scope.userAuth = data.objects[0].auth;

                $scope.created_otp = !!data.objects[0].createOTP;

                $scope.isDevice_Info = false;
                $scope.isMP_Info = false;
                $scope.isDP_Info = false;
                $scope.isReport_Info = false;

                angular.forEach($scope.userAuth, function(s){
                    if(s == 1) $scope.isDevice_Info = true;
                    if(s == 2) $scope.isMP_Info = true;
                    if(s == 4) $scope.isDP_Info = true;
                    if(s == 8) $scope.isReport_Info = true;
                });

                if($scope.userType == 'ADMIN' || $scope.userType == 'SUPERADMIN'){
                    Admin.get({}, function(data){
                        $scope.user = data.objects[0];
                        $scope.qr_code_uri = '/qr_code/' + $scope.user.userID + '.png?cache=' + (new Date()).getTime();   //이미지도 새로 고침
                    });
                }else if($scope.userType == 'MANAGER'){
                    Manager.get({}, function(data){
                        $scope.user = data.objects[0];
                        $scope.qr_code_uri = '/qr_code/' + $scope.user.userID + '.png?cache=' + (new Date()).getTime();   //이미지도 새로 고침
                    });
                }else if($scope.userType == 'MEMBER'){
                    Member.get({}, function(data){
                        $scope.user = data.objects[0];
                        $scope.qr_code_uri = '/qr_code/' + $scope.user.userID + '.png?cache=' + (new Date()).getTime();   //이미지도 새로 고침
                    });
                }else{
                    //alert('guest');
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------
        // Functions
        //--------------------------------------------------------------------------------------------------------------
        /*
        $scope.showGuide = function(created_otp){
            var guide = '';

            if(!created_otp){
                guide =
                    "<ul>" +
                        "<li>" +
                            "1. <a target='_blank' href='https://support.google.com/accounts/answer/1066447?hl=en'>Install Google Authenticator</a> on your phone" +
                        "</li>" +
                        "<li>" +
                            "2. Open the Google Authenticator app." +
                        "</li>" +
                        "<li>" +
                            "3. Tap menu, then tap 'Set up account', then tap 'Scan a barcode.'" +
                        "</li>" +
                        "<li>" +
                            "4. Your phone will now be in a 'scanning' mode. When you are in this mode, scan the barcode below:"+
                        "</li>" +
                    "</ul>";
            }

            return guide;
        };
        */

        $scope.submit_code = function(){
            var submit_args = {};
            submit_args['otp_code'] = $scope.verification_code;
            //submit_args['time_stamp'] = new Date().getTime();

            OTP.get(submit_args, function(data){
                $scope.otp = data.objects[0];
                Session.set('passOTP', $scope.otp.pass_otp);

                if($scope.otp.pass_otp){
                    var otp_argument = {};
                    otp_argument['createOTP'] = true;
                    OTP.update(otp_argument);

                    var argument = {};
                    argument['type'] = true;    //login
                    var userAgent = window.navigator.userAgent;
                    var browser_ver = null;
                    var os_ver = null;

                    if(userAgent.indexOf("Firefox") != -1)                                         browser_ver = "Firefox";
                    if(userAgent.indexOf("Opera") != -1)                                           browser_ver = "Opera";
                    if(userAgent.indexOf("Chrome") != -1)                                          browser_ver = "Chrome";
                    if(userAgent.indexOf("Safari") != -1 && userAgent.indexOf("Chrome") == -1)    browser_ver = "Safari";

                    if(userAgent.indexOf("MSIE 6") != -1)                                          browser_ver = "Internet Explorer 6";
                    if(userAgent.indexOf("MSIE 7") != -1)                                          browser_ver = "Internet Explorer 7";
                    if(userAgent.indexOf("MSIE 8") != -1)                                          browser_ver = "Internet Explorer 8";
                    if(userAgent.indexOf("MSIE 9") != -1)                                          browser_ver = "Internet Explorer 9";
                    if(userAgent.indexOf("MSIE 10") != -1)                                         browser_ver = "Internet Explorer 10";
                    if(userAgent.indexOf("rv") != -1)                                               browser_ver = "Internet Explorer 11";

                    argument['browser_ver'] = browser_ver || "unknown"; // browser version

                    var appVersion = window.navigator.appVersion;
                    var os_versions = {Windows: "Win", MacOS: "Mac", UNIX: "X11", Linux: "Linux"};

                    for(var key in os_versions) {
                        if (appVersion.indexOf(os_versions[key]) != -1) {
                            os_ver = key;
                        }
                    }
                    argument['os_ver'] = os_ver || "unknown";   //os version

                    Access.save(argument);

                    $location.path('dash', true);
                }else{
                    alert("Code Error");
                }
            });
        };

        $scope.logout = function(){
            Session.all_clear();
            $location.path('login', true);
            /*
            var argument = {};
            argument['type'] = false;    //logout
            var userAgent = window.navigator.userAgent;
            var browser_ver = null;
            var os_ver = null;

            if(userAgent.indexOf("Firefox") != -1)                                         browser_ver = "Firefox";
            if(userAgent.indexOf("Opera") != -1)                                           browser_ver = "Opera";
            if(userAgent.indexOf("Chrome") != -1)                                          browser_ver = "Chrome";
            if(userAgent.indexOf("Safari") != -1 && userAgent.indexOf("Chrome") == -1)    browser_ver = "Safari";

            if(userAgent.indexOf("MSIE 6") != -1)                                          browser_ver = "Internet Explorer 6";
            if(userAgent.indexOf("MSIE 7") != -1)                                          browser_ver = "Internet Explorer 7";
            if(userAgent.indexOf("MSIE 8") != -1)                                          browser_ver = "Internet Explorer 8";
            if(userAgent.indexOf("MSIE 9") != -1)                                          browser_ver = "Internet Explorer 9";
            if(userAgent.indexOf("MSIE 10") != -1)                                         browser_ver = "Internet Explorer 10";
            if(userAgent.indexOf("rv") != -1)                                               browser_ver = "Internet Explorer 11";

            argument['browser_ver'] = browser_ver || "unknown"; // browser version

            var appVersion = window.navigator.appVersion;
            var os_versions = {Windows: "Win", MacOS: "Mac", UNIX: "X11", Linux: "Linux"};

            for(var key in os_versions) {
                if (appVersion.indexOf(os_versions[key]) != -1) {
                    os_ver = key;
                }
            }
            argument['os_ver'] = os_ver || "unknown";   //os version

             Access.save(argument);
            */
        };

        /* auto focus */
        angular.element('.inp_code').trigger('focus');
    });
