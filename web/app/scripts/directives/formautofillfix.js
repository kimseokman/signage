'use strict';

angular.module('signageApp')
    .directive('formAutofillFix', function ($timeout) {
        return function (scope, element, attrs) {
            element.prop('method', 'post');
            if (attrs.ngSubmit) {
                $timeout(function () {
                    element.unbind('submit').bind('submit', function (event) {
                        event.preventDefault();
                        var inp = element.find('input');
                        inp.triggerHandler('change');
                        scope.$apply(attrs.ngSubmit);
                    });
                });
            }
        };
    });