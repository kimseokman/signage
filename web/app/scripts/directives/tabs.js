'use strict';

angular.module('signageApp')
    .directive('tabs', function () {
        return {
            restrict: 'A',
            transclude: true,
            scope: { paneChanged: '&'},
            controller: [ "$scope", function ($scope) {
                var panes = $scope.panes = [];
                var media_player_pane;

                $scope.select = function (pane) {
                    angular.forEach(panes, function (pane) {
                        pane.selected = false;
                    });
                    pane.selected = true;
                    $scope.paneChanged({selectedPane: pane});
                };

                this.addPane = function (pane) {
                    if (panes.length == 0) $scope.select(pane);
                    panes.push(pane);
                    if(pane.title == "Media Player"){
                        media_player_pane = pane;
                    }

                    if(media_player_pane)
                        media_player_pane.selected = true;
                };

                this.removePane = function (pane) {
                    var index = panes.indexOf(pane);
                    if (index > -1) {
                        panes.splice(index, 1);
                    }
                };

                $scope.test = function() {
                  alert('123');
                };
            }],
            template: "<div class='tabbable'>" + "<div class='tab_title'>" +
                "<ul class='nav nav-tabs'>" +
                //"<li ng-repeat='pane is panes' ng-class='{'active': pane.selected}'>"+
                "<li ng-repeat='pane in panes' ng-class='{active:pane.selected}'>" +
                "<a href='' ng-click='select(pane)' ng-class='{discon:pane.alive==1}'><span ng-if='pane.icon' class='fa fa-{{pane.icon}}'></span> <span translate='content.liveStatus.{{pane.title}}'></span></a>" +
                "</li>" +
                "</ul>" + "</div>" +
                "<div class='tab-content MP_content' ng-transclude></div>" +
                "</div>",
            replace: true
        };
    }).
    directive('pane', function () {
        return {
            require: '^tabs',
            restrict: 'A',
            transclude: true,
            scope: { icon: '@', title: '@', alive: '@', sn:'@' },
            link: function (scope, element, attrs, tabsCtrl) {
                tabsCtrl.addPane(scope);
                scope.$on('$destroy', function() {
                    tabsCtrl.removePane(scope);
                });
            },
            template: '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
                '</div>',
            replace: true
        };
    });
