/**
 * Created by ksm on 2014-11-27.
 */

angular.module('signageApp')
    .factory('TOP10', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/top10', null, {'update': {method: 'PUT'}});
    });