'use strict';

angular.module('signageApp')
    .factory('Organization', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/auto_complete/organizations');
    });