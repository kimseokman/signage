/**
 * Created by ksm on 2015-04-09.
 */
angular.module('signageApp')
    .factory('StatsDPList', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/stats/dp/malfunction/list', null, {'update': {method: 'PUT', 'query':  {method:'GET', isArray:true}}});
    });