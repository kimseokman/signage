/**
 * Created by ksm on 2015-01-29.
 */

'use strict';

angular.module('signageApp')
    .factory('UserGroup', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/user-group/:key', null, {'update': {method: 'PUT'}});
    });