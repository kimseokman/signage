/**
 * Created by ksm on 2014-12-12.
 */

'use strict';

angular.module('signageApp')
    .factory('AuthMyTagging', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/my-tag', null, {'update': {method: 'PUT'}});
    });

