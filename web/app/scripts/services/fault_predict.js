/**
 * Created by ksm on 2015-04-28.
 */

angular.module('signageApp')
    .factory('FaultPredict', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/fault-predict/:key', null, {'update': {method: 'PUT'}});
    });
