/**
 * Created by ksm on 2014-12-01.
 */
'use strict';

angular.module('signageApp')
    .factory('AuthSite', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/sites', null, {'update': {method: 'PUT'}});
    });