/**
 * Created by ksm on 2014-11-23.
 */

'use strict';

angular.module('signageApp')
    .factory('MemberList', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/users/members', null,{'update': {method: 'PUT'}});
    });