'use strict';

angular.module('signageApp')
    .factory('DisplayAlias', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/auto_complete/display-aliases');
    });
