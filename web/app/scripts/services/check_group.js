/**
 * Created by 박원배 on 2015-05-19.
 */

'use strict';

angular.module('signageApp')
    .factory('CHECK_GROUP', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/groups/check/:user_id', null, {'update': {method: 'PUT'}});
    });
