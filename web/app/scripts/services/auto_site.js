'use strict';

angular.module('signageApp')
    .factory('Site', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/auto_complete/sites');
    });