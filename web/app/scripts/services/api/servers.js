/**
 * Created by 박원배 on 2015-03-26.
 */
'use strict';

angular.module('signageApp')
  .factory('Servers', function ($resource, ENV) {
  return $resource(ENV.host + '/api/v1/admin/servers/:pk', null, {'update': {method: 'PUT'}});
});
