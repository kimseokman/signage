/**
 * Created by 박원배 on 2015-03-27.
 */
'use strict';

angular.module('signageApp')
  .factory('Display_Models', function ($resource, ENV) {
    return $resource(ENV.host + '/api/v1/admin/display_model/:pk', null, {'update': {method: 'PUT'}});
  });
