/**
 * Created by 박원배 on 2015-03-27.
 */
'use strict';

angular.module('signageApp')
  .factory('MediaPlayer_Models', function ($resource, ENV) {
    return $resource(ENV.host + '/api/v1/admin/mediaplayer_model/:pk', null, {'update': {method: 'PUT'}});
  });
