/**
 * Created by ksm on 2014-11-27.
 */

angular.module('signageApp')
    .factory('Incident', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/incident-list', null, {'update': {method: 'PUT'}});
    });