/**
 * Created by ksm on 2015-05-13.
 */
'use strict';

angular.module('signageApp')
    .factory('EXPORT_EXCEL', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/export/excel/:key', null, {'update': {method: 'PUT'}});
    });