'use strict';

angular.module('signageApp')
    .factory('User', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/users', null,{'update': {method: 'PUT'}});
    });