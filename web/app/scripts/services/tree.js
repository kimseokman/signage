'use strict';

angular.module('signageApp')
    .factory('Tree', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/trees/:shop', null, {'update': {method: 'PUT'}});
    });
