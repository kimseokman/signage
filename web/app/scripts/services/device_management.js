/**
 * Created by ksm on 2014-12-09.
 */
angular.module('signageApp')
    .factory('Device_Manage', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/device/:key', null, {'update': {method: 'PUT'}});
    });