'use strict';

angular.module('signageApp')
    .factory('MediaPlayerAlias', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/auto_complete/media-player-aliases');
    });
