'use strict';

angular.module('signageApp')
    .factory('Top10Display', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/displays/top10', null, {'update': {method: 'PUT'}});
    });