/**
 * Created by ksm on 2015-01-13.
 */

'use strict';

angular.module('signageApp')
    .factory('OTP', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/otp', null, {'update': {method: 'PUT'}});
    });