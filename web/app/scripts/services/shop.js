'use strict';

angular.module('signageApp')
    .factory('AuthShop', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/shops\\/');
    });