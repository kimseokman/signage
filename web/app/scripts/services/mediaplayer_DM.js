/**
 * Created by ksm on 2014-11-18.
 */

'use strict';

angular.module('signageApp')
    .factory('MP_DM', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/media-players/controller/:serial_number', null, {'update': {method: 'PUT'}});
    });
