/**
 * Created by ksm on 2015-04-06.
 */
angular.module('signageApp')
    .factory('StatsSetting', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/stats/setting', null, {'update': {method: 'PUT'}});
    });