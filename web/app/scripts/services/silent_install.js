/**
 * Created by ksm on 2014-12-22.
 */

'use strict';

angular.module('signageApp')
    .factory('SILENT_INSTALL', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/silent-install/:install_id', null, {'update': {method: 'PUT'}});
    });