/**
 * Created by ksm on 2014-11-17.
 */
'use strict';

angular.module('signageApp')
    .factory('History', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/history/:serial_number', null, {'update': {method: 'PUT'}});
    });