/**
 * Created by ksm on 2014-11-24.
 */

angular.module('signageApp')
    .factory('DeviceGroup', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/device-group/:key', null, {'update': {method: 'PUT'}});
    });