/**
 * Created by ksm on 2015-02-02.
 */

angular.module('signageApp')
    .factory('Incidents', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/incident-list', null, {'update': {method: 'PUT'}});
    });