/**
 * Created by ksm on 2014-11-21.
 */

'use strict';

angular.module('signageApp')
    .factory('Member', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/users/member', null,{'update': {method: 'PUT'}});
    });