/**
 * Created by ksm on 2015-04-17.
 */
angular.module('signageApp')
    .factory('StatsRatio', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/stats/ratio', null, {'update': {method: 'PUT'}});
    });
