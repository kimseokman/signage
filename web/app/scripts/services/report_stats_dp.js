/**
 * Created by ksm on 2015-04-07.
 */
angular.module('signageApp')
    .factory('StatsDP', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/stats/dp', null, {'update': {method: 'PUT'}});
    });

