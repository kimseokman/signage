/**
 * Created by ksm on 2014-12-29.
 */

'use strict';

angular.module('signageApp')
    .factory('DP_By_MP', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/display_list', null, {'update': {method: 'PUT'}});
    });