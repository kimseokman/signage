'use strict';

angular.module('signageApp')
    .factory('MediaPlayer', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/media-players/:serial_number', null, {'update': {method: 'PUT'}});
    });