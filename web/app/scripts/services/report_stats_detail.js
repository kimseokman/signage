/**
 * Created by ksm on 2015-04-08.
 */
angular.module('signageApp')
    .factory('StatsDetail', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/stats/detail', null, {'update': {method: 'PUT'}});
    });