/**
 * Created by ksm on 2014-12-04.
 */

'use strict';

angular.module('signageApp')
    .factory('AuthUserDeviceGroup', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/member-device-group/:key', null, {'update': {method: 'PUT'}});
    });
