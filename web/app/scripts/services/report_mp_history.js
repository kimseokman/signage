/**
 * Created by pjh on 2014-12-04.
 */

angular.module('signageApp')
  .factory('ReportMPHistory', function ($resource, ENV) {
    return $resource(ENV.host + '/api/v1/report/mp/history/:serial_number', null, {'update': {method: 'PUT'}});
  });
