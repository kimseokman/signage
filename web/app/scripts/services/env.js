'use strict';

angular.module('signageApp')
    .constant('ENV', {
        //host: 'https://lge.adamssuite.com'
        //host: 'http://127.0.0.1:8088'
        //host: 'http://192.168.1.201:8088'
        //host: 'https://dev.adamssuite.com',
        host: 'https://lgedev.adamssuite.com'
    });
