/**
 * Created by ksm on 2014-12-12.
 */

'use strict';

angular.module('signageApp')
    .factory('AuthTagging', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/tagging', null, {'update': {method: 'PUT'}});
    });
