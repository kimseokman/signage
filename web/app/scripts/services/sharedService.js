/**
 * Created by ksm on 2015-05-07.
 */
'use strict';

angular.module('signageApp')
    .factory('SharedService', function ($rootScope) {
        var sharedService = {};
        // communication data list -------------------------------------------------------------------------------------
        sharedService.user = null;
        sharedService.quick_dash = null;
        sharedService.media_player_serial_number = null;
        sharedService.display_serial_number = null;
        // data: user --------------------------------------------------------------------------------------------------
        sharedService.prepForUserBroadcast = function(user){
            this.user = user;
            this.broadcastRoot("USER");
        };
        sharedService.prepForQuickDashBroadcast = function(quick_dash){
            this.quick_dash = quick_dash;
            this.broadcastRoot("DASH");
        };
        sharedService.prepForMediaPlayerSerialNumberBroadcast = function(media_player_serial_number){
            this.media_player_serial_number = media_player_serial_number;
            this.broadcastRoot("MP_SERIAL_NUMBER");
        };
        sharedService.prepForDisplaySerialNumberBroadcast = function(display_serial_number){
            this.display_serial_number = display_serial_number;
            this.broadcastRoot("DP_SERIAL_NUMBER");
        };

        // communication -----------------------------------------------------------------------------------------------
        sharedService.broadcastRoot = function(type){
            $rootScope.$broadcast(type);
        };

        return sharedService;
    });