/**
 * Created by ksm on 2014-12-16.
 */

'use strict';

angular.module('signageApp')
    .factory('MediaPlayerSerialNumber', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/auto_complete/media-player-serial-number');
    });
