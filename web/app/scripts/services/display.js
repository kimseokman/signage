'use strict';

angular.module('signageApp')
    .factory('Display', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/displays/:serial_number', null, {'update': {method: 'PUT'}});
    });