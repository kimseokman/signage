'use strict';

angular.module('signageApp')
    .factory('SICompany', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/auto_complete/si-companies');
    });