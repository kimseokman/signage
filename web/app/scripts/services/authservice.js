'use strict';

angular.module('signageApp')
    .factory('AuthService', function ($http, $rootScope, $location, ENV, Session, Access, $window) {
        var url = ENV.host + '/api/v1/auth';

        // Public API here
        return {
            login: function (credentials) {
                return $http({
                    method: 'post',
                    url: url,
                    headers: {'Content-Type': 'application/json'},
                    data: credentials
                })
                .success(function (data) {
                    var auth = data.objects[0];

                    if(auth.login){
                        Session.set('accessToken', auth.token);
                        $http.defaults.headers.get = {token: auth.token};
                        $http.defaults.headers.get['If-Modified-Since'] = new Date().getTime();
                        $http.defaults.headers.get['Cache-Control'] = 'no-cache';
                        $http.defaults.headers.get['Pragma'] = 'no-cache';
                        $http.defaults.headers.put["token"] = auth.token;
                        $http.defaults.headers.post["token"] = auth.token;
                        $http.defaults.headers.common["token"] = auth.token;

                        if(auth.userOTP){
                            Session.set('passOTP', false);
                            $location.path('/login-otp');
                        }else{
                            Session.clear('passOTP');

                            var argument = {};
                            argument['type'] = true;    //login

                            var userAgent = window.navigator.userAgent;
                            var browser_ver = null;
                            var os_ver = null;

                            if(userAgent.indexOf("Firefox") != -1)                                         browser_ver = "Firefox";
                            if(userAgent.indexOf("Opera") != -1)                                           browser_ver = "Opera";
                            if(userAgent.indexOf("Chrome") != -1)                                          browser_ver = "Chrome";
                            if(userAgent.indexOf("Safari") != -1 && userAgent.indexOf("Chrome") == -1)    browser_ver = "Safari";

                            if(userAgent.indexOf("MSIE 6") != -1)                                          browser_ver = "Internet Explorer 6";
                            if(userAgent.indexOf("MSIE 7") != -1)                                          browser_ver = "Internet Explorer 7";
                            if(userAgent.indexOf("MSIE 8") != -1)                                          browser_ver = "Internet Explorer 8";
                            if(userAgent.indexOf("MSIE 9") != -1)                                          browser_ver = "Internet Explorer 9";
                            if(userAgent.indexOf("MSIE 10") != -1)                                         browser_ver = "Internet Explorer 10";
                            if(userAgent.indexOf("rv") != -1)                                               browser_ver = "Internet Explorer 11";

                            argument['browser_ver'] = browser_ver || "unknown"; // browser version

                            var appVersion = window.navigator.appVersion;
                            var appplatform = window.navigator.appName + window.navigator.platform;
                            var os_versions = {Windows: "Win", MacOS: "Mac", UNIX: "X11", Linux: "Linux"};

                            for(var key in os_versions) {
                                if (appVersion.indexOf(os_versions[key]) != -1) {
                                    os_ver = key;
                                }
                            }
                            argument['os_ver'] = os_ver || "unknown";   //os version

                            Access.save(argument);

                            var change_uri = "/";
                            if(navigator.userAgent.match("Android") || navigator.userAgent.match("iPhone") || navigator.userAgent.match("iPad")) {
                                change_uri = "/m.dash";
                            } else {
                                change_uri = "/";
                            }

                            $location.path(change_uri);
                        }
                    }else if(!auth.login){
                        alert('Wrong ID/PW');
                    }else{
                        alert('Server Error');
                    }
                });
            },
            isAuthenticated: function () {
                var accessToken = Session.get('accessToken');
                var passOTP = Session.get('passOTP');
                var isAuth = false;

                // [ Case by ] Login Success
                if(!!accessToken){
                    // [ Case by ] do not use OTP
                    if(passOTP == undefined){
                        isAuth = true;
                    // [ Case by ] use OTP
                    }else{
                        isAuth = passOTP;
                    }
                // [ Case by ] Login Fail
                }else{
                    isAuth = false;
                }

                return isAuth;
            }
        };
    });
