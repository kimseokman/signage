/**
 * Created by ksm on 2015-01-27.
 */
'use strict';

angular.module('signageApp')
    .factory('AdminList', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/users/admins', null,{'update': {method: 'PUT'}});
    });