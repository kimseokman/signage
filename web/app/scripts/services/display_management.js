/**
 * Created by ksm on 2014-12-09.
 */
angular.module('signageApp')
    .factory('Display_Manage', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/device/display/:key', null, {'update': {method: 'PUT'}});
    });