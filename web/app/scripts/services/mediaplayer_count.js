
'use strict';

angular.module('signageApp')
    .factory('MP_COUNT', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/media-players/count', null, {'update': {method: 'PUT'}});
    });
