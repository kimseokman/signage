/**
 * Created by ksm on 2015-04-14.
 */
angular.module('signageApp')
    .factory('ReportDPHistory', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/report/dp/history/:serial_number', null, {'update': {method: 'PUT'}});
    });
