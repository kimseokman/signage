/**
 * Created by ksm on 2014-11-23.
 */

'use strict';

angular.module('signageApp')
    .factory('CHECK_USER', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/users/check/:user_id', null, {'update': {method: 'PUT'}});
    });
