/**
 * Created by ksm on 2014-11-18.
 */

'use strict';

angular.module('signageApp')
    .factory('DP_DM', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/displays/controller/:serial_number', null, {'update': {method: 'PUT'}});
    });
