/**
 * Created by ksm on 2015-05-18.
 */

angular.module('signageApp')
    .factory('AlarmTest', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/alarm/:key', null, {'update': {method: 'PUT'}});
    });