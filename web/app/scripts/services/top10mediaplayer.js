'use strict';

angular.module('signageApp')
    .factory('Top10MediaPlayer', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/media-players/top10', null, {'update': {method: 'PUT'}});
    });