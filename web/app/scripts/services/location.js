'use strict';

angular.module('signageApp')
    .factory('AuthLocation', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/locations\\/');
    });