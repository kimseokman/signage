/**
 * Created by ksm on 2015-01-28.
 */
'use strict';

angular.module('signageApp')
    .factory('Access', function ($resource, ENV) {
        return $resource(ENV.host + '/api/v1/access_history', null, {'update': {method: 'PUT'}});
    });